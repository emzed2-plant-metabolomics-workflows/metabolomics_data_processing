# -*- coding: utf-8 -*-
"""
Created on Wed Dec 04 12:52:15 2019

@author: pkiefer
"""
from _fix_inspector_bug import inspect_tables, build_inspection_table
from emzed.core.data_types import Table, PeakMap
import emzed
import numpy as np
import matplotlib.pyplot as plt


def inspect(item):
    """ inspection tool for list of Tables and list of PeakMaps. Fixes emzed.gui.inspect bugs
        for list of tables with list length >54. Works analoguous to function emzed.gui.inspect
    """
    if _type_check(item):
        emzed.gui.inspect(item)
    elif _is_iterable(item):
        if all([isinstance(v, Table) for v in item]):
            inspect_tables(item)
        elif all([isinstance(v, PeakMap) for v in item]):
            t=build_inspection_table(item, 'peakmaps', PeakMap)
            emzed.gui.inspect(t)
    else:
        raise(Exception), 'Item of %s cannot be inspected!!' %type(item)


def _type_check(item):
    return isinstance(item, Table) or isinstance(item, PeakMap)


def _is_iterable(item):
    types=[set, tuple, list]
    return any([isinstance(item, type_) for type_ in types])

from collections import defaultdict



def multiplot_peakmaps(peakmaps, num_rows=6, save_path=None, plot_heatmap=False, plotsize=(2,4),
                       ref_plot=None, title=''):
    """ In default mode Function multiplot_peakmaps creates one figure depicting TICa of all 
        peakmaps. 
    *parameters 
    - peakmaps: list or tuple of PeakMap objects
    - num_rows: number of vertical arranged peakmaps
    - save_path: optional: if None, the multiplot is shown, if a path is given the multiplot
        will be stored in path. For suported file types see also pylab.savefig
    - plot_heatmap: plots a heatmap of binned mz rt values instead of a TIC profile 
      (much slower ~20x)
    - plotsize: only taken into accoutn if save_path is not None: defines the size of
        the individual plot in inches
    """
    title_font = {'fontname':'Arial', 'size':8.0, 'color':'black', 'weight':'normal',
              'verticalalignment':'center'}
    axis_font = {'fontname':'Arial', 'size':'medium'}
    nrows, ncols=_create_subplotrange(peakmaps, num_rows)
    figsize=_get_figsize(plotsize, nrows, ncols) if save_path else None
    fig, axs=plt.subplots(nrows, ncols, True, True, figsize=figsize)
    lastcent=-1
    fig.suptitle(title)    
    for j in range(ncols):
        for i in range(nrows):
            if i ==j==0:
                label='current'
                ref_label='ref'
            else:
                label=None
                ref_label=None
            ax=_get_ax(axs, i, j, nrows, ncols)
            index=i+ nrows*j
            if index<len(peakmaps):
                lastcent=show_progress(index, len(peakmaps), lastcent)
                pm=peakmaps[index]
                subplot_heatmap(pm, ax, axis_font) if plot_heatmap else plot_tic(pm, ax, label=label)
                if ref_plot and not plot_heatmap:
                    plot_tic(ref_plot, ax, color='--r', label=ref_label)
                ax.set_title(pm.meta['source'][:50], fontdict=title_font)
            if i==nrows-1:
                ax.set_xlabel('time [s]', fontdict=axis_font)
    if not save_path:
        plt.show()
        
        return 
    plt.savefig(save_path)
    plt.close()     

def _get_ax(axs, i, j, nrows, ncols):
    if nrows + ncols ==2:
        return axs
    if nrows>1:
        return axs[i][j] if ncols>1 else axs[i]
    return axs[j]
    

import sys

def show_progress(i, nos, lastcent):
    cent = ((i + 1) * 20) / nos
    if cent != lastcent:
        print cent * 5,
        try:
            sys.stdout.flush()
        except IOError:
            # migh t happen on win cmd console
            pass
        return cent
    return lastcent
                
                
def subplot_heatmap(pm, ax, fontdict):   
    mat, rts, mzs=_get_pm_heatmap(pm)
    ax.imshow(mat, cmap='hot', aspect='auto')
    ax.set_xticks(range(len(rts)))
    ax.set_yticks(range(len(mzs)))
    xlabels=['']*len(mzs)
    ylabels=['']*len(rts)
    ax.set_xticklabels(xlabels, fontdict=fontdict)
    ax.set_yticklabels(ylabels, fontdict=fontdict)
    

def plot_tic(pm, ax, color='b', label='current'):
    x,y = _get_tic(pm)    
    ax.plot(x, y, color, label=label)
    if label:
        ax.legend()


def _get_pm_heatmap(pm, rt_bin=5.0, mz_bin=5.0, max_int=1e5):
    d=get_value_dict(pm, rt_bin, mz_bin)
    min_i, max_i, min_j, max_j=_get_bounderies(d)
    mat=np.ones((max_i-min_i, max_j-min_j))
    for pos, value in d.items():
        i,j =pos
        v=value/max_int
        mat[i-min_i, j-min_j]= 1.0+ v if v<=2 else 3.0
    rts=[str('') for i in range(min_j, max_j)]
    mzs=[str('') for i in range(min_i, max_i)]
    return np.log2(mat), rts, mzs    


def _get_figsize(plotsize, nrows, ncols):
    height, width=plotsize
    height *= nrows+1
    width  *= ncols + 1
    return width, height


def _get_tic(pm):
    mzmin, mzmax = pm.mzRange()
    rtmin, rtmax = pm.rtRange()
    return pm.chromatogram(mzmin, mzmax, rtmin, rtmax)    
    

def _create_subplotrange(pms, n_rows=10):
    num=len(pms)
    n_cols=num/n_rows
    n_cols=n_cols if float(len(pms))/n_rows == n_cols else n_cols+1
    return (num, n_cols) if num<=n_rows else (n_rows, n_cols)
    

def _calc_pos(counts, nrows):
    j=counts/nrows
    i=counts-j*nrows
    return i, j

        
def get_value_dict(pm, rt_bin, mz_bin):
    d=defaultdict(float)
    for spec in pm.spectra:
        j=int(spec.rt/rt_bin)
        for mz, intensity in spec.peaks:
            i=int(mz/mz_bin)
            d[(i,j)]+=intensity
    return d
    

def _get_bounderies(d):
    i_s, j_s=zip(*d.keys())
    return min(i_s), max(i_s)+1, min(j_s), max(j_s)+1