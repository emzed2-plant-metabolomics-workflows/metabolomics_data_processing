# -*- coding: utf-8 -*-
"""
Created on Tue Oct 01 09:04:03 2019

@author: pkiefer
Top N MS / PRM
Algorithm:
required: LC-MS level 1 MS peak_
Select corresponding MS level2 spectra at appex of ms level 1 peaks
Select Top N most intense spectra of correspondind MS level 2 spectra
-> extract corresponding peaks within 2* ms level 1 peaks width 
-> determine RT at appex and select peaks coeluting with ms level1 
"""
import numpy as np
import emzed
import os

PeakMap=emzed.core.data_types.PeakMap



def top_n_prm_peaks(peaks_table, pm, n=25, precursor_tol=0.02, mztol=0.005):
    t=build_ms_level2_table(pm)
    t=assign_ms_level2_table(peaks_table, t, precursor_tol, n)  
    update_top_n_ms2_peaks_table(t, mztol)
    t=build_inspection_table(t)
    return manual_select_prm_peaks(t)
    

def build_ms_level2_table(pm):
    pairs= pm.splitLevelN(2, 4)
    mzs=[p[0] for p in pairs]
    pms=[p[1] for p in pairs]
    t=emzed.utils.toTable('mz', mzs, type_=float)
    t.addColumn('peakmap', pms, type_=PeakMap)
    t.addColumn('rtmin', t.apply(_getrtmx,(t.peakmap, )), type_=float, insertAfter='mz')
    t.addColumn('rtmax', t.apply(_getrtmx,(t.peakmap, False)), type_=float, insertAfter='rtmin')
    return t


def _getrtmx(pm, rtmin=True):
    i=0 if rtmin else 1
    return pm.rtRange()[i]


def assign_ms_level2_table(peaks_table, t, mztol, n):
    pt=peaks_table
    comb=pt.join(t, pt.mz.equals(t.mz, mztol) & pt.rt.inRange(t.rtmin, t.rtmax))
    comb.updateColumn('mzs', comb.apply(_get_mzs_at_appex, (comb.rt, comb.peakmap__0, n)),
                      type_=emzed.core.data_types.Spectrum)
    return comb


def _get_mzs_at_appex(rt, pm, n=10):
    specs=pm.spectra
    delta=[abs(spec.rt-rt) for spec in specs]
    spec=min(zip(specs, delta), key=lambda v: v[1])[0]
    return _get_top_n_mzs(spec, n)
    


def update_top_n_ms2_peaks_table(t, mztol=0.005):
    t.updateColumn('ms2_table', t.apply(_extract_peaks, (t.peakmap__0, t.mzs, t.rtmin, t.rtmax, mztol)))
    


def _get_top_n_mzs(spectrum, n=20):
    peaks=spectrum.peaks
    ordered=sorted(peaks, key=lambda v: v[1], reverse=True)
    n=n if len(spectrum)>=n else len(spectrum)
    return tuple(np.array(ordered)[:n, 0])



def _extract_peaks(pm, mzs, rtmin, rtmax, mztol):
    t=emzed.utils.toTable('mz', mzs, type_=float)
    t.updateColumn('mzmin', t.mz - mztol, type_=float)
    t.updateColumn('mzmax', t.mz + mztol, type_=float)
    t.updateColumn('rtmin', rtmin, type_=float)
    t.updateColumn('rtmax', rtmax, type_=float)
    t.updateColumn('peakmap', pm, type_=PeakMap)
    t.addEnumeration()
    t=emzed.utils.integrate(t, msLevel=2)
    t.updateColumn('rt', t.apply(_rt_at_appex,(t.params, )), type_=float, insertBefore='rtmin')
    return t


def _rt_at_appex(params):
    return max(zip(*params), key=lambda v: v[1])[0]


def build_inspection_table(t):
    colnames=['mz', 'mzmin', 'mzmax', 'rt', 'rtmin', 'rtmax', 'peakmap']
    t_ms1=t.extractColumns(*colnames)
    t_ms1.addEnumeration('pid')
    t_ms1.addColumn('ms_level', 1, type_=int, format_='%d', insertAfter='pid')
    t_ms1=emzed.utils.integrate(t_ms1, 'trapez', msLevel=1)
    extracted=[t_ms1]
    for peak, ms2 in zip(t_ms1.splitBy('pid'), t.ms2_table):
        t_ms2=ms2.extractColumns(*colnames)
        t_ms2.addColumn('pid', peak.pid.uniqueValue(), type_=int, insertBefore='mz')
        t_ms2.addColumn('ms_level', 2, type_=int, format_='%d', insertAfter='pid')
        t_ms2=emzed.utils.integrate(t_ms2, 'trapez')
        extracted.append(t_ms2)
    return emzed.utils.stackTables(extracted)
    
    

def manual_select_prm_peaks(t, id_col='pid'):
    text=""" Please check all prm peaks indiviadually.\n
    Do MS level 2 peaks coelute with corresponding MS level 1 peak?\n
    You can remove peaks by manualy reintegration peaks using `no_integration` method
    """
    emzed.gui.showInformation(text)
    tables=t.splitBy(id_col)
    emzed.gui.inspect(tables)
    t=emzed.utils.stackTables(tables)
    return t.filter(t.area.isNotNone())
    


#################################################################################################
#
def build_mgf_files(t, dir_, id_col='pid', top_n=20):
    for sub in t.splitBy(id_col):
        sub.sortBy('area', ascending=False)
        _build_mgf_file(sub, dir_, top_n, id_col='pid')


def _build_mgf_file(t, dir_, top_n, id_col='pid'):
    d=dict()
    peaks=[]
    colnames=(id_col, 'mz', 'ms_level', 'rt', 'area')
    for id_, mz, mslevel, rt, area in zip(*[t.getColumn(n) for n  in colnames]):
        if mslevel==1:
            rt_val=str(round(rt/60.0, 1))
            rt_val='_'.join([rt_val, 'min'])
            d['TITLE']=''.join(['id_col=', str(id_), '_mz=',str(round(mz, 2)), '_rt=', rt_val])
            d['PEPMASS']=round(mz, 2)
            d['RTINSECONDS']=str(int(rt))
        elif mslevel==2:
            peaks.append(' '.join([str(mz), str(area)]))
            if len(peaks)>=top_n:
                break                
    d['peaks']=peaks
    _write_mgf(d, dir_)
            

def _write_mgf(d, dir_):
    ext='.mgf'
    filename=''.join([d['TITLE'], ext])
    path=os.path.join(dir_, filename)
    
    with open(path, 'w') as df:
        df.write('BEGIN IONS\n')
        df.write(_writeline(d, 'TITLE'))
        df.write(_writeline(d, 'RTINSECONDS'))
        df.write(_writeline(d, 'PEPMASS'))
        df.write(write_peaks(d['peaks']))
        df.write('ENDIONS')
    
def _writeline(d, key):
    line='='.join([key, str(d[key])])
    return line+'\n'

def write_peaks(peaks):
    return '\n'.join(peaks)+'\n'
    
################################################################################################
# 
from basic_processing._rt_align import rtAlign
from copy import deepcopy

def rt_align_pms_by_peak_lib(pms, plib=None, ref=None, destination=None, max_rt_diff=60.0):
    original=[deepcopy(pm) for pm in pms]
    kwargs={'maxRtDifference': max_rt_diff, 'maxMzDifference': 0.004, 
            'maxMzDifferencePairfinder': 0.008, 'destination': destination}
    ref=get_ref_table(plib, ref)
    tables=fast_ff_detect(pms)
    __=rtAlign(tables, tables, refTable=ref, **kwargs)
    return get_transformed(original, destination)



    
def fast_ff_detect(pms):
    tables=[]
    for pm in pms:
       t=emzed.ff.runMetaboFeatureFinder(pm, ms_level=1, common_noise_threshold_int=8e4) 
       tables.append(t)
    return tables
    
    
def get_ref_table(plib, ref):
    if ref:
        return ref
    if not plib:
        path=emzed.gui.askForSingleFile(caption='open peak library', extensions=['table'])
        plib=emzed.io.loadTable(path)
    return build_ftable_from_plib(plib)
    


def build_ftable_from_plib(t):
    print 'creating feature table from peak library ...'
    required=['id', 'feature_id', 'mz', 'mzmin', 'mzmax', 'rt', 'rtmin', 'rtmax', 'intensity',
              'quality', 'fwhm', 'z', 'peakmap', 'source']    
    colnames=['ipid', 'fid', 'mzmean', 'mzmin', 'mzmax', 'rtmean', 'rtmin', 'rtmax', 'area',
               'norm_peak_area', 'params', 'z_fid']
    ref=t.extractColumns(*colnames)
    ref=_get_unique_rows(ref)
    pairs=(zip(colnames, required))
    for p in pairs:
        if p[0]==p[1]:
            continue
        else:
            ref.renameColumn(*p)
    ref.replaceColumn('fwhm', ref.apply(_update_fwhm, (ref.fwhm, )), type_=float, 
                    format_="'%.2fm' % (o/60.0)")
    pm=max(t.peakmap.values, key=lambda v: len)
    ref.addColumn('peakmap', pm, type_=PeakMap)
    ref.addColumn('source', ref.peakmap.uniqueValue().meta['source'], type_=str)
    print 'Done'
    return ref


def _get_unique_rows(t):
    t.updateColumn('_keep', t.area==t.area.max.group_by(t.fid), type_=bool)
    t1=t.filter(t._keep==True)
    t.dropColumns('_keep')
    t1.dropColumns('_keep')
    return t1

def _update_fwhm(params):
    max_int=max(params[1])
    pairs=zip(*params)
    select=[p for p in pairs if p[1]>=0.5*max_int]
    max_=max(select, key=lambda v: v[1])[0]
    min_=min(select, key=lambda v: v[1])[0]
    return max_-min_
    
##############################################################################################
import pyopenms

def get_transformed(pms, dir_):
    ext='.xml'
    transformed=[]
#    pms=[t.peakmap.uniqueValue() for t in tables]
    for pm in pms:
        filename=pm.meta['source']
        name, __=os.path.splitext(filename)
        name=name+ext
        path=os.path.join(dir_, name)
        trafo=load_transformation(path)    
        pm_trans=transform_peakmap_rt(pm, trafo)
        transformed.append(pm_trans)
    return transformed
        



def load_transformation(path):
    t_file=pyopenms.TransformationXMLFile()
    trafo=pyopenms.TransformationDescription()
    t_file.load(path, trafo, True) # True is a flag that got lost while code wrapping
    return trafo
    

def transform_peakmap_rt(peakmap, transformation):
    """
    """
    for spec in peakmap.spectra:
        spec.rt=transformation.apply(spec.rt)
    peakmap.meta['aligned']=True
    return peakmap


#######################################################
# def testing
def _get_test_table(pm, mztol=0.003):
    t=emzed.utils.toTable('mz', [455.0796, 465.0937, 277.1665], type_=float)
    t.updateColumn('mzmin', t.mz - mztol, type_=float)
    t.updateColumn('mzmax', t.mz + mztol, type_=float)
    t.updateColumn('rtmin', [3.75*60, 1.57*60, 1.62*60], type_=float)
    t.updateColumn('rtmax', [4.45*60, 1.98*60, 2.29*60], type_=float)
    t.updateColumn('peakmap', pm, type_=PeakMap)
    t=emzed.utils.integrate(t, msLevel=1)
    t.updateColumn('rt', t.apply(_rt_at_appex, (t.params, )), type_=float)
    return t
    




    