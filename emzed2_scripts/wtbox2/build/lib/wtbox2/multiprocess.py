# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 08:53:53 2015

@author: pkiefer
Multiprocessing
Multiprocssing requires import of the multiprocessing function and cannot be executed via runfile

the concept of using the function run_parallel:
    assuming you have a sequential process like e.g
    def process(values, args, kwargs):
        result=[]
        for v in in values:
            result.append(fun(v, *args, **kwargs))
    return result

To perform multiprocessing:
    
(1) nest the function fun
    
(2) create parameter tuples for nested fun:

(3) exectured the nested function as multicore process
        
"""
import multiprocessing
import threading
import Queue
import sys
from multiprocessing import Pool
import time

if sys.platform == "win32":
        # if subprocesses use python.exe a console window pops up for each
        # subprocess. this is quite ugly..
        import os.path
        multiprocessing.set_executable(os.path.join(os.path.dirname(sys.executable),
                                       "pythonw.exe"))

def build_input(values, fun, args, kwargs):
    return [(fun, v,  args, kwargs) for v in values]


def nested(params):
    fun, v, args, kwargs = params
    return fun(v, *args, **kwargs)
    

def run_parallel(args, cpus=None):
    # all parameter arguments must have the same length
    cpus=num_cpus(args, cpus)
    start=time.time()
    if cpus>1:
        # only switch to multiprocessing if more than 1 cpu is used
        pool=Pool(processes=cpus)
        result=pool.map(nested, args)
        pool.close()
        pool.join()
    else:
        result=[nested(arg) for arg in args]
    print 'took %.3fs' %(time.time()-start)
    return result #[r for r in result]

def num_cpus(args, cpus):
    if not cpus:
        cpus=len(args)
    max_cpus=multiprocessing.cpu_count()-1
    cpus=cpus if cpus<max_cpus else max_cpus
    print 'number of cores:', cpus    
    return cpus
 
   
def main_parallel(fun, values, args=list(), kwargs=dict(), cpus=None):
    """ fun, values: list or tuple
    """
    params = build_input(values, fun, args, kwargs)
    return run_parallel(params, cpus=cpus)    
    
#################################################
# Multithreading
class MultiThread:
    def __init__(self, iterable, fun, num_threads=10, fun_args=None, fun_kwargs=None, in_place=False):
        self._in_place=in_place
        self._fun=fun
        self._iterable=iterable
        self._funargs=[] if fun_args == None else fun_args
        self._funkwargs={} if fun_kwargs == None else fun_kwargs
        self._num_threads=num_threads
        

    def run_multithread(self):
        self._q=build_queue(self._iterable)
        if not self._in_place:
            self._result=[]
        for i in range(self._num_threads):
            t=threading.Thread(target=self.worker, args=(i,))
            t.setDaemon(True)
            t.start()
        self._q.join()
        print 'Done'


    def worker(self, i):
        while self._q.qsize()>=1:
            item=self._q.get(False)
            x=time.time()
            if self._in_place:
                self._fun(item, *self._funargs, **self._funkwargs)
            else:
                self._result.append(self._fun(item, *self._funargs, **self._funkwargs))
            y=time.time()
            print 'worker %d, time %.2fs' %(i, y-x)
            self._q.task_done()
            
    def get_result(self):
        return self._result

def build_queue(iterable):
    q=Queue.Queue()
    for item in iterable:
        q.put(item)
    return q
        
def test_fun(a):
    time.sleep(0.01)
    return a