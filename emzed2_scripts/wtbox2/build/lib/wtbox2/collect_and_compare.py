# -*- coding: utf-8 -*-
"""
Created on Tue Feb 06 14:01:52 2018

@author: pkiefer
"""
from collections import defaultdict, OrderedDict
from random import uniform
from operator import add, div, sub
import numpy as np
import itertools
import emzed
import os
import table_operations as top


def table2lookup(t, key2tol, d=None):
    """
    """
    key_cols=sorted(key2tol.keys())
    tolerances=[key2tol[key] for key in key_cols]
    if not d:
        d=defaultdict(dict)
    key_cols=sorted(key2tol.keys())
    ntuples=zip(*[t.getColumn(col).values for col in key_cols])
    ntuples=unique_items(ntuples, 1e-6)
    # rt values are identical within the same feature thus the key tuple might be no longer unique.
    for ntuple, row in zip(ntuples, t.rows):
        for key in calculate_keys(ntuple, tolerances):
            d[key].update({ntuple: row})
    # tolerances are added to d to allow key calculation from loopkup object
    d['bin_tols']=tolerances
    d['empty_clone']=t.buildEmptyClone()
    return d


def unique_items(items, offset, absolute=True):
    d={}
    unique_items=[]
    for item in items:
        _modify_item(item, offset, d, unique_items, absolute=True)
    return unique_items
    

def _modify_item(item, offset, d, uniques, absolute=True):
    # we modify the tuple values below significance
    while d.has_key(item):
        if isinstance(item, tuple):
            if absolute:
                item=tuple([v+uniform(-offset, offset) for v in item])
            else:
                item=tuple([v+uniform(-offset, offset)*v for v in item])
        elif isinstance(item, float):
            if absolute:
                item+=uniform(-offset, offset)
            else:
                item+=uniform(-offset, offset)*item
    d[item]=item
    uniques.append(item)

                
def calculate_keys(ntuple, tolerances):
    key=calculate_key(ntuple, tolerances)
    return get_neighbour_keys(key)

def calculate_key(ntuple, tolerances):
    ntuple=round_tuple_values(ntuple)
    key=tuple(itertools.imap(div, ntuple, tolerances))
    return tuple([int(v) for v in key])           


def get_neighbour_keys(key):
    modif=itertools.product((-1, 0, 1), repeat=len(key))
    return [add_tuples(key, tuple_) for tuple_ in modif]  


def round_tuple_values(ntuple):
    """ avoids rounding problems
    """
    if all([v!=None for v in ntuple]):
        return tuple([float(np.round(v, 6)) for v in ntuple])
    return []


def add_tuples(tup1, tup2):
    return tuple(itertools.imap(add, tup1, tup2))


def subtract_tuples(tup1, tup2):
    return tuple(itertools.imap(sub, tup1, tup2))    


def fullfilled(tup1, tup2, tols):
    delta=subtract_tuples(tup1, tup2)
    return all([abs(delta[i])<=tols[i] for i  in range(len(delta))])

################################################################################################

def _compare_tables(t1, t2,  key2abstol=None, key2reltol=None, leftJoin=True):
    """ Function based on table2lookup
    """
    t=t1.copy()
    t.updateColumn('_row_id', range(len(t)), type_=int)
    key2tol=_get_key2tol(t, key2abstol, key2reltol)
    key_cols=sorted(key2tol.keys())
    lookup=table2lookup(t2, key2tol)
    add_tols(t, key2abstol, key2reltol)
    expr=zip(*[t.getColumn(col) for col in key_cols])
    t.updateColumn('key', expr, type_=tuple)
    t.updateColumn('_matches', t.apply(_get_rows, (t.key, t.row_id, t.tolerances, key_cols, lookup)), 
                 type_=tuple, format_='%r')
    return build_table(t, t2, leftJoin)


def _get_key2tol(t, key2abstol, key2reltol):
    key2tol={}
    if len(key2abstol):
        key2tol.update(key2abstol)
    if len(key2reltol):
        for name in key2reltol.keys():
            abs_tol=t.getColumn(name).max() * key2reltol[name]
            key2tol[name]=abs_tol
    assert t.hasColumns(*key2tol.keys())
    return key2tol

    
def add_tols(t, key2abstol, key2reltol):
    """ adds a column with individual tolerances for each key value. In case of relative
        tolerances an individual absolute tolerance is calculated
    """
    keys=key2abstol.keys()
    keys.extend(key2reltol.keys())
    keys.sort()
    columns=[t.getColumn(n) for n in keys]
    key_values=zip(*columns)
    id2tol={}
    for id_, key_values in zip(t.row_id, key_values):
        id2tol[id_]=_calculate_tols(keys, key_values, key2abstol, key2reltol)
    def _update(id_, d):
        return d[id_]
    t.updateColumn('tolerances', t.apply(_update, (t.row_id, id2tol)), type_=tuple)


def _calculate_tols(keys, key_values, key2abstol, key2reltol):
    tols=[]
    for key, key_value in zip(keys, key_values):
        if key2abstol.has_key(key):
           tols.append(key2abstol[key])
        elif key2reltol.has_key(key):
            tols.append(key2reltol[ key]*key_value)
        else:
            assert False, 'key %d is missing' %key
    return tuple(tols)
        

def _get_rows(key_tuple, row_id, tols, key_cols, lookup):
#    if row_id==10:
#        import pdb; pdb.set_trace()
    bin_tols=lookup['bin_tols']
    # to avoid multiple entries we use dictionary with key float values values as key
    key2rows={}
    for key in calculate_keys(key_tuple, bin_tols):
        keytuple2row=lookup[key]
        for value_tuple in keytuple2row.keys():
            if fullfilled(key_tuple, value_tuple, tols):
                if not key2rows.has_key(value_tuple):
                    key2rows[value_tuple]=keytuple2row[value_tuple]
    return key2rows.values()
                

        
def compare_tables(t1, t2, key2tol, leftJoin=True):
    """
    Function based on Table.buildLookup method
    """
    t=t1.copy()
    t.addColumn('row_id', range(len(t)), type_=int)
    colname2lookup=key2lookup(t2, key2tol)
    t.updateColumn('matched_rows', None, type_=set)
    for colname in sorted(colname2lookup.keys()):
        t.updateColumn('matched_rows', t.apply(_get_matched_rows,
                (t.matched_rows, t.getColumn(colname), colname2lookup[colname]), keep_nones=True), 
                type_=tuple)
    t.updateColumn('_matches', t.apply(_get_matches, (t.matched_rows, t2)), type_=tuple)
    t.dropColumns('matched_rows')
    return build_table(t, t2, leftJoin)


def key2lookup(t, key2tol):
    d={}
    for key in sorted(key2tol.keys()):
        d[key]=t.buildLookup(key, key2tol[key], None)
    return d


def _get_matched_rows(matches, value, lookup):
    if isinstance(matches, set):
        return matches.intersection(set(lookup.find(value)))
    return set(lookup.find(value))
    

def _get_matches(matched_rows, t):
    return [t.rows[line]  for line in matched_rows]


def build_table(t1, t2, leftJoin=True):
    t2_id2rows=defaultdict(list)
    empty_row=[[None]*len(t2.getColNames())]
    id2length={}
    for row_id, match in zip(t1.row_id, t1._matches):
        if len(match):
            t2_id2rows[row_id].extend(match)
            id2length[row_id]=len(match)
        elif leftJoin and not len(match):
            t2_id2rows[row_id].extend(empty_row)
            id2length[row_id]=1
    t1.dropColumns('_matches')
    t1_id2row=dict(zip(t1.row_id, t1.rows))
    return _build_table(t1, t2, t1_id2row, t2_id2rows, id2length, leftJoin)
    
    
def _build_table(t1, t2, t1_id2row, t2_id2rows, id2length, leftJoin):
    empty=_build_empty_joined_clone(t1, t2)
    t1_block=[]
    t2_block=[]
    for id_ in t1_id2row.keys():
        if t2_id2rows.has_key(id_):
            sub=[t1_id2row[id_]]*id2length[id_]
            t1_block.extend(sub)
            t2_block.extend(t2_id2rows[id_])
    rows=[]
    for a, b in zip(t1_block, t2_block):
        line=[]
        line.extend(a)
        line.extend(b)
        rows.append(line)
    empty.rows=rows
    return empty


def _build_empty_joined_clone(t1, t2):
    # to avoid output of empty lines due to join we build empty table differently
    empty=t1.buildEmptyClone()
    empty2=t2.buildEmptyClone()
    expr=zip(empty2.getColNames(), empty2.getColTypes(), empty2.getColFormats())
    for name, type_, format_ in expr:
        name='__'.join([name, '0'])
        empty._addColumnWithoutNameCheck(name, None, type_=type_, format_=format_)
    return empty#.buildEmptyClone()
    



    
########################################################################################
"""
Comments on build consensus table:
    - Grouping of peaks from different tables starting with highest weighted peak.  
    2 dictionaries: 
        1. source2lookup
        2. weight2lookup_keys as orderedDict
        
        
"""
def build_concensus_table(tables, key2tol,  min_hits=1, weight_col='area', source_key='source',
                                       id_cols=None, replace_pm_by_meta_object = False):
    """
    all tables must have common column names, types and formats unless they are empty. 
    To increase data handling efficiency, Peakmaps
    will be replaced by their pathes if replace_pm_by_link = True. if pathes do not exist an 
    error will raise
    """
    rows=[]
    none_row_params=_get_none_row_params(tables, source_key)
    _get_unique_weight_col_values(tables, weight_col) # required for dictionary with weight_col value as key
    index=_get_weight_col_index(tables, weight_col)     
    lookup=_build_lookup(tables, key2tol, source_key)
    d=get_intensity2lookupkeys(tables, weight_col, source_key, key2tol)
    break_=0
    upper=len(d)*5
    while len(d):
        key=d.keys()[0]
        ref_keys=d.pop(key)
        source2row=_readout_lookup(lookup, d, ref_keys, index)
        if len(source2row)>=min_hits:
            grouped=_build_row(source2row, none_row_params, replace_pm_by_meta_object)
            rows.append(grouped)
        if break_>upper:
           break
        break_+=1
    return _build_final_table(tables, rows)

        
def _get_none_row_params(tables, source_col):
    sources=[t.getColumn(source_col).uniqueValue() for t in tables]
    def _get_peakmap(t):
        return t.peakmap.uniqueValue() if t.hasColumn('peakmap') else None
    peakmaps=[_get_peakmap(t) for t in tables ]
    t=tables[0]
    colnames=t.getColNames()
    index1=colnames.index(source_col) if source_col in colnames else None
    index2=colnames.index('peakmap') if 'peakmap' in colnames else None
    return len(colnames), index1, index2, sources, peakmaps
     

def _get_unique_weight_col_values(tables, weight_col):
    # tables must have same column order, type and format ...
    t=emzed.utils.stackTables(tables)
    weight_values=t.getColumn(weight_col).values
    t.replaceColumn(weight_col, unique_items(weight_values, 5.0, absolute=True), type_=float)
    return t.splitBy(weight_col)
    
        
def _get_weight_col_index(tables, weight_col):
    # since tables are stackable
    colnames=tables[0].getColNames()
    return colnames.index(weight_col)
    

def _build_lookup(tables, key2tol, source_key):
    # table specific lookups
    lookup={}
    for t in tables:
        source=t.getColumn(source_key).uniqueValue()
        _lookup=table2lookup(t, key2tol)
        lookup[source]=_lookup
    return lookup


def _pop_row_from_lookup(lookup, keys):
    source, bin_tuple, key_tuple=keys
    if lookup[source][bin_tuple].has_key(key_tuple):
        # suppress console output
        return lookup[source][bin_tuple].pop(key_tuple)
    
    
def _remove_value_from_intensity2lookupkeys(d, intensity):
    # suppress console output
    if d.has_key(intensity):
        __=d.pop(intensity)


def get_intensity2lookupkeys(tables, weight_col, source_col, key2tol):
    d={}
    key_cols=sorted(key2tol.keys())
    tols=[key2tol[key] for key in key_cols]
    for t in tables:
        expr=t.getColumn
        key_values=zip(*[expr(col) for col in key_cols])
        for weight, source, key_tuple in zip(expr(weight_col), expr(source_col), key_values):
            key=calculate_key(key_tuple, tols)
            d[weight]=(source, key, key_tuple)
    return OrderedDict(sorted(d.items(), key=lambda v: v[0], reverse=True))
            
            
def _readout_lookup(lookup, d, ref_keys, index):
    source2row={}
    source, bin_key, key_tuple=ref_keys
    row=_pop_row_from_lookup(lookup, ref_keys)
    if row is not None:
        source2row[source]=row
    bin_keys=get_neighbour_keys(bin_key)
    for _source in set(lookup.keys())-set([source]):
        _key_tuple, weight=_get_candidates(lookup, _source, bin_keys, index, key_tuple)
        _remove_value_from_intensity2lookupkeys(d, weight)
        _update_lookup(lookup, _source, _key_tuple, source2row)
    return source2row


def _get_candidates(lookup, source, bin_keys, index, key_tuple):
    candidates={}
    sub=lookup[source]
    tols=sub['bin_tols']
    for _key in bin_keys:
        for _key_tuple in sub[_key].keys():
            if fullfilled(_key_tuple, key_tuple, tols):
                
                candidates[_key_tuple]=sub[_key][_key_tuple][index]
    if len(candidates):
        selected=min_euklid(candidates.keys(), key_tuple)
        return selected, candidates[selected]
    return None, None


        
def min_euklid(pairs, ref):
    def euklid(v, ref=ref):
        tols, __=v
        return np.linalg.norm(np.array(tols)-np.array(ref))
    return min(pairs, key=euklid)


def _update_lookup(lookup, source , key_tuple, source2row):
    if key_tuple !=None:
        tols=lookup[source]['bin_tols']
        bin_key=calculate_key(key_tuple, tols)
        row=_pop_row_from_lookup(lookup, (source, bin_key, key_tuple))
        if row is not None:
            source2row[source]=row
        # remove entries in lookup
        for bin_key in calculate_keys(key_tuple, tols):
            __=_pop_row_from_lookup(lookup, (source, bin_key, key_tuple))

    
def _build_row(source2row, none_row_params, replace_pm_by_link):
    row_length, i, j, sources, pms=none_row_params
    rows=[]
    for source, pm in zip(sources, pms):
        if source2row.has_key(source):
            rows.append(source2row[source])
        else:
            none_row=[None]*row_length
            if i:
                none_row[i]=source
            if j:
                if replace_pm_by_link:
                    assert os.path.exists(pm.meta['full_source'])
                    none_row[j]=build_meta_peakmap(pm)
                else:
                    none_row[j]=pm 
            rows.append(none_row)
    return zip(*rows)
        

    
    


def _build_final_table(tables, rows):
    # since tables are stackable all tables contain columns in the same order with same, names, 
    # types, and formats00
    # Table explorer missinterprets column with name 'peaksmap' since it contains a tuple of peakmaps. 
    # We therefore set rename the column peakmap to _peakmap
    t=_get_default_empty_table(tables)
    t.rows=rows
    
    return t
    
def _get_default_empty_table(tables):
    t=tables[0].buildEmptyClone()
    names=[''.join(['peakmap', pstfx]) for pstfx in t.supportedPostfixes(['peakmap'])]
    for name in t.getColNames():
        if name in names:
            _name=''.join(['_', name])
            t.renameColumn(name, _name)
            t.setColType(_name, tuple)
            t.setColFormat(_name, None)
        elif name=='params':
            t.setColType(name, tuple)
            t.setColFormat(name, None)
        else:
            t.setColType(name, tuple)
            t.setColFormat(name, '%r')
    return t
    


class MetaPeakMap:
    pass

def build_meta_peakmap(pm):
    meta_pm=MetaPeakMap()
    meta_pm.full_source=pm.meta['full_source']
    meta_pm.source=pm.meta['source']
    meta_pm.rt_range=pm.rtRange()
    meta_pm.mz_range=pm.mzRange()
    meta_pm.polarity=pm.polarity
    meta_pm.msLevels=pm.getMsLevels()
    meta_pm.unique_id=pm.uniqueId()
    return meta_pm
        
