# -*- coding: utf-8 -*-
"""
Created on Tue May 12 12:10:37 2015

@author: pkiefer
"""
import os, glob, sys
import emzed
import in_out as _io
import multiprocess
import emzed_type_checks as _checks
import math
import numpy as np
from time import time
from scipy.optimize import curve_fit
from fitting import savitzky_golay, emg_exact as emg_exact_
from inspect import getargspec

#################################################################################################
def integrate_tables(tables, integratorid='max'):
    integrate=emzed.utils.integrate
    # since multiprocessing is applied n_cpus of integrator must be set to 1
    params={'n_cpus': 1, 'integratorid':'max'}
    return multiprocess.main_parallel(integrate, tables, kwargs=params)

#################################################################################################

def emg_smoothed(t, window_size=5, n_cpus=1, max_diff_percent=None, mslevel=1):
    """ improved emg_exact integration since emg_fitting is enhanced via providing a 
    savitzky_golay filtered intensity signal.
    
    """
    # each row requires a unique identifyer:
    t.updateColumn('id_', range(len(t)), type_=float)
    id_col='id_'
    #1. perform a std integration to obtain smoothed intensities
    t=emzed.utils.integrate(t, 'trapez',  n_cpus=n_cpus, msLevel=mslevel)
    id2area={id_: area for id_,area in zip(t.getColumn(id_col), t.area)}
    _update_smoothed_label(t)
    #2. perform emg_exact fit of smoothed signals
    colnames=['area', 'rmse', 'params', 'method', 'smoothed_eic']
    coltypes=[t.getColType(n) for n in colnames]
    colformats=[t.getColFormat(n) for n in colnames]
    for pstfx in t.supportedPostfixes(['area', 'rmse', 'params']):
        params_col=''.join(['params', pstfx])
        expr=t.getColumn
        t.updateColumn('temp_', t.apply(integrate_smoothed, (expr(params_col), window_size), 
                                        keep_nones=True), type_=tuple)
        for i in range(len(colnames)):
            name=''.join([colnames[i], pstfx])
            t.updateColumn(name, t.apply(_update_integration, (expr(id_col), expr(name), 
                                t.temp_, i, id2area, max_diff_percent), keep_nones=True), 
                                type_=coltypes[i], format_=colformats[i])
    t.dropColumns('id_')
    return t


def _update_integration(id_, value, values, i, d, max_diff_percent):
    area=values[0]
    if abs(d[id_]-area)/d[id_]*100.0<=max_diff_percent or max_diff_percent==None:
        return values[i]
    return value


def _update_smoothed_label(t):
    for pstfx in t.supportedPostfixes(['area', 'rmse', 'params']):
        smoothed=''.join(['smoothed_eic', pstfx])
        method=''.join(['method', pstfx])
        t.updateColumn(smoothed, False, type_=bool, insertAfter=method)
        

def integrate_smoothed(params, winsize):
    if params:
        rts, ints=params
        try:
            ints_sg=savitzky_golay(np.array(ints), winsize, 2)
            smoothed=True
        except:
            ints_sg=ints
            smoothed=False
        bounds=_determine_peak_param_bounderies(rts, ints_sg)
        try:
            param, pcov = curve_fit(emg_exact_, rts, ints_sg, bounds=bounds)
            y_fit=emg_exact_(rts, *param)
            method='emg_exact'
        except:
            y_fit=ints_sg
            param=np.array([rts,ints_sg])
            method='std' if smoothed else 'trapez'
        rmse = 1 / math.sqrt(len(rts)) * np.linalg.norm(y_fit - ints)
        area=np.trapz(y_fit, rts)
        return area, rmse, param, method, smoothed
    return 0.0, 0.0, None, 'max', False 
    



def _determine_peak_param_bounderies(rts, ints):
    pairs=zip(rts,ints)
    # bounfery for rt
    rt, appex=max(pairs, key=lambda v: v[1])
    appex_rts=[p[0] for p in pairs if p[1]>=0.5*appex]
    rt_bounds=(min(appex_rts), max(appex_rts))
    # to estimate the fwhm bounderies we determine the full width at 0.4 *appex and 0.6 * appex
    max_sigma=3*(max(rts)-min(rts))
    min_sigma=0.3
    s_bounds=(min_sigma, max_sigma)
    area=np.trapz(ints, rts)
    h_bounds=(max(ints), 1.5 *area)
    w_bounds=(1e-6, max(rts)-min(rts))
    
    return ([h_bounds[0], rt_bounds[0], w_bounds[0], s_bounds[0]], [h_bounds[1], rt_bounds[1], 
                 w_bounds[1], s_bounds[1]])

##################################################################################################
def score_peaks(t, max_dev):
    exp=t.getColumn
    pstfx=_get_postfix(t)
    t.addColumn('min_area', exp('area'+pstfx).min.group_by(exp('_id'+pstfx)), type_=float)
    t.addColumn('max_area', exp('area'+pstfx).max.group_by(exp('_id'+pstfx)), type_=float)
    t.addColumn('area_score', 
                t.apply(area_score,(exp('area'+pstfx), t.min_area, t.max_area, max_dev)), 
                type_=float)
    t._updateColumnWithoutNameCheck('asym'+pstfx, exp('params'+pstfx).apply(calc_skewness), 
                                    type_=float)
    t.addColumn('min_asym', exp('asym'+pstfx).min.group_by(exp('_id'+pstfx)), type_=float)
    t.addColumn('max_asym', exp('asym'+pstfx).max.group_by(exp('_id'+pstfx)), type_=float)
    t.addColumn('asym_score', t.apply(asymmetry_score, (exp('asym'+pstfx), t.min_asym, t.max_asym)), 
                type_=float)
    t._updateColumnWithoutNameCheck('score'+pstfx, t.asym_score + t.area_score, type_=float)
        
        
def calc_skewness(params):
    h,z,w,s=params
    l=1/s
    return abs(1-2.0/(w**3 * l**3)*(1+1/(w**2 * l**2))**(-3.0/2))

def area_score(a, min_, max_, max_dev):
    if a<=(1+max_dev)*min_:
        return 0.6
    elif (1+max_dev)*min_<a<(1-max_dev)*max_:
        return 0.3
    else:
        return 0.0

def asymmetry_score(f, min_, max_):
    if abs(f-min_)<1e-6:
        return 0.4
    elif (max_-f)>1e-2:
        return 0.2
    return 0.0
        
################################################################################################
    

def get_n_cpus(t, max_cpus=4):
   from multiprocessing import cpu_count
   n_cpus=cpu_count()-1 if cpu_count()>1 else cpu_count()
   if n_cpus>max_cpus:
       n_cpus=max_cpus
   estimated=int(np.ceil(np.log((len(t)+1)/500.0))) # log(0)  is not defined
   if estimated<=n_cpus:
       return estimated if estimated else 1
   return n_cpus 


def _get_postfix(t):
    required=['rtmin', 'rtmax', 'mzmin', 'mzmax', 'peakmap']
    postfix=t.supportedPostfixes(required)
    assert len(postfix)==1, 'Function accepts only one integreatable'\
                            ' peak per row not %d.' %len(postfix)
    return postfix.pop()
    
def merge_tables(tables):
    try:
        return emzed.utils.stackTables(tables)
    except:
        return emzed.utils.mergeTables(tables, force_merge=True)

def _sort(t, pstfx):
    t.sortBy('_id'+pstfx)
    t.dropColumns('_id'+pstfx)

    
##################################################################################################
def remove_files_from_dir(dir_, type_='*.*', remove_dir=True):
    path=os.path.join(dir_,type_)    
    pathes=glob.glob(path)
    if len(pathes):
        for p in pathes:
            os.remove(p)
    if remove_dir:
        os.rmdir(dir_)
        
########################################################
# caching of intermediate workfkow results

def cache_result(fun, params, path, foldername= 'cache'):
    """cache_function_result(fun, path, params) creates  subfolder `cache` in path and 
       saves funtion output in folder. Function arguments params must be list or tuple  with 
       parameter values or a dictionary with function argument keywords  as keys. 
       Inplace operations are not cached
    """
    cache_dir=_get_cache_dir(path, foldername)
    target=_get_cache_path(fun, params, cache_dir)
    if os.path.exists(target):
        return _io.load_pickled_item(target)
    else:
        if isinstance(params, list) or isinstance(params, tuple):
            result=fun(*params)
        elif isinstance(params, dict):
            result=fun(**params)
        else:
            assert False, 'Function arguments params must be list or'\
            ' tuple, or dictionary with argument names as keyy and not %s' %type(params)
        if result is not None: # not an inplace operation
           _io.save_item_as_pickle(result, target)
           return result
           
def _get_cache_dir(path, foldername='cache'):
    cache_dir=os.path.join(path, foldername)
    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)
    return cache_dir
               
      
def _get_cache_path(fun, params, cache_dir):
    name='_'.join([fun.func_name, _hash_params(params)])
    name= '.'.join( [name, 'pickled'])
    return os.path.join(cache_dir, name)


def _hash_params(params):
    return str(hash(str(params)))


def remove_cache(path, foldername='cache'):
    """ remove_cache(path) removes subfolder foldername (`cache` by default) and its content in 
    directory specified in argument `path`.
    """
    cache_dir=_get_cache_dir(path, foldername)
    from glob  import glob   
    targets=glob(os.path.join(cache_dir, '*.*'))
    [os.remove(target) for target in targets]
    os.rmdir(cache_dir)
    
#################################################################################################
# determine fwhm
def determine_typical_fwhm(peakmap, mslevel):
    params=_checks.default_ff_metabo_config()
    params['common_noise_threshold_int']
    # BAUSTELLE
    
##############################################################################################

def compare_peaks(t1, t2, mztol=0.003, rttol=5.0, keep_t1=False):
    """ returns a joined table containing peaks present in t1 and t2 based on rt and mz values. 
    If keep_t1=True, all peaks of t1 are kept and only those present in t2 and t1 are kept for t2
    """
    required=['mz', 'rt']
    assert t1.hasColumns(*required), 'columns are missing'
    assert t2.hasColumns(*required), 'columns are missing'
    tables=[]
    t1.sortBy('mz')
    t2.sortBy('mz')
    subtables=_split_table(t1, 1000)
    for sub_1  in subtables:
        try:
            sub_2=t2.filter(t2.mz.inRange(sub_1.mz.min()-mztol, sub_1.mz.max()+mztol))
        except:
            sub_2=t2
        expr=sub_1.mz.equals(sub_2.mz, abs_tol=mztol) & sub_1.rt.equals(sub_2.rt, abs_tol=rttol)
        if keep_t1:
            res=sub_1.leftJoin(sub_2, expr)
        else:
            res=sub_1.join(sub_2, expr)
        if len(res):
            tables.append(res)
    return emzed.utils.stackTables(tables) if len(tables) else [] 


def _split_table(t, length):
    if length>len(t):
        return [t]
    blocks=len(t)/length
    subtables=[]
    for i in range(1, blocks+1):
        start=(i-1)*length
        stop=i*length
        subtables.append(t[start:stop])
    if blocks*length<len(t):
        subtables.append(t[blocks*length:])
    return subtables
    
##################################################################################
    
def process_time(fun, args=None, kwargs=None, in_place=False):
    """ returns process time of function fun with parameters params. if in_place_== True no value 
        will be returned
    """
    args=[] if not args else args
    kwargs= {} if not kwargs else kwargs
    start=time()
    if in_place:
        
        fun(*args, **kwargs)
    else:
        res= fun(*args, **kwargs)
    stop=time()
    print 'runtime of function %s: %ds'%(fun.func_name, stop-start)
    if not in_place:
        return res
    
        
def show_progress_bar(iterable, fun, args=None, kwargs=None, in_place=False, bar_len=20):
    """ shows progress bar while processing iterable items with function fun. if in_place_== True no value 
        will be returned
    """
    
    args=[] if not args else args
    kwargs={} if not kwargs else kwargs
    results=[]
    count=1
    total=len(iterable)
    step=int(round(float(total) / bar_len))
    if step == 0:
        step=int(round(float(bar_len)/(total+1)))
    for item in iterable:
        if in_place:
            fun(item, *args, **kwargs)
        else:
            results.append(fun(item, *args, **kwargs))
        if total > bar_len:
            if (float(count)/step)==int(count/step):
                _progress(count, total, bar_len)
        else:
            _progress(count, total, bar_len)
        count+=1
    print 
    return None if in_place else results

def _progress(count, total, bar_len=20):
    fraction=int(round(bar_len*count/float(total)))
    bar = 100/bar_len*fraction #+ ' '*(bar_len - fraction)
    print bar, 
    sys.stdout.flush()
    
        
#############################################################################################
# 
def fast_isIn_filter(t, value_col, values, not_in=False):
    t=t.copy()
    d={v:v for v in values}
    def is_in(key, d, not_in=not_in):
        if not_in:
            return not d.has_key(key)
        return d.has_key(key)
    t.updateColumn('is_in_temp', t.apply(is_in,(t.getColumn(value_col), d)), type_=bool)
    t=t.filter(t.is_in_temp==True)
    t.dropColumns('is_in_temp')
    return t
        
######################################################################################

####################################################################################
# parameter handling
def run_function(fun, items, config, in_place=False):
    """ run_function(fun, items, config, in_place=False) allows executing a function fun whereby
    function key word arguments are extracted from dictionary config. 
    key names of config must be the same than argument names. Items is an iterable of  n values 
    and values are assigned  to the n first function arguments.  Function arguments assigned by
    items present in config dictionary will be ignored. To handle all function parameters via
    config only define items as empty list or tuple.
    In place functions can be applied setting `in_place`=True. run_function
    simplifies argument handling within a workflow since functions can now be called without
    manual selection of required parameters from a huge configuration dictionary. 
    Important: function arguments with the same name always  get the same parameter value!
    Example: with given dictionary
    config={'b':'hallo', 'c':{'a':4.5}, 'axis': None}
    items=([1,2,3],)
    Now, to calculate the mean of a applying numpy.mean, np.mean(a, axis=None)
    In [0]: run_function(np.mean, items, config)
    Out[1]: 2.0
    
    """
    kwargs=get_kwargs_from_config(fun, items, config)
    res=fun(*items, **kwargs)
    if not in_place:
        return res
    

def get_kwargs_from_config(fun, items, config):
    args=getargspec(fun).args
    # to remove the n first arguments defined by items we remove those arguments from 
    # selection:
    i=len(items)
    return _get_kwargs(args[i:], config)

def _get_kwargs(args, key2value):
    return {arg: key2value[arg] for arg in args if key2value.has_key(arg)}
    
