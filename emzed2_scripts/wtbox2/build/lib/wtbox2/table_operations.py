# -*- coding: utf-8 -*-
"""
Created on Tue May 12 12:11:19 2015

@author: pkiefer
"""

import emzed
from emzed.core.data_types import PeakMap, Table
import difflib
import emzed_type_checks as checks
from collections import defaultdict
import numpy as np
from pandas import read_clipboard
from collections import Counter


############################################################################


def update_column_by_dict(t, col, key_col, dict_, type_=None, in_place=False, insertBefore=None,
                          insertAfter=None):
    """ Function update_column_by_dict(t, col, ref_col, dict_, type_=None) updates column `col` 
        by ditionary where keys are in column key_col. By default it operates with a copy of
        the tabe object, for more memory efficient use set in_place = True
    """
    if not in_place:
        t=t.copy()
    if t.hasColumn(col):        
        _type=t.getColType(col)
        t.replaceColumn(col, (t.getColumn(key_col).apply(dict_.get)).ifNotNoneElse(t.getColumn(col)),
                        type_=_type)
    else:
        if not type_:
            type_=_get_type_from_dict(dict_)
        if type_:
            t.addColumn(col, t.getColumn(key_col).apply(dict_.get), type_=type_, 
                        insertBefore = insertBefore, insertAfter = insertAfter)
        else:
            t.addColumn(col, t.getColumn(key_col).apply(dict_.get), type_=object,
                        insertBefore = insertBefore, insertAfter = insertAfter)
    if not in_place:
        return t


def _get_type_from_dict(dict_):
     types=list(set([type(v) for v in dict_.values()]))
     if len(types)==1:
         return types.pop()
    
################################################################################################         
     
def update_column_by_array_operation(t, colname,  arg_cols, fun, args=[], type_=None, 
                    grouped_by=None, in_place=True, insertBefore=None, insertAfter=None,):
    """ function to apply numpy matrix operations on subtables grouped by group_cols
    """
    if not in_place:
        t=t.copy()                          
    _update_index_col(t, grouped_by)
    id2array=_collect_values(t, arg_cols)
    t.updateColumn(colname, t.apply(_apply_fun, ( (fun, t._id_temp, id2array, args))), 
                   type_=type_, insertBefore=insertBefore, insertAfter=insertAfter)
    t.dropColumns('_id_temp')
    if not in_place:
        return t

def _update_index_col(t, grouped_by):
    def _update(key, d):
        return d[key]
    if not grouped_by:
        t.addEnumeration('_id_temp')
    else:
       _add_group_index(t, grouped_by)
    
    
def _add_group_index(t, colnames):    
    t.addEnumeration('_local_index')
    d={}
    assigned={}
    expr=[t.getColumn(n) for n in colnames]
    index=0
    for i, ntuple in zip(t._local_index, zip(*expr)):
        if assigned.has_key(ntuple):
            d[i]=assigned[ntuple]
        else:
            assigned[ntuple]=index
            d[i]=index
            index+=1
    update_column_by_dict(t, '_id_temp', '_local_index', d, type_=int, in_place=True)
    t.dropColumns('_local_index')
    
            
def _collect_values(t, colnames):
    d=defaultdict(list)
    id2array={}
    expr=[t.getColumn(name) for name in colnames]
    for key, ntuple in zip(t._id_temp, zip(*expr)):
        d[key].append(ntuple)
    for key, value in d.items():
        id2array[key]=np.array(value)
    return id2array
    
def _apply_fun(fun, key, d, args):
    print key, d[key]
    print '====='
    return fun(d[key], *args)    

##
# def test fun

def test_array_op():
    t=_get_t()
    def _fun(ar, a=4.0):
        return float(np.sum(np.sum(ar, axis=0)*np.sum(ar, axis=1)/a))
    update_column_by_array_operation(t, 'xxx', ['a', 'b'],  _fun, grouped_by=['ind'], type_=float)
    return t        
    
def _get_t():
    t=emzed.utils.toTable('a', [0.5, 12.0], type_=float)
    t.addColumn('b', [14.0, 0.5], type_=float)
    t.addColumn('ind', 1, type_=int)
    return t
      
#################################################################################################
def transfer_column_between_tables(t_source, t_sink, data_col, ref_col, insert_before=None):
    """ Function (t_source, t_sink, data_col, ref_col)adds values from column data_col from Table 
        `t_source` to Table `t_sink` via common reference and returns a t_sink. 
    column `ref_col`. If column `data_col` exists already in t_sink an assertion occurs.
    
    """
    #    checks
    
    assert isinstance(t_source, Table), 't_source is not of Type table'
    assert isinstance(t_sink, Table), 't_sink is not of Type table'
    missing=set([data_col, ref_col])-set([n for n in t_source.getColNames()])
    assert len(missing)==0, 'column(s) %s are missing in Table %s' %(missing, t_source)
    assert t_sink.hasColumns(ref_col), 'column %s is missing in Table %s' %(ref_col, t_sink)
    assert t_sink.hasColumns(data_col)==False, 'column %s exists already in Table %s' \
                                                %(data_col, t_sink)
    t_sink=t_sink.copy()
    type_=t_source.getColType(data_col)
    format_=t_source.getColFormat(data_col)
    pairs=set(zip(t_source.getColumn(ref_col).values, t_source.getColumn(data_col).values))
    ref2data={v[0]:v[1] for v in pairs}
    def _add(v, dic=ref2data):
        return dic.get(v)
    t_sink.addColumn(data_col, t_sink.getColumn(ref_col).apply(_add), type_=type_, format_=format_, 
                     insertBefore=insert_before )
    
    return t_sink
#################################################################################################


def cleanup_table_join(t, remove_redundance=True):
    """ postfixes of unique columns of joint table t are removed. If remove redundance =
     True, all columns with postfixes having same values as corresponding column without postfix
     are removed
    """
    t=t.copy()
    postfixes=t.findPostfixes()
    postfixes.remove("")
    with_pstfx=[]
    colnames = [name for name in t.getColNames()]
    for fix in postfixes:
        names=[name.split(fix)[0] for name in t.getColNames() if name.endswith(fix)]
        with_pstfx.extend(names)
    if remove_redundance:
        unique=set(with_pstfx).difference(set(colnames))
        for colname in unique:
            for fix in postfixes:
                if t.hasColumn(colname+fix):
                    t.renameColumns(**{colname+fix:colname})
    return t


##############################################################################################
def peakmap_as_table(pm):
    """ converts peakmap pm into table format
    """
    assert isinstance(pm, PeakMap), 'item must be of type PeakMap!'
    t=emzed.utils.toTable('peakmap', [pm])
    t.addColumn('unique_id', pm.uniqueId())
    t.addColumn('full_source', pm.meta['full_source'])
    t.addColumn('source', pm.meta['source'])
    return t

#################################################################################################

def reduce_peakmap_size_in_table(peaks_table, ref_col='id', 
                                 tol=(-60.0, +60.0, -10.0, +10.0), mslevel=1):
    """ function  reduces data size for e.g. targeted data analysis. Peakmaps are cut 
    to fit targeted extraction window defined by argument ref_col. Argument tol is a tuple with 
    values (lower rttol, upper rttol, lower mztol, upper mztol) and defines tolerance 
    for peakmap cutting.
    """
    t=peaks_table.copy()
    number=len(set(t.getColumn(ref_col).values))
    print 'reducing peakmap size to individual extraction window sizes for %d compounds' %number  
    required=['rtmin', 'rtmax', 'mzmin', 'mzmax']   
    checks.colname_type_checker(t, required)
    assert t.hasColumns(ref_col)
    tuples=_get_large_rt_mz_windows(t, ref_col, tol)
    replace_peakmaps(t, ref_col, tuples, mslevel)
    print 'Done.'
    return t


def _extract_subpeakmap(id_, pm, rtmin, rtmax, mzmin, mzmax):
    pm.uniqueId()
    new=pm.extract(rtmin=rtmin, rtmax=rtmax, mzmin=mzmin, mzmax=mzmax)
    try:
        del new.meta['unique_id'] #bug fix
    except:
        pass # problem fixed in latest version
    new.uniqueId()
    return id_,  new
    
def _build_id_2pm(tuples):
    id_2pm=dict()
    for values in tuples:
        key,value=_extract_subpeakmap(*values)
        id_2pm[key]=value
    return id_2pm
        
def replace_peakmaps(t, ref_col, tuples, mslevel):
    postfix=_get_postfix(t, ['peakmap'])
    id_2pm=_build_id_2pm(tuples)
    def fun_(v, dic=id_2pm, mslevel=mslevel):
        pm=dic.get(v)
        pm.spectra=[spec for spec in pm.spectra if spec.msLevel==mslevel]
        return pm
    t.replaceColumn('peakmap'+postfix, t.getColumn(ref_col).apply(fun_), type_=PeakMap)
            

def _get_large_rt_mz_windows(t, ref_col, tol):
    window_cols=['rtmin', 'rtmax', 'mzmin', 'mzmax']
    pstfx=_get_postfix(t, window_cols)
#    window_cols=[''.join([n, pstfx]) for n in window_cols]
    pairs=zip(window_cols, tol)
    for name,add in pairs:
        if 'min' in name:
            t.addColumn('_'+name, t.getColumn(name+pstfx).min.group_by(t.getColumn(ref_col))+add, 
                        type_=float)
        else:
            t.addColumn('_'+name, t.getColumn(name+pstfx).max.group_by(t.getColumn(ref_col))+add, 
                        type_=float)
    values=set(zip(t.getColumn(ref_col).values, t.getColumn('peakmap'+pstfx).values, 
                   t._rtmin.values, t._rtmax.values, t._mzmin.values, t._mzmax.values))
    _drop_columns(t, window_cols)  
    return values

def _drop_columns(t, names):
    names=[''.join(['_', n]) for n in names]
    t.dropColumns(*names)


def _get_postfix(t, colnames):
    pstfxs=t.supportedPostfixes(colnames)
    assert len(pstfxs)==1, 'peakmap cutting is only possible if window per peak is defined'        
    return pstfxs[0]



#################################################################################################

def single_spec_to_table(prec, spec):
    """ Single_spec_to_table(prec, spec) converts ms2 spectrum into table. Suitable for to 
        visualize MS spectra from top_n or similar approaches.
    """
    from copy import deepcopy
    spec.msLevel=1
    spec1=deepcopy(spec)
    spec1.rt+=0.05
    pm=PeakMap([spec, spec1])
    t=emzed.utils.toTable('precursor', [prec])
    t.addColumn('mzmin', pm.mzRange()[0], type_=float, format_='%.5f')
    t.addColumn('mzmax', pm.mzRange()[1], type_=float, format_='%.5f')
    t.addColumn('rtmin', pm.rtRange()[0]-1.0, type_=float, format_='"%.2fm" %(o/60)')
    t.addColumn('rtmax', pm.rtRange()[1]+1.0, type_=float, format_='"%.2fm" %(o/60)')
    t.addColumn('peakmap', pm)
#    pm1=t.peakmap.uniqueValue()
#    t.replaceColumn('peakmap', pm1)
    return emzed.utils.integrate(t, 'max')

###################################################################################################


def find_common_postfix(t, colnames=None):
    """ Function find_common_postfix(t, colnames=None) finds any common postfix for all colnames 
        of table t. Argument `colnames` is a list of strings, and if set common postfix of 
        selected colnames is returned if listed colnames are all columns of t else, 
        an empty string is returned.
    """
    if not colnames:
        colnames=[n  for n in t.getColNames()]
    if  t.hasColumns(*colnames):
        ref=colnames[0]
        matches=[]
        for name in colnames[1:]:
            s=difflib.SequenceMatcher(None, ref, name)
            match=s.find_longest_match(0, len(ref), 0, len(name))
            pstfx=ref[match.a:match.a+match.size]
            matches.append((pstfx, len(pstfx)))
        return min(matches, key=lambda v: v[1])[0]
    return ''
    
##################################################################################################


def update_rt_by_integration(t, method='std', mslevel=1):
    """update_rt_by_integration(t) returns new column with updated rt value. 
    Required columns rtmin, rtmax, mzmin, mzmax, peakmap
    """
    checks.is_integratable_table(t)
    t=t.copy()
    t.updateColumn('index', range(len(t)), type_=int)
    tt=emzed.utils.integrate(t, method, msLevel=mslevel)
    get=tt.getColumn
    for pstfx in tt.supportedPostfixes(['rtmin', 'rtmax']):
        expr=(get('rt'+pstfx), get('method'+pstfx), get('params'+pstfx))
        tt.updateColumn('rt'+pstfx, 
            tt.apply(_update_rt_by_params, expr, keep_nones=True), type_=float)
    if t.hasColumn('rt' + pstfx):
        t.dropColumns('rt'+pstfx)
    t=transfer_column_between_tables(tt, t, 'rt', 'index', insert_before='rtmin')
    t.dropColumns('index')
    return t        
        
 

###################################################################

def update_rt_from_integrated_peaks(t, insertBefore=None, insertAfter=None):
   """ update_rt_from_integrated_peaks returns rt value at appex for any integration method
        applied. In case the method `no_integration` was applied rt value will be replace by None.
        in place
   """
   checks.is_integrated_table(t)
   t.updateColumn('rt', t.apply(_update_rt_by_params, (t.rt, t.method, t.params), keep_nones=True), 
                  type_=float, insertBefore=insertBefore, insertAfter=insertAfter)
   
   
def _update_rt_by_params(rt, method, params):
    method2fun=_method2fun()
    return method2fun[method](params) if method2fun.has_key(method) else rt
    
    
def _method2fun():
    d={}
    d['trapez'] = _get_appex
    d['trapez_with_baseline'] = _get_appex
    d['std'] = _get_appex
    d['max']=_get_appex
    d['emg_exact']=lambda v: v[1]
    d['emg_with_base_line']=lambda v: v[1]
    d['asym_gauss']=lambda v: v[-1]
    d['no_integration']=lambda v: None
    return d

def _get_appex(params):
    return max(zip(*params), key=lambda v: v[1])[0]
##################################################################################################

def replace_ids_by_unique_ids(tables, id_col, id_ini=0):
    """ function replace_ids_by_unique_ids replace int values of funtion value_col by ascending
        number of values starting from initial_value id_ini over all tables. It simplifies table
        stacking and merging for id values that are table specific
    """
    for t in tables:
        ids=t.getColumn(id_col).values
        # get sorted ascending list of unique values:
        ids=sorted(list(set(ids)))
        d=dict(zip(ids, range(id_ini, len(ids)+id_ini)))
        id_ini+=len(ids)
        update_column_by_dict(t, id_col, id_col, d, type_=int, in_place=True)
        
        
#################################################################################################


def table_from_clipboard():  
    """ * Builds a table from clibboard values if data is an array like structure with column 
    titles in the first row. If the coppied data array contain empty cells an error will raise.
    """
    x=read_clipboard()
    t = Table.from_pandas(x)
    return _evaluate(t)    
    

def _evaluate(t):
    d=dict()
    for row in t.rows:
        d.update(Counter(row))
    if d.has_key(None):
        assert False, 'Empty cells in clipboard data are not allowed!'
    return t
    
            
    
    
            
    
    
    