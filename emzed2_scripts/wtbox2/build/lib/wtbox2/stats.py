# -*- coding: utf-8 -*-
"""
Created on Fri May 04 15:25:50 2018

@author: pkiefer
"""
from sklearn import datasets
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from collections import Counter
from _lowess import lowess
import numpy as np


def run_pca(data, pc_count = None):
    # logtransform data
    data=np.log2(data)
    data = StandardScaler().fit_transform(data)
    return PCA(n_components = 4).fit_transform(data), PCA(n_components = 4)


def build_test_data(t):
    t=t.extractColumns('idms_pid', 'source', 'norm_area' )
    t=t.filter(t.norm_area.isNotNone()==True)
    pid2num=Counter(t.idms_pid.values)
    def _help(id_, d=pid2num):
        return True if d.get(id_)==6 else False
    t.updateColumn('_keep', t.apply(_help, (t.idms_pid, )),type_=bool)
    t=t.filter(t._keep==True)
    t=t.filter(t.idms_pid.isIn(range(50)))
    x=[]
    for sample in t.splitBy('source'):
        s=sample.extractColumns('norm_area')
        z=[np.float64(v[0]) for v in s.rows]
#        import pdb; pdb.set_trace()
        x.append(z)
    return np.array(x)  
     


iris=datasets.load_iris()
x=iris.data
pca = PCA(n_components=2)
#pca.fit(x)
#y=pca.transform(x)
y=pca.fit_transform(x)