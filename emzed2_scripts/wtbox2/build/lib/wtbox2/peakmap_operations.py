# -*- coding: utf-8 -*-
"""
Created on Tue May 19 11:11:25 2015

@author: pkiefer
"""

from emzed.core.data_types import PeakMap
from collections import defaultdict
import numpy as np


def determine_spectral_baseline_and_noise(peakmap):
    """
    Function determine_spectral_baseline_and_noise(pm) determines baseline and noise level assuming 
    that the baseline contains the highest point denity 
    (J Ams Soc Mass Spectrom 2000, 11, 320-3332). To this end, we build a histogramm of all 
    intensities over all spectra, and the bin width of the histogramm is determined by the 
    rule of Freedman und Diaconis. the mean intensity of the bin with highest abundance is 
    said to be the baseline value and the fwhm of the histogramm is said to 
    be the noise.
    """
    intensities=_get_intensities(peakmap)
    return _determine_spectral_noise_and_baseline(intensities)


def _get_intensities(pm):
    intensities=[]
    for spec in pm.spectra:
        intensities.extend(spec.peaks[:,1])
    return sorted([v for v in intensities if v>0])


def _determine_spectral_noise_and_baseline(intensities):
    binwidth=get_binwidth(intensities)
    intensities, counts=_bin_sum(intensities, binwidth)
    return  _determine_noise_params(intensities, counts)


def get_binwidth(intensities):
    # rule of Freedman und Diaconis
    # David Freedman, Persi Diaconis: n the histogram as a density estimator: 
    # L 2 {\displaystyle L_{2}} L_2 theory. 
    # In: Zeitschrift für Wahrscheinlichkeitstheorie und verwandte Gebiete. 
    # Band 57, Nr. 4, 1981, S. 453–476, doi:10.1007/BF01025868
    nominator=(2*(np.percentile(intensities,75)-np.percentile(intensities,25)))
    denominator=len(intensities)**(1/3.0)
    return int(round(nominator/denominator))


def _bin_sum(intensities, width=500):
    d=defaultdict(list)
    x_values=[]
    counts=[]
    for i in intensities:
        d[int(i/width)].append(i)
    for group in sorted(d.keys()):
        x_values.append(np.mean(d[group]))
        counts.append(len(d[group]))
    return x_values, counts


def _determine_noise_params(x,y):
    pairs=zip(x,y)
    _handle_start_value_problem(pairs)
    baseline, appex=max(pairs, key=lambda v: v[1])
    lower=[(p[0], abs(p[1]-appex/2)) for p in pairs if p[0]<baseline]
    upper=[(p[0], abs(p[1]-appex/2)) for p in pairs if p[0]>baseline]
    fwhm1=min(lower, key=lambda v: v[1])[0]
    fwhm2=min(upper, key=lambda v: v[1])[0]
    return baseline, fwhm2-fwhm1


def _handle_start_value_problem(pairs):
    """ Q Ex data have a maximum in the initial bin which leads to wrong estimation
    """
    while not pairs.index(max(pairs, key=lambda v: v[1])):
        index=pairs.index(max(pairs, key=lambda v: v[1]))
        __=pairs.pop(index)
        if not len(pairs):
            assert False, 'Peakmap intinsity distribution is not in line with assumption'\
            'the baseline should contain the highest point denity and the the distribution is'\
            ' Gaussian like.'
            
            
##################################################################################################
def cut_peakmap(pm, rtmin, rtmax, mzmin, mzmax, mslevel=1):
    sub=pm.extract(rtmin=rtmin, rtmax=rtmax, mzmin=mzmin, mzmax=mzmax)
    sub.spectra=[spec for spec in sub.spectra if spec.msLevel==mslevel]
    return sub
    

    
    
###################################################################################################    
def merge_peakmaps(peakmaps):
    specs=defaultdict(list)
    for pm in peakmaps:
        for spec in pm.spectra:
            specs[(spec.rt, spec.msLevel)].append(spec)
    for key in specs.keys():
        specs[key]=_merge_specs(specs[key])
    spectra_=specs.values()
    spectra_.sort(key=lambda v: v.rt)
    return PeakMap(spectra_, {})


def check_origine(peakmaps):
    """
    """
    assert len(set([pm.meta['full_source'] for pm in peakmaps]))==1, 'merging only allowed for'\
            'sub peakmaps originating from same peakmap'

def _merge_specs(specs):
    peaks=[]
    [peaks.extend(s.peaks) for s in specs]    
    peaks=set([tuple(peak) for peak in peaks])
    peaks=list(peaks)
    peaks.sort(key=lambda v: v[0])
    spectrum=specs[0]
    spectrum.peaks=np.array(peaks)
    return spectrum

#############################################################################################

def count_chromatogram_spectra(pm, rtmin, rtmax, mzmin, mzmax):
    """ counts number of peaks in an extracted chromatogram
    """
    rts_ints_tuple=pm.chromatogram(mzmin, mzmax, rtmin, rtmax)
    return len(rts_ints_tuple[0])
    
