

######################################################################################
#
# YOU CAN / SHOULD EDIT THE FOLLOWING SETTING
#
######################################################################################

PKG_NAME = 'wtbox2'

VERSION = (0, 0, 45)

# list all required packages here:

REQUIRED_PACKAGES = []


### install package as emzed extension ? #############################################
#   -> package will appear in emzed.ext namespace after installation

IS_EXTENSION = True


### install package as emzed app ?  ##################################################
#   -> can be started as app.wtbox2()
#   set this variable to None if this is a pure extension and not an emzed app

APP_MAIN = "wtbox2.app:run"


### author information ###############################################################

AUTHOR = 'Patrick Kiefer'
AUTHOR_EMAIL = 'pkiefer@ethz.ch'
AUTHOR_URL = ''


### package descriptions #############################################################

DESCRIPTION = "Additional fnctionalities for enhanced workflow development"
LONG_DESCRIPTION = """

Folow up of wtbox project. Wtbox2 contains only basic tool whereas more complex 
processing steps based on several wtbox emzed tools like e.g. enhanced feature finding, 
feature_extraction are now all covered by the basic_processing module. 

updates 
- 0.0.36: bug fix of emzed.gui.inspect for inpection of list of tables with len>54
    via wtbox2.gui.inspect
- 0.0.37: new function wtbox2.peakmap_operations.count_chromatogram_spectra added
- 0.0.38: bux fix in module collect_and_compare.compare_tables : comparing empty tables
- 0.0.43: module _iso_corr: modified isotope correction excluding negative isotopologue fractions 
    module gui inspect introduced, that allows inspecting list of peakmaps and long list of tables
    which is an issue in emzed.gui.inspect
- 0.0.44: bug fix of _shift_rt_windows_module
- 0.0.45: new gui module function multiplot_peakmaps; update of function utils.show_progress_bar
    and progress bar to in_out module functions load_tables and load_peakmaps.

"""

LICENSE = "http://opensource.org/licenses/GPL-3.0"


######################################################################################
#                                                                                    #
# DO NOT TOUCH THE CODE BELOW UNLESS YOU KNOW WHAT YOU DO !!!!                       #
#                                                                                    #
#                                                                                    #
#       _.--""--._                                                                   #
#      /  _    _  \                                                                  #
#   _  ( (_\  /_) )  _                                                               #
#  { \._\   /\   /_./ }                                                              #
#  /_"=-.}______{.-="_\                                                              #
#   _  _.=('""')=._  _                                                               #
#  (_'"_.-"`~~`"-._"'_)                                                              #
#   {_"            "_}                                                               #
#                                                                                    #
######################################################################################


VERSION_STRING = "%s.%s.%s" % VERSION

ENTRY_POINTS = dict()
ENTRY_POINTS['emzed_package'] = [ "package = " + PKG_NAME, ]
if IS_EXTENSION:
    ENTRY_POINTS['emzed_package'].append("extension = " + PKG_NAME)
if APP_MAIN is not None:
    ENTRY_POINTS['emzed_package'].append("main = %s" % APP_MAIN)


if __name__ == "__main__":   # allows import setup.py for version checking

    from setuptools import setup
    setup(name=PKG_NAME,
        packages=[ PKG_NAME ],
        author=AUTHOR,
        author_email=AUTHOR_EMAIL,
        url=AUTHOR_URL,
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        license=LICENSE,
        version=VERSION_STRING,
        entry_points = ENTRY_POINTS,
        install_requires = REQUIRED_PACKAGES,
        )
   