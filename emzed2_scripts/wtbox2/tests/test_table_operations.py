# -*- coding: utf-8 -*-
"""
Created on Wed Apr 03 15:17:25 2019

@author: pkiefer
"""

import emzed
import random
import wtbox2.table_operations as top
from collections import Counter
def _build_tables():
    tables=[]
    for i in range(3):
        k=random.randint(10,40)
        ids=[random.randint(0,10) for i in range(k)]
#        ids=random.sample(range(20), k)
        tables.append(emzed.utils.toTable('bbid', ids, type_=int))
    return tables

def test_replace_ids_by_unique_ids_0():
    # 1 no overlapping ids between tables
    tables=_build_tables()
    top.replace_ids_by_unique_ids(tables, 'bbid')
    s1,s2,s3=[set(t.bbid.values) for t in tables]
    assert len(s1.intersection(s2))==len(s2.intersection(s3))==0


def test_replace_ids_by_unique_ids_1():
    # same for each id there is a number of elements with similar counts
    tables=_build_tables()
    tables=tables[:1]
    t=tables[0]
    before=Counter(t.bbid.values)
    print before
    top.replace_ids_by_unique_ids(tables, 'bbid')
    t=tables[0]
    after=Counter(t.bbid.values)
    counts=after.values()
    for nos in before.values():
        _=counts.pop(counts.index(nos))
    assert len(counts)==0
    