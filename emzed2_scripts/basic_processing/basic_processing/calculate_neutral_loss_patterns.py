# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 14:35:11 2020

@author: pkiefer

Example:
A = [a0, a1, a2, a3]
C = [c0, c1, c2, c3, c4, c5, c6, c7]
B = [b0, b1, b2, b3, b4]
Equation system example
 a0*b0 + 0*b1  + 0*b2  +  0*b3 +  0*b4 = c0
 a1*b0 + a0*b1 + 0*b2  +  0*b3 +  0*b4 = c1
 a2*b0 + a1*b1 + a0*b2 +  0*b3 +  0*b4 = c2
 a3*b0 + a2*b1 + a1*b2 + a0*b3 +  0*b4 = c3
  0*b0 + a3*b1 + a2*b2 + a1*b3 + a0*b4 = c4
  0*b0 +  0*b1 + a3*b2 + a2*b4 + a1*b4 = c5
  0*b0 +  0*b1 +  0*b2 + a3*b4 + a2*b4 = c6
  0*b0 +  0*b1 +  0*b2 + a0*b4 + a3*b4 = c7

"""

import numpy as np
from scipy.optimize import nnls

def calculate_neutral_loss_pattern(fragment, parent):
    mat=combining_mat(fragment, parent)
    x, s=nnls(mat, np.array(parent))
    return x, s


def combining_mat(frac, iv_c):
    cols=len(iv_c)-len(frac)+1
    rows=len(iv_c)
    mat=np.zeros((rows, cols))
    for i in range(rows):
        for j in range(cols):
            if i-j>=0 and i-j <len(frac):
                mat[i][j]=frac[i-j]
    return mat


def _combining_mat(frac, iv_c):
    cols=len(iv_c)-len(frac)+1
    rows=len(iv_c)
    mat=np.zeros((rows, cols))
    for i in range(rows):
        for j in range(cols):
            if i-j>=0 and i-j <len(frac):
                mat[i][j]=frac[i-j]
    return mat


def build_test_pattern():
    iv_a=[0.5, 0.25, 0.25]
    iv_b=[0.2, 0.2, 0.6]
    cols=len(iv_b)
    rows=len(iv_a)+len(iv_a)-1
    mat=np.zeros((rows, cols))
    for i in range(rows):
        for j in range(cols):
            if i-j>=0 and i-j <len(iv_a):
                mat[i][j]=iv_a[i-j]*iv_b[j]
    iv_c =np.sum(mat, axis=1)
    x,  s = calculate_neutral_loss_pattern(iv_a, iv_c)
    return iv_b, x

udp_gluc=[0.032546272, 0.110681374, 0.221832934, 0.281449797, 0.236154064,
	0.106991225, 0.005194596, 0.00433384, 0.000815898, 0.0 ,0.0, 0.0, 0.0, 0.0 ,0.0, 0.0]
udp=[0.269752307, 0.361246197, 0.369001496,0.0 ,0.0 , 0.0, 0.0, 0.0, 0.0, 0.0]


def strainid2leafno():
    keys=[str(i) for i in range(1, 42)]
    ids=['axenic']
    values=[76, 130, 289, 69, 137, 49, 177, 405, 154, 261, 61, 53, 82, 186, 85, 117, 122, 
            416, 203, 288, 41, 15, 48, 434, 68, 311, 384, 386, 51, 21, 28, 32, 230, 257, 357, 
            000, 70, 220, 131, 666]
    ids.extend([str(i) for i in values])
    d=dict(zip(keys, ids))
    d['37']='Fr1'
    d['42']='community'
    return d


def strainid2genus():
    keys=[str(i) for i in range(2,42)]
    genus=['Acidovorax', 'Acinetobacter', 'Aeromicrobium', 'Arthrobacter', 'Arthrobacter',
           'Bacillus', 'Burkholderia', 'Chryseobacterium', 'Curtobacterium', 'Curtobacterium',
           'Duganella', 'Erwinia', 'Flavobacterium', 'Frigoribacterium', 'Methylobacterium',
            'Methylobacterium', 'Methylobacterium', 'Methylophilus', 'Microbacterium',
            'Microbacterium', 'Pedobacter', 'Pseudomonas', 'Pseudomonas', 'Pseudomonas',
            'Rhizobium', 'Rhizobium', 'Rhizobium', 'Rhizobium', 'Serratia', 'Sphingomonas',
            'Sphingomonas', 'Sphingomonas', 'Sphingomonas', 'Sphingomonas', 'Sphingomonas',
            'Sphingomonas', 'Stenotrophomonas', 'Variovorax', 'Xanthomonas', 'community']    
    return dict(zip(keys, genus))