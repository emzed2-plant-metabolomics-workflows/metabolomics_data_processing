# -*- coding: utf-8 -*-
"""
Created on Wed Aug 07 16:19:51 2019

@author: pkiefer
"""
from wtbox2.fitting import gauss
from wtbox import utils
import numpy as np

def remove_satelite_lc_peaks(t, mztol=0.003, frac=0.6):
    """ Function removes artefact peaks by ff_metabo from detected peaks
    """
    # get pairs
    t_pair=_find_pairs(t, mztol)
    _add_epsilon_area(t_pair)
    return remove_satelites(t, t_pair, frac)

def _find_pairs(t, mztol):
    t1=t.copy()
    tt=t.join(t1, t.mz.equals(t1.mz, abs_tol=mztol))
    tt=tt.filter(~(tt.id==tt.id__0))
    tt=tt.filter((tt.area>tt.area__0))
    return tt

def _add_epsilon_area(t):
    t.updateColumn('area_eps', t.apply(_calc_area, (t.params, t.fwhm, t.params__0)), type_=float)
 
   
def _calc_area(params, fwhm, params0):
    rt, imax=max(zip(*params), key=lambda v: v[1]) # max intensity of main peak
    rts=params0[0]
    sigma=fwhm # we set sigma to fwhm to take into account for non gaussian peaks 
             #tailing and do not use the correct sigma
             #   https://de.wikipedia.org/wiki/Halbwertsbreite
    ints=np.array([gauss(x, sigma, rt) for x in rts])*imax
    return np.trapz(ints, rts)


def remove_satelites(t, tt, frac=0.6):
    ids=_get_satelites_ids(tt, frac=frac)
    return utils.fast_isIn_filter(t, 'id', ids, not_in=True)
    

def _get_satelites_ids(t, frac=0.6):
    def fun(a, a_eps, frac):
        return a-a_eps<=frac*a
        
    t.updateColumn('is_satelite', t.apply(fun, (t.area__0, t.area_eps, frac)),
                   type_=bool)
    selected=[p[0] for p in zip(t.id__0, t.is_satelite) if p[1]]
    return selected