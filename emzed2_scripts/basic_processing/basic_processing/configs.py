# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 10:03:30 2019

@author: pkiefer
"""
import os
import _ff_metabo_config as ff
from emzed import gui

def default_feature_detection(lc_method, ms, rt_align=False, subtract_blank=True, 
                              idms_tables=False, destination=None, 
            determine_noise_level=True, remove_sholders=True):
    """ Function provides parameter sets for default lc_methods and ms instruments
        current lc_methods: 
            - HILIC_BEH_NH3_metabolome
            - nanoLC_TBA
            - UPLC_C18_metabolome
        current ms:
            - Q_exactive
            - LTQ_Orbitrap
    """
    subtract_blank=_check_lc_method(lc_method, subtract_blank)
    config=dict()
    config['ff_config']=ff_metabo_config(lc_method=lc_method, ms=ms)
    config['rtalign']=rt_align
    config['rt_align_params']=enhanced_rt_align_config(None, destination, idms_tables)
    config['determine_noise_level']=determine_noise_level
    config['subtract_blank']=subtract_blank
    config['mztol']=0.003
    config['rttol']=_lc_method2rttol(lc_method)
    config['remove_sholders']=remove_sholders
    return config


def _check_lc_method(lc_method, subtract_blank):
    if lc_method=='nanoLC_TBA' and subtract_blank:
        gui.showWarning('You chose blank subtraction for the method `%s` . Due to important'\
        ' rt variations the method is not suited.'\
        'We therefore recommmend to set `subtract_blank` to False !!! Please adapt.'%lc_method)
        subtract_blank=gui.DialogBuilder('Blank subtraction').\
        addBool('subtract_blank', default=False).show()
    return subtract_blank

def _lc_method2rttol(lc_method):
    d={'HILIC_BEH_NH3_metabolome': 3.0, 'nanoLC_TBA': 25.0, 'UPLC_C18_metabolome' : 5.0}
    return d[lc_method]


def enhanced_adduct_grouper_config(config=None):
    if not config:
        config={'mz_tolerance' : 0.002, 'ignore_clusters_larger_than' : 600, 
                            'fid_col' : 'feature_id'}
    return config


def ff_metabo_config(config=None, lc_method='HILIC_BEH_NH3_metabolome', ms='Q_exactive', adapt=False, 
                     advanced=False):
    if not config:
        config=ff.get_default_ff_metabo(lc_method=lc_method, ms=ms)
    return ff.adapt_ff_metabo_config(config, advanced=advanced) if adapt else config
        

def enhanced_rt_align_config(config=None, destination=None, is_idms_table=False):
    if not config:
        config={}
        config['destination']=destination
        config['maxRtDifference']=12
        config['maxMzDifference']=0.005
        config['maxMzDifferencePairfinder']=0.008
        config['numBreakpoints']=5
        config['is_idms_table']=False
        config['max_c']=30 if is_idms_table else 0
    return config


here = os.path.dirname(os.path.abspath(__file__))

def modify_ff_config_parameter_table():
    parameter_table='ff_metabo_lc_dependent_parameters.csv'
    """ opens parameter talbe in excel and allows local modifications of parameter_settings
    """
    path=os.path.join(here, parameter_table)
    os.startfile(path)
        