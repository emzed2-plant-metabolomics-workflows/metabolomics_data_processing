from collections import defaultdict, namedtuple
from itertools import product
from contextlib import contextmanager
import re
import time

import emzed
from hires.algorithms import assign_components, find_closed_components


"""
restrictions: negative mode gemessen
"""


@contextmanager
def timer(msg):
    print
    print "start:", msg
    started = time.time()
    yield
    needed = time.time() - started
    print "finished: %s, needed %.1f [s]" % (msg, needed) 


MainPeak = namedtuple('MainPeak', 'id_, mz_main, rt_main, z, elements')


def dump(graph, vertices):
    for in_, outs in graph.items():
        for out in outs:
            for v0, v1 in vertices.get((in_, out)):
                continue
#                print in_, "->", out, ":", v0, "->", v1


def restrict_dict(dd, sub_key_set):
    return dict((k, v) for (k, v) in dd.items() if k in sub_key_set)


def restrict_graph(vertices, nodes):
    sub_graph = defaultdict(list)
    for n1 in nodes:
        for n2 in nodes:
            vertex = vertices.get((n1, n2))
            if vertex:
                sub_graph[n1].append(n2)
    return sub_graph


def _major_adducts_in_graphs(graph, mode):
    if mode=='negative_mode':
        major=set(('M-H', 'M-2H', 'M-3H'))
    elif mode=='positive_mode':
        major=set(['M+H', 'M+2H', 'M+3H'])
    present=set(graph.values()).intersection(major)
    return len(present)>0
    
    

def build_sub_graph(graph, vertices, assignment):
    """ create sub graph which is consistent with the current assignment
        that is: only one edge per pair of nodes, not multiples """
    sub_graph = defaultdict(set)
    for in_node, out_nodes in graph.items():
        for out_node in out_nodes:
            for (a0, a1) in vertices.get((in_node, out_node), []):
                # only add vertex if current assignment allows this:
                if assignment[in_node] == a0 and assignment[out_node] == a1:
                    sub_graph[in_node].add(out_node)
    return sub_graph


def create_all_assignments(possible_adducts, nodes, upper_limit=1e6):
    """ parameter possible_adducts: maps (node -> list of possible adducts)
    """
    # iterate over crossproducts:
    # to avoid overflow: if nodes>50
    counter=0
    combs=_estimate_number_of_combs(possible_adducts, nodes)
    for assignment in product(*[possible_adducts[k] for k in nodes]):
        if counter and combs>upper_limit:
            print 
            print 'WARNING !!!! OVERFLOW'
            print 
            print 'MORE THAN %.1e possible assignments reached !!!'% upper_limit
            print 
            break
        counter +=1        
        if len(set(assignment)) < len(assignment):
        
            # assignment if same adduct to two isotope clustes
            continue
        assignment = dict(zip(nodes, assignment))
        yield assignment


def _estimate_number_of_combs(possible_adducts, nodes):
    return sum([len(a)**len(nodes) for a in possible_adducts.values()])
    
    
    


    
class AdductAssigner(object):

    def __init__(self, mode, mz_tolerance, rt_tolerance, cl_only_as_adduct,
                 allow_acetate, ignore_clusters_larger_than, adducts):

        assert mode in ["negative_mode", 'positive_mode'] , "other modes not implemented yet"
        self.adducts=adducts
        self.mode = mode
        self.mz_tolerance = mz_tolerance
        self.rt_tolerance = rt_tolerance
        self.cl_only_as_adduct = cl_only_as_adduct
        self.allow_acetate = allow_acetate
        self.ignore_clusters_larger_than = ignore_clusters_larger_than

    def process(self, table):

        col_names = table.getColNames()

        assert "isotope_cluster_id" in col_names
        assert "mass_corr" in col_names
        assert "unlabeled_isotope_shift" in col_names
        assert "carbon_isotope_shift" in col_names
        assert "mz" in col_names
        assert "rt" in col_names
        assert "area" in col_names
        assert "z" in col_names
        with timer("extract main peaks"):
            peaks = self._extract_main_peaks(table)
#            print "got", len(peaks), "peaks"

        with timer("setup graph"):
            graph, vertices = self._build_graph(peaks)
#            print "computed graph with ", len(graph), "entries" 

        with timer("assign components"):
            adduct_cluster_assignment, reverted_assignment = assign_components(graph)
#            print "found adduct clusters", len(adduct_cluster_assignment)

        with timer("infer adduct assignments"):
            assigned_adducts = self._resolve_adducts(vertices, reverted_assignment)
#            print "assigned", len(assigned_adducts), "adducts"
            
        
        with timer("setup result table"):
            self._enrich_table(table, adduct_cluster_assignment, assigned_adducts)
        for j, adducts in assigned_adducts.items():
            if len(adducts) > 1:
                print "isotope_cluster= %5d" % j, "alternatives=", set(adducts)

    @staticmethod
    def find_consistent_assignments(graph, vertices, nodes, mode):
        
        """
        we consider the nodes of the graph as variables and each connection
        represents an constraint.
        so we iterate over all possible variable assignments and check
        the constraints.

        we assume a symmetric graph, that is each starting node occurs in another
        nodes target nodes and vice versa.
        """

        nodes = set(graph.keys())
        TO_OBSERVE = 84  # None
        DEBUG = TO_OBSERVE in nodes

        if DEBUG:
#            import pprint
            dump(graph, vertices)

        # we iterativliy look for components which have max size and are consistent
        # with the adduct asignment hypthesises

        possible_adducts = defaultdict(set)
        for in_, outs in graph.items():
            for out in outs:
                for a0, a1 in vertices.get((in_, out), []):
                    possible_adducts[in_].add(a0)
                    possible_adducts[out].add(a1)

        if not(possible_adducts):
            # might happen if nodes are not connected
            return

        # now we iterate over each possible node assignment setting
        # and try to find the best one, aka the one which creates the largest sub graph

        partial_solutions = []
        # we assume that only graphs are valuable that contoin a major adduct of the 
        # measurement 
        accepted_nodes=[]
            
#        overflow_count=0
        for assignment in create_all_assignments(possible_adducts, nodes):
#                overflow_count+=1
#                if overflow_count>1e4:
#                    print 'WARNING overflow reached!'
#                    break
                restricted_graph = build_sub_graph(graph, vertices, assignment)
                partial_solution = []
                
                for component in find_closed_components(restricted_graph):
                    reduced_assignment = restrict_dict(assignment, sub_key_set=component)
                    partial_solution.append(reduced_assignment)
                if len(partial_solution):    
#                 2   import pdb; pdb.set_trace()                    
                    if all([_major_adducts_in_graphs(part, mode) for part in partial_solution]):
                        [accepted_nodes.extend(part.keys()) for part in partial_solution]
                        partial_solutions.append((len(partial_solution), partial_solution))
#        print partial_solutions
        nodes.intersection_update(accepted_nodes)
        if len(partial_solutions):
            import pdb; pdb.set_trace()
            lmax, __ = max(partial_solutions)
            for l, solution in partial_solutions:
                if l == lmax:
                    for assignment in solution:
                        yield assignment

    def _extract_main_peaks(self, table):
        peaks = []
        table.sortBy("isotope_cluster_id")
        for group in table.splitBy("isotope_cluster_id"):
            if group.z.uniqueValue() == 0:
                continue
            id_ = group.isotope_cluster_id.uniqueValue()
            main_peak = group.filter(group.area == group.area.max)
            mz_main = main_peak.mz.values[0]
            rt_main = main_peak.rt.values[0]
            z = group.z.values[0]

            reg_exp = re.compile("([A-Z][a-z]?){\d+,\d+}")
            isos = [isotope_shifts for isotope_shifts in group.unlabeled_isotope_shift
                    if isotope_shifts is not None]

            elements = [el_name for isotope_shifts in list(group.unlabeled_isotope_shift)
                        if isotope_shifts is not None
                        for el_name in reg_exp.findall(isotope_shifts)]
            elements = set(elements)

            peak = MainPeak(id_, mz_main, rt_main, z, elements)
            peaks.append(peak)

        return peaks

    def _build_graph(self, peaks):
        """
        vertices in this graph connect isotope clusters which could represent the
        same adduct
        """
        graph = defaultdict(list)
        vertices = defaultdict(list)
        # remove rare adducts for positive
        adducts=self.adducts
        remove=[]
        if adducts is None:
            if self.mode == "negative_mode" :
                adducts = emzed.adducts.negative.adducts
                remove=['M+F']
            else:
                adducts = emzed.adducts.positive.adducts
                # remove very rare adducts
                remove=['M+Li', 'M+H-2H2O', 'M+CH3OH+H', 'M+2Na+H', 'M+2Na-H', 'M+ACN+Na']
                adducts=[(name, delta, abs(z)) for (name, delta, z) in adducts
                       if name not in remove]
                delta_m=emzed.mass.p-emzed.mass.of('NH3')
                adducts.append(('M+H-NH3', delta_m, 1))
        if self.mode=='negative_mode':    
            adducts = [(name, delta, abs(z)) for (name, delta, z) in adducts
                       if self.allow_acetate or name != "M+CH3COO"]

        rt_tolerance = self.rt_tolerance
        mz_tolerance = self.mz_tolerance

        for i, peak_i in enumerate(peaks):

            mz_i = peak_i.mz_main
            id_i = peak_i.id_
            for (name_i, delta_m_i, z_i) in adducts:
                if self.cl_only_as_adduct:
                    if "Cl" in peak_i.elements and "Cl" not in name_i:
                        continue
                if z_i != peak_i.z:
                    continue
                m0_i = mz_i * z_i - delta_m_i
                if m0_i < 0:
                    continue
                for j, peak_j in enumerate(peaks):
                    if i <= j:
                        continue
                    id_j = peak_j.id_
                    mz_j = peak_j.mz_main
                    if abs(peak_j.rt_main - peak_i.rt_main) < rt_tolerance:
                        for (name_j, delta_m_j, z_j) in adducts:
                            if self.cl_only_as_adduct:
                                if "Cl" in peak_j.elements and "Cl" not in name_j:
                                    continue
                            if z_j != peak_j.z:
                                continue
                            m0_j = mz_j * z_j - delta_m_j
                            if m0_j < 0:
                                continue
                            if abs(m0_i - m0_j) <= mz_tolerance:
                                graph[id_i].append(id_j)
                                graph[id_j].append(id_i)
                                vertices[id_i, id_j].append((name_i, name_j))
                                vertices[id_j, id_i].append((name_j, name_i))
        return graph, vertices

    def _resolve_adducts(self, vertices, reverted_assignment):
        """
        the graph may contain multiple vertices between two nodes, where each vertex
        represents an adduct assignment for theses nodes.
        this method determines all consistent adduct assignments within each group.
        """

        assigned_adducts = defaultdict(list)
        for group_id in reverted_assignment.keys():
            nodes = set(reverted_assignment[group_id])

            sub_graph = restrict_graph(vertices, nodes)
            if len(sub_graph) > self.ignore_clusters_larger_than:
                print "ignore graph of size", len(sub_graph)
#                assigned_adducts.update({k: ["cluster_too_small"] for k in sub_graph})
                continue
            print
            print sub_graph
            print
            full_assignments = []
            for assignment in self.find_consistent_assignments(sub_graph, vertices, nodes, self.mode):
                if len(assignment) == len(nodes):
                    full_assignments.append(assignment)

            # if the assignemnt is unique we add this information
            if len(full_assignments):
                for full_assignment in full_assignments:
                    for k, v in full_assignment.items():
                        assigned_adducts[k].append(v)

        return assigned_adducts

    def _enrich_table(self, table, adduct_cluster_assignment, assigned_adducts):
        """
        adds the calculated assignments as columns to the table
        """

        # only assign a adduct cluster if we found consistend adduct assignment:
        cluster_id_to_group_map = dict()
        for iso_cluster_id, adduct_cluster_id in adduct_cluster_assignment.items():
            if assigned_adducts.get(adduct_cluster_id):
                cluster_id_to_group_map[iso_cluster_id] = adduct_cluster_id

        if cluster_id_to_group_map:
            max_id = max(cluster_id_to_group_map.values())
        else:
            max_id = -1

        class AdductGroupAssigner(object):

            """mimmics stateful function"""

            def __init__(self, next_id=max_id + 1):
                self.next_id = next_id

            def __call__(self, id_):
                """ if we succeeded in assigning adducts to cluster we return the corresponding
                cluster id. else we count up for new_ids.
                """
                group_id = cluster_id_to_group_map.get(id_)
                if group_id is not None:
                    return group_id
                group_id = self.next_id
                cluster_id_to_group_map[id_] = group_id
                self.next_id += 1
                return group_id

        def assign_adducts(id_):
            adducts = assigned_adducts.get(id_)
            if adducts:
                return ", ".join(adducts)
            return None
        table.addColumn("adduct_group",
                        table.isotope_cluster_id.apply(AdductGroupAssigner()),
                        insertAfter="isotope_cluster_id", type_=int)

        if assigned_adducts:
            table.addColumn("possible_adducts",
                            table.isotope_cluster_id.apply(assign_adducts),
                            insertAfter="adduct_group", type_=str)
        else:
            table.addColumn("possible_adducts",
                            None,
                            insertAfter="adduct_group",
                            type_=str)
      # due to post reduction of nodes the adduct_group has to be resetted to isotope_cluster_id
      # in case possible adduct is None:
        def _replace(id_, group_id, adducts):
            return id_ if not adducts else group_id
        table.replaceColumn('adduct_group', table.apply(_replace, (table.isotope_cluster_id, 
                        table.adduct_group, table.possible_adducts), keep_nones=True), type_=int)  

def assign_adducts(table, mz_tolerance=8e-4, rt_tolerance=5, cl_only_as_adduct=True,
                   allow_acetate=False, ignore_clusters_larger_than=30, mode="negative_mode", 
                   adducts=None):
    """
    Groups isotope clusters in respect of different adducts.
    Beta version supporting data measured in negative mode and positive mode.
    Table needs to be processed by hires.feature_regrouper before.
    """
    AdductAssigner(mode, mz_tolerance, rt_tolerance, cl_only_as_adduct,
                   allow_acetate, ignore_clusters_larger_than, adducts).process(table)


