# -*- coding: utf-8 -*-
"""
Created on Wed Aug 09 08:31:51 2017

@author: pkiefer
"""
import numpy as np
import emzed
from wtbox2.table_operations import update_rt_by_integration
from wtbox2.collect_and_compare import compare_tables
from wtbox2.utils import fast_isIn_filter
from collections import defaultdict
from _rt_align import rtAlign
from emzed.core.data_types import PeakMap


def targeted_rt_align(tables, ref_table,  destination=None, nPeaks=-1,
            numBreakpoints=5, maxRtDifference=150, maxMzDifference=0.005,
            maxMzDifferencePairfinder=0.005, forceAlign=False, resetIntegration=False):
    """ targeted_rt_align(tables, ref_table) is based on emzed rtAlign algortihm but 
    instead of using all detected peaks it extracts peaks of ref_table in peakmaps of tables and
    uses common peaks for alignment. rtAlign parameters maxMzDifference and maxRtDifference are
    used to determine peak extraction window sizes.
        
    """
    peakmaps=[t.peakmap.uniqueValue() for t in tables]
    colnames=['id', 'feature_id', 'rt', 'rtmin', 'rtmax', 'mz', 'mzmin', 'mzmax', 'z', 'area', 'peakmap']
    t=ref_table.extractColumns(*colnames)
    t.renameColumn('area', 'intensity')
    source=t.peakmap.values[0].meta['source']
    t.addColumn('source', source, type_=str)
    samples=[]
    for pm in peakmaps:
        sample=build_table_from_ref(pm, t, maxRtDifference, maxMzDifference )
        samples.append(find_common_peaks(sample, t, maxMzDifference, maxRtDifference))
    kwargs={'destination' : destination, 'nPeaks':nPeaks, 'numBreakpoints' : numBreakpoints,
            'maxRtDifference' : maxRtDifference, 'maxMzDifference' : maxMzDifference,
            'maxMzDifferencePairfinder' : maxMzDifferencePairfinder, 'forceAlign' : forceAlign,
            'resetIntegration' : resetIntegration}
    samples.append(t) # bug: ref_table must be in list
#    return samples, kwargs
    return rtAlign(samples, tables, t, **kwargs)
    return samples[:-1]


def build_table_from_ref(pm, ref, rt_diff, mz_diff):
    t=ref.copy()
    t.replaceColumn('peakmap', pm, type_=PeakMap)
    t.replaceColumn('rtmin', t.rtmin-rt_diff, type_=float)
    t.replaceColumn('rtmin', (t.rtmin<0).thenElse(0, t.rtmin), type_=float)
    t.replaceColumn('rtmax', t.rtmax+rt_diff, type_=float)
    t.replaceColumn('mzmin', t.mz-mz_diff, type_=float)
    t.replaceColumn('mzmax', t.mz+mz_diff, type_=float)
    t.updateColumn('source', t.peakmap.uniqueValue().meta['source'], type_=str)
    t=t.filter((t.rtmin<t.rtmax)==True)
    return update_rt_by_integration(t)
    

def find_common_peaks(t, ref, mz_diff, rt_diff):
    key2tol={'mz': mz_diff, 'rt': rt_diff}
    comp=compare_tables(ref, t, key2tol=key2tol, leftJoin=False)
    comp=_get_unique_pairs(comp)
    postfix=''.join(['__', str(comp.maxPostfix())])
    colnames=[n for n in comp.getColNames() if n.endswith(postfix)]
    comp=comp.extractColumns(*colnames)
    comp.removePostfixes()
    return comp


def _get_unique_pairs(comp):
    id_num=defaultdict(int)
    id1_num=defaultdict(int)
    postfix=str(comp.maxPostfix())
    _id_0='__'.join(['id', postfix])
    for id_, id_0 in zip(comp.id, comp.getColumn(_id_0)):
        id_num[id_]+=1
        id1_num[id_0]+=1
    ids=[key for key in id_num.keys() if id_num[key]==1]
    ids_0=[key for key in id1_num.keys() if id1_num[key]==1]
    comp=fast_isIn_filter(comp, 'id', ids)
    return  fast_isIn_filter(comp, _id_0, ids_0)


def top_n_bin(t, bin_width=5, value_col='area'):
    bin_width=bin_width*60.0
    steps=int(np.ceil(t.rt.max()/bin_width))
    lower=range(0,steps)
    upper=range(1, steps+1)
    tables=[]
    for min_, max_ in zip(lower, upper):
        sub=t.filter((t.rt>=bin_width*min_) & (t.rt<bin_width*max_))
        # only substables with peaks are further processed
        try:
            max_fwhm=float(np.percentile(sub.fwhm.values, 75))
            min_int=float(np.percentile(sub.getColumn(value_col).values, 25))
            sub=sub.filter((sub.fwhm<max_fwhm)& (sub.getColumn(value_col)>min_int))
            tables.append(sub)
        except:
            pass
    return emzed.utils.stackTables(tables)
    
