# -*- coding: utf-8 -*-
"""
Created on Tue Apr 02 13:00:45 2019

@author: pkiefer
"""

import emzed
import itertools as it
from _adduct_grouping import AdductAssigner
from collections import defaultdict
from _update_grouping_id import update_consensus_group_id
from wtbox2 import utils


def find_consensus_adducts(subsets):
    subset=_get_grouped(subsets)
    subset.updateColumn('mzmean', subset.mz, type_=float)
    subset.updateColumn('mz0mean', subset.mzmean.min.group_by(subset.feature_id), type_=float)
    update_consensus_group_id(subset, group_id='adduct_group', sub_consensus_id='feature_id', 
                                     consensus_id='aid')
    return _find_consensus_adducts(subset)
    

def _find_consensus_adducts(t):
    fid2adduct=dict()
    fid2aid=dict()
    mode=t.polarity.uniqueValue()
    adducts_dict= _get_adductsdict(mode)
    for aid in t.splitBy('aid'):
        graph, vertices, node2mz0=_build_graph(aid, adducts_dict)    
        d=_build_fid2adducts(graph, vertices, node2mz0, adducts_dict, mode)
        fid2adduct.update(d)
        _update_fid2aid(d, fid2aid)
    fid2z=_get_fid2z(fid2adduct, adducts_dict)
    return fid2adduct,  fid2aid, fid2z


#def _update_fid2aid(t, d, fid2aid):
#    for fid, aid in zip(t.feature_id, t.aid):
#        if d.has_key(fid):
#            fid2aid[fid]=aid

def _update_fid2aid(d, fid2aid):
    for fid in d.keys():
        fid2aid[fid]=min(d.keys())

def _get_adductsdict(mode):
    exp=emzed.adducts
    adducts=exp.positive.adducts if mode=='+' else exp.negative.adducts
    names, shifts, zs  = zip(*adducts)
    zs=[abs(z) for z in zs]
    d = dict(zip(names, zip(shifts, zs)))
    if mode=='+':
        d['M+H-NH3']=(emzed.mass.p-emzed.mass.of('NH3'), 1)
        remove=['M+Li', 'M+CH3OH+H', 'M+ACN+Na']
        [d.pop(key) for key in remove if d.has_key(key)]
    return d
    
        
def _get_grouped(subsets):
    # assign unique group_ids to subset_tables
#    t=subsets[0].copy()
    sels=[]
    ungs=[]
    fids=[]
    selfids=[]    
#    shift=0 # +1  is required since 0 is possible id
    for t in subsets:
        s=t.filter(t.possible_adducts.isNotNone())
        fids.extend(s.feature_id.values)
        selfids.extend(s.feature_id.values)
        u=utils.fast_isIn_filter(t, 'feature_id', fids, not_in=True)
        fids.extend(u.feature_id.values)
        if len(s):
#            s.replaceColumn('adduct_group', s.adduct_group+shift, type_=int)
#            shift+=t.adduct_group.max()+1
            sels.append(s) 
        if len(u):
            ungs.append(u)
    ung=emzed.utils.stackTables(ungs)
    if len(ung):
        ung=utils.fast_isIn_filter(ung, 'feature_id', selfids, not_in=True)
        sels.append(ung)
    return emzed.utils.stackTables(sels)
    


def _build_graph(t, adducts_dict, id_col='feature_id'):
    exp=t.getColumn(id_col)
    vertices=defaultdict(list)
    graph=defaultdict(list)
    fid2pairs=defaultdict(set)
    for fid, z, names in zip(exp, t.z, t.possible_adducts):
        if names:
            # combine each pid with all assigned adducts for given z_fid
            # for z_fid
            adducts=names.split(', ')
            adducts=[add for add in adducts if adducts_dict[add][1]==z]
            if len(adducts):
                pairs=it.product([fid], adducts)
                fid2pairs[fid].update(set(pairs))
    node2mz0=dict(zip(exp, t.mz0mean))
    node2m0={fid: node2mz0[fid] for fid in fid2pairs.keys()}
    combs=it.permutations(node2m0.keys(),2)
    for comb in combs:
        graph[comb[0]].append(comb[1])
        for p1, p2 in it.product(fid2pairs[comb[0]], fid2pairs[comb[1]]):
            vertices[p1[0], p2[0]].append((p1[1], p2[1]))
    return graph, vertices, node2m0
    

def _build_fid2adducts(graph, vertices, node2mz0, adducts_dict, mode, mtol=0.003):
    mode='positive_mode' if mode=='+' else 'negative_mode'
    nodes=set(node2mz0.keys())
    fid2adducts=defaultdict(set)
    for d in AdductAssigner.find_consistent_assignments(graph,vertices, nodes, mode):
        if _is_valid(d, node2mz0, adducts_dict, mtol):
            for fid, adduct in d.items():
                fid2adducts[fid].update(adduct.split(', '))
    for fid, values in fid2adducts.items():
        fid2adducts[fid]=tuple(sorted(list(values)))
    return fid2adducts


def _is_valid(node2adduct, node2mz0, adducts_dict, mtol=0.003):
    m0s=[]
    for node in node2adduct.keys():
        add=node2adduct[node]
        delta, z=adducts_dict[add]
        m0s.append(node2mz0[node]*z-delta)
    for comb in it.combinations(m0s,2):
        if abs(comb[0]-comb[1])>mtol:
            return False
    return True


def _get_fid2z(fid2add, adducts_dict):
    fid2z=dict()
    for fid, adducts in fid2add.items():
        # since charge state is consistent for all assigned adducts :
        fid2z[fid]=adducts_dict[adducts[0]][1]
    return fid2z