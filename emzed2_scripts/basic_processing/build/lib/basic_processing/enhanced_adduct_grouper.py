# -*- coding: utf-8 -*-
"""
Created on Wed May 10 14:40:04 2017
@author: pkiefer


1) handles z==0
2) adapts retention time window to binned peaks widths
3) includes positive mode adduct grouping
"""


import wtbox2
from _adduct_grouping import assign_adducts
import emzed
from collections import defaultdict, Counter
from _consensus_adducts import find_consensus_adducts

def parallel_enhanced_adduct_grouper(tables, kwargs, n_cpus=None):
    return wtbox2.multiprocess.main_parallel(enhanced_adduct_grouper, tables, kwargs=kwargs,
                                             cpus=n_cpus)
                                        

def enhanced_adduct_grouper(t, mz_tolerance=0.001, ignore_clusters_larger_than=800, 
                            fid_col='feature_id'):
    """ 
    """
    kwargs={'mz_tolerance': mz_tolerance, 
            'ignore_clusters_larger_than':ignore_clusters_larger_than}
    t0=get_representing_peaks(t,  fid_col)
    _update_hires_fgrouper_cols(t0, fid_col)
    rt_groups=select_potential_adduct_groups(t0, fid_col)
    
    print 'grouping adducts ....'
    assigned=group_adducts(t0, rt_groups, fid_col, kwargs)
    handle_special_case_FA(assigned, fid_col)   
    update_m0(assigned, fid_col)
    label_grouping(assigned)
    return update_adducts(t, assigned, fid_col)
    

def get_representing_peaks(t, id_col, value_col='mz'):
    """ selects monoisotopic peaks 
    """
    remove = False if t.hasColumn('mz0') else True
    t.updateColumn('mz0', t.mz.min.group_by(t.getColumn(id_col)),type_=float)
    t1=t.filter(t.mz==t.mz0)
    t1.dropColumns('mz0')
    if remove:
        t.dropColumns('mz0')
    return t1


def _update_hires_fgrouper_cols(t, fid_col):
    colnames=set(['isotope_cluster_id', 'carbon_isotope_shift', 'unlabeled_isotope_shift',
              'mass_corr',])
    crit=len(colnames-set(t.getColNames()))
    if crit==len(colnames):
        t.addColumn('isotope_cluster_id', t.getColumn(fid_col), type_=int)
        t.addColumn('carbon_isotope_shift', None, type_=str, format_='%s')        
        t.addColumn('unlabeled_isotope_shift', None, type_=str, format_='%s')                        
        t.addColumn('mass_corr', None, type_=float, format_='%+.5f')
        t.addColumn('remove', True, type_=bool)
    elif crit==0:
        t.addColumn('remove', False, type_=bool)
    else:
        assert False, 'The required columns %s are missing' %('_'.join(crit))
        
        
def _remove_hires_fgrouper_cols(t):
    colnames=['isotope_cluster_id', 'carbon_isotope_shift', 'unlabeled_isotope_shift',
              'mass_corr',]
    if t.remove.uniqueValue():
        t.dropColumns(*colnames)
    t.dropColumns('remove')
    
    
#def _get_polarity(t):
#    try:
#        return t.polarity.uniqueValue()
#    except:
#        PeakMap=emzed.core.data_types.PeakMap
#        colnames=[p[0] for p in zip(t.getColNames(), t.getColTypes()) if p[1]==PeakMap]
#        assert len(colnames)==1, 'only one column per Table with PeakMap is allowed'
#        colname=colnames[0]
#        pm=t.getColumn(colname).uniqueValue()
#        return pm.polarity


def select_potential_adduct_groups(t, id_col):
    #1 lookup is based on Table.buildLookup method!!
    tuples=zip(t.rt, t.fwhm, t.getColumn(id_col), t.area)
    tuples.sort(reverse=True, key=lambda v: v[-1]) # sort by area decreasing
    subset=t.copy()
    # t requires a row id column
    subset.addEnumeration('_lookid')
    selected=[]
    while len(subset):
        subset.sortBy('area', ascending=False)
        subset.updateColumn('_lookid', range(len(subset)), type_=int)
        assert list(subset._lookid.values)==sorted(subset._lookid.values)
        rt=subset.rt.values[0]
        fwhm=subset.fwhm.values[0]
        lookup=subset.buildLookup('rt', fwhm/2, None)
        potential=wtbox2.utils.fast_isIn_filter(subset, '_lookid', lookup.find(rt)) 
        potential.updateColumn('_delta', potential.apply(_min_fwhm, (potential.rt, rt, potential.fwhm ,fwhm)),
                               type_=bool)
        select=potential.filter(potential._delta==True)
        assert len(select)>0
        subset=wtbox2.utils.fast_isIn_filter(subset, '_lookid', select._lookid.values, not_in=True)
        select.dropColumns('_delta', '_lookid')
        selected.append(select)
        if not len(subset):
            return selected
    t.dropColumns('_lookid')
    return selected
        
        
def _min_fwhm(rt1, rt2, fwhm1, fwhm2):
    fwhm=min([fwhm1, fwhm2])
    delta=abs(rt1-rt2)
    return True if delta < fwhm/2.0 else False

    
def group_adducts(t, rt_groups, id_col, kwargs):
    subgroups=[permutate_charge_states_within_group(group, kwargs, id_col) for group in rt_groups]
    update_adduct_group(subgroups)
    t=emzed.utils.stackTables(subgroups)
    final_adduct_grouping(t, id_col)
    return t


def permutate_charge_states_within_group(rt_group, kwargs, id_col='idms_pid'):
    subgroup=_map_charge_states_within_group(rt_group, kwargs, id_col)
    update_adduct_group([subgroup])
    return subgroup


def update_adduct_group(fractions):
    d={}
    def _replace(v, d):
            return max(d.keys())+1 if d.has_key(v) else v
    for f in fractions:
        f.updateColumn('adduct_group', f.apply(_replace,(f.adduct_group, d)), type_=int)
        [d.update({v:v}) for v in f.adduct_group.values]
        


def _map_charge_states_within_group(rt_group, kwargs, id_col='feature_id'):
    subset=rt_group.copy()
    subsetpairs=_build_charge_states_pairs(subset)
    before={p[id_col]: int(p['z']) for p in rt_group if p['z']>0}
    subsets=[]
    for sub, adduct_group in subsetpairs:
        kwargs['adducts']=adduct_group
        _assign_adducts(sub, before, id_col, **kwargs)
        subset=_collapse_subset(sub, subset, id_col)
        after={p[id_col]: int(p['z']) for p in subset if p['z']>0}
        subsets.append(subset)
    fid2adduct, fid2aid, after=find_consensus_adducts(subsets)
    label_new_assigned(fid2adduct, fid2aid, rt_group, before, after, id_col)
    return rt_group


def _get_subset(t, id_col, max_len):
    t.sortBy('area', ascending=False)
    length=len(t) if len(t)<max_len else max_len
    x=t[:length]
    return x.copy()


def _build_charge_states_pairs(t):
    charged=t.filter(t.z>0)
    uncharged=t.filter(t.z==0)
    copies=[charged]
    for z in range(1,3):
        t=uncharged.copy()
        t.replaceColumn('z', z, type_=int)
        copies.append(t)
    t=emzed.utils.stackTables(copies)
    t.replaceColumn('isotope_cluster_id', range(len(t)), type_=int)
    single=t.filter(t.z==1)
    mode=_get_mode(t)
    adduct_groups=get_adducts_subgroups(mode)
    # since a table can be empty we check for len
    tables=[t, single] if  mode=='negative_mode' else [t, t.copy(), single]
    return [p for p in zip(tables, adduct_groups) if len(p[0])]
        


def _build_charge_state_table(t):
    charged=t.filter(t.z>0)
    uncharged=t.filter(t.z==0)
    copies=[charged]
    for z in range(1,3):
        t=uncharged.copy()
        t.replaceColumn('z', z, type_=int)
        copies.append(t)
    t=emzed.utils.stackTables(copies)
    t.replaceColumn('isotope_cluster_id', range(len(t)), type_=int)
    return t


def _assign_adducts(t, id2fixedz, id_col, mz_tolerance=0.001, ignore_clusters_larger_than=800,
                    adducts=None):
    mode=_get_mode(t)
    cols=['adduct_group', 'possible_adducts']
    if t.hasColumns(*cols):
        t.dropColumns(*cols)
    rttol=t.fwhm.max()/2
    assign_adducts(t, mz_tolerance=mz_tolerance, rt_tolerance=rttol, 
               ignore_clusters_larger_than=ignore_clusters_larger_than,
               mode=mode, adducts=adducts)
    return 
    _check_adducts(t, id_col)
    #  bug from hires adduct grouper it returns numpy.int32 as z values: bug fix: int(z)
    id2assigned={id_:int(z) for id_, z, adducts in zip(t.getColumn(id_col), 
                                              t.z, t.possible_adducts) if adducts}
    id2fixedz.update(id2assigned)
    # set z of all peaks not in id2fixedz back to 0
    def _reset(key, d):
        return d[key] if d.has_key(key) else 0
    t.replaceColumn('z', t.apply(_reset,(t.getColumn(id_col),id2fixedz), keep_nones=True), type_=int)
     
     
def _get_mode(t):
    
    try:
        mode='negative_mode' if t.peakmap.uniqueValue().polarity=='-' else 'positive_mode'
    except:
        mode='negative_mode' if t.polarity.uniqueValue()=='-' else 'positive_mode'
    return mode


def get_adducts_subgroups(mode):
    # to reduce the number of possible combinations we buils subgroups with different cluster sizes:
    if mode == 'negative_mode':
        multiple=[('M-H', -1.007276466812, -1), ('M-2H', -2.014552933624, -2),
                  ('M-3H', -3.0218294004360002, -3)]
        single=[('M-H', -1.007276466812, -1), ('M-H2O-H', -19.017841530612003, -1),
                ('M+Na-2H', 20.975216347276, -1), ('M+Cl', 34.96940125990946, -1),
                 ('M+K-2H', 36.949153746376, -1), ('M+KCl-H', 72.925282893188, -1),
                ('M+FA-H', 44.998203596988, -1)]
        return multiple, single
    else:
        delta_m=emzed.mass.p-emzed.mass.of('NH3')
        multiple1=[('M+H', 1.007276466812, 1), ('M+2H', 2.014552933624, 2), 
                   ('M+3H', 3.0218294004360002, 3)]
        multiple2=[('M+Na', 22.98922070099054, 1), ('M+H+Na', 23.99649716780254, 2), 
                   ('M+2H+Na', 25.00377363461454, 3), ('M+2Na', 45.97844140198108, 2),
                   ('M+2Na+H', 46.98571786879308, 3)]
        single=[('M+H', 1.007276466812, 1), ('M+NH4', 18.033825562512, 1), 
               ('M+Na', 22.98922070099054, 1), ('M+H-2H2O', -35.013853660788, 1), 
                ('M+H-H2O', -17.00328861180946, 1),  ('M+K', 38.96315810009054, 1), 
                ('M+ACN+H', 42.033825562512, 1), ('M+2Na-H', 44.97116494999054, 1),
                ('M+H-NH3', delta_m, 1)]
        return multiple1, multiple2, single
        

def _check_adducts(t, id_col):
    # only one adduct per feature is allowed
#    _reset_cluster_to_large(t)
    d=defaultdict(set)
    exp=t.getColumn
    for id_, adds in zip(exp(id_col), t.possible_adducts):
        if adds:
            d[id_].add(adds)
    def _replace(key, id_, d, value):
        if len(d[key])>1:
            return id_ if isinstance(value, int) else None
        return value
    for name, type_ in zip(['adduct_group', 'possible_adducts'], [int, str]):
        t.replaceColumn(name, 
                        t.apply(_replace, (exp(id_col), t.isotope_cluster_id, d, exp(name))), 
                        type_=type_)
    # all adduct_groups with len == 1 are reset
    d=Counter(t.adduct_group.values)
    def _reset(d, key, value, col_value, type_):
        if d[key]==1:
            return value if isinstance(col_value, type_) else None
        return col_value
    for name, type_ in zip(['adduct_group', 'possible_adducts'], [int, str]):
        t.replaceColumn(name, 
                        t.apply(_reset, (d, t.adduct_group, t.isotope_cluster_id, exp(name), type_)), 
                        type_=type_)
        


def _collapse_subset(t, subset, id_col):
    # get all features with z==0; if no adduct groupinng occured 
    _adapt_adduct_group_id(t, id_col)
    grouped=t.filter(t.possible_adducts.isNotNone())
    ids=grouped.getColumn(id_col).values
    ungrouped=wtbox2.utils.fast_isIn_filter(subset, id_col, ids, True)
    _update_adduct_grouper_columns(ungrouped, id_col)
    return emzed.utils.stackTables([grouped, ungrouped])




################################################################################################
def _adapt_adduct_group_id(t, id_col):
    """ Adduct grouper algorithm uses isotope cluster id as adduct group id. The function
    _build_charge_states_table assigns different isotope_cluster_id values to the table.
    we therefore have to replace the values by the consistent value row_id
    """
    d=defaultdict(list)
    for rid, adduct_group in zip(t.getColumn(id_col), t.adduct_group):
        d[adduct_group].append(rid)
    def _replace(key, d):
        return min(d.get(key))
    t.replaceColumn('adduct_group', t.apply(_replace, (t.adduct_group, d)), type_=int)
    

def _update_adduct_grouper_columns(t, id_col):
    colnames=[ 'possible_adducts', 'adduct_group']
    coltypes=[str, int]
    values=(None, t.getColumn(id_col))
    for name, value, type_  in zip(colnames, values, coltypes):
        t.updateColumn(name, value, type_=type_, insertAfter='isotope_cluster_id')
        
    

def label_new_assigned(fid2adducts, fid2aid, rt_group, before, after, id_col):
    new_assigned=set(after.keys())-set(before.keys())
    id2id={v:v for v in new_assigned} # dict as hashed list
    
    def _update(key, d):
        return d.has_key(key)
#    fid2aid=dict(zip(subset.getColumn(id_col), subset.adduct_group))
#    fid2possible_adducts=dict(zip(subset.getColumn(id_col), subset.possible_adducts))
    def _update_aid(key, d):
        return d[key] if d.has_key(key) else key
    rt_group.updateColumn('adduct_group', 
                          rt_group.apply(_update_aid, (rt_group.getColumn(id_col), fid2aid)), 
                            type_=int, insertAfter='isotope_cluster_id')
    wtbox2.table_operations.update_column_by_dict(rt_group, 'possible_adducts', 'feature_id',
                        fid2adducts, type_=tuple, in_place=True, insertAfter='adduct_group')
    rt_group.updateColumn('z_by_adduct_grouping', 
                          rt_group.apply(_update,(rt_group.getColumn(id_col), id2id)), type_=bool)



def _assign_unique_adduct_group(tables):
    max_id=0
    for t in tables:
        _renumber_adduct_groups(t)
        _max=max(t.adduct_group.values)
        t.updateColumn('adduct_group', t.adduct_group+max_id, type_ = int)
        max_id+=_max+1


def _renumber_adduct_groups(t):
    adduct_groups=list(set(t.adduct_group.values))
    adduct_groups.sort()
    id2new_id=dict(zip(adduct_groups, range(len(adduct_groups))))
    def _update(key, d):
        return d.get(key)
    t.updateColumn('adduct_group', t.apply(_update, (t.adduct_group, id2new_id)), type_=int)


#################################################################################################
def final_adduct_grouping(t, id_col):
    _major_ions_present(t, id_col)
    _remove_short_adducts(t, id_col)
    _consistent_adduct_group_id(t, id_col)
    _reset_z_values(t)


def _remove_unlikely_adducts(t, id_col):
    def _remove(adduct_names):
        rare=['M+FA-H', 'M+F']
        if len(adduct_names):
            names=adduct_names.split(', ')
            names=set(names)-set(rare)
            return ', '.join(names)
        return adduct_names
    t.replaceColumn('possible_adducts', t.apply(_remove,(t.possible_adducts,)), type_=str)


def _major_ions_present(t, id_col):
    mode=t.polarity.uniqueValue()
    if mode=='-':
        major=['M-H', 'M-2H', 'M-3H']
    elif mode == '+':
        major=['M+H', 'M+Na', 'M+2H', 'M+3H', 'M+NH4']
    else:
        assert False, 'peakmap must be of mode + or -'
    correct=dict()
    for id_, adducts in zip(t.adduct_group, t.possible_adducts):
        if adducts:
#            adducts=adducts.split(', ')
            if len(set(major)-set(adducts))<len(major):
                correct[id_]=id_
    def fun_(key, d, adduct):
        return adduct if d.has_key(key) else None
    t.replaceColumn('possible_adducts', t.apply(fun_,(t.adduct_group, correct, 
                                                      t.possible_adducts)), type_=str)


def _remove_short_adducts(t, id_col):
    d=defaultdict(int)
    for id_, adds in zip(t.adduct_group, t.possible_adducts):
        if adds:
            d[id_]+=1
    def fulfills(id_, id2count, adducts):
        return adducts if id2count[id_]>1 else None
    t.replaceColumn('possible_adducts', t.apply(fulfills,(t.adduct_group, d, 
                                                      t.possible_adducts)), type_=str)
    
def _consistent_adduct_group_id(t, id_col):
    # function updates adduct_group after removing some unlikely grouper results.
    # Required: the id_col groups isotopologues and each group of isotopologues is assigned to the
    # same adduct_group. 
    adduct_id2ids=defaultdict(set)
    id2adduct_id=dict()
    id2adduct=dict()
    for add_id, id_, adducts in zip(t.adduct_group, t.getColumn(id_col), t.possible_adducts):
        adduct_id2ids[add_id].add(id_)
        id2adduct_id[id_]=add_id
        id2adduct[id_]=adducts
    keys=id2adduct_id.keys()
    max_adduct=t.adduct_group.max()
    for id_ in keys:
        # in case no possible adduct was assigned to isotopologue group
        if not id2adduct[id_]:
            add_id=id2adduct_id[id_]
            ids=adduct_id2ids[add_id]
            # split 
            # if more than one group isotopologues is assigned to the same adduct_id we assign
            # a new individual adduct_group number to each group
            if len(ids)>1:
                ids=adduct_id2ids.pop(add_id)
                ids=ids-set([id_])
                for id_ in ids:
                    max_adduct+=1
                    id2adduct_id[id_]=max_adduct
    def _select(key, d):
        return d[key]
    t.replaceColumn('adduct_group', t.apply(_select, (t.getColumn(id_col), id2adduct_id)), type_=int)


def _reset_z_values(t):
    def _reset(z, adduct_name, assigned_by_grouping):
        failed=adduct_name==None and assigned_by_grouping==True
        return 0 if failed else z
    t.replaceColumn('z', t.apply(_reset, (t.z, t.possible_adducts, t.z_by_adduct_grouping), keep_nones=True),
                    type_=int)
    
    
def handle_special_case_FA(t, id_col='idms_pid'):
    """ in the HILIC negative mode co-occurence of M+Cl  and M+Fa was 
        opserved for mono and disaccharides. In that case the M+FA was the most abundant 
        ion. Heuristic: assign M+FA in case of co-occurence with CL adduct. 
    """
    group2name=defaultdict(set)
    for gid, adducts in zip(t.adduct_group, t.possible_adducts):
        if adducts:
            group2name[gid].update(set(adducts))
        else:
            group2name[gid].update('')
    t.updateColumn('reset', 
                t.apply(_evaluate_adduct_group, (t.adduct_group, t.possible_adducts, group2name),
                        keep_nones=True), type_=bool)
   
    _undo_adduct_grouping(t, id_col)
    t.dropColumns('reset')
    

def _evaluate_adduct_group(gid, adduct, gid2addts):
    fa='M+FA-H'
    cl='M+Cl'
    adducts=gid2addts[gid]
    remaining=len(adducts-set([fa]))
    if not fa in adducts:
        return False
    if fa in adducts:
        return False if  cl in adducts else True
    return False if remaining>1 else True

    
def _undo_adduct_grouping(t, id_col):
    t.replaceColumn('adduct_group', (t.reset==True).thenElse(t.getColumn(id_col), t.adduct_group), type_=int)
    t.replaceColumn('possible_adducts', (t.reset==True).thenElse(None, t.possible_adducts), type_=str)
    
        
def update_event(t):
    def _update(update, text, z):
        if update and z:
            comment='z assigned by adduct_grouping'
            return ';'.join([text, comment]) if len(text) else comment
        return text
    t.updateColumn('charge_assigned_by', 
                   t.apply(_update,(t.z_by_adduct_grouping, t.charge_assigned_by, t.z)), type_=str)
    t.dropColumns('z_by_adduct_grouping')
    

def update_adducts(t, assigned, id_col):
    colnames=['adduct_group', 'possible_adducts', 'z_by_adduct_grouping', 'z', 
              'no_of_adducts', 'adduct_assigned_by' ]
    if not assigned.remove.uniqueValue():
        fgrouper_cols=['isotope_cluster_id', 'carbon_isotope_shift', 'unlabeled_isotope_shift',
              'mass_corr',]
        colnames.extend(fgrouper_cols)
        
    for name in colnames:
        d= _build_id2col(assigned, id_col, name)
        t=wtbox2.table_operations.update_column_by_dict(t, name, id_col, d)
    return t
    
def _build_id2col(t, id_col, colname):
    expr=t.getColumn
    return {id_:v for id_, v in zip(expr(id_col), expr(colname))}


def _add_adduct_grouper_columns(t, id_col):
    for col in['adduct_group', 'possible_adducts', 'possible_m0']:
        if t.hasColumn(col):
            t.dropColumns(col)
    t.addColumn('adduct_group', t.feature_id, type_=int, insertAfter='mass_corr')
    t.addColumn('possible_adducts', None, type_=str, format_='%s')
    t.addColumn('possible_m0', None, type_=tuple, format_='%r')

###################################################################
# update possible m0


def update_m0(t, id_col):
    d=adduct2dict()
    t.addColumn('mz0', t.mz.min.group_by(t.getColumn(id_col)),type_=float)
    t.updateColumn('possible_adducts', t.apply(_possible_adducts, (t.possible_adducts, t.z,
                                                   t.polarity), keep_nones=True), type_=str)
    t.updateColumn('possible_m0', t.apply(_add_m0,(t.mz0, t.possible_adducts, d), keep_nones=True), 
                   type_=tuple)
    t.dropColumns('mz0')     

        
def adduct2dict():
    # In negative mode: all features with unassigned adduct are reduced to deprotonation -> 
    # since enhanced grouper allows grouping with charge state z=0
    d={}
    t=emzed.adducts.all.toTable()
    for row in t.rows:
        d[row[0]]=row[1:]
        d['M+H-NH3']=[emzed.mass.p - emzed.mass.of('NH3'),1,1]
    return d


def _possible_adducts(adducts, z, polarity):
    if adducts:
        return adducts
    if polarity=='-':
        z2adduct={0: ('M-H', 'M-2H', 'M-3H'),
                  1: ('M-H',),
                  2: ('M-2H',),
                  3: ('M-3H',)}
    elif polarity=='+':
        z2adduct={0: ('M+H', 'M+2H', 'M+3H'),
                  1: ('M+H',),
                  2: ('M+2H',),
                  3: ('M+3H',)}
    else:
        assert False, 'polarity of peakmap must be + or  - !!'
    try:
        return z2adduct[z]
    except:
        return ('M-H', 'M-2H', 'M-3H') if polarity == '-' else ('M+H', 'M+2H', 'M+3H')


def _add_m0(mz, adduct_name, d):
    masses=[]
    for name in adduct_name:
        if d.has_key(name):
            mass_shift, __, z=d[name]
            masses.append(mz*z-mass_shift)
    return tuple(masses)



def label_grouping(t):
    _update=wtbox2.table_operations.update_column_by_dict
    d=Counter(t.adduct_group)
    _update(t, 'no_of_adducts', 'adduct_group', d, type_=int, in_place=True)
    t.updateColumn('adduct_assigned_by', (t.no_of_adducts>1).thenElse('grouping', 'guess'), 
                   type_=str, insertAfter='adduct_group')
    return t

###########################################################################

