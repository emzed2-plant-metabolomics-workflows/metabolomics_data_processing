# -*- coding: utf-8 -*-
"""
Created on Fri Aug 17 09:58:39 2018

@author: pkiefer
"""

import emzed
import numpy as np
from wtbox2.collect_and_compare import compare_tables
from wtbox2 import peakmap_operations as pmop
from wtbox2 import multiprocess, utils
from collections import Counter
import enhanced_rt_align, subtract_blank_from_peakmap
import hires
from feature_grouper_label_free import ff_labelfree
import os


def detect_features(samples, config, blanks=None, ncpus=None):
    """
    """
    mztol=config['mztol']
    ff_config=config['ff_config']
    signal2noise=ff_config['common_chrom_peak_snr']
    subtract_blank=config['subtract_blank']
    remove_sholders=config['remove_sholders']
    use_threshold=config['determine_noise_level']
    rtalign=config['rt_align_params']
    thresholds=_calc_thresholds(samples, signal2noise)
    if subtract_blank and blanks:
        pairs=zip(samples, blanks)
        print 'subtracting blanks ...'
        
        samples=subtract_blanks(pairs, config, ncpus)
        print 'done.'
    tables, source2thresholds=run_ff(samples, ff_config, thresholds, 
                                determine_noise_level=use_threshold, sn=signal2noise, ncpus=ncpus)
    #Instead of using the default hires.remove_shoulder_peaks function which runs feature_finder
    # metabo to detect major peaks we use ff_detection results directly
    #we remove the sholder peaks based on detected peaks. Potential shoulder will
    # be removed if peak_intensity is 50 x noise level.
    if remove_sholders:
        tables=remove_shoulder_peaks(tables, remove_sholders, thresholds, signal2noise, mztol)
    # remove shoulder _peaks includes an integration step. If remove_shodlder == False the
    # peaks are not integrated and the update_rt function will fail. Therefore we integrate
    # tables in case remove_sholder == False.
    if not remove_sholders:
        tables=integrate_with_enlarged_mz_window(tables, mztol)
    # since ff_metabo determines rt =(rtmin + rtmax)/2 and not the value at rt(max_int) we correct
    # for the appex position
    [update_rt(t) for t in tables]
    if config['rtalign']:
        ref=_get_ref_table(tables)
        tables=enhanced_rt_align.enhanced_rt_align(tables, ref, **rtalign)
    # double peaks might occur if satelites were not completely removed
    tables=[remove_multiple_peaks(t) for t in tables]
    return [ff_labelfree(t) for t in tables]


def _get_ref_table(tables):
    lenths=[len(t) for t in tables]
    index=lenths.index(max(lenths))
    return tables[index]


def _calc_thresholds(samples, sn):
    baseline_and_noise=pmop.determine_spectral_baseline_and_noise
    params=[baseline_and_noise(s) for s in samples]
    return [baseline + sn*noise for baseline, noise in params]
    


def subtract_blanks(pairs, config, ncpus):
    kwargs={'rttol':config['rttol'], 'mztol': config['mztol'], 'scaling': 2.0}
    return multiprocess.main_parallel(_subtract_blank, pairs, kwargs=kwargs, cpus=ncpus)


def _subtract_blank(pair, mztol, rttol, scaling ):
    sample, blank=pair
    sample=subtract_blank_from_peakmap.subtract_blank(sample, blank, mztol, rttol, scaling)
    return sample
    

def _edit_sample(t):
    t.replaceColumn('feature_id', t.fid)

#---------------------------------------------------

def _save_peakmaps(pms, config):
    print 'saving modified peakmaps ...'
    utils.show_progress_bar(pms, _save_pm, args=[config])
        
def _save_pm(pm, config):
    dir_, filename=os.path.split(pm.meta['full_source'])    
    if any([config['subtract_blank'], config['remove_sholders']]):
        filename=_modify_filename(filename)
        _save(pm, dir_, filename)
    if config['local_project_folder'] and os.path.exists(config['local_project_folder']):
        # create a local pm folder
        dir_=os.path.join(config['local_project_folder'], 'peakmaps')
        if not os.path.exists(dir_):
            os.mkdir(dir_)
        _save(pm, dir_, filename)
        config['local_peakmap_folder']=dir_
        
def _save(pm, dir_, filename):
    path=os.path.join(dir_, filename)
    emzed.io.storePeakMap(pm, path)


def _modify_filename(filename, modifier='processed'):
    name, ext=os.path.splitext(filename)
    name='_'.join([name, modifier])
    return ''.join([name, ext])
    
#----------------------------------------------------------------------------------------------
def run_ff(pms, config, thresholds, determine_noise_level=True, sn=3.0, ncpus=None):
    """ function run_ff run_ff(pms, config, determine_noise_level=True) runs in parallel
        emzed.ff.runMetaboFeatureFinder. If determine noise_level== True, peakmap noise level
        is determined for each peak,ap and ff_config_parameter `common_noise_threshold_int`
        is replaced by the noise_level value. Return values:  list of tables and dictionary
        peakmap sorce -> noise_level value
    """
    from copy import copy
    configs=[copy(config) for i in range(len(pms))]
    source2threshold={}
    for config_, pm, threshold in zip(configs, pms, thresholds):
        if determine_noise_level:
            # we determine baseline and and noise over all spectra of the peakmap
            # and we calcuate the threshold as baseline + 3 sigma of baseline
            config_['common_noise_threshold_int']=threshold
        else:
            threshold=config_['common_noise_threshold_int']
        source2threshold[pm.meta['source']]= threshold
    pairs=zip(pms, configs)
    tables=multiprocess.main_parallel(_run_ff, pairs, cpus=ncpus)
    # bug fix: fixed in later emzed version
    [_update_formats(t) for t  in tables]
    return tables, source2threshold


def _run_ff(pair):
    pm ,config=pair    
    return emzed.ff.runMetaboFeatureFinder(pm, **config)


def _update_formats(t):  
    # fixes emzed bug  [fixed in higher emzed versions]
    # enlarges mz window with to obtain less noisy and more comparible signals
    t.setColFormat('mzmin', '%.5f')
    t.setColFormat('mzmax', '%.5f')
        
#####################################################
def remove_shoulder_peaks(tables, remove_sholders, thresholds, signal2noise, mztol):
    if remove_sholders:
        remove_shoulders(tables, remove_sholders, thresholds)
        return remove_shoulder_peaks_from_samples(tables, thresholds, signal2noise, mztol)


def remove_shoulders(tables, remove_sholders, thresholds):
    print 'removing shoulder peaks from peakmap...'
    for t, threshold in zip(tables, thresholds):
        min_int=threshold*50
        _remove(t, min_int)
    print 'Done.'

    
def _remove(t, min_int):
    pm=t.peakmap.uniqueValue()
    ntuples=zip(t.rtmin, t.rtmax, t.mzmin, t.mzmax, t.intensity)
    ntuples.sort(key = lambda v: v[-1], reverse=True)
    i=0
    while i<len(ntuples):
        if ntuples[i][-1]>=min_int:
            hires.remove_shoulder_peaks(pm, *ntuples[i][:-1])
        i+=1

def remove_shoulder_peaks_from_samples(samples, thresholds, sn, mztol):
    print 'removing sholder peaks from tables ...'
    # samples must be reintegrated to determine changes in area due to peakmap correction
    samples=integrate_with_enlarged_mz_window(samples, mztol)
    filtered=[]
    for s, threshold in zip(samples, thresholds):
        min_area=threshold*sn
        filtered.append(s.filter(s.area>=min_area))
        # reducing the number of peaks might lead to undetermined feature charge states if len==1
        [check_z(f) for f  in filtered]
    print 'done'
    return filtered 
                       
def _keep_min_sn_peak_only(sample, thresholds):
    s=sample
    source=s.source.uniqueValue()
    min_int=thresholds[source]
    return s.filter(s.area>min_int)    

def _filter_min_spectra(sample, min_spectra=3):
    def _get_len(params, min_spectra):
        __, ints=params
        return len([i for i in ints if i>0])>=min_spectra
    sample.updateColumn('_keep', sample.apply(_get_len, (sample.params, min_spectra)), type_=bool)
    sample=sample.filter(sample._keep==True)
    sample.dropColumns('_keep')
    return sample

def integrate_with_enlarged_mz_window(tables, mztol):
    for t in tables:
        t.replaceColumn('mzmin', t.mz-mztol, type_=float)
        t.replaceColumn('mzmax', t.mz+mztol, type_=float)
    return integrate_tables(tables, 'trapez')

#------------------------------------------------------------------------------------------------

def remove_multiple_peaks(t, quantity_col='area', rttol=None, mztol=0.002):
    t1=t.copy()
    rttol=get_typical_fwhm(t)/4.0
    key2tol={'mz': mztol, 'rt': rttol}
    comp=compare_tables(t1, t, key2tol=key2tol)
    comp.addColumn('keep', (comp.id==comp.id__0).thenElse(False, True), type_=bool)
    doubles=comp.filter(comp.keep==True)
    doubles.sortBy('area', ascending=False)
    # most intense peak is on top
    remove={}
    for id_, id_0 in zip(doubles.id, doubles.id__0):
        if not remove.has_key(id_):
            remove[id_0]=id_0
    return utils.fast_isIn_filter(t, 'id', remove.keys(), True)

#-------------------------------------------------------------------------------------------

def update_rt(t, id_col='feature_id'):
    """ ff_metabo sets rt to (rtmin+rtmax)/2 assuming gaussian peak shapes. In many cases peak rt
        shape is not symmetric and rt is shifted. We therefore replace rt value by 
        rt at max_intensity. Function requires integration with 'max' integration algorithm prior
        to execution
    """
    d={}
    assert t.method.uniqueValue() in ['max', 'trapez', 'std']
    t.updateColumn('max_area', t.area.max.group_by(t.getColumn(id_col)), type_=float)
    sub=t.filter(t.area==t.max_area)
    t.dropColumns('max_area')
    for fid, params, rt in zip(sub.getColumn(id_col), sub.params, sub.rt):
        d[fid]=_get_rt(params, rt)
    def _update(fid, d):
        return d.get(fid)
    t.replaceColumn('rt', t.apply(_update,(t.getColumn(id_col), d)), type_=float)


def _get_rt(params, rt_, threshold=0.01):
    if params ==None:
        return rt_
    else:
        pairs=zip(*params)
    rt, inten= max(pairs, key=lambda v: v[1])
    # params contains numpy values
    return float(rt)


def integrate_tables(tables, integratorid='max'):
    integrate=emzed.utils.integrate
    # since multiprocessing is applied n_cpus of integrator must be set to 1
    params={'n_cpus': 1, 'integratorid': integratorid}
    return multiprocess.main_parallel(integrate, tables, kwargs=params)


def check_z(t, id_col='feature_id'):
    """
    if len of feature == 1 set z value to 0
    """
    d=Counter(t.getColumn(id_col).values)
    def _replace(fid, z, d):
        return z if d[fid]>1 else 0
    t.replaceColumn('z', t.apply(_replace, (t.getColumn(id_col), t.z, d)), type_=int)

def get_typical_fwhm(t):
    return float(np.percentile(t.fwhm.values, 70))