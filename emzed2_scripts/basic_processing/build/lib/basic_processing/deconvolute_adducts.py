# -*- coding: utf-8 -*-
"""
Created on Thu Nov 09 16:30:54 2017

@author: pkiefer
"""
import wtbox2
from collections import defaultdict
import numpy as np


def deconvolute_adducts(t, id_col='isotope_cluster_id'):
    """
    Function deconvolute_adducts(t, id_col='isotope_cluster_id') assigned by hires.assign_adducts 
    or basic_processing.assign_adducts function. Each adduct group is reduced to the assigned 
    possible adduct with highest area. The ids of removed features are logged in column 
    `id_col2adduct`. 
    """
    required=('area', 'adduct_group', 'possible_adducts', id_col)
    assert t.hasColumns(*required)==True, 'columns %s are missing'% ', '.join(set(required)-set(t.getColNames()))
    find_most_intense(t)
    t=reduce_table2major_adduct(t, id_col)
    t.dropColumns('max_area', 'mean_area')
    return t
    

def find_most_intense(t):
    def mean(values):
        return float(np.mean(values))
    t.updateColumn('mean_area', t.apply(mean,(t.area,)), type_=float)
    t.updateColumn('max_area', t.mean_area.max.group_by(t.adduct_group), type_=float)


def reduce_table2major_adduct(t, id_col):
    summarize_adducts(t, id_col)
    return _reduce_table2major(t, id_col)


def _reduce_table2major(t, id_col):
    ids=set([])
    for id_, area, area_max in zip(t.getColumn(id_col), t.mean_area, t.max_area):
        if area==area_max:
            ids.add(id_)
    return wtbox2.utils.fast_isIn_filter(t, id_col, ids)


def summarize_adducts(t, id_col):
    d=defaultdict(dict)
    n_tuples=zip(t.getColumn(id_col), t.adduct_group, t.possible_adducts)
    for id_, adduct_group, adducts in n_tuples:
        if isinstance(adducts, str):
            adducts=adducts.split(', ')
        d[adduct_group][id_]=adducts
    def add_dict(id_, d):
        return d.get(id_)
    name='2'.join([id_col, 'adduct']) 
    t.updateColumn(name, t.apply(add_dict, (t.adduct_group, d)), type_=dict)  
    

