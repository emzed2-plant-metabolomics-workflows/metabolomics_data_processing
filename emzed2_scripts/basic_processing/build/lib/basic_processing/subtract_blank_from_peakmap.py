# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 09:19:18 2018

@author: pkiefer
"""

from wtbox2 import collect_and_compare as cc
from collections import defaultdict
from emzed.core.data_types import Spectrum, PeakMap


def subtract_blank(sample, blank, mztol, rttol, scaling=3.0 ):
    """ function substract_blank(sample, blank, mztol, rttol, scaling=2.0 ) subtracts all blank
        spectra from sample spectra multiplying blank peak intensity with scaling value.
        * blank, sample: emzed PeakMap objects
        *rttol, mztol: mz rt window size: max intensitiy within window will be removed
        *scaling: spectral peak intensity scaling factor; caling * blank intensity is removed
    """
    print 'building lookup of %s ...' %blank.meta['source']
    lookup=pm2lookup(blank, mztol, rttol)
    print 'subtracting blank from peakmap %s ...' %sample.meta['source']
    return _subtract_peakmap(sample, lookup, mztol, rttol, scaling )
    


def pm2lookup(pm, mztol, rttol):
    d=defaultdict(list)
    for spec in pm.spectra:
        rt=spec.rt
        for mz, int_ in spec.peaks:
            keys=cc.calculate_keys((rt, mz),(rttol, mztol))
            for key in keys:
                d[key].append(int_)
    return d


def _subtract_peakmap(pm, d, mztol, rttol, scaling=3.0):
    spectras=[]
    for spec in pm.spectra:
        rt=spec.rt
        ints=[]
        mzs=[]
        for mz, int_ in spec.peaks:
            keys=cc.calculate_keys((rt, mz),(rttol, mztol))
            values=[0.0]
            for key in keys:
                if d.has_key(key):
                   values.extend(d[key])
            int_=int_-max(values)*scaling
            int_=int_ if int_>0 else 0.0
            ints.append(int_)
            mzs.append(mz)
        y=spec.toMSSpectrum()
        y.set_peaks([mzs, ints])
        spectras.append(Spectrum.fromMSSpectrum(y))
    pm_corr=PeakMap(spectras)
    for key in pm.meta.keys():
        pm_corr.meta[key]=pm.meta[key]
    pm_corr.meta['blank_subtracted']=(True, scaling)
    pm_corr.meta['source']=pm.meta['source']
    pm_corr.meta['full_source']=pm.meta['full_source']
    return pm_corr

                
                    