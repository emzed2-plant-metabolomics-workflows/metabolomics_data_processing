# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 09:27:09 2018

@author: pkiefer

rtbin peaks using typical fwhm
build table2lookup
build dictionary startpoints -> endpoints 

evtl. use peak metrics
"""
import wtbox2.collect_and_compare as cc
from wtbox2 import utils
import emzed
import numpy as np
import wtbox2.emzed_type_checks as etc
from collections import defaultdict


def ff_labelfree(t, mz_accuracy=0.0009):
    # mztol is based on isotopogue mass shifts which can be expected from elements CHNOPS
    etc.is_ff_metabo_table(t)
    etc.is_integrated_table(t)
    print 'processing table %s ...' %t.source.uniqueValue()
    fwhm=np.percentile(t.fwhm.values, 75)
    rttol=fwhm/2.0
    delta_m, mztol=_get_mztol(mz_accuracy)
    key2tol = {'mz': mztol, 'rt': rttol}
    lookup=cc.table2lookup(t, key2tol)
    print 'find connected peaks ...'
    id2ids, id2zs=find_connected_peaks(t, lookup, mztol, rttol,delta_m)
    print 'done.'
    print 
    id2ids=get_m0id2group(id2ids)
    id2fid=get_id2fid(id2ids)
    def _update_fid(id_, d):
        return d[id_] if d.has_key(id_) else id_
    t.updateColumn('fid', t.apply(_update_fid, (t.id, id2fid)), type_=int, 
                   insertAfter='id')
    _update_evaluation_columns(t, id2zs)               
    print 'grouping finished.'
    return evaluate_groups(t)
    
    

def _get_mztol(mass_accuracy):
    # delta were estimated from pubchme data base using function determine_delta_mass. 
    # extreme case with mass diffs delta<0.9 or delta>1.1 were excluded since they originate from 
    # presence of isotopologe shifts with the same nominal mass but with resolved mass  
    # differences e.g. 15N and 13C and they are already included by the limits. 
    min_mass=0.992441-mass_accuracy 
    max_mass=1.003375+mass_accuracy 
    delta_m = (min_mass + max_mass)/2
    mztol=(max_mass-min_mass)/2
    return delta_m, mztol



def determine_delta_mass(db=None, mass_accuracy=0.0009):
    """ function calculates bounds of mass shifts from data base (default: pubchem). Heuristic to
        estimate the mass difference range of isotopologues. The function output is integrated
        into the function _get_mztol. 
        -> a more sophisticated approach like e.g. scoring on histogramm could be used
    
    """ 
    d = set([])
    db=db.filter(db.mf.containsElement('C'))
    db=db.filter(db.mf.containsElement('H'))
    db=db.filter(db.mf.containsOnlyElements('CHNOPS'))
    for mf in db.mf:
        t=emzed.utils.isotopeDistributionTable(mf, R=30000)
        deltas=np.array(zip(t.mass.values, t.mass.values[1:]))
        deltas=set(deltas[:,1]-deltas[:,0])
        d.update(deltas)
    return d
    d=[v for v in deltas if 0.9<=v<=1.1]
    return float(min(d))-mass_accuracy, float(max(d))+mass_accuracy


def find_connected_peaks(t, lookup, mztol, rttol, delta_m):
    id2ids=defaultdict(set)
    id2zs=defaultdict(set)
    for id_, mz, rt in zip(t.id, t.mz, t.rt):
        extract_values(lookup, id_, mz, rt, 0, mztol, rttol, delta_m, id2ids, id2zs)
    return id2ids, id2zs


def extract_values(lookup, id_, mz, rt, z, mztol, rttol, delta_m, id2ids, id2zs):
    keys, zs=calc_keys(mz, rt, rttol, z, mztol, delta_m)
    id_index=lookup['empty_clone'].getColNames().index('id')
    for key, z in zip(keys, zs):
        for key_tuple, row in zip(lookup[key].keys(),lookup[key].values()):
            if _fullfills(mz, rt, delta_m/z, key_tuple, (mztol, rttol)):
                iid=row[id_index]
                id2ids[id_].add(iid)
                id2zs[id_].add(z)
                id2zs[iid].add(z)


def _fullfills(mz, rt, delta_m, key_tuple, tol_tuple):
    args=[(mz+delta_m, rt), key_tuple, tol_tuple]
    args2=[(mz-delta_m, rt), key_tuple, tol_tuple]
    return cc.fullfilled(*args) or  cc.fullfilled(*args2)
        

def calc_keys(mz, rt, rttol, z, mztol, delta_m):
    """
    """
    z_range=range(z, z+1) if z else range(1,4)
    keys=[]
    zs=[]
    for _z in z_range:
        keys.extend(cc.calculate_keys((mz+delta_m/_z, rt), (mztol,rttol)))
        zs.extend([_z]*9)
    assert len(keys)==len(zs)
    return keys, zs    


def get_m0id2group(id2ids):
    d=defaultdict(set)
    for key in id2ids.keys():
        ids=_pop_values(id2ids, key)
        d[key].update(ids)
        for id_ in ids:
            d[key].update(_pop_values(id2ids, id_))
    all_values=_build_id_set(d)
    check=set(d.keys()).intersection(all_values)
    while len(check):
        check=set(d.keys()).intersection(all_values)
        for key in d.keys():
            for sub in check.intersection(d[key]):
                d[key].update(_pop_values(d, sub))
        all_values=_build_id_set(d)
        check1=set(d.keys()).intersection(all_values)
        if len(check1)==len(check):
            return {key: value for key, value in d.items() if len(value)}
    return d


def _pop_values(d, key):
    return d.pop(key) if d.has_key(key) else set([])

        
def _build_id_set(d):
    set_id=set([])
    [set_id.update(d[key]) for key in d.keys()]
    return set_id

            
def extract_ids(id2ids, key, group=None):
    group=id2ids.pop(key)
    if len(group):
        group.add(key)

    
def get_id2fid(collect):
    d={}
    for fid, ids in collect.items():
        d[fid]=fid # m0 is always key
        fids=[fid]*len(ids)
        d.update(dict(zip(list(ids), fids)))
    return d


def _update_evaluation_columns(t, id2zs):
    def _add_zs(id_, d):
        zs=d.get(id_)
        return tuple(zs) if zs else tuple([0])
    t.updateColumn('zs', t.apply(_add_zs, (t.id, id2zs)), type_=tuple)
    t.updateColumn('z', 0, type_=int, insertAfter='mz')
   
##########################################################################################
# evaluating feature groups

def evaluate_groups(t):
    print 'evaluating feature_groups ...'
    ungrouped, grouped=_split_features(t)
    ungrouped=[ungrouped]
    features=[]
    num_c2ratio=build_solution_ratio_space()
    features=grouped.splitBy('fid')
    features=utils.show_progress_bar(features, _evaluate_feature, args=(ungrouped, num_c2ratio))
    features.extend(ungrouped)
    t=emzed.utils.stackTables(features)
    _edit_table(t)
    print 'Finished'
    return t
    
def _edit_table(t):
    t.replaceColumn('feature_id', t.fid, type_=int)
    t.dropColumns('zs', 'fid')
        
        
def _evaluate_feature(f, ungrouped, num_c2ratio):
    features=[]
    groups=[]
    apply_max_mz(f, features)
    for feature in features:
        f=feature
        feature.sortBy('mz')
        _f=heuristic_pattern_rules(feature, ungrouped, num_c2ratio)
        if len(_f):
            f.sortBy('mz')
            determine_charge_state(_f)
        groups.append(_f)
    return emzed.utils.stackTables(groups)


def _split_features(t):
    d=defaultdict(int)
    for fid in t.fid:
        d[fid]+=1
    _update_col_from_dict(t, 'fid_len', 'fid', d, type_=int)
    ungrouped=t.filter(t.fid_len==1)
    ungrouped.dropColumns('fid_len')
    grouped=t.filter(t.fid_len>1)
    grouped.dropColumns('fid_len')
    t.dropColumns('fid_len')
    return ungrouped, grouped


def _update_col_from_dict(t, colname, key_col, d, type_, insertAfter=None):
    def _update(key, d):
        return d.get(key)
    t.updateColumn(colname, t.apply(_update, (t.getColumn(key_col), d)), type_=type_,
                   insertAfter=insertAfter)


def determine_charge_state(feature):
    # feature is sorted by mz
    f=feature
    if len(f)>1:
        dms=np.array(f.mz.values[1:])- f.mz.values[0]
        z=_charge_check(dms, range(1,4))
    else:
        z=0
    # since single features result in none values for z and fid
    feature.updateColumn('z', z, type_=int)
    feature.updateColumn('fid', (feature.z>0).thenElse(f.id.min(), f.id), type_=int)
    

def _charge_check(dms, z_range):
    dm2z=dict()
    if len(dms):
        rows=int(round(max((dms))*max(z_range)))
    else:
        return 
    z_array=np.zeros((rows, len(z_range)))
    dm_array=np.zeros((rows, len(z_range)))
    for dm in dms:
        for z in z_range:
            mi=int(round(dm*z,1))-1
            if mi >=0:
                z_array[mi, z-1]=1 # since z==1 in 0th column
                dm_array[mi, z-1]=dm
    pos_isos=np.argwhere(np.sum(z_array, 0)==len(dms))
    if not len(pos_isos):
        return 0
    pos_isos=pos_isos[0]
    # determine  isotopologue gaps in z_array
    check=z_array[:-1]-z_array[1:]
    cols=np.argwhere(check<0) # e.g. [1,0,1] gives [1,-1] -> gap due to -1 in matrix
    cols=[v[1] for v in cols]
    pos_isos=list(set(pos_isos.tolist())-set(cols))
    # start with highest charge state
    for i in sorted(pos_isos, reverse=True):
        rows=np.argwhere(z_array[:,i]==1)
        for j in rows[:,0].tolist():
            # since mi array with single element
            dm=dm_array[j, i]
            if not dm2z.has_key(dm):
                z=int(i)+1
                if abs(j+1-dm*z)<=0.005*(j+1):
                    dm2z[dm]=int(i)+1 # since i+1 == z and i 1d-array with single element
    zs=set(dm2z.values())
    return  zs.pop() if len(zs)==1 else 0
    return dm2z

############################################################

def apply_max_mz(f, features):
    fs=max_mz0_rule(f)
    if len(fs)==1:
        features.extend(fs)
        return 
    for f_ in fs:
        apply_max_mz(f_, features)
    return 


def max_mz0_rule(f):
    if _rule(f):
        return  _split_pattern(f)
    return [f]

def _rule(f):
    mz0=f.mz.min()
    mzmax=max(zip(f.mz, f.area), key=lambda v: v[1])[0]
    return mzmax>mz0

def _split_pattern(f):
    mzmax=max(zip(f.mz, f.area), key=lambda v: v[1])[0]
    f.updateColumn('split', f.mz<mzmax, type_=bool)
    f.sortBy('mz')
    lower, upper=f.splitBy('split')
    lower.dropColumns('split')
    lower.replaceColumn('fid', lower.id.min(), type_=int)
    upper.dropColumns('split')
    upper.replaceColumn('fid', upper.id.min(), type_=int)
    return [lower, upper]


def _get_fid_pattern(id2params, ids):
    pattern=np.array([])
    for i, id_ in enumerate(ids):
        pattern[i]=(id_, id2params[id_])
    # sort pattern by increasing mz
    return sorted(pattern, key = lambda v: v[1][0])
    

def heuristic_pattern_rules(f, ungrouped_features, numc2ratio):
    while len(f)>=2:
        mz0=f.mz.min()
        #since feature is sorted by mz        
        z=max(f.zs.values[0])
        max_ratio=_get_max_ratio(mz0, z, numc2ratio)
        measured_ratio=f.area.values[1]/f.area.values[0]
        min_ratio=0.0051
        delta_m=int(round((f.mz.values[1]-f.mz.values[0])*z,1))
        if measured_ratio >max_ratio:
           f = _remove_m0(f, ungrouped_features)
        elif measured_ratio < min_ratio:
            f = _remove_m1(f,ungrouped_features)
        # the remaining peaks must have a mass distance of at least 1
        elif delta_m<1:
            f = _remove_m1(f, ungrouped_features)
        else:
            break
    if len(f)>1:
        return f
    ungrouped_features.append(f)
    return f.buildEmptyClone()

    

def _remove_m0(f, ungrouped):
    f1=f[:1]
    f1.replaceColumn('fid', f1.id.min(), type_=int)
    f=f[1:]        
    f.replaceColumn('fid', f.id.min(), type_=int)
    ungrouped.append(f1)
    return f

def _remove_m1(f, ungrouped):
    id_=f.id.values[1]
    f1=f.filter(f.id==id_)
    f=utils.fast_isIn_filter(f, 'id', [id_], True)
    f1.replaceColumn('fid', id_, type_=int)
    ungrouped.append(f1)
    return f

def _get_max_ratio(mz0, z, numc2ratio):
    max_c=int((mz0*z)/emzed.mass.of('CH2'))
    return numc2ratio[max_c] if numc2ratio.has_key(max_c) else numc2ratio[max(numc2ratio.keys())]
     

def build_solution_ratio_space():
    num_c2ratio={}
    for max_c in range(6, 101):
        formula=''.join(['C', str(max_c),'H', str(max_c*2), 'O'])
        pattern=emzed.utils.isotopeDistributionTable(formula,R=30000, minp=0.05).abundance.values
        num_c2ratio[max_c]=pattern[1]/pattern[0]
    return num_c2ratio

