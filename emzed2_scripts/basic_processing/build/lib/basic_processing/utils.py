# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 14:49:34 2018

@author: pkiefer
"""
#from collections import defaultdict
#import wtbox2
from wtbox2.collect_and_compare import subtract_tuples, add_tuples
import numpy as np
#
#def collect_unique_peaks(tables, mztol, only_labeled=True, id_col='idms_pid'):
#    collected=defaultdict(list)
#    key2tol={'mz': mztol}
#    for t in tables:
#        delta_rt=t.rtmax.max()/5.0
#        # we select only fully labeled species
#        if only_labeled:
#            t=t.filter(t.is_nl==False)
#        __, uniques=wtbox2._shift_rt_windows.find_overlapping_peaks(t, delta_rt, id_col)
#        t=wtbox2.utils.fast_isIn_filter(t, id_col, uniques)
#        collected=wtbox2.collect_and_lookup.table2lookup(t, key2tol,d=collected)
#    return collected


#def filter_unique_peaks(tables, mztol, only_labeled=True, id_col='idms_pid'):
#    only_uniques=[]
#    for t in tables:
#        delta_rt=t.rtmax.max()/5.0
#        # if we select only fully labeled species
#        if only_labeled:
#            t=t.filter(t.is_nl==False)
#        __, uniques=wtbox2._shift_rt_windows.find_overlapping_peaks(t, delta_rt, id_col)
#        t=wtbox2.utils.fast_isIn_filter(t, id_col, uniques)
#        only_uniques.append(t)
#    return only_uniques
#    
#-----------------------------------------------------------------------------------------------
def get_typical_fwhm(t):
    return float(np.percentile(t.fwhm.values, 70))

#-----------------------------------------------------------------------------------------------

def add_representing_mz(t, id_col='feature_id', value_col='intensity'):
    expr=t.getColumn
    t.addColumn('max_', expr(value_col).max.group_by(expr(id_col)), type_=float)
    t.updateColumn('mz_rep', (expr(value_col)==t.max_).thenElse(t.mz, 0.0), type_=float, 
                   format_='%.5f')
    t.replaceColumn('mz_rep', t.mz_rep.max.group_by(expr(id_col)), type_=float)
    t.dropColumns('max_')

#-----------------------------------------------------------------------------------------------

def calc_nsse(a,b):
    diff=subtract_tuples(a,b)
    added=add_tuples(a,b)
    numerator= squared_sum(diff)**2
    denominator=squared_sum(added)**2
    return numerator/denominator

def squared_sum(vec):
    return sum([v**2 for v in vec])