# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 17:03:05 2019

@author: pkiefer
"""
from collections import defaultdict
from wtbox2 import table_operations as top


def update_consensus_group_id(lib, group_id='feature_id', sub_consensus_id='pid', 
                                     consensus_id='fid'):
    _add_ugid(lib, group_id, sample_col='source')
    update_grouping_id(lib,  sub_consensus_id, consensus_id, insertAfter=group_id)
    lib.dropColumns('ugid')

def _add_ugid(t, gid, sample_col='source'):
    exp=t.getColumn
    pairs=list(set(zip(exp(gid).values, exp(sample_col).values)))
    d=dict(zip(pairs, range(len(pairs))))
    def _update(d, fid, source):
        return d.get((fid, source))
    t.updateColumn('ugid', t.apply(_update, (d, exp(gid), exp(sample_col))), type_=int, 
                   insertAfter=gid)



def update_grouping_id(lib, subcons_id='pid', group_id_name='fid', insertBefore=None,
                       insertAfter=None):        
    # ugid upper level grouping id e.g. feature id; 
    # lgid lower level grouping id e.g. peak id
    # tha ugid is sample specific unique identifyer
    ugid2lgids=defaultdict(set)
    lgid2ugids=defaultdict(set)
    for ugid, lgid in zip(lib.ugid, lib.getColumn(subcons_id)):
        ugid2lgids[ugid].add(lgid)
        lgid2ugids[lgid].add(ugid)
    pid2fid=get_groups(ugid2lgids, lgid2ugids)
    top.update_column_by_dict(lib, group_id_name, subcons_id, pid2fid, type_=int, in_place=True, 
                              insertBefore=insertBefore, insertAfter=insertAfter)




def get_groups(ugid2lgids, lgid2ugids):
    pid2fid={}
    pset=set([])
    ufid=ugid2lgids.keys()[0]
    _update_pset(ugid2lgids, [ufid], pset)
    fid=0
    while len(lgid2ugids):
        ufids=_readout_dict(lgid2ugids, pset)
        if len(ufids):
            _update_pset(ugid2lgids, ufids, pset)
        else:
            pid2fid.update({pid:fid for pid in pset})
            ufid=ugid2lgids.keys()[0]
            pset=set([])
            _update_pset(ugid2lgids, [ufid], pset)
            fid+=1
    pid2fid.update({pid:fid for pid in pset})
    assert len(lgid2ugids)==0, False
    return pid2fid                              
    
def _update_pset(ufid2pid, ufids, fset):
    for ufid in ufids:
        if ufid2pid.has_key(ufid):
            fset.update(ufid2pid.pop(ufid))
        

def _readout_dict(d, keys):
    values=set([])
    [values.update(d.pop(key)) for key in keys if d.has_key(key)]
    return values