# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 15:31:07 2018

@author: pkiefer
"""
import pytest
import emzed
from emzed.utils import toTable
from basic_processing import deconvolute_adducts as da

@pytest.fixture
def t_neg_adducts():
    adducts=emzed.adducts.negative.toTable()
    t=toTable('adduct_group', [1,1,1], type_=int)
    t.addEnumeration()
    t.addColumn('possible_adducts', adducts.adduct_name.values[:3], type_=str)
    t.addColumn('area', [1e6, 1e5, 1e4], type_=float)
    return t
    
    

def test_deconvolute_adducts_0(t_neg_adducts):
    # minimal Table that is processed by function
    print t_neg_adducts
    deconvoluted=da.deconvolute_adducts(t_neg_adducts, 'id')
    t_neg_adducts.dropColumns('mean_area', 'max_area')
    comp=t_neg_adducts[:1]
    print comp
    print comp.rows
    print deconvoluted.rows
    assert comp.rows==deconvoluted.rows

def test_deconvolute_adducts_1(t_neg_adducts):
    # None value test
    t_=t_neg_adducts[:1]
    t_.rows=[[None]*4]
    deconvoluted=da.deconvolute_adducts(t_, 'id')
    assert deconvoluted.rows==[]


    