# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 09:00:07 2019

@author: pkiefer
"""

from basic_processing import feature_grouper_label_free as fglf
import numpy as np

def test__charge_check_0():
    # empty list of dms 
    assert fglf._charge_check([], [1,2,3]) is None

def test__charge_check_1():
    # single element list of dms
    for i in range(1,4):
        dms=1.00335/i*np.array([1])
        assert fglf._charge_check(dms, [1,2,3]) == i

def test__charge_check_2():
    # multiple element list of dms
    for i in range(1,4):
        dms=1.00335/i*np.array([1,2,3])
        assert fglf._charge_check(dms, [1,2,3]) == i
    
def test__charge_check_3():
    # list of dms with gaps
    for i in range(1,4):
        dms=1.00335/i*np.array([1,3])
        assert fglf._charge_check(dms, [1,2,3]) == 0

def test__charge_check_4():
    # dms of mixed charge states
    for i in range(1,4):
        dms=1.00335*np.array([1,2,3])/np.array([2,1,3])
        assert fglf._charge_check(dms, [1,2,3]) == 0