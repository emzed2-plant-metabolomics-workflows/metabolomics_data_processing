# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 11:53:18 2019

@author: pkiefer
"""
import emzed
from add_cross_reference import add_cross_reference


def merge_libraries(libs, lc_col='lc_method', mtol=0.003, max_rt_shift=6.0, 
                    label_col='adduct_assigned_by', id_col='fid'):
    assert len(libs)>=1, 'library tables are missing !!'
    _update_index_cols(libs)
    lib=emzed.utils.stackTables(libs)
    if not lib.hasColumn(label_col):
        lib.updateColumn(label_col, '', type_=str)
    lc_groups=lib.splitBy(lc_col)
    libs=[]
    for t in lc_groups:
        t.sortBy('polarity')
        add_cross_reference(t, mtol, max_rt_shift, id_col, label_col)
        libs.append(t)          
    if len(libs)>1:
        ref=libs[0]
        for g in lc_groups[1:]:
            ref=merge_lc_libs_with_diff_lc_ms(ref, g)
        update_ascending_indices(ref)
        return ref
    ref=libs[0]
    update_ascending_indices(ref)
    return ref


def merge_lc_libs_with_diff_lc_ms(lib1, lib2):
    _different_lc_ms_methods(lib1, lib2)
    return emzed.utils.stackTables([lib1, lib2])


def _different_lc_ms_methods(lib1, lib2):
    m1=set(lib1.lc_method.values)
    m2=set(lib2.lc_method.values)
    assert  m1.intersection(m2)==set([])


def _update_index_cols(libs):
    index_cols=['ipid', 'pid', 'fid', 'aid']
    assert all([lib.hasColumns(*index_cols) for lib in libs])
    for lib1, lib2 in zip(libs[:-1], libs[1:]):
        for i_col in index_cols:
            shift=lib1.getColumn(i_col).max()+1
            lib2.updateColumn(i_col, lib2.getColumn(i_col)+ shift, type_=int)

    
def update_ascending_indices(lib, id_cols=['ipid', 'pid', 'fid', 'aid', 'crid']):
    for id_col in id_cols:
        _consecutive_index(lib, id_col)
    

def _consecutive_index(t, id_col):
    t.sortBy(id_col)
    indices=[0]
    values=t.getColumn(id_col).values
    m=0
    for i, j in zip(values, values[1:]):
        if i<j:
            m+=1
        indices.append(m)
    t.replaceColumn(id_col, indices, type_=int)
    
    