# -*- coding: utf-8 -*-
"""
Created on Fri May 24 14:09:59 2019

@author: pkiefer
"""
from collections import defaultdict
from emzed import utils
import wtbox2
import os
    

def build_value_matrix_table(t, row_col='pid', column_col='sample_name', 
                             value_col='norm_crid_area', type_=float, format_='%.2e', 
                             log_transform=False):
    get=t.getColumn
    value_col='_'.join(['log2', value_col]) if log_transform else value_col
    d=get_row2col2value(t, row_col, column_col, value_col)
    row_cols=_get_set_as_sorted_list(get(row_col))
    colnames=_get_set_as_sorted_list(get(column_col))
    t_data=utils.toTable(row_col, row_cols, type_=int, format_='%d')
    _get=t_data.getColumn
    for name in colnames:
        t_data.updateColumn(name, t_data.apply(_get_value, 
                        (_get(row_col), name, d)), type_=type_, format_=format_)
    return t_data


def _check_save_path(path):
    if not os.path.exists(path):
        os.mkdir(path)


def _update_grouping_ids(t, t_data, lower='pid', upper='aid'):
    exp=t.getColumn
    d=dict(zip(exp(lower), exp(upper)))
    wtbox2.table_operations.update_column_by_dict(t_data, upper, lower, d,
                                                  type_=int, insertBefore=lower, in_place=True)
    
    
def _get_set_as_sorted_list(values):
    return sorted(list(set(values)))
    

def _get_value(key1, key2, d):
    return d[key1][key2]
#    return log(value,2) if log2 else value


def get_row2col2value(t, id_col, column_col, value_col):
    get=t.getColumn
    d=defaultdict(dict)
    for row, col, value in set(zip(get(id_col), get(column_col), get(value_col))):
        d[row][col]=value
    return d







