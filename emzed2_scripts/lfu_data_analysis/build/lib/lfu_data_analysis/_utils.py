# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 16:04:30 2019

@author: pkiefer
"""
from inspect import getargspec


def get_kwargs_from_config(fun, config):
    args=getargspec(fun).args
    return _get_kwargs(args, config)
    

def _get_kwargs(args, key2value):
    return {arg: key2value[arg] for arg in args if key2value.has_key(arg)}
    
###################################################################################
# helper funs:
    
def _get_norm_colname(group_col):
    return '_'.join(['norm', group_col, 'area'])
    

