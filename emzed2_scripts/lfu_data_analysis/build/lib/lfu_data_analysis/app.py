import main_data_analysis as run_anal
import config_lfu as clfu
from emzed import gui
import manage_data as md


def run():
    def_dir=r'\\gram\biol_micro_gr_vorholt\OMICS'
    project_path=md.set_project_path(def_dir)
    class DefaultMainGui(gui.WorkflowFrontend):
        project_dir=project_path
        plib=gui.WorkflowFrontend().set_defaults()
        ident=gui.WorkflowFrontend().set_defaults()
        mat=gui.WorkflowFrontend().set_defaults()
        config=md.get_config(project_path)
        complete=gui.RunJobButton('merge and complete libraries', method_name='run_complete_library')
        ident=gui.RunJobButton('build identification library', method_name='run_build_ident_lib')
        mat=gui.RunJobButton('build data_matrix', method_name='run_build_data_matrix')
        stats=gui.RunJobButton('perform pairwise stats', method_name='run_pairwise_stats')
        

        def run_complete_library(self):
            process_key='complete'
            self.libs=md.load_libraries(self.project_dir)
            self.config['plibs']=self.libs
            self.config=clfu.config_gui(self.config, process_key)
            self.plib=run_anal.build_overall_library(self.libs, self.config[process_key])
            md.save_result(self.plib, self.config[process_key], self.project_dir)
            md.save_config(self.config, self.project_dir)
            gui.showInformation('Finished')


        def run_build_ident_lib(self):
            process_key='ident'
            self.config=clfu.config_gui(self.config, process_key)
            
            if not self.plib:
                self.plib=md.load_result(self.config['complete'], self.project_dir)
            if self.plib:
                self.ident=run_anal.get_identification_library(self.plib, self.config[process_key])
                md.save_result(self.ident, self.config[process_key], self.project_dir)
                md.save_config(self.config, self.project_dir)
            gui.showInformation('Finished')
                
            
    
        def run_build_data_matrix(self):
            process_key='mat'
            if not self.plib:
                self.plib=md.load_result(self.config['complete'], self.project_dir)
            if self.plib:
                self.config['plib']=self.plib
                self.config=clfu.config_gui(self.config, process_key)
                self.mat=run_anal.build_data_matrix(self.plib, self.config[process_key])
                md.save_result(self.mat, self.config[process_key], self.project_dir)
                md.save_config(self.config, self.project_dir)
            gui.showInformation('Finished')
            
    
        def run_pairwise_stats(self):
            process_key='ps'
            if not self.plib:
                self.plib=md.load_result(self.config['complete'], self.project_dir)
            if not self.ident:
                self.ident=md.load_result(self.config['ident'], self.project_dir)
            self.config['plib']=self.plib
            self.config=clfu.config_gui(self.config, process_key)
            if self.plib and self.ident:
                self.candidates=run_anal.pair_wise_analysis(self.plib, self.ident, 
                                            self.config[process_key], self.project_dir)
                md.save_config(self.config, self.project_dir)
                md.save_results(self.candidates, self.config[process_key], self.project_dir)
            gui.showInformation('Finished')

    DefaultMainGui().show()
        
if __name__ == '__main__':
    run()            
            


    