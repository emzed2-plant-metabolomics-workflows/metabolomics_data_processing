# -*- coding: utf-8 -*-
"""
Created on Fri Nov 15 12:16:58 2019

@author: pkiefer
"""
from emzed import gui, io, core
from glob import glob
from wtbox2 import in_out, utils
from config_lfu import get_default_config
import os

def set_project_path(default=None):
    project_path=gui.DialogBuilder('select project path')\
    .addDirectory('project path', default=default, 
                                       help='enter main project folder path. '\
                                    'If a new project is initialized enter path with LC-MS data.'\
                                    'A new project structure will be built automaticaly')\
    .show()
    project_path=os.path.normpath(project_path)
    handle_subfolders(project_path)
    return project_path
    


def handle_subfolders(project_path):
    subfolders=['lfu_config', 'lfu_result']
    for subfolder in subfolders:
        path=os.path.join(project_path, subfolder)
        if not os.path.exists(path):
            os.mkdir(path)

#-------------------------------------------------
# load data

def get_config(project_path):
    sub='lfu_config/lfu_config.json'
    path=os.path.join(project_path, sub)
    if os.path.exists(path):
        return in_out.load_dict(path)
    return get_default_config()
    


def load_libraries(project_path):
    pathes=[]
    while True:
        files=gui.askForMultipleFiles(caption='please select peak libraries', 
                                   extensions=['table', 'tables'], startAt='project_path')
        pathes.extend(files)
        more=gui.askYesNo('Do you want to load additional libraries?')
        if not more:
            break
    print 'loading peak libraries ...'
    plibs= utils.show_progress_bar(pathes, _load_lib)
    print 'done'
    return plibs
    
                                   
def _load_lib(path):
    try:
        return in_out.load_list_of_tables(path)[0]
    except:
        return io.loadTable(path)
 

def load_result(config, project_path):
    path=get_path(config, project_path)
    pathes=_get_all_pathes(config, project_path)
    if os.path.exists(path):
        load_fun=_get_load_fun(path)
        return load_fun(path)
    else:
        if len(pathes):
            path=_select_path(pathes)
            load_fun=_get_load_fun(path)
            print 'loading result file ...'
            return load_fun(path)
        dir_, filename=os.path.split(path)
        gui.showWarning('file %s in %s is missing! Please run the previous workflow step!!!'\
        %(filename, dir_))
                
            
def _load_result(path):
    ext=os.path.splitext(path)[-1]
    assert ext in ['.table', '.csv']    
    return io.loadTable(path) if ext=='.table' else io.loadCSV()

    
def _get_load_fun(path):
    ext=os.path.splitext(path)[-1]
    assert ext in ['.table', '.csv']    
    return io.loadTable if ext=='.table' else io.loadCSV


def _select_path(pathes):
    i=gui.DialogBuilder('select result table')\
    .addChoice('select', pathes).show()
    return pathes[i]

#---------------------------------------------------
# save data
    
def save_config(config, project_path):
    sub='lfu_config/lfu_config.json'
    path=os.path.join(project_path, sub)
    config=_cleanup_config(config)
    in_out.save_dict(config, path, True)



def _cleanup_config(config):
    d={}
    allowed_keys=['complete', 'mat', 'ident', 'ps']
    for key in allowed_keys:
        if config.has_key(key):
            d[key]=config[key]
    return d



def save_result(item, config, project_path):
    path=get_path(config, project_path)
    path=handle_path(path)
    save_fun=_get_save_fun(path)
    save_fun(item, path)

def save_results(items, config, project_path):
    path=get_path(config, project_path)
    path=handle_path(path)
    in_out.save_list_of_tables(items, path)

def _get_save_fun(path):
    ext=os.path.splitext(path)[-1]
    assert ext in ['.table', '.csv']    
    return io.storeTable if ext=='.table' else io.storeCSV

    
    
#--------------------------------------------------------------------

def get_relative_path(path, project_dir):
    if os.path.isabs(path):
        path=os.path.normpath(path)
        project_dir=os.path.normpath(project_dir)
        try:
            if os.path.commonprefix([path, project_dir])==project_dir:
                return os.path.relpath(path, project_dir)
        except:
            pass
    return path


def get_path(config, project_path):
    dir_=os.path.join(project_path, 'lfu_result', config['result_folder'])
    if not os.path.exists(dir_):
        os.mkdir(dir_)
    filename= ''.join([config['filename'], config['ext']])
    return os.path.join(dir_, filename)


def handle_path(path):
    if os.path.exists(path):
        replace=gui.askYesNo('Do you want to replace existing result file?')
        if replace:
            os.remove(path)
    else:
        label=in_out.time_label()
        name, ext=os.path.splitext(path)
        name='_'.join([name, label])
        path=''.join([name, ext])
    return path
        
        

def _get_all_pathes(config, project_path):
    dir_=os.path.join(project_path, 'lfu_result', config['result_folder'])
    if not os.path.exists(dir_):
        os.mkdir(dir_)
    filename= ''.join(['*', config['ext']])
    path=os.path.join(dir_, filename)
    return glob(path)



    
    