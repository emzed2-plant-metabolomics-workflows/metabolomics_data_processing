# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 15:43:04 2019

@author: pkiefer
"""
import os
from merge_libraries import merge_libraries as ml
import selecting_library_peaks as sl
from missing_values import complete_missing_values as cmv
from build_identification_library import build_identification_library
from wtbox2.utils import run_function
#from pairwise_stats import run_batch_pairwise_analysis
import pairwise_stats as ps
import data_matrices as datmat


def build_overall_library(libs, config):
    lib=run_function(ml, (libs,), config)
    filter_values=sl.filter_values_via_min_hits
    lib=run_function(filter_values, (lib,), config)
    lib=run_function(cmv, (lib,), config)
    preprocess=sl.pre_process_library
    run_function(preprocess, (lib,), config, True)
    return lib
    

def build_data_matrix(lib, config):
    get_mat=datmat.build_value_matrix_table
    return run_function(get_mat, (lib,), config)


def get_identification_library(lib, config):
    return run_function(build_identification_library, (lib,), config)


def pair_wise_analysis(lib, ident, config, project_path):
    save_path=os.path.join(project_path, 'lfu_result', config['result_folder'])
    # Geknaupe but anyway 
    result_folder=config['result_folder']
    config['result_folder']=save_path
    fun=ps.perform_pairwise_analysis
    config['ident']=ident
    config['save_path']=save_path
    result=run_function(fun, (lib,), config)
    _=config.pop('ident')
    config['result_folder']=result_folder
    return result



    