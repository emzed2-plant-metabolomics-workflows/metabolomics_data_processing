# -*- coding: utf-8 -*-
"""
Created on Fri Aug 09 09:22:30 2019

@author: pkiefer
"""
from collections import defaultdict
import wtbox2
import numpy as np
from emzed.utils import stackTables
from random import uniform

###################################################################################################
#  
def pre_process_library(lib, group_id='crid', sg_col='sample_group_id', ref_group='Con1', 
                        value_col='norm_peak_area', combine_areas='sum'):
    """ note: if average = true an averaged area value weighted norm_crid_area is calculated. By
        default we calculate the  sum of all crid areas.
        -> we can apply the sum to determine  metabolite fold change by crid areas since 
        1) each ms peak adduct peak pair (ai, bi) of sample group and reference has the same ratio 
        and therefore
        if a1/b1 = a2/b2 = ... = an/bn = c = (a1+a2+ ... an)/(b1+b2+ ... +bn)
        2) Since we applied MNAR and MAR filling for all adduct species values are present
    """
    def _fun(v):
        return float(np.log2(v))
    
    def _get_name(prefix, norm_col_name):
        return '_'.join([prefix, norm_col_name])
        
    norm_colname=_get_norm_col_name(group_id)
    update_norm_crid_area(lib, value_col, group_id, combine_areas, norm_colname)
    get=lib.getColumn
    
    lib.updateColumn(_get_name('mean', norm_colname), 
                     lib.norm_crid_area.mean.group_by(get(group_id), get(sg_col)), 
                     type_=float, format_='%.2e', insertAfter='norm_crid_area')
    lib.updateColumn(_get_name('std', norm_colname),
                     lib.norm_crid_area.std.group_by(get(group_id), get(sg_col)), 
                     type_=float, format_='%.2e', insertAfter='mean_norm_crid_area')
    lib.updateColumn(_get_name('cv', norm_colname), lib.std_norm_crid_area/lib.mean_norm_crid_area, 
                     type_=float, format_='%.2f', insertAfter='std_norm_crid_area')            
    lib.updateColumn(_get_name('log2', norm_colname), 
                     lib.norm_crid_area.apply(_fun), type_=float, format_='%.2e',
                      insertAfter='cv_norm_crid_area')
    lib.updateColumn(_get_name('mean_log2', norm_colname),
                     lib.log2_norm_crid_area.mean.group_by(get(group_id), get(sg_col)), 
                     type_=float, format_='%.2e', insertAfter='log2_norm_crid_area')
    lib.updateColumn(_get_name('std_log2', norm_colname),
                     lib.log2_norm_crid_area.std.group_by(get(group_id), get(sg_col)), 
                     type_=float, format_='%.2e', insertAfter='mean_log2_norm_crid_area')
    lib.updateColumn(_get_name('cv_log2', norm_colname),
                     lib.std_log2_norm_crid_area/lib.mean_log2_norm_crid_area.apply(abs), 
                     type_=float, format_='%.2f', insertAfter='std_log2_norm_crid_area')            
    determine_fold_changes(lib, ref_group, norm_colname, _get_name('mean', norm_colname), 
                           group_id, sg_col)
    
    
        
##-------------------------------------------------------------------------------------------------
def determine_fold_changes(t, ref, value_col, norm_col, group_col, sg_col):
    d=_collect_ref_norms(t, ref, norm_col, group_col, sg_col)
    get=t.getColumn
    t.updateColumn('fold_change_to_mean_ref', t.apply(_fc2mean, (get(value_col), d, get(group_col))), 
                   type_=float, format_='%.2e')
    t.updateColumn('log2_fold_change', t.fold_change_to_mean_ref.apply(lambda v: float(np.log2(v))),
                   type_=float, format_='%.2e')               
    t.updateColumn('mean_fold_change', 
                   t.fold_change_to_mean_ref.mean.group_by(get(group_col), get(sg_col)), 
                    type_=float, format_='%.2e')
    t.updateColumn('log2_mean_fold_change', t.mean_fold_change.apply(lambda v: float(np.log2(v))),
                   type_=float, format_='%.2e')               
    t.updateColumn('ref_fold_change', ref, type_=str)


def _collect_ref_norms(t, ref, norm_col, group_col, sg_col):
    get=t.getColumn
    t=t.filter(get(sg_col)==ref)
    # since after filtering t is a different object we have to redefine get
    get=t.getColumn    
    return dict(zip(get(group_col), get(norm_col)))

def _det_fc(v, d, crid):
    lower=np.percentile(d.values(), 20) 
    return v/(d[crid]+uniform(lower/100, lower))

def _fc2mean(v, d, crid):
    return v/d[crid]
###----------------------------------------------------------------------------------------------


def update_norm_crid_area(t, value_col='norm_peak_area', id_col='crid', combine_areas='sum',
                          norm_colname='crid_norm_area'):
    d=defaultdict(dict)
    get=t.getColumn
    ntuples=zip(get(id_col), t.sample_name, get(value_col))
    for crid, sid, v in ntuples:
        if not d[crid].has_key(sid):
            d[crid][sid]=[]
        d[crid][sid].append(v)
    _build_crid2value(d, combine_areas)
    def _update(d, crid, sid):
        return d[crid][sid]
    t.updateColumn(norm_colname, t.apply(_update, (d, get(id_col), t.sample_name)), type_=float,
                   format_='%.2e')


def _build_crid2value(d, combine):
    for crid in d.keys():
        for sid in d[crid].keys():
            values=d[crid][sid]
            if combine=='average':
                weights=np.array(values)+1e-4  # to avoid zero weighting
                v=float(np.average(values, weights=weights))
            elif combine == 'sum':
                v=np.sum(values)
            
            else:
                assert False, '%s is not possible as value for function argument `combine`! '\
                'Allowed values: sum, average'
            d[crid][sid]=v
    
##################################################################################################
    

def select_data_for_ttest(t, group_col='crid',  sg_col='sample_group_id',
                          sample_col='sample_name', det_type_col='detection_type', 
                          ref='Con1', cv_col='cv_norm_crid_area', 
                          fc_col='log2_mean_fold_change', min_hits=4, max_cv=None, min_fc=None):
    groups=set(t.sample_group_id.values)-set([ref])
    pairs=[]
    print 'get groups ...'
    groups=t.splitBy(sg_col)
    ref_g=[g for g in groups if g.getColumn(sg_col).uniqueValue()==ref][0]
    groups=[g for g in groups if not g.getColumn(sg_col).uniqueValue()==ref]
    print 'done'
    for group in groups:
        sub=stackTables([ref_g, group])
        print 'data_reduction for strain %s:' %group.getColumn(sg_col).uniqueValue()
        print 'length' , '\t number of selected features'
        print len(sub), '\t\t%d' %len(set(sub.getColumn(group_col).values))
        sub=filter_values_via_min_hits(sub, min_hits, group_col, sg_col)
        _print_change(sub, 'minimal number of feature detections', min_hits, group_col)
        sub=filter_values_via_max_cv(sub, group_col, cv_col, max_cv)    
        _print_change(sub, 'maximal CV', max_cv, group_col)
        sub=filter_values_via_min_fc(sub, group_col, fc_col, min_fc)    
        _print_change(sub, 'minimal required absolute (log2)fold change', min_fc, group_col)
        sub.title=group.getColumn(sg_col).uniqueValue()
        if len(sub):
            pairs.append(sub)
        else:
            # ttest is not possible for empty talbes
            print 'No features for ttesting remained for %s' %group
    return pairs
        
def _print_change(t, param_name, param_value, group_col):
    length=len(t.getColumn(group_col).values)
    num=len(set(t.getColumn(group_col).values))
    if param_value is  None:
        print '%d\t\t%d\t No %s filtering executed' %(length, num, param_name)
    else:
        print '%d\t\t%d\t after %s (%.2f) filtering' %(length, num, param_name, param_value)


def filter_values_via_min_hits(t, min_hits, id_col='fid', group_col='sample_group_id'):
    if t.hasColumn('detection_type'):
        t.updateColumn('has_peak', (t.detection_type=='detected').thenElse(True, False), type_=bool)
    else:
        t.updateColumn('has_peak', True, type_=bool)
    
    d=defaultdict(int)
    expr=t.getColumn
    for fid, sg, is_in, __ in set(zip(expr(id_col), expr(group_col), t.has_peak, t.sample_name)):
        if is_in:
            d[(fid, sg)]+=1
    ids=set([])
    def _update_counts(fid, sg, d):
        return d[(fid, sg)]
    # we use the column for cv filtering
    for pair, counts in d.items():
        if counts>=min_hits:
            ids.add(pair[0])
    t.dropColumns('has_peak')
    return wtbox2.utils.fast_isIn_filter(t, id_col, ids)     
    

def _collect_value(t, group_col, sg_col, value_col):
    d=defaultdict(dict)
    exp=t.getColumn
    ntuples=zip(exp(group_col), exp(sg_col), exp(value_col))
    for crid, sgid, value in set(ntuples):
            d[crid][sgid]=value
    return d

    
def filter_values_via_max_cv(t, id_col, value_col, max_cv):
    # keap feature if cv value < max cv for at least one sample_group 
    # handle None
    if max_cv is None:
        return t
    get=t.getColumn
    t.updateColumn('min_cv', get(value_col).min.group_by(get(id_col)), type_=float)
    t=t.filter(t.min_cv<max_cv)
    t.dropColumns('min_cv')
    return t
    

def filter_values_via_min_fc(t, id_col, value_col, min_fc):
    # handle None
    if min_fc is None:
        return t
    
    get=t.getColumn
    t.updateColumn('max_fc', get(value_col).max.group_by(get(id_col)), type_=float)
    #since we determine log2 value the minimal we must log2 transfom min_fc
    t=t.filter(t.max_fc > np.log2(min_fc))
    t.dropColumns('max_fc')
    return t
    

def select_min_fc(t, d, gid_col, sg_col, min_fc):
    if min_fc is None:
        # no selection
        return t
    get=t.getColumn
    gids=set(get(gid_col).values)
    sgids=set(get(sg_col).values)
    selected=[]
    for gid in gids:
        if any([abs(d[gid][sgid])>=min_fc for sgid in sgids]):
            selected.append(gid)  
    return wtbox2.utils.fast_isIn_filter(t, gid_col, selected)



###################################################################################################
###################################################################################################

def select_data_for_zscore(t, id_col='crid', value_col='mean_log2_fc', threshold=2.0):
    get=t.getColumn
    t.updateColumn('_abs_log2_fc', get(value_col).apply(abs), type_=float)
    t.updateColumn('_max_per_group', t._abs_log2_fc.max.group_by(get(id_col)), type_=float)
    t1=t.filter(t._max_per_group>=threshold)
    t.dropColumns('_abs_log2_fc', '_max_per_group')
    t1.dropColumns('_abs_log2_fc', '_max_per_group')
    return t1
    
def _sort_by_max_group(t, id_col, value_col):
    get=t.getColumn
    t.updateColumn('_max_group', get(value_col).max.group_by(get(id_col)), type_=float)
    

def _update_zscore(t, id_col, value_col):
    exp=t.getColumn
    d=_get_gid2fc(t, id_col, value_col)
    id2pair=_get_gid2pair(d)
    t.updateColumn('z_score', t.apply( _calculate_z_score, (exp(id_col), exp(value_col), id2pair)), 
                   type_=float)


def _get_gid2fc(t, gid_col, value_col):
    d=defaultdict(list)    
    exp=t.getColumn
    ntuple=set(zip(exp(gid_col), exp(value_col)))
    for gid, v in ntuple:
        d[gid].append(v)
    return d

def _calculate_z_score(gid, x, d):
    mu, s=d[gid]
    return (x-mu)/s

def _get_gid2pair(d):
    gid2pair={}
    for gid in d.keys():
        mu=_calc(d, gid, np.mean)
        s=_calc(d, gid, np.std)
        gid2pair[gid]=(mu, s)
    return gid2pair

def _calc(d, gid, fun):
    return fun(d[gid])        
    
#def update_var(t, id_col='crid', value_col='norm_crid_area'):
#    d=defaultdict(list)
#    id2var={}
#    get=t.getColumn
#    for crid, value in set(zip(get(id_col), get(value_col))):
#        d[crid].append(value)
#    for crid, values in d.items():
#        id2var[crid]=float(np.var(values))
#    wtbox2.table_operations.update_column_by_dict(t, 'variance', id_col, id2var, type_=float, 
#                                                  in_place=True)


# function for z_score_filtering
def select_peaks_by_max_group(t, percentile, value_col, group_col):
    get=t.getColumn
    norm_colname=_get_norm_col_name(group_col)
    get=t.getColumn
    colname=_get_name('max', norm_colname)
    t.updateColumn(colname, get(norm_colname).max.group_by(get(group_col)), 
                   type_=float)
    values=list(set(get(colname).values))
    threshold=np.percentile(values, percentile)
    print threshold
    return t.filter(t.max_mean_norm_crid>=threshold)
##################################################
# utils

def _get_norm_col_name(group_col):
    return '_'.join(['norm', group_col, 'area'])
    

def _get_name(prefix, norm_col_name):
        return '_'.join([prefix, norm_col_name])