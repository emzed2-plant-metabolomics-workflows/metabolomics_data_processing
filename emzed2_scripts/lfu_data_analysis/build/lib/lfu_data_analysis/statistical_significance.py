# -*- coding: utf-8 -*-
"""
Created on Thu Aug 16 15:45:28 2018

@author: pkiefer
"""

from statsmodels.stats.multitest import multipletests
import numpy as np
import os
from sklearn.decomposition import PCA
import pylab

def correct_p_values(t, alpha=0.05, method='fdr_bh', value_col='p_value'):
    assert t.hasColumn(value_col), 'column %s is missing' %value_col
    pvals=t.p_value.values
    reject, corrected, alphacSidak, alphacBonf=multipletests(pvals, alpha, method)
    colname='_'.join(['p_corr', method])
    t.updateColumn(colname, corrected, type_=float, format_='%.2e', insertAfter='p_value')
    t.updateColumn('alpha_bonferroni', alphacBonf, type_=float, format_='%.2e')
    
    
def  logfold_normalization(t, value_col, norm_col):
    """ log2 normalize values by the mean of the reference data
    """
    data=np.array(t.getColumn(value_col).values)
    norm_values=np.array(t.getColumn(norm_col).values)
    return np.transpose(np.log2(data/norm_values[:,np.newaxis]))



def apply_pca(data, sids, title='PCA', path=None, colors=None):
    if not colors:
        colors=['red']
    pca = PCA(n_components=3)
    pca.fit(data)    
    x=pca.transform(data)
    vars_=pca.explained_variance_ratio_
    plot_pca(x, data, vars_, title=title, color_list=colors, symbols=sids, path=path)



def plot_pca(x, X, vars_, title='PCA', color_list=['blue'], symbols=None, path=None):
    fig = pylab.figure()
    ax = fig.add_subplot(1, 1, 1)
    colors=['black']*5
    for color in color_list:
        colors.extend([color]*5)
    for i, color in enumerate(colors):
        ax.scatter(x[i, 0], x[i, 1], c=color, s=80, lw=0.5)
        if symbols:
            ax.annotate(symbols[i], (x[i, 0], x[i, 1]))
            
    ax.grid(True)
    if title is None:
        ax.set_title("Dataset ({} samples)".format(X.shape[0]))
    else:
        ax.set_title(title + " ({} samples)".format(X.shape[0]))
    xlabel="1st component %.2f" %vars_[0]
    ylabel="2nd component %.2f" %vars_[1]
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if not path:
        pylab.show()
    else:
        filename='.'.join([title, 'png'])
        save_path=os.path.join(path, filename)
        pylab.savefig(save_path)
        pylab.close()

    
######
#


def estimate_p_value_threshold():
    pass


def bonferroni_corrected_p_values(t, p_value_col='p_value'):
    t.addColumn('bf_corrected_p_value', t.getColumn(p_value_col)/len(t), type_=float, format_='%.2e')

import emzed    
def store_pms(pms, save_dir=r'Z:\pkiefer\temp\bemaier\20181203_exB7_precise\mzML_C18_pos'):
    for pm in pms:
        filename=_build_file_name(pm.meta['source'])    
        path=os.path.join(save_dir, filename)
        emzed.io.storePeakMap(pm, path)



     
def _build_file_name(filename):
    name, ext=os.path.splitext(filename)
    start,end=name.split('Con')
    fields=end.split('_')
    if fields[0]=='5':
        fields[1]='R5'
    if fields[0]=='22':
        fields[1]='R1'
    fields.append('processed')
    blocks=[start, 'Con']
    blocks.extend(fields)
    name='_'.join(blocks)
    return ''.join([name, ext])
    