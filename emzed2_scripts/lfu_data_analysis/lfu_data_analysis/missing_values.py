# -*- coding: utf-8 -*-
"""
Created on Thu May 16 13:17:43 2019

@author: pkiefer
"""

import emzed
import wtbox2
import numpy as np
from collections import defaultdict, Counter
from random import gauss


def complete_missing_values(lib, min_specs=5, max_cv=0.3, mztol=0.03, rttol=2.0,
                            biomass_col='biomass'):
    t=prepare_extraction_table(lib, biomass_col)
    missing=build_missing_table(t, biomass_col)
    t.updateColumn('detection_type', 'detected', type_=str, insertAfter='area')
    add_missing_not_at_random(missing, 'fid', 'area', max_cv=max_cv, min_specs=min_specs)
    final = emzed.utils.stackTables([t, missing])
    update_norm_areas(final, biomass_col)
    update_mean_values(final)
    return filter_multiple_peaks(final, 2*mztol, rttol)

def prepare_extraction_table(lib, biomass_col):
    print 'preparing extraction table'
    lib=_reset_missing(lib)
    colnames=['crid', 'aid', 'fid', 'pid', 'ipid', 'sample_group_id', 'sample_name', 'lc_method',
          'polarity', 'mzmean', 'mz0mean', 'rtmean', 'z_fid', 'consensus_adducts',
          'possible_m0', 'mz', 'mzmin', 'mzmax', 'rt', 'rtmin', 'rtmax', 'peakmap',
          'area', 'method','rmse', 'params', 'baseline', 'tic_area', 'tic_method', 
          biomass_col, 'unit', 'norm_peak_area',]
    t=lib.extractColumns(*colnames)
    t= t.filter(t.mzmean.approxEqual(t.mz0mean, 1e-3))
    print 'reintegrating peaks ...'
    ncpus=wtbox2.utils.get_n_cpus(t, 10)
    t=emzed.utils.integrate(t, 'trapez', n_cpus=ncpus)
    print 'Done.'
    return check_for_multiple_entrances(t)    


def _reset_missing(t):
    if t.hasColumn('detection_type'):
        print 'resetting missing values ...'
        return t.filter(t.detection_type=='detected')
    return t

def build_missing_table(t,  biomass_col='biomass', unit_col='unit', norm_col='norm_peak_area'):
    common=['crid', 'aid', 'fid', 'pid', 'lc_method', 'polarity', 'mzmean', 'mz0mean', 'rtmean', 
            'z_fid', 'consensus_adducts', 'possible_m0', 'tic_area', 
            'tic_method', biomass_col, unit_col, 'norm_peak_area']
    bounds=['mzmin', 'mzmax',  'rtmin', 'rtmax']    
    fid2name2value=get_fid2common(t, common)
    update_fid2bounds(t, 'fid', bounds, fid2name2value)
    update_mz_rt(fid2name2value)
    update_integration_columns(fid2name2value)
    fid2missing=find_missing_peaks(t, 'fid', 'sample_group_id', 'sample_name')
    missing=t.buildEmptyClone()
    rows=build_rows(t, biomass_col, fid2missing, fid2name2value)
    missing.rows=rows
    ncpus=wtbox2.utils.get_n_cpus(t, 10)
    missing = emzed.utils.integrate(missing, 'trapez', n_cpus=ncpus)    
    update_peak_values(missing)
    return missing
  
  
    
    
def get_fid2common(t, common):
    """ fid, rtmins, rtmaxs by min/max(subgoup) and min/max(all_peaks) + correspondind stds, 
        window width rtmin - 2*std, rtmax + 2*std
        
    """
    expr=zip(*[t.getColumn(name) for name in common])
    d=defaultdict(dict)
    for fid, values in zip(t.fid,expr):
        for colname, value in zip(common, values):
            d[fid] [colname]=value
    return d


def update_fid2bounds(t, id_col, colnames, fid2name2value):
    exp=t.getColumn
    #1. collect values
    d=defaultdict(dict)
    ntuples= zip(*[exp(name) for name in colnames])
    for fid, values in zip(exp(id_col), ntuples):
        for name, value in zip(colnames, values):
            if d[fid].has_key(name):
                d[fid][name].add(value)
            else:
                d[fid][name]=set([value])
    # 2. calculate min max
    for fid in d.keys():
        for name in d[fid].keys():
            assert name.endswith('min') or name.endswith('max')
            value=min(d[fid][name]) if name.endswith('min') else max(d[fid][name])
            fid2name2value[fid][name]=value
    
            
def update_mz_rt(d):
    for fid in d.keys():
        d[fid]['mz']=d[fid]['mzmean']
        d[fid]['rt']=d[fid]['rtmean']
    
def update_integration_columns(d):
    for fid in d.keys():
        d[fid]['area']=None
        d[fid]['method']=None
        d[fid]['rmse']=None
        d[fid]['params']=None
        d[fid]['baseline']=None
        
        
def find_missing_peaks(t, id_col, group_col, sample_col):
    exp=t.getColumn
    ntuples=zip(exp(sample_col).values, t.lc_method, t.polarity)
    samples=set(ntuples)
    d=defaultdict(set)
    # since a library can contain several lc-ms method for the same sample name
    # we have to take into account lc-method and polarity to assign the correct peakmap
    fid2lcms=dict(zip(exp(id_col), zip(t.lc_method, t.polarity)))
    fid2missing=defaultdict(set)
    for fid, sample in set(zip(exp(id_col), ntuples)):
        d[fid].add(sample)
    for fid in d.keys():
        missing = samples-d[fid]
        missing=[v for v in missing if v[1:]==fid2lcms[fid]]
        fid2missing[fid].update(missing)
    return fid2missing


def build_rows(t, biomass_col, fid2missing, fid2name2value):
    rows=[]
    ipid=t.ipid.max()+1
    sample2col2value=_get_sample2col2value(t, biomass_col)
    sampletuple2pm=get_sampletuple2peakmap(t)
    for fid, samples_tuples in fid2missing.items():
        for sample, lc_met, pol in samples_tuples:
            row=[]
            for col in t.getColNames():
                if fid2name2value[fid].has_key(col):
                    row.append(fid2name2value[fid][col])
                elif sample2col2value[sample].has_key(col):
                    row.append(sample2col2value[sample][col])
                elif col=='peakmap':
                    ntuple=sample, lc_met, pol
                    row.append(sampletuple2pm[ntuple])
                elif col=='ipid':
                    row.append(ipid)
                    ipid+=1
                else:
                    assert False, 'lo hascht de nett klor gedenkt'
            rows.append(row)
    return rows


def _update_sample_values(t, row, col2idx, sample):
    pass

def _get_sample2col2value(t, biomass_col='biomass'):
    d=defaultdict(dict)
    sample2dw=_get_sample2dw(t, biomass_col=biomass_col)
    sample2grp=_get_sample2group(t)
    for sample in sample2dw.keys():
        d[sample]['biomass']=sample2dw[sample]
        d[sample]['sample_group_id']=sample2grp[sample]
        d[sample]['sample_name']=sample
    return d
        

def get_sampletuple2peakmap(t):
    keys=zip(t.sample_name, t.lc_method, t.polarity)
    return dict(zip(keys, t.peakmap))


def _get_sample2dw(t, sample_col='sample_name', biomass_col='biomass'):
    get=t.getColumn
    return dict(zip(get(sample_col), get(biomass_col)))
    
def _get_sample2group(t, sample_col='sample_name', group_col='sample_group_id'):
    exp=t.getColumn
    return dict(zip(exp(sample_col), exp(group_col)))



##############################################################

def add_missing_not_at_random(t, id_col, value_col, max_cv=0.03, min_specs=3):
    """ Values not detected since close to detection limit or not present
        min_detected, LOD peakmap -> baseline (values must be determined prior to blank 
        subtraction)
        uses cv value of specific feature
        
    """
    get=t.getColumn
    t.updateColumn('_num_specs', t.apply(_count_specs, (t.params,)), type_=int)
    t.updateColumn('detection_type', 
                   t.apply(_add_detection_types, (get(value_col),t._num_specs, min_specs), 
                           keep_nones=True), 
                   type_=str, insertAfter=value_col)
    sample2int=sample_name2int(t)
    exp=(t.area, t.detection_type, t.sample_name, t.rtmin, t.rtmax, sample2int, max_cv)
    t.updateColumn('area', t.apply(update_mnar, exp, keep_nones=True),type_=float, format_='%.2e')    
    t.dropColumns('_num_specs')


def _count_specs(params):
    ints=params[1]
    return len(*np.where(ints>0))

def _add_detection_types(area, no_specs, min_specs):
    condition= area and (no_specs>=min_specs)
    return 'MAR' if condition else 'NMAR'    

def sample_name2int(t):
    d={}
    pairs=set(zip(t.sample_name, t.peakmap))
    for samp, pm in pairs:
        base, noise=wtbox2.peakmap_operations.determine_spectral_baseline_and_noise(pm)
        d[samp]=base+10*noise
    return d
    

def update_mnar(area, detection_type, sample, rtmin, rtmax, d, cv):  
    if detection_type=='NMAR':
        inten=d[sample]
        rt=(rtmin+rtmax)/2
        mu=(rtmax-rtmin)/3
        # to obtain inten at max area:
#        inten=inten *np.sqrt(np.pi*2.0)*mu
        rts=np.arange(rtmin, rtmax, (rtmax-rtmin)/20)
        curve=np.array([wtbox2.fitting.gauss(x, mu, rt) for x  in rts])
        curve=inten/max(curve)*curve
        area=np.trapz(curve, rts)
        return get_random_value(area, cv*area)
    return area
        
    
def get_random_value(mu, sigma):
    return abs(gauss(mu, sigma))

##########

def update_peak_values(t):
    emzed.utils.recalculateMzPeaks(t)
    t.updateColumn('rt', t.apply(_get_rt, (t.rtmean, t.params)), type_=float)

def _get_rt(rtmean, params):
        rt=None
        if len (params):
            rts, ints=params
            try:
                ints=wtbox2.fitting.savitzky_golay(ints, 5,2)
            except:
                pass
            rt=max(zip(rts, ints), key=lambda v: v[1])[0]
        return rt if rt is not None else rtmean            


def update_mean_values(t):
    id_col='crid'
    t.updateColumn('mzmean', t.mz.mean.group_by(t.pid), type_=float)
    t.updateColumn('rtmean', t.rt.mean.group_by(t.getColumn(id_col)), type_=float)


def update_norm_areas(t, biomass_col):
    expr=t.area/t.tic_area/t.getColumn(biomass_col)
    t.updateColumn('norm_peak_area', expr, type_=float)


def filter_multiple_peaks(t, mztol=0.003, rttol=2.0):
    # 1. represent library peaks via mzmean0, rtmean0
    t.updateColumn('_mean_norm_area', t.norm_peak_area.mean.group_by(t.pid), type_=float)
    colnames=['fid', 'mz0mean', 'rtmean', '_mean_norm_area']
    t1=t.extractColumns(*colnames)
    t1=t1.uniqueRows()
    key2tol={'mz0mean':mztol, 'rtmean' : rttol}
    check=wtbox2.collect_and_compare.compare_tables(t1,t1, key2tol)
    check=check.filter(~(check.fid==check.fid__0))
    trash=_select_trash(check)
    t.dropColumns('_mean_norm_area')
    return wtbox2.utils.fast_isIn_filter(t, 'fid', trash, not_in=True)
    
def _select_trash(t):
    d=defaultdict(set)
    fids=set(t.fid.values)
    fids.update(t.fid__0.values)
    exp=zip(t.fid, t._mean_norm_area, t.fid__0, t._mean_norm_area__0)
    for fid, area, fid0, area0 in exp:
        d[fid].update(set([(fid, area), (fid0, area0)]))
    selected=set([])
    for values in d.values():
        select=max(values, key=lambda v: v[1])[0]
        selected.add(select)
    return fids-selected

    
#######################################################################
#  helper funs for debugging

def check_for_multiple_entrances(t):
    d=defaultdict(list)
    pair2pairs=defaultdict(list)
    for fid, ipid, sample, area in zip(t.fid, t.ipid, t.sample_name, t.area):
        d[fid].append(sample)
        pair2pairs[(fid, sample)]=(ipid, area)
    cases={}
    for fid, samples in d.items():
        if len(samples)>len(set(samples)):
            values=Counter(samples)
            cases[fid]=[key for key, value in values.items() if value>1]
    remove=[]
    for fid, samples in cases.items():
        pairs=[pair2pairs[(fid, sample)] for sample in samples]
        print pairs
        ipid_sel=max(pairs, key=lambda v: v[1])[0]
        [remove.append(p[0]) for p in pairs if p[0]!=ipid_sel]
    return wtbox2.utils.fast_isIn_filter(t, 'ipid', remove, not_in=True)
    
    
def reprocess_nmar_merged_lib(lib, min_specs=5, max_cv=0.2):
    # 1. separate detected values
    print 'extract detected ...'
    detected=lib.filter(lib.detection_type=='detected')
    print 'extract MAR and NMAR ...'
    missing=lib.filter(~(lib.detection_type=='detected'))
    blocks=missing.splitBy('lc_method', 'polarity')
    print 'replacing ...'
    for x in blocks:
        add_missing_not_at_random(x, 'fid', 'area', max_cv=max_cv, min_specs=min_specs)
        print '...'
    blocks.append(detected)
    print 'stacking ...'
    t=emzed.utils.stackTables(blocks)
    print 'updating ...'
    update_norm_areas(t)
    _edit_reprocessed_lib(t)
    print 'Done'
    return t

def _edit_reprocessed_lib(t):
    remove=['norm_crid_area', 'ref_fc', 'cv_fold_change', 'log2_fold_change', 'mean_log2_fc',
            'std_log2_fc', 'cv_log2_fc', 'z_score', 'fc_rel_to_mean_ref']
    t.dropColumns(*remove)