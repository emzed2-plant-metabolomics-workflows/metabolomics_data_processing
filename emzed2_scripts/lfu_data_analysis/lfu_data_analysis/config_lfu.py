# -*- coding: utf-8 -*-
"""
Created on Wed Nov 06 10:31:36 2019

@author: pkiefer
"""
from emzed import gui
from collections import defaultdict

def config_gui(config, process_key='complete'):
    if process_key == 'complete':
        complete_lib_gui(config, process_key)
    elif process_key == 'ident':
        ident_gui(config, process_key)
    elif process_key == 'mat':
        data_mat_gui(config, process_key)
    elif process_key == 'ps':
        pair_stats_gui(config, process_key)
    return config
    
#!!!!!!!!!!!
    # Baustelle: key2default makes config needless  which simplifies function _get_default 
    # and the parameter config has to be removed

def complete_lib_gui(config, process_key):
    default_config=get_default_config()[process_key]
    key2default=config[process_key] if config.has_key(process_key) else default_config
    config[process_key].update(key2default)
    key2help=_key2help()[process_key]
    key2choice=_get_key2choice()
    _update_sample_groups(key2choice, config.pop('plibs'))
    key2value2index, key2index2value=get_converting_dicts(key2choice)
    keys=['min_specs', 'max_rt_shift',  'mztol', 'rttol', 'min_hits', 'biomass_col',
          'ref_group',    'combine_areas', 'filename' ]
    dia=gui.DialogBuilder('complete peaks library parameter setup')
    params=dia.addInt('minimal number of specrtra per peak', 
                      default=_get_default(keys[0], key2default), 
                    help=key2help[keys[0]])\
        .addFloat('max retention time shift', default=_get_default(keys[1], key2default),
                   help=key2help[keys[1]])\
        .addFloat('mz tolerance', default=_get_default(keys[2], key2default), 
                    help=key2help[keys[2]])\
        .addFloat('rt tolerance', default=_get_default(keys[3], key2default), 
                    help=key2help[keys[3]])\
        .addInt('minimal number of feature occurence per sample group',
             default=_get_default(keys[4], key2default),  help=key2help[keys[4]])\
        .addChoice('biomass column name', key2choice[keys[5]], help=key2help[keys[5]],
               default=_get_default(keys[5], key2default, key2value2index))\
        .addChoice('select reference group', key2choice[keys[6]], help=key2help[keys[6]],
               default=_get_default(keys[6], key2default, key2value2index))\
        .addChoice('combine group peak area', key2choice[keys[7]], 
                   default=_get_default(keys[7], key2default, key2value2index))\
        .addString('save file name', default=_get_default(keys[8], key2default), 
                    help=key2help[keys[8]])\
        .show()
    pairs=(zip(keys, params))
    _update_keys(config, process_key, pairs, key2index2value)



def ident_gui(config, process_key):
    default_config=get_default_config()[process_key]
    key2default=config[process_key] if config.has_key(process_key) else default_config
    config[process_key].update(key2default)
    key2help=_key2help()[process_key]
    key2choice=_get_key2choice()
    key2value2index, key2index2value=get_converting_dicts(key2choice)
    keys=['mtol', 'data_base', 'filename']
    dia=gui.DialogBuilder('complete identification library parameter setup')
    params=dia.addFloat('mass tolerance', default=_get_default(keys[0], key2default), 
                    help=key2help[keys[0]])\
           .addChoice('select data base', key2choice[keys[1]], help=key2help[keys[1]],
               default=_get_default(keys[1], key2default, key2value2index))\
           .addString('save file name', default=_get_default(keys[2], key2default), 
                    help=key2help[keys[2]]).show()
    pairs=(zip(keys, params))
    _update_keys(config, process_key, pairs, key2index2value)


def data_mat_gui(config, process_key):
    default_config=get_default_config()[process_key]
    key2default=config[process_key] if config.has_key(process_key) else default_config
    config[process_key].update(key2default)
    key2help=_key2help()[process_key]
    key2choice=_get_key2choice()
    _update_dat_mat_cols(key2choice, config.pop('plib'))
    key2value2index, key2index2value=get_converting_dicts(key2choice)
    keys=['log_transform', 'row_col', 'column_col', 'value_col', 'filename']
    dia=gui.DialogBuilder('Define data matrix extraction parameters')
    params=dia.addBool('log transform values', default=_get_default(keys[0], key2default), 
                    help=key2help[keys[0]])\
         .addChoice('row colname', key2choice[keys[1]], help=key2help[keys[1]],
               default=_get_default(keys[1], key2default, key2value2index))\
         .addChoice('column colname', key2choice[keys[2]], help=key2help[keys[2]],
               default=_get_default(keys[2], key2default, key2value2index))\
         .addChoice('value colname', key2choice[keys[3]], help=key2help[keys[3]],
               default=_get_default(keys[3], key2default, key2value2index))\
         .addString('save file name', default=_get_default(keys[4], key2default), 
                    help=key2help[keys[4]]).show()
         
    pairs=(zip(keys, params))
    _update_keys(config, process_key, pairs, key2index2value)
                    

def pair_stats_gui(config, process_key):
    default_config=get_default_config()[process_key]
    key2default=config[process_key] if config.has_key(process_key) else default_config
    # since some parameters can't be modified by the users:
    config[process_key].update(key2default)
    key2help=_key2help()[process_key]
    key2choice=_get_key2choice()
    _update_sample_groups(key2choice, [config.pop('plib')] )
    key2value2index, key2index2value=get_converting_dicts(key2choice)
    keys=['log_transform', 'ref_group', 'min_hits', 'min_fc', 'max_cv','filename']
    dia=gui.DialogBuilder('complete data matrix parameter setup')
    params=dia.addBool('log transform values', default=_get_default(keys[0], key2default), 
                    help=key2help[keys[0]])\
        .addChoice('select reference group', key2choice[keys[1]], help=key2help[keys[1]],
               default=_get_default(keys[1], key2default, key2value2index))\
        .addInt('minimal number of feature occurence per sample group', 
                default=_get_converted_default(keys[2], key2default), help=key2help[keys[2]])\
        .addFloat('minimal required mean fold change',  help=key2help[keys[3]],
                default=_get_converted_default(keys[3], key2default))\
        .addFloat('maximal allowed CV',  help=key2help[keys[4]],
                default=_get_converted_default(keys[4], key2default))\
        .addString('save file name', default=_get_default(keys[5], key2default), 
                    help=key2help[keys[5]])\
    .show()
    pairs=(zip(keys, params))
    pairs=_convert_params(pairs)
    _update_keys(config, process_key, pairs, key2index2value)


def _get_converted_default(key, d):
    return _convert_none(d[key])


def _convert_params(pairs):
    d=dict(pairs)
    for key in ['min_hits', 'min_fc', 'max_cv']:
        d[key]=_convert_none(d[key], True)
    return d.items()


def _convert_none(value, reverse=False):
    """ required for DialogBuilder None handling
    """
    if value is None and not reverse:
        return -1
    if value == -1 and reverse:
        return None
    return value
    
#--------------------------------------------------
def _update_keys(config, process_key, pairs, key2index2value):
    for key, value in pairs:
        if key2index2value.has_key(key):
            config[process_key][key]=key2index2value[key][value]
        else:
            config[process_key][key]=value
            
            

def _get_key2choice():
    d={}
    d['id_col']=['pid', 'fid', 'aid', 'crid']
    d['data_base']=['pubchem', 'kegg', 'human_metabolone', 'biocyc', 'aracyc', 'other']
    d['ms']=['LTQ_Orbitrap', 'Q_exactive', 'other']
    d['lc_method']=['HILIC_BEH_NH3_metabolome', 'UPLC_C18_metabolome', 'nanoLC_TBA', 'other']
    d['label_by']=['pattern', 'source']
    d['sample_grouping']=['pattern', 'individualy', 'no_grouping']
    d['biomass_col']=['biomass', 'dry_weight', 'DW', 'cell_dry_weight', 'CDW']
    d['combine_areas']=['sum', 'average']
    return d




def _update_sample_groups(d, plibs):
    if len(plibs):
        groups=set([])
        for plib in plibs:
            groups.update(set(plib.sample_group_id.values))
        groups=sorted(list(groups))
        d['ref_group']=groups
    else:
        group=gui.DialogBuilder('enter reference group id')\
        .addString('reference sample group')\
        .show()
        d['sample_group_ids']=[group]
    
def _update_dat_mat_cols(d, t):
    pairs=pairs=zip(t.getColNames(), t.getColTypes())
    id_cols=[p[0] for p in pairs if p[1] in [int, str]]
    value_cols=[p[0] for p in pairs if p[1] == float]
    d['row_col']=id_cols
    d['column_col']=id_cols
    d['value_col']=value_cols
    


def _key2help():
    d=defaultdict(dict)
    d['complete']['min_specs']='peak pre-selection, the required number of spectra per EIC peak to be recognized.'
    d['complete']['mztol']='maximal allowed mass to charge tolerance for missing peak detection'
    d['complete']['rttol']='maximal allowed retention time tolerance for missing peak detection'
    d['complete']['min_hits']='the minimal number of detections of a feature within the same sample group.' \
        '\n remark: it is sufficient that the condition is fullfilled in at least 1 group'
    d['complete']['id_col']='hit counts identifier level pid, fid, aid, crid. By default we filter by fid'\
    ' since peaks from low abundant adducts are not selected.'
    d['complete']['group_col']='the column that groups samples belonging to the same group'
    d['complete']['group_id']='for each lc method calculate averaged value over all adducts and '\
        'polarities. The default value should only be changed for very special cases e.g. '\
        'evaluation of similarity in feature response (different adducts reveal similar results)'
    d['complete']['max_rt_shift']='maximal allowed retention time tolerance for '\
            'peak grouping (i.e. cross ref id)'
    d['complete']['ref_group']='the name of the reference group. It is required to calculate '\
    'fold changes relative to the reference sample group'
    d['complete']['biomass_col']='the column name containing biomass values. DO NOT CHANGE'\
    ' if your input libraries were built with label_free_peak_library_builder.'
    d['complete']['combine_areas']='downstream data analysis is based on values calculated from'\
    'a combined value of all group peaks. This can be the sum or peak value averaged of all areas.'\
    ' By default it is the sum. Averaging further increases the influence of major peaks.'
    d['complete']['filename']='filename of the peak library'
    d['ident']['mtol']='maximal allows mass tolerance for metabolite matching'
    d['ident']['data_base']='you can select provided data_base or load your individual data base '\
        'of types *.table and *.csv. Required column is m0 containing the exact monoisotopic mass'
    d['ident']['filename']='filename of the identification library'
    d['mat']['log_transform']='add log transformed values to data matrix'
    d['mat']['filename']='filename of the feature x sample data matrix'
    d['mat']['row_col']='table column containing row (X) identifier of data matrix. Values'\
                            ' are of types int or str'
    d['mat']['column_col']='table column containing Column (Y) identifier of data matrix. Values'\
                            ' are of types int or str'
    d['mat']['value_col']='table column containing values (float) assigned by row_col and'\
                    ' column_col. Assignment must be unique!'
    d['ps']['log_transform']='use log transformed values for PCA and ttest'
    d['ps']['ref_group']='the name of the reference group for pair-wise comparison'    
    d['ps']['id_col']='select peak grouüing level. The corresponding normalized area value must exist!'
    d['ps']['filename']='filename of the pair-wise t-test results'
    d['ps']['min_hits']='minimal number of detection of a feature per sample group pair, '\
        'i.e.: given each of the 2 groups contains 5 replicates: min_hits = 4 means the feature '\
        'was detected in 4 out of 5 replicates in at least 1 group.'\
        ' \nNote: Selection criteria are additiv! To remove filter set value to -1'
    d['ps']['max_cv']='Upper cv value limit of a feature that must be fullfilled for at least one '\
    'sample group.\nNote: Selection criteria are additiv! To remove filter set value to -1'
    d['ps']['min_fc']='log2 lower fold change  value limit of a feature that must be fullfilled for at least one '\
    'sample group.\nNote: Selection criteria are additiv! To remove filter set value to -1'
    return d



def get_default_config():
    d=defaultdict(dict)
    # merge libraries
    d['complete']['lc_col']='lc_method'
    d['complete']['max_rt_shift']=6.0
    d['complete']['label_col']='adduct_assigned_by'
    # missing values paramters
    d['complete']['crid']=True # missing values update without library merging: crid is missing
    d['complete']['min_specs']=5
    d['complete']['mztol']=0.003
    d['complete']['rttol']=3.0
    # select features
    d['complete']['min_hits']=5
    d['complete']['id_col']='fid'
    d['complete']['group_col']='sample_group_id'
    # biomass column
    d['complete']['biomass_col']='biomass'
    # preprocess library
    d['complete']['group_id']='crid'
    d['complete']['ref_group']=None
    d['complete']['value_col']='norm_peak_area'
    d['complete']['result_folder']='complete_library'
    d['complete']['filename']='peak_library'
    d['complete']['ext'] = '.table'
    d['complete']['combine_areas']='sum'
    
    # identification library
    d['ident']['mtol']=0.003
    d['ident']['data_base']=None
    d['ident']['ident_colname']='pubchem_matches'
    d['ident']['id_col_value']='norm_crid_area'
    d['ident']['filename']='identification_library'
    d['ident']['result_folder']='identification_library'
    d['ident']['ext'] = '.table'
    
    # build_data_matrices
    d['mat']['value_col']='norm_crid_area'
    d['mat']['log_transform']=True
    d['mat']['row_col']='crid'
    d['mat']['column_col']='sample_name'
    d['mat']['filename']='feature_x_sample_matrix'
    d['mat']['result_folder']='features_x_samples_matrix'
    d['mat']['ext'] = '.csv'
    # pairwise stats
    d['ps']['log_transform']=True
    d['ps']['column_col']='sample_name'
    d['ps']['group_col']='sample_group_id'
    d['ps']['id_col']='crid'
    d['ps']['ref_group']=None
    d['ps']['result_folder']='pairwise_stats'
    d['ps']['filename']='pairwise_ttests'
    d['ps']['min_fc']=None
    d['ps']['min_hits']=4
    d['ps']['max_cv']=None
    d['ps']['ext'] = '.tables'
    return d    

#---------------------------------------------------------------------    
def _get_default(key, key2default, key2value2index=None):
    default= key2default[key]
    return key2value2index[key].get(default) if key2value2index else default

def get_converting_dicts(key2choice):
    key2value2index=_build_key2value2index(key2choice)
    key2index2value=_build_key2value2index(key2choice, True)
    return key2value2index, key2index2value
    
def _build_key2value2index(key2choice, inverse=False):
    d={}
    for key, values in key2choice.items():
        i2value=_value2index(values)
        d[key]=_inverse_dict(i2value) if inverse else i2value
    return d    

def _value2index(values):
    return dict(zip(values, range(len(values))))


def _inverse_dict(d):
    values, keys=zip(*d.items())
    return dict(zip(keys, values))
    
#######################################################################
# for manial config gui testing
import emzed
def _get_test_config():
    d=defaultdict(dict)
    values=['a', 'b', 'c']
    t=emzed.utils.toTable('sample_group_id', values, type_=str)
    d['plibs']=[t]
    return d