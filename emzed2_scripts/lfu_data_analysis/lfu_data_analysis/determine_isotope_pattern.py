# -*- coding: utf-8 -*-
"""
Created on Tue Nov 05 16:59:16 2019

@author: pkiefer
"""

import numpy as np
from collections import defaultdict
from wtbox2.table_operations import update_column_by_dict as update


def determine_pattern(plib):
    d=collect_values(plib)
    fid2pattern=get_pattern_dict(d)
    update(plib, 'isotopologue_pattern', 'fid', fid2pattern, type_=tuple, in_place=True) 


def collect_values(t):
    d=dict()
    for fid, sid, mzmean, area in zip(t.fid, t.sample_name, t.mzmean, t.area):
        if not d.has_key(fid):
            d[fid]=defaultdict(list)
        d[fid][sid].append((mzmean, area))
    return d


def get_pattern_dict(d):
    fid2pattern=dict()
    for fid, sample2array in d.items():
        mzs, mat=_build_array(sample2array)
        if mzs is not None:
            mz2frac=_calculate_mid(mzs, mat)
            fid2pattern[fid]=mz2frac
    return fid2pattern
            
            
def _build_array(sample2array):
    # take only samples into account with at least 2 isotopolgues
    pairs=[p for p in sample2array.values() if len(p)>1]
    try:
        longest=len(max(pairs, key=lambda v: len(v)))
    except:
        return None, None
    
    mat=np.zeros((len(pairs), longest))
    for j, p in enumerate(pairs):
        p.sort(key=lambda v: v[0])
        mzs, areas=zip(*p)
        mat[j, :len(areas)]=areas
    return mzs, mat


def _calculate_mid(mzs, mat):
    atot=np.sum(mat, axis=1)
    ax0, ax1=np.shape(mat)
    normmat=np.transpose(np.vstack([atot]*ax1))
    pattern=np.round(mat/normmat, 3)
    return zip(mzs, np.average(pattern, axis=0, weights=mat).tolist())
    