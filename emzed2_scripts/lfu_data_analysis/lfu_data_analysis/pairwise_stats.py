# -*- coding: utf-8 -*-
"""
Created on Tue May 28 14:13:41 2019

@author: pkiefer
"""


from statsmodels.stats.multitest import multipletests
import numpy as np
import os
import re
import wtbox2
from sklearn.decomposition import PCA
from scipy.stats import ttest_ind
import pylab
from data_matrices import build_value_matrix_table
from selecting_library_peaks import select_data_for_ttest
from _utils import  _get_norm_colname
from emzed.core.data_types import Table


def perform_pairwise_analysis(t, id_col, column_col, group_col, ref_group, 
                             log_transform, min_hits, min_fc, max_cv,  result_folder, ident=None):
    value_col=_get_norm_colname(id_col)
    exp='_'.join(['log2', value_col]) if log_transform else value_col
    cv_col='_'.join(['cv', exp])
    pairs=select_data_for_ttest(t, group_col=id_col,  sg_col=group_col, sample_col=column_col, 
                          ref=ref_group, cv_col=cv_col, min_hits=min_hits, 
                          max_cv=max_cv, min_fc=min_fc)
    args=(id_col, column_col, value_col, log_transform, ref_group, group_col, result_folder, ident)
    pairs=wtbox2.utils.show_progress_bar(pairs, analyze_pair, args=args, in_place=False)
    return pairs


def analyze_pair(sub, id_col, column_col, value_col, log_transform, ref_group, group_col, save_path,
                 ident):
    data=build_value_matrix_table(sub, id_col, column_col, value_col, 
                                          log_transform= log_transform)
    _check_save_path(save_path)
    group_name=_get_group_name(sub, ref_group, group_col)
    apply_pca_on_table(data, id_col, (group_name, ref_group), save_path)                                
    update_ttest_results(sub, data, id_col, ref_group, group_name)
    return update_candidates(sub, ident, id_col)


def update_ttest_results(t, data, id_col, ref_group, group):
    update=wtbox2.table_operations.update_column_by_dict
    colname2crid2value=perform_ttest(data, id_col, ref_group, group)
    for colname in ['p_corr_fdr_bh', 'p_value', 'alpha_bonferroni']:
        update(t, colname, id_col, colname2crid2value[colname], type_=float, in_place=True)
        t.setColFormat(colname, '%.2e')

def _get_group_name(t, ref_group, group_col):
    names=set(t.getColumn(group_col).values)
    return (names-set([ref_group])).pop()
    
def _check_save_path(path):
    if not os.path.exists(path):
        os.mkdir(path)

def perform_ttest(data, id_col, ref_group, sample_group):
    sample_names=[n for n in data.getColNames() if re.search(sample_group, n)]
    ref_names=[n for n in data.getColNames() if re.search(ref_group, n)]
    ref_data=data.extractColumns(*ref_names)
    ref_data=np.array(ref_data.rows)
    sample_data=data.extractColumns(*sample_names)
    sample_data=np.array(sample_data.rows)
    # we assume equal variance for both data sets
    equal_var = True if len(sample_names)==len(ref_names) else False
    tstats=  ttest_ind(ref_data, sample_data, equal_var=equal_var, nan_policy='omit', axis=1)
    p_values=tstats.pvalue
    data.updateColumn('p_value', p_values, type_=float, format_='%.2e')
    correct_p_values(data)
    return _get_colname2id2value(data, id_col)

def update_candidates(t, mlib, id_col):
    if isinstance(mlib, Table):
        y=t.fastLeftJoin(mlib, id_col)
        _edit(y, mlib)
        return y
    return t
        
def _edit(t, mlib):
     common_cols=set(t.getColNames()).intersection(set(mlib.getColNames()))
     drop_cols=[''.join([name, '__0']) for name in common_cols]
     t.dropColumns(*drop_cols)
     t.removePostfixes()
     
         
        


##################################################################################################
def apply_pca_on_table(data, id_col, group_names, path):
    sids, ds=build_data_array(data)
    comps=['PCA']
    comps.extend(group_names)
    title='_'.join(comps)
    apply_pca(ds, sids, title, path)


def build_data_array(data):
    sids=[n for n in data.getColNames()[1:]]
    ds=data.extractColumns(*sids)
    ds=np.transpose(np.array(ds.rows))
    return sids, ds


def apply_pca(data, sids, title='PCA', path=None, colors=None):
    if not colors:
        colors=['red']
    pca = PCA(n_components=3)
    pca.fit(data)    
    x=pca.transform(data)
    vars_=pca.explained_variance_ratio_
    plot_pca(x, data, vars_, title=title, color_list=colors, symbols=sids, path=path)



def plot_pca(x, X, vars_, title='PCA', color_list=['blue'], symbols=None, path=None):
    fig = pylab.figure()
    ax = fig.add_subplot(1, 1, 1)
    colors=[]
    rows=np.shape(x)[0]/(len(color_list)+1)
    for color in color_list:
        colors.extend([color]*rows)
    colors.extend(['green']*rows)
    for i, color in enumerate(colors):
        ax.scatter(x[i, 0], x[i, 1], c=color, s=80, lw=0.5)
        if symbols:
            ax.annotate(symbols[i], (x[i, 0], x[i, 1]))
            
    ax.grid(True)
    if title is None:
        ax.set_title("Dataset ({} samples)".format(X.shape[0]))
    else:
        ax.set_title(title + " ({} samples)".format(X.shape[0]))
    xlabel="1st component %.2f" %vars_[0]
    ylabel="2nd component %.2f" %vars_[1]
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if not path:
        pylab.show()
    else:
        filename='.'.join([title, 'png'])
        save_path=os.path.join(path, filename)
        pylab.savefig(save_path)
        pylab.close()


def correct_p_values(t, alpha=0.05, method='fdr_bh', value_col='p_value'):
    assert t.hasColumn(value_col), 'column %s is missing' %value_col
    pvals=t.p_value.values
    reject, corrected, alphacSidak, alphacBonf=multipletests(pvals, alpha, method)
    colname='_'.join(['p_corr', method])
    t.updateColumn(colname, corrected, type_=float, format_='%.2e', insertAfter=value_col)
    t.updateColumn('alpha_bonferroni', alphacBonf, type_=float, format_='%.2e')    
    

def _get_colname2id2value(t, id_col='crid'):
    d={}
    get=t.getColumn
    colnames=t.getColNames()[-3:]
    for colname in colnames:
        d[colname]=dict(zip(get(id_col), get(colname)))
    return d

########################################################################################

from lfu_data_analysis.selecting_library_peaks import _update_zscore
import wtbox.default_plotting as plotting

def analyse_error_distibution(t, path, id_col='crid', value_col='log2_fold_change', 
                              sample_group_id='Con22'):
    t=t.filter(t.sample_group_id==sample_group_id)
    _update_zscore(t, id_col, value_col)
    t_data = build_value_matrix_table(t, row_col='crid', column_col='sample_name', value_col='z_score')
    plot_data_matrix(t_data, path)
    return t

def plot_data_matrix(t, path, title='z_score_Con22'):
    x_lables=t.getColNames()[1:]
    y_lables=t.crid.values
    mat=np.array(t.rows)
    data=mat[:,1:]
    norm_data=data/np.max(data)
    plotting.plot_heatmap(norm_data, x_lables,y_lables,colorbar=False, title=title, save_dir=path )
    

