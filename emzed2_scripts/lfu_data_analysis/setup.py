

######################################################################################
#
# YOU CAN / SHOULD EDIT THE FOLLOWING SETTING
#
######################################################################################

PKG_NAME = 'lfu_data_analysis'

VERSION = (0, 0, 14)

# list all required packages here:

REQUIRED_PACKAGES = []


### install package as emzed extension ? #############################################
#   -> package will appear in emzed.ext namespace after installation

IS_EXTENSION = True


### install package as emzed app ?  ##################################################
#   -> can be started as app.lfu_data_analysis()
#   set this variable to None if this is a pure extension and not an emzed app

APP_MAIN = "lfu_data_analysis.app:run"


### author information ###############################################################

AUTHOR = 'Patrick Kiefer'
AUTHOR_EMAIL = 'pkiefer@ethz.ch'
AUTHOR_URL = ''


### package descriptions #############################################################

DESCRIPTION = "data analysis of peak librarys created with label_free_peak_library_builder"
LONG_DESCRIPTION = """
Allow further data processing of peak librarys created with label_free_peak_library_builder
It comprises following processing steps:
1) merging peak libraries from different lc_ms measurements
2) extracting 2d data arrays saved as csv
3) building an identification library 
4) performing pairwise statistics

update 0.0.5: bug fix: for missing values
update 0.0.6: bug fix: for missing values (minor)
update 0.0.7: bug fix: extimating rttol, bug fix consensus_adducts
              additional column labeling crid assifnment consistency introduced  
update 0.0.8: bug fix: add_crid_evaluation col in case of missing cross_ref
update 0.0.9: bug fix: identification_library issue
update 0.0.10 bug fix: pca plot
update 0.0.12 bug fix: pair-wise ttest: sample grouping
update 0.0.14 bug fix: fold change is now calculated to mean ref for each feature
"""

LICENSE = "http://opensource.org/licenses/GPL-3.0"


######################################################################################
#                                                                                    #
# DO NOT TOUCH THE CODE BELOW UNLESS YOU KNOW WHAT YOU DO !!!!                       #
#                                                                                    #
#                                                                                    #
#       _.--""--._                                                                   #
#      /  _    _  \                                                                  #
#   _  ( (_\  /_) )  _                                                               #
#  { \._\   /\   /_./ }                                                              #
#  /_"=-.}______{.-="_\                                                              #
#   _  _.=('""')=._  _                                                               #
#  (_'"_.-"`~~`"-._"'_)                                                              #
#   {_"            "_}                                                               #
#                                                                                    #
######################################################################################


VERSION_STRING = "%s.%s.%s" % VERSION

ENTRY_POINTS = dict()
ENTRY_POINTS['emzed_package'] = [ "package = " + PKG_NAME, ]
if IS_EXTENSION:
    ENTRY_POINTS['emzed_package'].append("extension = " + PKG_NAME)
if APP_MAIN is not None:
    ENTRY_POINTS['emzed_package'].append("main = %s" % APP_MAIN)


if __name__ == "__main__":   # allows import setup.py for version checking

    from setuptools import setup
    setup(name=PKG_NAME,
        packages=[ PKG_NAME ],
        author=AUTHOR,
        author_email=AUTHOR_EMAIL,
        url=AUTHOR_URL,
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        license=LICENSE,
        version=VERSION_STRING,
        entry_points = ENTRY_POINTS,
        install_requires = REQUIRED_PACKAGES,
        )
   