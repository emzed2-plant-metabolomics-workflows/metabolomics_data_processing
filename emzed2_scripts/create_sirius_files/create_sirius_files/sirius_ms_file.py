# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 11:57:55 2019

@author: pkiefer
table columns
id_col, mz, mslevel, assigned_adduct, area

"""
from collections import defaultdict
import os
import emzed

def build_sirius_ms_file_from_table(ms1, ms2, id_col, directory):
    info2entry=_build_info2entry(ms1, ms2, id_col)
    write_ms_files(info2entry, directory)

    
def _build_info2entry(ms1, ms2, id_col):
    d=defaultdict(dict)
    expr=ms1.getColumn(id_col)
    if ms1:
        ms1.updateColumn('mz0', ms1.mz.min.group_by(expr), type_=float, insertAfter='mz')
        for id_, mz0, adducts in set(zip(ms1.getColumn(id_col), ms1.mz0, ms1.assigned_adduct)):
            for adduct in adducts:
                mz=float2str(mz0, 5)
                crid='='.join([id_col, str(id_)])
                d[id_, adduct]['>compound']='_'.join([crid, mz, adduct])
                d[id_, adduct]['>parentmass']=str(round(mz0,5))
                d[id_, adduct]['>ion']=adduct
        _add_ms1_peaks(ms1, id_col, 'mid', d, '>ms1')
    elif ms2 and not ms1:
        expr=set(zip(ms2.getColumn(id_col), ms2.precursor, ms2.assigned_adduct))
        for id_, prec, adduct in expr:
            for adduct in adducts:
                mz=float2str(prec, 5)
                crid='='.join([id_col, str(id_)])
                d[id_, adduct]['>compound']='_'.join([crid, mz])
                d[id_, adduct]['>parentmass']=mz
                d[id_, adduct]['>ion']=adduct
    if ms2:
        _add_peaks(ms2, id_col, 'area', d, '>ms2') 
    return d

def _add_ms1_peaks(t, id_col, value_col, d, key):
    get=t.getColumn
    expr=zip(get(id_col), t.mz, get(value_col), t.assigned_adduct)
    for id_, mz, value , adducts in expr:
        for adduct in adducts:
            d[id_, adduct][key]=_get_pattern(value)

        
def _get_pattern(tuples):
    pattern=''
    for mz, frac in tuples:
        line=' '.join([float2str(mz, 5), float2str(frac, 2)])
        pattern='\n'.join([pattern, line]) if len(pattern) else line
    return pattern


def _add_peaks(t, id_col, value_col, d, key):
    pattern=defaultdict(list)
    get=t.getColumn
    expr=zip(get(id_col), t.mz, get(value_col), t.assigned_adduct)
    for id_, mz, value, adducts in expr:
        for adduct in adducts:
            pattern[id_, adduct].append(' '.join([float2str(mz, 5), float2str(value, 2)]))
    for key_ in d.keys():
        if pattern.has_key(key_):
            _pattern='\n'.join(pattern[key_])
            d[key_][key]=_pattern    
    
    

def write_ms_files(d, directory):
    for id_ in d.keys():
        write_ms_file(d[id_], directory)
    

            
def write_ms_file(d, directory):
    path=_make_path(d, directory)
    text=' '.join(['>compound', d['>compound']])
    entries=['>ion', '>parentmass', '>ms1', '>ms2']
    for entry in entries:
        if d.has_key(entry):
            expr=entry in ['>ms1', '>ms2']
            sep='\n' if expr else ' '
            line=sep.join([entry, d[entry]])
            block=[text,'', line] if expr else [text, line]
            text='\n'.join(block)
    with open(path, 'w') as fp:
        fp.write(text)
    print 'Done'   

def _make_path(d, directory):
    # since ? is not allowed in file name we replace ? of [M+?] ion by X
    name_info=d['>compound'].replace('?', 'X')
    filename=''.join([name_info, '.ms'])
    return os.path.join(directory, filename)
#-------------------------------------------------------------------------------
def float2str(f, digits=5):
    return str(round(f, digits))
    

#def build_sirius_ms_file_from_mgf_file(mgf_path, id_col, id_, directory, t_ms1=None):
#    info2entry=_get_values(mgf_path, id_col, t_ms1)
#
#def _get_values(mgf_path, id_col, id_, ms1):
#    d=defaultdict(dict)
#    
#    if ms1:
#        expr=ms1.getColumn(id_col)    
#        sub=ms1.filter(expr)==id_
#        if len(sub):
#            
#        
#        




def build_test_table():
    mzs=[455.08334, 456.08702, 457.07879, 
         196.04599, 196.0460358, 132.0478058, 358.0985413, 132.0478363, 358.0987244, 87.02685547,
         87.0268631, 128.052887, 130.0322418, 128.052948, 130.0321503, 276.002594, 129.0606842,
         276.0026245, 129.0608368, 98.06044769, 129.0369263, 89.04238892, 61.01142883]
    precursors=[None]*3
    precursors.extend([455.08334]*20)
    mslevel=[1]*3
    mslevel2=[2]*20
    mslevel.extend(mslevel2)
    areas=[79.646, 9.735, 10.619, 100, 75.35663814, 35.73104063, 30.30047593, 26.85362728, 
           22.47777713, 15.79217305, 11.8152285, 10.40699805, 7.746243179, 7.717167, 5.657216484, 
           5.494375477, 4.531469423,3.890585698,3.376479281, 3.330622875, 2.932967753, 2.930694564, 
          2.842106553]
    t=emzed.utils.toTable('mz', mzs, type_=float)
    t.updateColumn('ms_level', mslevel, type_=int)
    t.updateColumn('area', areas, type_=float)
    t.updateColumn('assigned_adduct', '[M+H3N+H]+', type_=str)
    t.updateColumn('precursor', precursors, type_=float, format_='%.5f')
    t.addColumn('crid', 0, type_=int, insertBefore='mz')
    return t
    
    