from main_build_mgf_ms_files_from_ms_prm import main_create_mgf_and_ms_files
from main_build_mgf_ms_files_from_ms_prm import get_arguments
from main_build_mgf_ms_files_from_ms_prm import show_info

def run():
    show_info()
    args= get_arguments()
    main_create_mgf_and_ms_files(*args)
    