# -*- coding: utf-8 -*-
"""
Created on Tue Oct 01 09:04:03 2019

@author: pkiefer

"""
#import emzed
import os
from collections import defaultdict

def build_mgf_files(ms1, ms2, destination, id_col='crid', top_n=50):
    id2info=_build_info2entry(ms1, ms2, id_col)
    write_mgf_files(id2info, destination)


def _build_info2entry(ms1, ms2, id_col):
    d=defaultdict(dict)
    expr=ms1.getColumn(id_col)
    if ms1:
        ms1.updateColumn('mz0', ms1.mz.min.group_by(expr), type_=float, insertAfter='mz')
        for id_, mz0, rt , adducts in set(zip(ms1.getColumn(id_col), ms1.mz0, ms1.rt, ms1.assigned_adduct)):
            for adduct in adducts:
                mz=float2str(mz0, 5)
                crid='='.join([id_col, str(id_)])
                d[id_, adduct]['TITLE']='_'.join([crid, mz, adduct])
                d[id_, adduct]['PEPMASS']=str(round(mz0,5))
                d[id_, adduct]['RTINSECONDS']=rt
    if ms2:
        _add_peaks(ms2, id_col, 'area', d, 'peaks') 
    return d



def float2str(f, digits=5):
    return str(round(f, digits))
    
    
def _add_peaks(t, id_col, value_col, d, key):
    pattern=defaultdict(list)
    get=t.getColumn
    expr=zip(get(id_col), t.mz, get(value_col), t.assigned_adduct)
    for id_, mz, value, adducts in expr:
        for adduct in adducts:
            pattern[id_, adduct].append(' '.join([float2str(mz, 5), float2str(value, 2)]))
    for key_ in d.keys():
        if pattern.has_key(key_):
            _pattern='\n'.join(pattern[key_])
            d[key_][key]=_pattern    


 
def write_mgf_files(id2info, destination):
    for key in id2info.keys():
        _write_mgf(id2info[key], destination)

def _write_mgf(d, dir_):
    ext='.mgf'
    title=d['TITLE'].replace('?', 'X')
    filename=''.join([title, ext])
    path=os.path.join(dir_, filename)
    
    with open(path, 'w') as df:
        df.write('BEGIN IONS\n')
        df.write(_writeline(d, 'TITLE'))
        df.write(_writeline(d, 'RTINSECONDS'))
        df.write(_writeline(d, 'PEPMASS'))
        if d.has_key('peaks'):
            df.write(d['peaks'])
        df.write('\nEND IONS')
    
def _writeline(d, key):
    line='='.join([key, str(d[key])])
    return line+'\n'

def write_peaks(peaks):
    return '\n'.join(peaks)+'\n'
    





