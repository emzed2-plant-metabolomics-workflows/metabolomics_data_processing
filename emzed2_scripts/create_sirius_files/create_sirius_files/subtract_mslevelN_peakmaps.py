# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 10:52:29 2019

@author: pkiefer
"""
from basic_processing.subtract_blank_from_peakmap import subtract_blank
from emzed.core.data_types import PeakMap
import numpy as np
from collections import defaultdict


def subtract_blanks_ms_levelN(samples, blanks, mztol, rttol, scaling=3.0, precursor_tol=0.05):
    subtract=subtract_blank_mslevelN
    params=[mztol, rttol, scaling, precursor_tol]
    return [subtract(pair, *params) for pair in zip(samples, blanks)]


def subtract_blank_mslevelN(pair, mztol, rttol, scaling=3.0, precursor_tol=0.05):
    # separate ms_levels
    print 'subtracting mslevel 1 blank'
    sample, blank=pair
    s1=sample.getDominatingPeakmap()
    b1=blank.getDominatingPeakmap()
    s_corr=[subtract_blank(s1, b1, mztol, rttol, scaling)]
    print 'subtracting mslevel 2 blanks'
    s2_tuples=sample.splitLevelN(2)    
    b2_tuples=blank.splitLevelN(2)    
    for s2, b2 in _map_ms2_pms(s2_tuples, b2_tuples, precursor_tol):
        s_corr.append(subtract_blank(s2, b2, mztol, rttol, scaling))
    pm_corr = _merge_pms(s_corr)
    pm_corr.meta['source']=sample.meta['source']
    pm_corr.meta['full_source']=sample.meta['full_source']
    return pm_corr


def _map_ms2_pms(pairs1, pairs2, tol):
    s_precs, s_pms=zip(*pairs1)
    b_precs, b_pms=zip(*pairs2)
    i1_to_i2=_map_indices(s_precs, b_precs, tol)
    return _map_pm_pairs(i1_to_i2, s_pms, b_pms)
    
    
def _map_indices(values1, values2, tol):
    i1toi2=defaultdict(list)
    for i, v1 in enumerate(values1):
        deltas = np.abs(np.array(values2)-v1)
        if min(deltas)<tol:
            selected=np.where(deltas==min(deltas))[0]
            i1toi2[i].extend(selected)  
    return i1toi2

def _map_pm_pairs(i1_to_i2, s_pms, b_pms):
    pm_pairs=[]
    for i in sorted(i1_to_i2.keys()):
        indices = i1_to_i2[i]
        if len(indices)==0:
            pm_pairs.append((s_pms[i], None))
        elif len(indices)==1:
            j=indices[0]
            pm_pairs.append((s_pms[i], b_pms[j]))
        else:
            b_pm=_get_best_match(s_pms[i], b_pms)
            pm_pairs.append(s_pms[i], b_pm)
    return pm_pairs

def _get_best_match(pm, pms):
    sums=[]
    ref_mat=np.array(pm.rtRange(), pm.mzRange())
    for blank in pms:
        mat=np.array(blank.rtRange(), blank.mzRange())
        value=np.sum(np.abs(ref_mat-mat))
        sums.append(value)
    sums=np.array(sums)
    indices=np.where(sums==min(sums)[0])
    assert len(indices)==1
    i=indices[0]
    return pms[i]
    
        
    
        
        



def _merge_pms(pms):
    # case only 1 peakmap
    if len(pms)==1:
       return pms[0]
    spectras=list(pms[0].spectra)
    for pm in pms[1:]:
        spectras.extend(list(pm.spectra))
    return PeakMap(spectras)
        
    