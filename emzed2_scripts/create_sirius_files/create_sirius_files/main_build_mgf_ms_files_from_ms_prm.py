# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 13:07:25 2019

@author: pkiefer
"""

from wtbox2 import in_out, utils, multiprocess
from subtract_mslevelN_peakmaps import subtract_blank_mslevelN
from label_free_peak_library_builder._match_samples_and_blanks import get_sample_specific_blanks
from extract_ms_level_n_patterns import extract_ms_level_n_tables
from emzed import gui,io
from sirius_ms_file import build_sirius_ms_file_from_table
from build_mgf_files import build_mgf_files


def main_create_mgf_and_ms_files(samples, config, blanks=None):
    """ creates .ms and .mgf files from peakmaps ms prm measurements. 
    
    
        
        - samples: Type: list of PeakMaps
                   Description: list of samples containing MS level1 and PRM MS level2 data.
        - blanks: optional
                  Type: list of PeakMaps  
                   Decfiption: list of blank peakmaps containing MS level1 and PRM MS level2 data.
                   Blank measurements are used for optional blank subtraction. Note, list of 
                   blanks and samples must have the same length. Default value is None 
                   (subtraction omitted).
        - config: Type: dict
                    for datail see `get_main_function_argurments`
        Returns: None
        
        Run ``get_argurments`` to obtain and configure arguments.
        Function main_create_mgf_and_ms_files(samples, blanks=None, **config) allows creating
        SIRUS .ms files and .mgf files.  It support ms level2 peak detection using 
        runMetaboFeatureFinder for parent ions. parent ions are defined by 
        a table defining candidates by crid and mz values and by the corresponding peak_library 
        providing peak mt-rtwindows. All extracted peaks will be visualized and fragment ion peaks
        can be selected manually (you can overlay EIC peaks ans check for coelution). Curated 
        MS, MS2 tables will be converted into .ms and .mgf files. 
        
    """
    run_function=utils.run_function
    if blanks is not None:
        samples=run_function(multiprocess_blank_subtraction, (samples, blanks), config)
    ms1, ms2=run_function(extract_ms_level_n_tables, (samples,), config)
    run_function(build_sirius_ms_file_from_table, (ms1, ms2), config, True)
    run_function(build_mgf_files, (ms1, ms2), config, True)
    

def get_arguments():
    """ Provides parameters of function ``main_create_mgf_and_ms_files`` optimized for current study.
    
    Returns tuple samples, blanks, config. 
    Parameters:
        -
    Function provides.
    1) A GUI to select and load sample files
    2) A  GUI to select and load blank files if ``subtract_blanks`` is True
    3) A default configuration with parameters for runFeatureFinderMetabo, rt_alignment
    4) A Gui to select a peak_library as emzed Table.  
    5) A gui to select a candidates Table with required columns ``id_col`` and mz of the parent
       ion. peak_libtray and candidates must have the same id_col. Default id_col is `crid`.
    """
    blanks = None
    config=default_config()
    samples=load_samples()
    if config['subtract_blanks']:
        blanks = load_blanks(samples, config)
    load_candidates(config)
    load_peak_library(config)
    return samples, config, blanks


def multiprocess_blank_subtraction(samples, blanks, mztol, rttol, scaling=3.0, precursor_tol=0.05):
    fun=subtract_blank_mslevelN
    args=[mztol, rttol, scaling, precursor_tol]
    n_cpus=len(samples) if len(samples)<=10 else 10
    pairs=zip(samples, blanks)
    print 'subtracting blanks ...'
    return multiprocess.main_parallel(fun, pairs, args, cpus=n_cpus)
    
    
def default_config():
    config={}
    config['destination']=gui.askForDirectory(caption='please select saving folder')
    config['subtract_blanks']=gui.askYesNo('Do you want to subtract blanks?')
    config['directory']=config['destination']
    config['kwargs']={'n':30, 'precursor_tol':0.01, 'mztol':0.005, 'isolation_width':1.0, 
                    'id_col': 'crid'}
    config['rtalign']=True
    config['mztol']=0.003
    config['max_rt_diff']=60.0
    config['id_col']='crid'
    config['top_n']=20
    config['rttol']=2.0
    return config
                    

def load_samples():
    pathes=gui.askForMultipleFiles(extensions=['mzML', 'mzXML'], caption='select samples ...')
    print 'loading samples ...'
    samples= in_out.load_peakmaps(pathes, exclude_blanks=True)
    print 'Done'
    return samples
    

def load_blanks(samples, config):
    print 'loading blanks ...'
    blanks =get_sample_specific_blanks(samples, config)
    print 'done'    
    return blanks    


def load_peak_library(config):
    path=gui.askForSingleFile(caption='please select_peak library', extensions=['table', 'tables'])
    print 'loading peak library' 
    print 'this might take several minutes ...'
    config['plib']=io.loadTable(path)
    print 'done'


def load_candidates(config):
    """
    a table or csv file containing with required colums crid and mz
    """
    path=gui.askForSingleFile(caption='please candidates table', extensions=['table', 'csv'])
    print 'loading table with canidate ids...' 
    load = io.loadTable if path.endswith('.table') else io.loadCSV
    config['candidates']=load(path)
    print 'done'



####################################################################################
def show_info():
    text = """ This application was made and configured for the study `"A general non-self response 
    as part of plant immunity" by Maier et al. It creates Sirius .ms files and  MASCOT .mgs files 
    from MS-PRM experiments. The tool does:
    1)	Targeted extraction of parent ions using peak_library of the study created with 
    label_free_peak_library_builder workflow including all samples. 
    Peaks are selected by cross reference id crid and the m/z value of the selected parent ion. 
    2)	MS level2 Peak detection on PRM Data using emzed2 integrated featureFinderMetabo from OpenMS.  
    3)	Co-visualization of parent and fragment ions including MS level one peaks  overlapping with
    parent ion isolation window enhancing manual curation.
    4)	 Creating .ms and .mgf files in destination.
   For feature detection, all parameters were optimized for the LC-MS method and instrument 
   configuration used for the study. To use different parameters you have to execute directly 
   function `` main_create_mgf_and_ms_files`` from module ``main_build_ms_mgf_ms_files_from_ms_prm 
   and adapt parameters, accordingly. 
   """
    gui.showInformation(text)
