# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 14:54:23 2019

@author: pkiefer
"""


def translate_adducts(t, adducts_col='consensus_adducts', charge_col='z_fid'):
    d=emzed2sirius_adduct()
    expr=t.getColumn(adducts_col), t.polarity, t.getColumn(charge_col), d
    t.updateColumn('adducts_sirius', t.apply(_translate_adducts, expr), type_=tuple,
                   insertAfter=adducts_col)


def _translate_adducts(adducts, polarity, z, key2value):
    if z==0:
        return ('[M+?]+',) if polarity=='+' else ('[M+?]-',)
    if z==1:
        return tuple([key2value.get(adduct) for adduct in adducts])



def emzed2sirius_adduct():
    d={}
    d['M+H'] = '[M+H]+'
    d['M+NH4'] = '[M+H3N+H]+'
    d['M+Na'] = '[M+Na]+'
    d['M+H-H2O'] = '[M-H2O+H]+'
    d['M+H-2H2O'] = 'M-2H2O+H]'
    d['M+K'] = '[M+K]+'
    d['M+ACN+H'] = '[M+C2H3N+H]+'
    d['M+ACN+Na'] = '[M+C2H3N+K]+'
    d['M+2Na-H'] = '[M-H+Na+Na]+'
    d['M+CH3OH+H'] = '[M+CH4O+H]+'
    d['M-H'] = '[M-H]-'
    d['M-H2O-H'] = '[M-H2O-H]-'
    d['M+Na-2H'] = '[M-H+Na-H]-'
    d['M+Cl'] = '[M+Cl]-'
    d['M+K-2H'] = '[M-H+K-H]-'
    d['M+FA'] = '[M-CH2O2-H]-'
    d['M+CH3COO'] ='[M+C2H4O2-H]-'
    return d
    