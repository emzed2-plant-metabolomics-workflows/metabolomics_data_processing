# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 15:54:19 2019

@author: pkiefer
"""
import emzed
import numpy as np
from wtbox2 import utils, table_operations, gui
from collections import defaultdict
Table=emzed.core.data_types.Table




def extract_isotopologues(t, id_col='crid', mztol=0.003):
    expr=(t.mz, t.z, t.rt, t.rtmin, t.rtmax, mztol, t.peakmap, id_col, t.getColumn(id_col))
    t.updateColumn('t_isotopologue', t.apply(_extract_isotopologues, expr), type_=Table)
    _check_and_update_isotopologues(t, id_col)
    return t



def _extract_isotopologues(mz, z, rt, rtmin, rtmax, mztol, pm, id_col, id_):
#    import pdb; pdb.set_trace()
    spec=_get_spec_at_rt(pm, rt)
    pt=peaks_to_table(spec, id_col, id_)
    selected=_find_isotopologues(mz, pt, z)
    return _build_peakstable(selected, rt, rtmin, rtmax, mztol, pm)
    


def _get_spec_at_rt(pm, rt):
    pm=pm.getDominatingPeakmap()
    specs=pm.spectra
    delta=[abs(spec.rt-rt) for spec in specs]
    return min(zip(specs, delta), key=lambda v: v[1])[0]


def peaks_to_table(spec, id_col, id_):
    peaks=spec.peaks
    mzs, ints=zip(*peaks)
    t=emzed.utils.toTable('mz', mzs, type_=float)
    t.addColumn('intensities', ints, type_=float)
    t.addEnumeration()
    t.updateColumn(id_col, id_, type_=int, format_='%d', insertAfter='id')
    return t



def _find_isotopologues(mz, pt, z, min_rel_int=0.01):
    i, _, _, mz0_int=_get_mz0_intensity(pt, mz)
    if mz0_int:
        z=1 if not z else z
        pt=pt.filter(pt.intensities>=mz0_int*min_rel_int)
        row2id=dict(zip(range(len(pt)),pt.id.values))
        lookup=pt.buildLookup('mz', 0.07, None)
        mzs = np.array([1.0, 2.0])/z+mz
        ids=[i]
        for mz_ in mzs:
            ids.extend([row2id[v] for v in lookup.find(mz_)])
        return utils.fast_isIn_filter(pt, 'id', ids)
    return pt.buildEmptyClone()


    
def _get_mz0_intensity(pt, mz):
    selected=pt.filter((pt.mz-mz).apply(abs)<=0.003)
    if len(selected):
        return max(selected.rows, key=lambda v: v[-1])
    return None, None, None, None
 
   
def _build_peakstable(t, rt, rtmin, rtmax, mztol, pm):
    t.addColumn('rt', rt, type_=float, insertAfter='mz')
    t.addColumn('rtmax', rtmax, type_=float, insertAfter='rt')
    t.addColumn('rtmin', rtmin, type_=float, insertAfter='rt')
    t.addColumn('mzmax', t.mz+mztol, type_=float, insertAfter='mz')
    t.addColumn('mzmin', t.mz-mztol, type_=float, insertAfter='mz')
    t.addColumn('peakmap', pm, type_=emzed.core.data_types.PeakMap)
    return emzed.utils.integrate(t, 'trapez')


def _check_and_update_isotopologues(t, id_col):
    emzed.gui.showInformation('please check extracted isotopolgue peaks. You can'\
    ' remove a peak by deleting the corresponding row')
    tables=t.t_isotopologue.values
    gui.inspect(tables)
    _check_isotopologues(tables, id_col)
    t.replaceColumn('t_isotopologue', tables, type_=Table)
    t.updateColumn('mid', t.apply(_update_mids, (t.t_isotopologue, id_col)),
                   type_=tuple, format_='%r')


def _check_isotopologues(tables, id_col):
    def _calc_mi(mz, mz0):
            return int(round(mz-mz0, 1))
    for t in tables:
        t.updateColumn('mi', t.apply(_calc_mi, (t.mz, t.mz.min.group_by(t.getColumn(id_col)))), 
                       type_=int)
        while len(t.mi.values)!=len(set(t.mi.values)):
            emzed.gui.showInformation('for at least one isotopologue more than 1 peak exists!'\
            '\n please remove the wrong peak(s)')
            emzed.gui.inspect(t)
        t.dropColumns('mi')
        

def _update_mids(t, id_col):
    t.updateColumn('area_sum', t.area.sum.group_by(t.getColumn(id_col)), type_=float)
    t.updateColumn('mid', t.area/t.area_sum*100.0, type_=float)
    t.sortBy('mz')
    emzed.utils.recalculateMzPeaks(t)
    return  zip(t.mz, t.mid)
    

######################################################################################
def mean_isotope_pattern(t, id_col='crid'):
    d=dict()
    
    for sub in t.splitBy(id_col):
        pattern=_mean_pattern(sub)
        id_=sub.getColumn(id_col).uniqueValue()
        d[id_]=pattern
    t.updateColumn('max_area', t.area.max.group_by(t.getColumn(id_col)), type_=float)
    averaged=t.filter(t.area==t.max_area)
    table_operations.update_column_by_dict(averaged, 'mid', id_col, d, in_place=True, type_=tuple)
    averaged.dropColumns('area')
    t.dropColumns('max_area')
    return averaged


def _mean_pattern(t):
#    import pdb; pdb.set_trace()
    col2mz=defaultdict(list)
    col2values=defaultdict(list)
    patterns=t.mid.values
    rows=len(patterns)
#    imat=np.zeros((rows, cols))
    for pattern in patterns:
        convert_pattern(pattern, col2mz, col2values)
    row2mz={i: float(np.mean(col2mz[i])) for i in col2mz.keys()}
    cols=max(col2values.keys())+1
#    rows=max(row2mz.keys())+1
    imat=np.zeros((rows, cols))
    for j, values in col2values.items():
        for i,v in enumerate(values):
            imat[i,j]=v
    if len(imat):
        pattern=np.sum(imat, axis=0) / np.sum(imat)*100.0
        return tuple([(row2mz[i], float(pattern[i])) for i in range(len(pattern))])


def convert_pattern(pattern, index2mz, index2fraction):
    if len(pattern):
        mzmin=min(pattern, key=lambda v: v[0])[0]
        for mz, value in pattern:
            index=int(round(mz-mzmin,1)) # since z is always 1 !!
            index2mz[index].append(mz)
            index2fraction[index].append(value)
    
        
    
def test_mean_pattern():
    t=emzed.utils.toTable('crid', [0], type_=int)
    t.updateColumn('mz', None, type_=float)
    t.updateColumn('area', None, type_=float)
    return t.buildEmptyClone()
    
    
    
def test_table():
    mids=[[(350.0,0.75), (351.003, 0.25)], [(350.0,0.75), (352.004, 0.25)]]
    t=emzed.utils.toTable('mz', [350.0, 350.0], type_=float)
    t.updateColumn('area', [750.0, 276.0], type_=float)
    t.updateColumn('mid', mids, type_=list)
    t.updateColumn('crid', 0, type_=int)
    return t
    
    
    
    