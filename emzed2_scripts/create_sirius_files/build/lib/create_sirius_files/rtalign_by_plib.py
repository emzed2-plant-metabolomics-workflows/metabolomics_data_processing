# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 13:46:06 2019

@author: pkiefer
"""

import emzed
import os
import pyopenms
from emzed.core.data_types import PeakMap
from basic_processing._rt_align import rtAlign
from copy import deepcopy

def rt_align_pms_by_peak_lib(pms, plib=None, ref=None, destination=None, max_rt_diff=60.0):
    original=[deepcopy(pm) for pm in pms]
    kwargs={'maxRtDifference': max_rt_diff, 'maxMzDifference': 0.004, 
            'maxMzDifferencePairfinder': 0.008, 'destination': destination}
    ref=get_ref_table(plib, ref)
    tables=fast_ff_detect(pms)
    __=rtAlign(tables, tables, refTable=ref, **kwargs)
    return get_transformed(original, destination)


def fast_ff_detect(pms):
    tables=[]
    for pm in pms:
       t=emzed.ff.runMetaboFeatureFinder(pm, ms_level=1, common_noise_threshold_int=8e4) 
       tables.append(t)
    return tables
    
    
def get_ref_table(plib, ref):
    if ref:
        return ref
    if not plib:
        path=emzed.gui.askForSingleFile(caption='open peak library', extensions=['table'])
        plib=emzed.io.loadTable(path)
    return build_ftable_from_plib(plib)
    

def build_ftable_from_plib(t):
    print 'creating feature table from peak library ...'
    required=['id', 'feature_id', 'mz', 'mzmin', 'mzmax', 'rt', 'rtmin', 'rtmax', 'intensity',
              'quality', 'fwhm', 'z', 'peakmap', 'source']    
    colnames=['ipid', 'fid', 'mzmean', 'mzmin', 'mzmax', 'rtmean', 'rtmin', 'rtmax', 'area',
               'norm_peak_area', 'params', 'z_fid']
    ref=t.extractColumns(*colnames)
    ref=_get_unique_rows(ref)
    pairs=(zip(colnames, required))
    for p in pairs:
        if p[0]==p[1]:
            continue
        else:
            ref.renameColumn(*p)
    ref.replaceColumn('fwhm', ref.apply(_update_fwhm, (ref.fwhm, )), type_=float, 
                    format_="'%.2fm' % (o/60.0)")
    pm=max(t.peakmap.values, key=lambda v: len)
    ref.addColumn('peakmap', pm, type_=PeakMap)
    ref.addColumn('source', ref.peakmap.uniqueValue().meta['source'], type_=str)
    print 'Done'
    return ref


def _get_unique_rows(t):
    t.updateColumn('_keep', t.area==t.area.max.group_by(t.fid), type_=bool)
    t1=t.filter(t._keep==True)
    t.dropColumns('_keep')
    t1.dropColumns('_keep')
    return t1


def _update_fwhm(params):
    max_int=max(params[1])
    pairs=zip(*params)
    select=[p for p in pairs if p[1]>=0.5*max_int]
    max_=max(select, key=lambda v: v[1])[0]
    min_=min(select, key=lambda v: v[1])[0]
    return max_-min_


def get_transformed(pms, dir_):
    ext='.xml'
    transformed=[]
    for pm in pms:
        filename=pm.meta['source']
        name, __=os.path.splitext(filename)
        name=name+ext
        path=os.path.join(dir_, name)
        trafo=load_transformation(path)    
        pm_trans=transform_peakmap_rt(pm, trafo)
        transformed.append(pm_trans)
    return transformed
        

def load_transformation(path):
    t_file=pyopenms.TransformationXMLFile()
    trafo=pyopenms.TransformationDescription()
    t_file.load(path, trafo, True) # True is a flag that got lost while code wrapping
    return trafo
    

def transform_peakmap_rt(peakmap, transformation):
    """
    """
    for spec in peakmap.spectra:
        spec.rt=transformation.apply(spec.rt)
    peakmap.meta['aligned']=True
    return peakmap    