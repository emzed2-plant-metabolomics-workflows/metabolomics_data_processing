# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 13:58:10 2019

@author: pkiefer
"""
import numpy as np
import emzed
import basic_processing as bp
from emzed.core.data_types import PeakMap, Spectrum, Table


def top_n_prm_peaks(pm, peaks_table, n=40, precursor_tol=0.02, mztol=0.005, isolation_width=1.0,
                    id_col='crid'):
    t=build_ms_level2_table(pm)
    # we assign the peaks of peaks table to dominating peakmap
    peaks_table=map_ms_level_1_peak(peaks_table, pm, id_col)
    t=assign_ms_level2_table(peaks_table, t, precursor_tol, n, id_col)  
    update_top_n_ms2_peaks_table(t, id_col, mztol, min_specs=2)
    y=find_ms1_overlap_peaks(t, pm, isolation_width, mztol, id_col)
    return t, y

def map_ms_level_1_peak(peaks_table, pm, id_col):
    pm=pm.getDominatingPeakmap()
    pt=peaks_table
    rttol=_calc_rttol(pt)
    t=bp.feature_extraction.smart_peak_extraction(pm, pt, integratorid='trapez', id_col=id_col,
                                                  max_delta_rt=rttol)
    t.updateColumn('rt', t.apply(_rt_at_appex,(t.params, )), type_=float)
    return t


def _calc_rttol(t):
    fwhms=[]
    for rts, ints in t.params.values:
        min_int=max(ints)*0.5
        rts=sorted([p[0] for p in zip(rts, ints) if p[1]>=min_int])
        fwhm=rts[-1]-rts[0]
        fwhms.append(fwhm)
    return np.mean(fwhms)*3
        

def assign_ms_level2_table(peaks_table, t, mztol, n, id_col):
    pt=peaks_table
    comb=pt.join(t, pt.mz.equals(t.mz, mztol) & pt.rt.inRange(t.rtmin, t.rtmax))
    comb.replaceColumn('rtmin__0', comb.rtmin, type_=float)
    comb.replaceColumn('rtmax__0', comb.rtmax, type_=float)
    comb=emzed.utils.integrate(comb, 'trapez')
    comb.updateColumn('mzs', comb.apply(_get_mzs_at_appex, (comb.rt, comb.peakmap__0, n)),
                      type_=emzed.core.data_types.Spectrum)
    return comb


def update_top_n_ms2_peaks_table(t, id_col, mztol=0.005, min_specs=2):
    expr=(id_col, t.getColumn(id_col), t.peakmap__0, t.mzs, t.rtmin, t.rtmax, mztol, min_specs)
    t.updateColumn('ms2_table', t.apply(_extract_peaks, expr))


def _extract_peaks(id_colname, id_col, pm, mzs, rtmin, rtmax, mztol, min_specs):
    t=emzed.utils.toTable('mz', mzs, type_=float)
    t.updateColumn('mzmin', t.mz - mztol, type_=float)
    t.updateColumn('mzmax', t.mz + mztol, type_=float)
    t.updateColumn('rtmin', rtmin, type_=float)
    t.updateColumn('rtmax', rtmax, type_=float)
    t.updateColumn('peakmap', pm, type_=PeakMap)
    t.addColumn(id_colname, id_col, type_=int, insertBefore='mz')
    t=emzed.utils.integrate(t, msLevel=2)
    t.updateColumn('rt', t.apply(_rt_at_appex,(t.params, )), type_=float, insertBefore='rtmin')
    t=emzed.utils.integrate(t, 'trapez', msLevel=2)
    return _filter_by_min_specs(t, min_specs)


def _filter_by_min_specs(t, min_specs):
    def _get_num_specs(params):
        return len([p for p in params[1] if p])
    t.updateColumn('_num_specs', t.apply(_get_num_specs, (t.params, )), type_=int)
    t=t.filter(t._num_specs>=min_specs)
    t.dropColumns('_num_specs')
    return t    
    
def find_ms1_overlap_peaks(t, pm, isolation_width, mztol, id_col):
    colnames=[id_col, 'mz', 'mzmin', 'mzmax', 'rt', 'rtmin', 'rtmax', 'peakmap']
    t=t.extractColumns(*colnames)
    t=emzed.utils.integrate(t, msLevel=1)
    pm1=pm.getDominatingPeakmap()
    t.updateColumn('peakmap', pm1, type_=PeakMap)
    t.updateColumn('spec_at_rt', t.apply(_get_spec_at_rt,(pm1, t.rt)), type_=Spectrum, 
                   format_='%r')
    expr=(t.spec_at_rt, t.mz, t.mzmin, t.mzmax, isolation_width)
    t.updateColumn('overlaps', t.apply(_extract_overlaps, expr, keep_nones=True), type_=tuple)
    expr =(id_col, t.getColumn(id_col), t.overlaps, mztol, t.rt, t.rtmin, t.rtmax, pm1)
    t.replaceColumn('overlaps', t.apply(_add_overlap_table, expr), type_=Table)
    return _edit_overlap_table(t)
    
    
def _extract_overlaps(spec, mz, mzmin, mzmax, isolation_width):
    mzs=[]

    lower=mz-isolation_width/2.0
    upper=mz+isolation_width/2.0
    peaks=sorted(spec.peaks, key=lambda v: v[0])
    i=_get_start_index(peaks, mz, 0, iters=3)
    for mz_, int_ in peaks[i:]:
        if lower<=mz_<=upper:
             _add_mzs(mz_, mzmin, mzmax, mzs)
        if mz_>=upper:
            break
    return tuple(mzs)


def _get_start_index(peaks, mz, i=0, iters=3):
    j=len(peaks)/2
    upper=len(peaks)
    lower=0
    m=0
    while True:
       if peaks[j][i]>mz:
           upper=j
       elif peaks[j][i]<mz:
           lower=j
       j=(lower+upper)/2
       m+=1
       if m>=iters:
           break
    return j if peaks[j][i]<=mz else lower
       
       
def _add_mzs(mz, mzmin, mzmax, mzs):
    if not mzmin<=mz<=mzmax:
        mzs.append(mz)


def _add_overlap_table(id_col, id_, overlaps, mztol, rt, rtmin, rtmax, pm):
    overlaps=overlaps if len(overlaps) else (None,)
    t=emzed.utils.toTable('mz', overlaps, type_=float)
    t.updateColumn(id_col, id_, type_=int, format_='%d', insertBefore='mz')
    t.updateColumn('ms_level', 1, type_=int, format_='%d', insertBefore='mz')
    t.addColumn('overlapping_peak', True, type_=bool, format_='%r', insertAfter='ms_level')
    t.updateColumn('mzmin', t.mz-mztol, type_=float)
    t.updateColumn('mzmax', t.mz+mztol, type_=float)
    t.updateColumn('rt', rt, type_=float)
    t.updateColumn('rtmin', rtmin, type_=float)
    t.updateColumn('rtmax', rtmax, type_=float)
    t.updateColumn('peakmap', pm, type_=PeakMap)    
    t=emzed.utils.integrate(t, 'std')
    t.updateColumn('rt', t.apply(_rt_at_appex,(t.params, )), type_=float)
    return t.filter(t.area>0.0)


def _edit_overlap_table(t):
    extracted=t.overlaps.values
    t.dropColumns('overlaps', 'spec_at_rt')
    listed=[]
    listed.extend(extracted)
    return emzed.utils.stackTables(listed)    


def build_ms_level2_table(pm):
    pairs= pm.splitLevelN(2, 4)
    mzs=[p[0] for p in pairs]
    pms=[p[1] for p in pairs]
    t=emzed.utils.toTable('mz', mzs, type_=float)
    t.addColumn('peakmap', pms, type_=PeakMap)
    t.addColumn('rtmin', t.apply(_getrtmx,(t.peakmap, )), type_=float, insertAfter='mz')
    t.addColumn('rtmax', t.apply(_getrtmx,(t.peakmap, False)), type_=float, insertAfter='rtmin')
    return t


#---------------------------------------------------------------------------------------
# helper funs

def _getrtmx(pm, rtmin=True):
    i=0 if rtmin else 1
    return pm.rtRange()[i]


def _get_mzs_at_appex(rt, pm, n=10):
    spec=_get_spec_at_rt(pm, rt)
    # required for ms_level2
    if not n:
        n=len(spec)
    return _get_top_n_mzs(spec, n)
    

def _get_top_n_mzs(spectrum, n=20):
    peaks=spectrum.peaks
    ordered=sorted(peaks, key=lambda v: v[1], reverse=True)
    n=n if len(spectrum)>=n else len(spectrum)
    return tuple(np.array(ordered)[:n, 0])


def _get_spec_at_rt(pm, rt):
    specs=pm.spectra
    delta=[abs(spec.rt-rt) for spec in specs]
    return min(zip(specs, delta), key=lambda v: v[1])[0]


def _rt_at_appex(params):
    return max(zip(*params), key=lambda v: v[1])[0]