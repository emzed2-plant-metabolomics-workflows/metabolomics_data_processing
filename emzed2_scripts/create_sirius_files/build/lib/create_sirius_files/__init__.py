

# IMPORTS WHICH SHOULD APPEAR IN emzed.ext AFTER INSTALLING THE PACKAGE:
#from minimal_module import hello # makes emzed.ext.create_sirius_files.hello() visible
from main_build_mgf_ms_files_from_ms_prm import main_create_mgf_and_ms_files
from main_build_mgf_ms_files_from_ms_prm import get_arguments

# DO NOT TOUCH THE FOLLOWING LINE:
import pkg_resources
__version__ = tuple(map(int, pkg_resources.require(__name__)[0].version.split(".")))
del pkg_resources