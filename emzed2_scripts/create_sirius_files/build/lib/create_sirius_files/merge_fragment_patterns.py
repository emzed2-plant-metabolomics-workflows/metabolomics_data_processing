# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 14:18:02 2019

@author: pkiefer
"""
import emzed
import wtbox2
import numpy as np

def merge_fragment_patterns(ms2, mztol, id_col):
    tables=ms2.splitBy('source')
    # update ids    
    [t.addEnumeration('_id') for t in tables]
    ref=tables[0]
    isin=wtbox2.utils.fast_isIn_filter
    for t in tables[1:]:
        comp=ref.leftJoin(t, ref.mz.equals(t.mz, mztol) & \
            (ref.getColumn(id_col)==t.getColumn(id_col)))
        colname='__'.join(['_id', '0'])
        common=set(comp.getColumn(colname).values)
        frac_ids=set(t._id.values)-common
        new=isin(t, '_id', frac_ids)
        ref= build_ref(comp, new, '_id')
        
    # remove temporary column '_id'
    ref.dropColumns('_id')
    _=ref.sortBy('area', ascending=False)
    return ref


def build_ref(t, new, id_col):
    format_=t.getColFormat('area')
    t.updateColumn('mz', t.apply(_update_mean, (t.mz, t.mz__0), keep_nones=True), type_=float)
    t.updateColumn('mzmin', t.apply(_update_mean, (t.mzmin, t.mzmin__0), keep_nones=True), type_=float)
    t.updateColumn('mzmax', t.apply(_update_mean, (t.mzmax, t.mzmax__0), keep_nones=True), type_=float)
    t.updateColumn('rt', t.apply(_update_mean, (t.rt, t.rt__0), keep_nones=True), type_=float)
    t.updateColumn('rtmin', t.apply(_update_mean, (t.rtmin, t.rtmin__0), keep_nones=True), type_=float)
    t.updateColumn('rtmax', t.apply(_update_mean, (t.rtmax, t.rtmax__0), keep_nones=True), type_=float)
    t.updateColumn('area', t.apply(_update_sum, (t.area, t.area__0), keep_nones=True), type_=float, format_=format_)
    colnames=[n for n in t.getColNames() if not n.endswith('__0')]
    ref = t.extractColumns(*colnames)
    ref=ref.filter(ref.mz.isNotNone()==True)
    ref = emzed.utils.stackTables([ref, new])
    ref.updateColumn(id_col, range(len(ref)), type_=int)
    
    return ref
    

def _update_mean(v1, v2):
    return float(np.nanmean([v1, v2])) if v2 is not None else v1

def _update_sum(v1, v2):
    return sum([v1, v2]) if v2 is not None else v1
    