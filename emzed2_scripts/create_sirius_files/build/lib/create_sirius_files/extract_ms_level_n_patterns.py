# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 13:39:33 2019

@author: pkiefer
"""

import emzed
import wtbox2

from rtalign_by_plib import rt_align_pms_by_peak_lib
from extract_isotope_pattern import extract_isotopologues, mean_isotope_pattern
from convert_adducts_from_emzed2sirius import translate_adducts
from top_n_prm import top_n_prm_peaks
from merge_fragment_patterns import merge_fragment_patterns

kwargs={'n':30, 'precursor_tol':0.01, 'mztol':0.005, 'isolation_width':1.0, 'id_col': 'crid'}

def extract_ms_level_n_tables(pms, plib, candidates, kwargs, mztol=0.003, rtalign=False,  
                        max_rt_diff=60.0, destination=r'y:\temp', id_col='crid'):
    if rtalign:
        pms=rt_align_pms_by_peak_lib(pms, plib=plib, destination=destination, 
                                     max_rt_diff=max_rt_diff)
    peaks_table=create_peaks_table(plib, candidates, id_col, mztol=0.003)
    kwargs['id_col'] = id_col
    kwargs['peaks_table']=peaks_table
    t=get_peaks(pms, kwargs)
    assert len(t)>0, 'sample files do not contain any parent ions defined in candidates!'
    t.sortBy('ms_level')
    translate_adducts(peaks_table, 'consensus_adducts')
    ms1, ms2 = t.splitBy('ms_level')
    update_missing(ms1, peaks_table, id_col)
    update_missing(ms2, peaks_table, id_col)
    ms2=merge_fragment_patterns(ms2, mztol, id_col)
    ms1=extract_isotopologues(ms1, id_col, mztol)
    ms1= mean_isotope_pattern(ms1, id_col)    
    return ms1, ms2
   

def get_peaks(pms, kwargs):
    id_col=kwargs['id_col']
    show_prog=wtbox2.utils.show_progress_bar
    tuples=show_prog(pms, top_n_prm_peaks, kwargs=kwargs)
    t_insps=[]
    for t, y in tuples:
        t_insps.append(build_inspection_table(t, y, id_col))
    t_insp=emzed.utils.stackTables(t_insps)
    print 'inspection_table_done'
    return manual_select_prm_peaks(t_insp, id_col)

#--------------------------------------------------------------------------
def manual_select_prm_peaks(t, id_col='pid'):
    text=""" Please check all prm peaks indiviadually.\n
    Do MS level 2 peaks coelute with corresponding MS level 1 peak?\n
    You can remove peaks by manualy reintegration peaks using `no_integration` method
    """
    if len(t):
        emzed.gui.showInformation(text)
        tables=t.splitBy(id_col, 'source')
        wtbox2.gui.inspect(tables)
        t=emzed.utils.stackTables(tables)
    t.replaceColumn('overlapping_peak', t.overlapping_peak.ifNotNoneElse(False), type_=bool)
    t=t.filter(t.overlapping_peak==False)
    t.dropColumns('overlapping_peak')
    return t.filter(t.area.isNotNone())

    
#------------------------------------------------------------
def build_inspection_table(t, y, id_col='crid'):
    colnames=[id_col, 'mz', 'mzmin', 'mzmax', 'rt', 'rtmin', 'rtmax', 'peakmap']
    t_ms1=t.extractColumns(*colnames)
#    t_ms1.addEnumeration('pid')
    t_ms1.addColumn('ms_level', 1, type_=int, format_='%d', insertAfter=id_col)
    t_ms1.addColumn('overlapping_peak', False, type_=bool, format_='%r', insertAfter='ms_level')
    t_ms1=emzed.utils.integrate(t_ms1, 'trapez', msLevel=1)
    extracted=[t_ms1]
    for peak, ms2 in zip(t_ms1.splitBy(id_col), t.ms2_table):
        t_ms2=ms2.extractColumns(*colnames)
#        t_ms2.addColumn('pid', peak.pid.uniqueValue(), type_=int, insertBefore='mz')
        t_ms2.addColumn('ms_level', 2, type_=int, format_='%d', insertAfter=id_col)
        t_ms2.addColumn('overlapping_peak', None, type_=bool, format_='%r', insertAfter='ms_level')
        t_ms2=emzed.utils.integrate(t_ms2, 'trapez')
        
        extracted.append(t_ms2)
        extracted.append(y)
    t=emzed.utils.stackTables(extracted)
    def _get_source(pm):
        return pm.meta['source']
    t.updateColumn('source', t.apply(_get_source, (t.peakmap, )), type_=str)
    return t


def update_missing(t, peakstable, id_col):
    update=wtbox2.table_operations.update_column_by_dict
    pt=peakstable
    d=dict(zip(pt.getColumn(id_col), pt.adducts_sirius))
    update(t, 'assigned_adduct', id_col, d, type_=tuple, in_place=True)
    d=dict(zip(pt.getColumn(id_col), pt.z_fid))
    update(t, 'z', id_col, d, type_=int, in_place=True)
################################################################################################


def create_peaks_table(plib, candidates, id_col, mztol=0.003):
    pt=_select_candidates(plib, candidates, mztol, id_col)
    return _select_most_intense_peak(pt, id_col)

def _select_candidates(plib, candidates, mztol, id_col):
    t=wtbox2.utils.fast_isIn_filter(plib, id_col, candidates.getColumn(id_col).values)
    d=dict(zip(candidates.getColumn(id_col), candidates.mz))
    wtbox2.table_operations.update_column_by_dict(t, 'mz_', id_col, d, type_=float, in_place=True)
    t=t.filter(t.mz.approxEqual(t.mz_, 0.005))
    t.dropColumns('mz_')
    return t

def _select_most_intense_peak(t, id_col):
    t.updateColumn('areamax', t.area.max.group_by(t.getColumn(id_col)), type_=float)
    t=t.filter(t.area==t.areamax)
    t.dropColumns('areamax')
    t.updateColumn('z', t.z_fid, type_=int, format_='%d')
    return t

