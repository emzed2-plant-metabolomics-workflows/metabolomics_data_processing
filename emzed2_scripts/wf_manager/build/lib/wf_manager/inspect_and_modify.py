# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 11:01:59 2016

@author: pkiefer
"""
import emzed
from wtbox._fix_inspector_bug import fix_ms2_inspect_integration_bug as _inspect
from wtbox.table_operations import split_table_by_columns
from emzed.core.data_types import Table
from inspect import isfunction


def reprocess_result(result, processing_steps, config):
    result=_get_result_as_list(result)
    result=handle_empty_tables(result)
    result=add_ids(result)
    id2title={t.split_id.uniqueValue(): t.title for t  in result}
    before=memo(result)
    after=before.copy()
    modifications=[]
    [t.dropColumns('split_id', 'id_') for t in result]
    diff_cols=_get_diff_cols(processing_steps)
    for process, args in processing_steps:
        if isfunction(process): 
            if process.func_name=='inspect_result':
                _diff_col_info(diff_cols)
                after=inspect_result(after, args)
            else :
                after, modified=reprocess(after, before, args, process, config)
                modifications.append(modified)
                __=diff_cols.pop(0) # suppress output
        else:
            assert False, 'procesing steps must be pairs of a function and its arguments!'
    return reset_splittings(after, id2title), any(modifications)
      
      
def add_ids(result):
    if not (isinstance(result, Table) or all([isinstance(r, Table) for r in result])):
        assert False, 'result must be iterable of Tables and not %s' %type(result) 
    if isinstance(result, Table):
        result=[result]
    _add_split_id(result)
    m=0
    for r  in result:
          num_rows=len(r) if len(r) else 1
          r.addColumn('id_', range(m, num_rows+m), type_=int, format_=None)
          m+=len(r)
    return result

    
def memo(results):
    if isinstance(results, list):
        return emzed.utils.stackTables(results)
    return results    


def inspect_result(result, split_cols):
    if all([v!=None for v in split_cols]):
        results=split_table_by_columns(result, split_cols)
    else:
        results=split_table_by_columns(result, ('split_id',))
    _inspect(results)
    return emzed.utils.stackTables(results)

def _get_diff_cols(steps):
    #only=all([process.func_name=='inspect_result' for process, __ in steps])
    def select(p, kwargs):
        return p.func_name!='inspect_result' and len(kwargs.get('diff_col'))
    return [kwargs.get('diff_col') for p, kwargs in steps if select(p, kwargs)]


def _diff_col_info(diff_cols):
    if len(diff_cols):
        diff_col=diff_cols[0]
        if diff_col.split('__')[0]=='area':
           emzed.gui.showInformation('you can change peak areas by reintegrating peaks')
        else:
           emzed.gui.showInformation('you can modify column %s manually' %diff_col)
    

def reprocess(result, before, kwargs, fun, config):
    
    find_differences(before, result, kwargs['diff_col'], kwargs['min_diff'])
    # case diff col exists
    done, reprocess=select_for_reprocessing(result, kwargs['split_cols'])
    modify=True if len(reprocess) else False
    for subset in reprocess:
        done.append(fun(subset, config))
    return emzed.utils.stackTables(done), modify


def _split(t, split_cols):
    if all([v!=None for v in split_cols]):
        return split_table_by_columns(t, split_cols)
    elif any([v!=None for v in split_cols]):
        assert False, 'split columns for processing contain none Value!, no splitting'
    return [t]
        

def find_differences(before, after, diff_col, min_diff):
    if not diff_col:
        after.addColumn('changed', True, type_=bool)
    else:
        id2diff=dict()
        for key,value in zip(before.id_, before.getColumn(diff_col).values):
             id2diff[key]=value
        after.addColumn('changed', after.apply(label_diff, (after.id_, after.getColumn(diff_col), 
                                                id2diff, min_diff), keep_nones=True), type_=bool)


def label_diff(key, value, d, min_diff=None):
#    import pdb; pdb.set_trace()
    if not min_diff:
        min_diff=0.0
    former=d.get(key)
    if _isnumber(former) and _isnumber(value):
        return False if abs(former-value)<=min_diff else True
    else:
        return False if  former==value else True
                    
def _isnumber(v):
    check=isinstance(v, float) or isinstance(v, int)
    return True if check else False


def select_for_reprocessing(result, split_cols):
    keep=[]
    reprocess=[]
    for subset in _split(result, split_cols):
        if any(subset.changed.values):
            subset.dropColumns('changed')
            reprocess.append(subset)
        else:
            subset.dropColumns('changed')
            keep.append(subset)
    return keep, reprocess
 

def reset_splittings(table, id2split):
    table.sortBy('id_')
    results=table.splitBy('split_id')
    return [_reset(t, id2split) for t in results]
#    return results

def _reset(t, id2split):
    t.title=id2split.get(t.split_id.uniqueValue())
    t.dropColumns('split_id', 'id_')
    if any([v!=None for v in t.rows]):
        return t
    return t.buildEmptyClone()
        
           
def _add_split_id(tables):
    [r.addColumn('split_id', i, type_=int, format_=None) for r,i in zip(tables, range(len(tables)))]             
    
def handle_empty_tables(tables):
    return [_handle(t) for t in tables]
    
def _handle(t):
    t=t.copy()
    if not len(t):
        t.rows=[[None] * len(t.getColNames())]
    return t
    
def _get_result_as_list(tables):
    if isinstance(tables,Table):
        return [tables]
    else:
        assert all([isinstance(t, Table) for t in tables])
        return tables
                
        
    
        
    
    
    