from example_my_workflow import my_main_workflow, modify_peak
from example_my_config import my_config_gui, my_default_config
from wf_manager.run_workflow import run_workflow, inspect
"""
Framework for accelerated workflow development.
 Implemented example below
"""

def run():
    print 
    print 'name of executed workflow: %s' %my_main_workflow.func_name
    # define inspection process
    split_cols=('source',)
    inspect1=(inspect, split_cols)
    #define postprocessing
    #  kwargs are required for modification 
    kwargs={'diff_col': 'area', 'min_diff': 1.0, 'split_cols': ('source',)}
    postprocess1=(modify_peak, kwargs)
    run_workflow(my_main_workflow, my_default_config, my_config_gui=my_config_gui, save_excel=True,
                 inspect_or_reprocess=(inspect1, postprocess1))

if __name__=='__main__':
    run()


