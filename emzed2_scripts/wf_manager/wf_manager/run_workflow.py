# -*- coding: utf-8 -*-
"""
Created on Thu Aug 18 17:05:28 2016

@author: pkiefer
"""
from emzed import gui
def_dir=r'\\gram\biol_micro_gr_vorholt\OMICS'
import workflow_manager as manager
#from _inspect_and_modify_dev import inspect_result as inspect
from inspect_and_modify import inspect_result as inspect
#################################################################################################
def run_workflow(my_workflow, my_config, my_config_gui=None, 
                 inspect_or_reprocess=((inspect, (None,))), save_excel=True):

    """ Main function of the workflow_builder module: Function arguments:
        - my_workflow: 
        Main function of the workflow you want to execute in workflow_builder environment. 
        -my_config: 
        Contains parameter2value dictionary of your workflow main function
        - my_config_gui [optional]: you can provide configuration graphical user interface to change
        workflow parameters by the user. If none values from my_config are used for anaylsis
        - modify [optional]: if True all changes of column specified in diff col are taken into 
        account and will be saved.
        - inspect_or_reprocess: 
        - save_excel: If True results are additionaly saved in excel sheet
    """
    project_path= set_project_path(def_dir)        
    class DefaultMainGui(gui.WorkflowFrontend):
        """
        """
        workflow_label=my_workflow.func_name # identifyer from function name
        id2results=gui.WorkflowFrontend().set_defaults()
        config=gui.WorkflowFrontend().set_defaults()
        project_dir=project_path
#        project_path=gui.DirectoryItem('project path', default=def_dir, 
#                                       help='enter main project folder path. '\
#                                    'If a new project is initialized enter path with LC-MS data.'\
#                                    'A new project structure will be built automaticaly')
        project_info=gui.RunJobButton('show project details',method_name='show_details')
        settings=gui.RunJobButton('define sets', method_name='config_settings')
        start=gui.RunJobButton('run_workflow', method_name='start_analysis')
        inspect_result=gui.RunJobButton('inspect result', method_name='inspect')
        reset_params=gui.RunJobButton('remove_sets', method_name='reset')
        switch=gui.RunJobButton('switch project', method_name='new_session')
        
        
        def show_details(self):
            self.load_config()
            manager.show_project_info(self.config, self.project_dir)
        
        def config_settings(self):
           self.config = manager.manage_config(self.config, self.project_dir, my_config_gui, 
                                               my_config, label= self.workflow_label)
        
        def start_analysis(self):
            """ import the script where you put all pieces of your workflow together.
                example
                self.result, self.config=your_script.run_analysis(self.config)
            """
            self.load_config()
            if self.config:
            #self.result, self.config=your_script.run_analysis(self.config)
                self.id2results, self.config=manager.run_workflow(self.config, my_workflow, 
                                                                  save_excel=save_excel)
                # load samples
            else:
                gui.showWarning('parameters have to be configured first !!')
                    
            
        def inspect(self):
            #  checks for existing config data
            self.load_config()
            if not self.id2results:
                self.id2results=dict()
            manager.inspect_and_modify_results(self.id2results, self.config, inspect_or_reprocess, 
                                               save_excel=save_excel)
            
        
        def load_config(self):
            if not self.config: 
                self.config=manager.load_config(self.project_dir, self.workflow_label)
                
                    
        
        def reset(self):
            self.load_config()
            manager.remove_batches(self.config, self.project_dir, self.workflow_label)
        
        
        def new_session(self):
            self.config=None
            self.id2results=None
            self.project_dir=set_project_path(self.project_dir)
            
            
    DefaultMainGui().show()
        
def set_project_path(default=None):
    project_path=gui.DialogBuilder('select project path')\
    .addDirectory('project path', default=default, 
                                       help='enter main project folder path. '\
                                    'If a new project is initialized enter path with LC-MS data.'\
                                    'A new project structure will be built automaticaly')\
    .show()
    
    return project_path
    
