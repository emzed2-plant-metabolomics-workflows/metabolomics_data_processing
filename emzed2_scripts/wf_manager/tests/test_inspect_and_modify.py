# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 18:01:54 2017

@author: pkiefer
"""
from emzed import utils
from emzed.core.data_types import Table
from wf_manager import inspect_and_modify as iam


def test_find_differences_0():
    before=utils.toTable('a', range(3), type_=int)
    before.addColumn('id_', range(3), type_=int)
    after=before.copy()
    iam.find_differences(before, after, 'a', 0.0)
    assert after.changed.values==(False, False, False)

def test_find_differences_1():
    before=utils.toTable('a', range(3), type_=int)
    before.addColumn('id_', range(3), type_=int)
    after=before.copy()
    after.replaceColumn('a', (0, None, 2), type_=int)
    iam.find_differences(before, after, 'a', 0.0)
    print after
    assert after.changed.values==(False, True, False)

def test_find_differences_2():
    before=utils.toTable('a', ['a', 'b'], type_=str)
    before.addColumn('id_', range(2), type_=int)
    after=before.copy()
    after.replaceColumn('a', ('a', 'a'), type_=int)
    iam.find_differences(before, after, 'a', None)
    print after
    assert after.changed.values==(False, True)


def test_find_differences_3():
    before=utils.toTable('a', [(1,), (2,)], type_=tuple)
    before.addColumn('id_', range(2), type_=int)
    after=before.copy()
    after.replaceColumn('a', ((1,0), (0,)), type_=tuple)
    iam.find_differences(before, after, 'a', None)
    print after
    assert after.changed.values==(True, True)

def test_find_differences_4():
    t=utils.toTable('a', [(1,), (2,)], type_=tuple)
    before=utils.toTable('id_', range(2), type_=int)
    before.addColumn('a', [t,t], type_=Table)
    after=before.copy()
    t1=utils.toTable('a', [(1,), (3,)], type_=tuple)
    after.replaceColumn('a', [t, t1], type_=tuple)
    iam.find_differences(before, after, 'a', None)
    print after
    assert after.changed.values==(False, True)

def test_find_differences_5():
    before=utils.toTable('id_', range(2), type_=int)
    before.addColumn('a', [{1:2},{2:3}], type_=Table)
    after=before.copy()
    after.replaceColumn('a', [{1:2}, {0:5}], type_=tuple)
    iam.find_differences(before, after, 'a', None)
    print after
    assert after.changed.values==(False, True)


def test_find_differences_6():
    before=utils.toTable('a', range(3), type_=int)
    before.addColumn('id_', range(3), type_=int)
    before.replaceColumn('a', (0, None, 2), type_=int)
    after=before.copy()
#    after.replaceColumn('a', (0, None, 2), type_=int)
    iam.find_differences(before, after, 'a', 0.0)
    print after
    assert after.changed.values==(False, False, False)
