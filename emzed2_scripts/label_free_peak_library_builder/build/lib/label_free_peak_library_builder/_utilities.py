# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 12:35:01 2019

@author: pkiefer
"""
import emzed
import re, string
from collections import defaultdict, Counter
import wtbox2, itertools
from inspect import getargspec

#################################################################################################

def update_possible_m0(lib, adduct_col, mz0_col, insertAfter='possible_adducts'):
    d=_get_adductsdict()
    exp=lib.getColumn
    lib.updateColumn('possible_m0', 
                     lib.apply(_calculate_m0, (exp(adduct_col), exp(mz0_col), d)), 
                    type_=tuple, insertAfter=insertAfter)
    

def _calculate_m0(adduct_names, mz0, d):
    m0s=[]
    for adduct in adduct_names:
        delta_m, z=d[adduct]
        m0=mz0*z-delta_m
        m0s.append(m0)
    return tuple(m0s)

    

def _get_adductsdict():
    t=emzed.adducts.all.toTable()
    d=dict(zip(t.adduct_name, zip(t.mass_shift, t.z)))
    d['M+H-NH3']=(emzed.mass.p-emzed.mass.of('NH3'), 1)
    return d


##################################################################################################•
def build_normle_area_table(lib, min_hits_per_group=4, group_col='sample_group_id'):
    sample2group=dict(zip(lib.sample_name, lib.getColumn(group_col)))    
    aid2names=defaultdict(set)      
    colnames=['aid', 'sample_name', 'norm_aid_area', group_col]
    exp=[lib.getColumn(col) for col in colnames]
    d=defaultdict(dict)
    aids=[]
    for aid, name, area, group_col in zip(*exp):
        d[name][aid]=area
        aid2names[aid].add(name)
    for aid in aid2names.keys():
        groups=Counter([sample2group[v] for v in aid2names[aid]])
        if max(groups.values())>=min_hits_per_group:
            aids.append(aid)
    aids.sort()
    t=emzed.utils.toTable('aid', aids, type_=int)
    for sample in sorted(d.keys()):
        t.updateColumn(sample, None, type_=float)
        t.updateColumn(sample, t.apply(_readout, (sample, t.aid, d)), type_=float, format_='%.1e')
    return t


def _readout(key1, key2, d):
    return d[key1].get(key2)
    
def _build_selection():
    names=[]    
    for i in [1, 8, 12, 24]:
        for j in range(1,6):
            name=''.join(['Con', str(i), 'R', str(j)])
            names.append(name)
    return names
            


################################################################################################
def unfold_tuple_col(t, row_col='ipid', tuple_col='consensus_adduct', iterable_type=str):
    
    ut=_build_pair_table(t, row_col, tuple_col, iterable_type)
    tt=t.fastJoin(ut, row_col)
    exp1=tt.getColumn
    drop_cols=['__'.join([row_col, '0'])]
    col1='__'.join([tuple_col, '0'])
    tt.replaceColumn(tuple_col, exp1(col1), iterable_type)
    drop_cols.append(col1)
    tt.dropColumns(*drop_cols)
    return tt


def _build_pair_table(t, row_col, tuple_col, iterable_type):
     pairs=[]
     exp=t.getColumn
     for id_, ntuple in zip(exp(row_col), exp(tuple_col)):
        for value in ntuple:
            pairs.append((id_, value))
     ids, values=zip(*pairs)
     uf=emzed.utils.toTable(row_col, ids, type_=int)
     uf.addColumn(tuple_col, values, type_=iterable_type)
     return uf

def _join_uts(ts, id_col):
    tt=ts[0]
    if len(ts)==1:
        return tt
    for t in ts[1:]:
       tt=tt.fastJoin(t, id_col)
    pstfxs=[s for s in tt.supportedPostfixes([id_col]) if s]
    colnames=[''.join([id_col, pstfx]) for pstfx in pstfxs]
    tt.dropColumns(*colnames)
    tt.removePostfixes()
    return tt.uniqueRows()
    
        


def collapse_to_tuple(t, row_col, tuple_col):
    update=wtbox2.table_operations.update_column_by_dict
    d=defaultdict(list)
    t=t.copy()
    exp=t.getColumn
    for id_, value in zip(exp(row_col), exp(tuple_col)):
        if len(set([value]) -set(d[id_])):
            d[id_].append(value)
    update(t, tuple_col, row_col, d, type_=tuple, in_place=True)
    t.replaceColumn(tuple_col, exp(tuple_col).apply(tuple), type_=tuple)
    return t.uniqueRows()
    

################################################################################

def get_kwargs_from_config(fun, config):
    args=getargspec(fun).args
    return _get_kwargs(args, config)
    

def _get_kwargs(args, key2value):
    return {arg: key2value[arg] for arg in args if key2value.has_key(arg)}
    
###################################################################################
#

def build_source2name(sources, pattern):
    s2n={}
    blocks=_blocks_to_lists(pattern)
    for source in sources:
        name=find_name(source, blocks)
        s2n[source]=name
    return s2n


#def extract_name_by_pattern(source, pattern):
#    blocks=_blocks_to_lists(pattern)
#    return _get_name(source, blocks)

def extract_name_by_pattern(source, pattern):
    blocks=_blocks_to_lists(pattern)
    return find_name(source, blocks)


#def _get_name(source, blocks):
#    hits=[]
#    for name in blocks:
#        match=re.search(name, source)
#        if match:
#             hits.append(name)
##            break
#    if len(hits):
#        # select longest hit
#        return max(hits, key=lambda v: len(v))
#    raise(Exception, 'no pattern match for sample %s' %source )
#    
#
#
def get_sample_patterns(pattern):
    blocks=_blocks_to_lists(pattern)
    return [''.join(bl) for bl in itertools.product(*blocks)]


def _blocks_to_lists(pattern):
    lists=[]
    for block in re.split('[\]?\[]', pattern):
        if len(block): # split results also empty strings
            lists.append(_transform_block(block))
    return lists
#    return _block2name(lists)

#def _block2name(blocks):
#    return [''.join(elements) for elements in itertools.product(*blocks)]

def _transform_block(block):
    letter2order=dict(zip(string.ascii_lowercase, range(26)))
    order2letter=dict(zip(range(26), string.ascii_uppercase))
    elements=[]
    number_pattern1='[0-9]-[1-9][0-9]*'
    number_pattern2='[1-9][0-9]*-[1-9][0-9]*'
    letter_pattern='[A-Za-z]-[A-Za-z]'
    if re.search(number_pattern1, block) or re.search(number_pattern2, block) or re.search(letter_pattern, block):
        for element in re.split(',\s?', block):
            if re.match(number_pattern1,element) or re.match(number_pattern2, element):
                lower, upper=element.split('-')
                elements.append([str(i) for i in range(int(lower), int(upper)+1)])
            elif re.match(letter_pattern,element):
                lower, upper=element.split('-')
                lower=letter2order[lower.lower()]
                upper=letter2order[upper.lower()]
                assert lower<upper, 'please respect ascending order a-z 0-9 for indizes'
                elements.append([order2letter[i] for i in range(lower, upper+1)])
            else:
                elements.append([element])
        return [''.join(el) for el in itertools.product(*elements) ]
    return re.split(',\s?', block)

#-----------------------------------------------------
def find_name(source, blocks):
    patterns=None
    for subblocks in blocks:
        patterns=_build_pattern(patterns, subblocks)
        patterns=_select_patterns(patterns, source)
        assert len(patterns), 'No match for sample %s' %source
    return max(patterns, key = lambda v: len(v))


def _build_pattern(patterns, subblocks):
    if patterns is not None:
        pairs=itertools.product(patterns, subblocks)
        return [''.join(pair) for pair in pairs] 
    return subblocks

def _select_patterns(patterns, source):
    new_patterns=[]
    for pattern in patterns:
        mos=re.finditer(pattern, source)
        if mos:
            for mo in mos:
                new_patterns.append(source[mo.start(): mo.end()])
    return new_patterns
    
            