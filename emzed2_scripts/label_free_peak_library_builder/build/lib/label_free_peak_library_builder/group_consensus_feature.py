# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 16:03:47 2019

@author: pkiefer
"""
from _update_grouping_id import update_consensus_group_id
import emzed
import numpy as np
from collections import defaultdict
from wtbox2 import utils, table_operations

    
def group_consensus_features(t, mztol):
    print 'updating group_id `fid` ...'
    update_consensus_group_id(t, group_id='feature_id', sub_consensus_id='pid', 
                        consensus_id='fid')
    print 'done'
    t.replaceColumn('fid', t.pid.min.group_by(t.fid), type_=int)
    regroup_fids(t, mztol)
    
################################################################################################ 

def regroup_fids(t, mztol=0.004):
    print 'evaluating fids ...'
    update_z_fid(t)
    fid2pids, fid2z, pid2mzmean, pid2rtmean=_get_grouping_dicts(t)
    pid2fid={}
    fids=fid2pids.keys()
    graphs=utils.show_progress_bar(fids, get_regrouping_graph, args=[fid2pids, fid2z, pid2mzmean, pid2rtmean, mztol])    
    for graph in graphs:
        pid2fid.update(graph)
    def _update_fid(key, d):
        return d[key] if d.has_key(key) else None
    t.updateColumn('fid', t.apply(_update_fid, (t.pid, pid2fid), keep_nones=True), type_=int)
    update_z_fid(t)
    print 'Done.'


def update_z_fid(t):
    fid2z={}
    fid2mzs=defaultdict(set) 
    fid2zs=defaultdict(set)
    for fid, mz, z in set(zip(t.fid, t.mzmean, t.z)):
        fid2mzs[fid].add(mz)
        fid2zs[fid].add(z)
#    import pdb; pdb.set_trace()
    for fid in fid2mzs.keys():
        mzs=sorted(list(fid2mzs[fid]))
        zs=sorted(list(fid2zs[fid]), reverse=True)
        _update_fid2z(zs, mzs, fid, fid2z)
#    return fid2z
    table_operations.update_column_by_dict(t, 'z_fid', 'fid', fid2z, type_=int, in_place=True,
                                           insertAfter='z')
    

def _update_fid2z(zs, mzs, fid, fid2z):
    delta_cl=emzed.mass.Cl37-emzed.mass.Cl35
    if len(mzs)==1:
        fid2z[fid]=0
        return 
    for z in zs:
        masses=[mz*z for mz in mzs]
        masses.sort()
        pairs=np.array(zip(masses, masses[1:]))
        deltas=pairs[:,1]-pairs[:,0]
        crit=all([int(round(d,1))==1 or abs(delta_cl-d*z)<3e-3 for d  in deltas])
        if crit:
            fid2z[fid]=z
            return 
    fid2z[fid]=0


def _get_grouping_dicts(t):
    fid2pid=defaultdict(set)
    for fid, pid in set(zip(t.fid, t.pid)):
        fid2pid[fid].add(pid)
    pid2rtmean=dict(zip(t.pid, t.rtmean))
    pid2mzmean=dict(zip(t.pid, t.mzmean))
    fid2z=dict(zip(t.fid, t.z_fid))
    return  fid2pid, fid2z, pid2mzmean, pid2rtmean


def get_regrouping_graph(fid, fid2pids, fid2z, pid2mzmean, pid2rtmean, mztol):
    pid2fid={}
    pids=fid2pids[fid]
    z=fid2z[fid]
    counts=0
    while len(pids):
        graph=None
        pairs=[(pid, pid2mzmean[pid]) for pid in pids]
        pairs=_build_group_dict(pairs, mztol)
        _mzs, _pids=zip(*pairs)
        if len(_pids)>1:
            graph=dict()
            i=0
            j=1
            while _condition(_mzs, _pids, i,j, z):
                if _no_mass_gap(_mzs[i], _mzs[j], z):
                    rows=list(_pids[i])
                    cols=list(_pids[j])
                    get_min_mat(pid2rtmean, rows, cols, graph)
                    i+=1
                    j=0+i
                j+=1
        if not graph:
            # first element alone             
            pid=_pids[0].pop()
        g=get_pid2fid(graph) if graph else {pid:pid}
        pid2fid.update(g)
        pids=set(pids)-set(g.keys())
        pids=list(pids)
        counts+=1
        if counts>30:
            assert False
    return pid2fid


def _build_group_dict(pairs, mztol):
    pairs=sorted(pairs, key=lambda v: v[1])
    groups=defaultdict(set)
    for pid, mzmean in pairs:
        mzs=np.array(groups.keys())
        index=np.argwhere(np.abs(mzs-mzmean)<mztol)
        assert len(index)<=1
        if len(index)==1:
            i=index[0][0]
            groups[mzs[i]].add(pid)
        else:
            groups[mzmean].add(pid)
    return sorted(groups.items(), key=lambda v: v[0])


def _condition(mzs, pids, i, j, z):
    if i<len(pids)-1:
        return _in_gap_limit(mzs[i], mzs[j], z)
    return False


def _in_gap_limit(mz1, mz2, z):
    delta=abs(mz2-mz1)
    if z:
        return round(delta*z,1)<=1
    a=np.arange(1,4)*delta   
    return  len(np.argwhere(np.round(a,1)==1))>=1


def _no_mass_gap(mz1, mz2, z):
    delta=abs(mz2-mz1)
    if z:
        return round(delta*z,1)==1
    a=np.arange(1,4)*delta   
    return  len(np.argwhere(np.round(a,1)==1))==1
    
    
def get_min_mat(pid2rtmean, rows, cols, graph):
    mat=np.zeros((len(rows), len(cols)))
    for i in range(len(rows)):
        for j in range(len(cols)):
            mat[i][j]=abs(pid2rtmean[rows[i]]-pid2rtmean[cols[j]])
    indices=np.argwhere(mat==np.min(mat))
    d={rows[i]:cols[j] for i,j in indices}
    graph.update(d)



def get_pid2fid(graph):
    starting_points=set(graph.keys())-set(graph.values())
    d=dict()
    for sp in starting_points:
        group=[sp]
        while graph.has_key(sp):
            value=graph.pop(sp)
            group.append(value)
            sp=value
        key=min(group)
        for id_ in group:
            d[id_]=key
    return d
        
        
def _reprocess(t):
    t.replaceColumn('mzmean', t.mz.mean.group_by(t.pid), type_=float)
    t.replaceColumn('rtmean', t.mz.mean.group_by(t.pid), type_=float)                
    return t        

