# -*- coding: utf-8 -*-
"""
Created on Wed May 08 10:12:09 2019

@author: pkiefer
"""

import emzed
import numpy as np
from collections import defaultdict
from wtbox2 import collect_and_compare as cc
from wtbox2 import table_operations as top
from wtbox2 import utils


def build_identification_library(lib, mtol, db=None, ident_col=None, id_col='crid',
                                 value_col='norm_crid_area'):
    key2tol={'m0': mtol}
    mlib=build_mass_library(lib, key2tol, value_col, id_col)
    
    if not db:
       db=_get_pubchem_db()
       ident_col='pubchem_db'
    add_identification_results(mlib, db, key2tol, ident_col, id_col=id_col)  
    return mlib


def build_mass_library(t, key2tol, value_col='norm_crid_area', id_col='crid'):
    # get unige values:
    mlib=_building_mlib(t)
    fun=np.mean
    print 'update rtmean values ...'
    _update_values(t, mlib, fun, 'rtmean', id_col)
#    print 'update m0 values ...'
#    _update_values(t, mlib, fun, value_col='possible_m0', id_col='aid')
    print 'update norm aid area values ...'
    _update_values(t, mlib, max, value_col, id_col)
    
    print 'updating mass id ...'
    mlib.renameColumn('possible_m0', 'm0')
    new_value_col='_'.join(['max',  value_col])
    mlib.renameColumn(value_col, new_value_col)
    mlib.setColFormat('m0', '%.4f')
    update_mass_id(mlib, key2tol, new_value_col)
    return mlib.uniqueRows()
    
    
def _building_mlib(t):
    rows=[]
    colnames=['crid', 'lc_method', 'possible_m0']
    mlib=t.extractColumns(*colnames)
    _merge_possible_m0_values(mlib, 'crid')
    expr=set(zip(*[mlib.getColumn(col) for col in colnames]))
    utils.show_progress_bar(expr, _add_row, args=[rows], in_place=True)
    mlib=mlib.buildEmptyClone()
    mlib.rows=rows
    return mlib

def _merge_possible_m0_values(t, group_col):
    d=defaultdict(list)
    for aid, m0 in set(zip(t.getColumn(group_col), t.possible_m0)):
        d[aid].append(m0)
    def _update(key, d):
        try:
            mean=np.mean(np.array(d[key]), axis=0)
        except:
            import pdb; pdb.set_trace()
        return tuple(mean)
    t.updateColumn('possible_m0', t.apply(_update, (t.getColumn(group_col), d)), type_=tuple)
        
            

def update_hits_per_group(t, mlib):
    d=defaultdict(dict)
    for crid, group, sample in set(zip(t.crid, t.sample_group_id, t.sample_name)):
        if d[crid].has_key(group):
            d[crid][group].add(sample)
        else:
            d[crid][group]=set([sample])
    for key in d.keys():
        for group in d[key]:
            d[key][group]=len(d[key][group])
    def _update(key, d):
        return tuple(sorted(d[key].items(), key=lambda v: v[0]))
    top.update_column_by_dict(mlib, 'detection', 'crid', d, type_=dict, in_place=True)
    mlib.updateColumn('detection', mlib.apply(_update, (mlib.crid, d)), type_=tuple)
    

def _add_row(expr, rows):
    crid, lc_method,  m0s = expr
    for m0 in m0s:
        rows.append([crid, lc_method, m0])

def _update_values(t, lib, fun, value_col='rtmean', id_col='aid'):
    d=defaultdict(list)
    expr=set(zip(t.getColumn(id_col).values, t.getColumn(value_col).values))
    for key, value in expr:
        d[key].append(value)
    id2val={}    
    for key, values in d.items():
        id2val[key]=fun(values)
    top.update_column_by_dict(lib, value_col, id_col, id2val, type_=float, in_place=True)
    

def _update_mass_id(t, key2tol, value_col):
    t.addEnumeration('_id')
    check=t.extractColumns('_id', 'm0')
    check=check.uniqueRows()
    check.sortBy('_id')
    added=set([])
    id2m0s=defaultdict(set)
    id2mid={}
    comp=cc.compare_tables(check, check, key2tol)
    # we start with them mot intense peak
    comp.sortBy(value_col, ascending=False)
    for id0, id1 in zip(comp._id, comp._id__0):
        if id2m0s.has_key(id0):
            id2m0s[id0].add(id1)
            id2m0s[id0].add(id0)
            added.add(id0)
            added.add(id1)
        elif len(set([id0])-added):
            id2m0s[id0].add(id1)
            added.add(id0)
            added.add(id1)
    for key, values in id2m0s.items():
        d=dict(zip(list(values), [key]*len(values)))
        id2mid.update(d)
    top.update_column_by_dict(t, 'mass_id', '_id', id2mid, type_=int, in_place=True,
                              insertBefore='m0')
    t.setColFormat('mass_id', '%d')
    t.dropColumns('_id')
    


def update_mass_id(t, key2tol, value_col):
    t.sortBy(value_col, ascending=False)
    t.addEnumeration('_id')
    check=t.extractColumns('_id', 'm0', value_col)
    check=check.uniqueRows()
    check.sortBy('_id')
    added=set([])
    id2m0s=defaultdict(set)
#    _id2m0=dict(zip(check._id, check.m0))
#    _id2value=dict(zip(check._id, check.getColumn(value_col)))
    id2mid={}
    comp=cc.compare_tables(check, check, key2tol)
    # we start with them most intense values
    comp.sortBy(value_col, ascending=False)
    for id0, id1 in zip(comp._id, comp._id__0):
        if id2m0s.has_key(id0):
            id2m0s[id0].add(id0)
            if len(set([id1])-added):
                id2m0s[id0].add(id1)
            added.add(id0)
            added.add(id1)
        elif len(set([id0])-added):
            id2m0s[id0].add(id0)
            if len(set([id1])-added):
                id2m0s[id0].add(id1)
            added.add(id1)
    for key, values in id2m0s.items():
        d=dict(zip(list(values), [key]*len(values)))
        id2mid.update(d)
    top.update_column_by_dict(t, 'mass_id', '_id', id2mid, type_=int, in_place=True,
                              insertBefore='m0')
    t.setColFormat('mass_id', '%d')
    t.dropColumns('_id')        
    
    
def _get_pubchem_db():
    db=emzed.db.load_pubchem()
    if not len(db):
        path=r'Z:\emzed2\tables_of_version_2.7.5\pubchem.table'
        db=emzed.io.loadTable(path)
    db=db.filter(db.m0.isNotNone())
    db=db.filter(db.mf.containsOnlyElements('CHNOPS'))
    db=db.filter(db.mf.containsElement('C'))
    db.addEnumeration()
    return db


def add_identification_results(t, db, key2tol, colname, id_col='aid'):
    Table=emzed.core.data_types.Table
    ident =  assign_metabolites(t, db, key2tol)
    id2table=_getid2table(ident, id_col)
    top.update_column_by_dict(t, colname, id_col, id2table, type_=Table, in_place=True)
    

def match_m0(t, db, key2tol):
    t = cc.compare_tables(t, db, key2tol, leftJoin=False)
    t.updateColumn('delta_ppm', 
                t.apply(_delta_ppm,(t.m0__0, t.m0), keep_nones=True), 
                type_=float, format_='%.1f', insertAfter='m0__0')
    t.dropColumns('row_id') # columns from compare_tables
    t.removePostfixes()
#    compare_patterns(t)
    return t

def _delta_ppm(m0, m_measured):
    return (m_measured-m0)/m0*1e6
    
 
def assign_metabolites(t, db, key2tol):
    matches=cc.compare_tables(t, db, key2tol, leftJoin=False)
    edit_matches(t, matches)
    return matches


def _getid2table(t, id_col):
   t.sortBy(id_col)
   keys=list(set(t.getColumn(id_col).values))
   keys.sort()
   return dict(zip(keys, t.splitBy(id_col)))


def edit_matches(ref, t):
    # rename m0 -> possible_m0
    drops=[n for n in ref.getColNames() if not n in ['crid', 'aid']]
    t.updateColumn('delta_ppm', (t.m0__0-t.m0)/t.m0__0*1e6, type_=float, format_='%.1f')
    t.dropColumns(*drops)
#    t.renameColumn('m0', 'possible_m0')
    t.removePostfixes()
    drops=['iupac', 'mw']
    t.dropColumns(*drops)
    
def convert_table2rows(t):
     dbs=[]
     empty_kegg=_empty_kegg(t, 'kegg_db')
     empty_ara=_empty_kegg(t, 'aracyc_db')
     for aid, kegg, ara in zip(t.aid, t.kegg_db, t.aracyc_db):
        if not isinstance(kegg, emzed.core.data_types.Table):
             kegg=empty_kegg.copy()
             kegg.replaceColumn('aid', aid, type_=int)
        if not isinstance(ara, emzed.core.data_types.Table):
             ara=empty_ara.copy()
             ara.replaceColumn('aid', aid, type_=int)
        rows=max([len(kegg), len(ara)])
        kegg=set_same_len(kegg, rows)       
        ara=set_same_len(ara, rows)       
        dbs.append(horizontal_merge_id_tables(kegg, ara))
     db=emzed.utils.stackTables(dbs)
     t=t.fastLeftJoin(db, 'aid')
     _edit_merged(t)
     return t
     
def set_same_len(kegg, rows):
    kegg=kegg.copy()
    aid=kegg.aid.uniqueNotNone()
    num=len(kegg.getColNames())
    empty=[None]*num
    add=rows-len(kegg)
    if add:
        emptys=[empty]*add
        kegg.rows.extend(emptys)
    kegg.updateColumn('aid', aid, type_=int)
    return kegg
    
    
def _empty_kegg(t, db_col):
    # extract one exemplary table
    exp=t.getColumn(db_col).values
    kegg=[x for x in exp if isinstance(x, emzed.core.data_types.Table)][0]
    empty=kegg.buildEmptyClone()
    row=[None]*len(kegg.getColNames())
    empty.rows=[row]
    return empty 

def _horizontal_merge_id_tables(kegg, ara):
    import pdb; pdb.set_trace()
    assert len(kegg)==len(ara)
    ara.addEnumeration('num_rows')
    kegg.addEnumeration('num_rows')
    merged=kegg.fastJoin(ara, 'num_rows')
    pstfxs=merged.supportedPostfixes(['num_rows'])
    for pstfx in pstfxs:
        merged.dropColumns(''.join(['num_rows', pstfx]))
    kegg.dropColumns('num_rows')
    ara.dropColumns('num_rows')
    return merged
    
def  horizontal_merge_id_tables(kegg, ara):
    kegg=kegg.copy()
    ara=ara.copy()
    k=kegg.buildEmptyClone()
    a=ara.buildEmptyClone()
    merged=k.join(a, True)
    rows=[]
    for r1, r2 in zip(kegg.rows, ara.rows):
        r1.extend(r2)
        rows.append(r1)
    merged.rows=rows
    return merged

def _edit_merged(t):
    detection=t.detection.values
    t.dropColumns('kegg_db', 'aracyc_db', 'id__0', 'detection')
    drop_cols=['aid','crid',  'inchi', 'row_id', 'is_in_kegg', 'is_in_hmdb', 'is_in_bioycyc',]
    for pstfx in ['__0', '__1']:
        t.dropColumns(*[''.join([n, pstfx]) for n in drop_cols])
    t.addColumn('detection', detection, type_=tuple)
        

##################################################################################################
# Create a data base from library matches for manual curation

def build_mass_lib_db(mlib, id_col='mass_id', db_cols=['pubchem_db', 'aracyc_db']):  
    colnames=[id_col, 'm0']
    colnames.extend(db_cols)
    t=mlib.extractColumns(*colnames)
    t=t.uniqueRows()
    empty_pc=_get_empty_db_table(t, 'pubchem_db')
    empty_ara=_get_empty_db_table(t, 'aracyc_db')
    subtables=[]
    get=t.getColumn
    for id_, m0,  t1, t2 in zip(*[get(name) for name in colnames]):
        if not t1 and t2:
            continue
        if not t1:
            t1=empty_pc.copy()
        if not t2:
            t2=empty_ara.copy()
        # the matching in db table might be ambiguous since there is more than 1 charge state
        # possible if z==0 
        t1=t1.filter(t1.m0.approxEqual(m0, 0.005))
        t2=t2.filter(t2.m0.approxEqual(m0, 0.005))
        cids = set(t1.cid.values).union(set(t2.cid.values))
        
        if len(cids):
            cids=sorted(list(cids))
            sub=emzed.utils.toTable('cid', cids, type_=int)
        else:
            sub=emzed.utils.toTable('mass_id', [id_], type_=int, format_='%d')
            sub.addColumn('cid', None, type_=int, format_='%d')
        sub.updateColumn('mass_id', id_, type_=int, insertBefore='cid', format_='%d')
        sub=sub.fastLeftJoin(t1, 'cid')
        sub=sub.fastLeftJoin(t2, 'cid')
        subtables.append(sub)
    db=emzed.utils.stackTables(subtables)
    return edit_data_base(db)

def _get_empty_db_table(t, colname):
    for sub in t.getColumn(colname):
        if isinstance(sub, emzed.core.data_types.Table):
            return sub.buildEmptyClone()

def edit_data_base(t):
    t=t.filter(t.cid.isNotNone())
    colnames=['crid', 'row_id', 'delta_ppm']
    t.dropColumns('mass_id__0')
    t.renameColumn('m0__0', 'm0_calc')
    for postfix in t.supportedPostfixes(colnames):
        drop_cols=[''.join([name, postfix]) for name in  colnames]
        t.dropColumns(*drop_cols)
    t.removePostfixes()
    return t.uniqueRows()