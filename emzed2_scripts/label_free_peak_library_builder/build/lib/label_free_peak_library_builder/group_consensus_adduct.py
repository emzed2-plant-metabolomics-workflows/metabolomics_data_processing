# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 10:45:17 2019

@author: pkiefer
"""
from _update_grouping_id import update_consensus_group_id
import emzed
from assign_aids import assign_consensus_adducts


def group_consensus_adduct(lib, group_id='adduct_group', sub_consensus_id='fid', 
                              consensus_id='aid'):
    lib.updateColumn('mz0mean', lib.mzmean.min.group_by(lib.fid), type_=float)
    print 'Identifying consensus groups ...'
    grouped=lib.filter(lib.adduct_assigned_by=='grouping')
    update_consensus_group_id(grouped, group_id, sub_consensus_id, consensus_id)
    print 'Done. '
    adducts=_get_adductsdict()
    assign_consensus_adducts(lib, grouped, adducts)
    print 'updating possible m0 ...'
    _update_possible_m0(lib, adducts)
    _update_charge_state(lib, adducts)

def _update_possible_m0(t, adductsdict):
    def _calc(mz0, adducts, d):
        m0s=[]
        for adduct in adducts:
            delta, z=d[adduct]
            m0s.append(mz0*z-delta)
        return tuple(m0s)
    expr=(t.mz0mean, t.consensus_adducts, adductsdict)
    t.updateColumn('possible_m0', t.apply(_calc, expr, keep_nones=True), type_=tuple, 
                   insertAfter='consensus_adducts')
    
    
def _get_adductsdict():
    t=emzed.adducts.all.toTable()
    d=dict(zip(t.adduct_name, zip(t.mass_shift, t.z)))
    d['M+H-NH3']=(emzed.mass.p-emzed.mass.of('NH3'), 1)
    return d


def unfold_adducts(t):
    pairs=[]
    for ipid, adducts in zip(t.ipid, t.consensus_adducts):
        for adduct in adducts:
            pairs.append((ipid, adduct))
    ipids, adducts=zip(*pairs)
    uf=emzed.utils.toTable('ipid', ipids, type_=int)
    uf.addColumn('consensus_adduct', adducts, type_=str)
    print 'joining ...'
    tt=t.fastJoin(uf, 'ipid')
    print 'done'
    tt.replaceColumn('consensus_adducts', tt.consensus_adduct__0, type_=str)
    tt.dropColumns('consensus_adduct__0','ipid__0')
    tt.renameColumn('consensus_adducts', 'consensus_adduct')
    return tt


def _update_charge_state(t, d):
    def _update(z, adducts, d):
        zs=set([d[add][1] for add in adducts]) 
        if z==0:
           return zs.pop() if len(zs)==1 else 0
        if len (zs)>1: 
            assert adducts==('M+H', 'M+2H', 'M+3H') or adducts==('M-H', 'M-2H', 'M-3H')
            return 0
        return z
    t.updateColumn('z_fid', t.apply(_update, (t.z_fid, t.consensus_adducts, d)), type_=int)
         
               
           
        
        
    