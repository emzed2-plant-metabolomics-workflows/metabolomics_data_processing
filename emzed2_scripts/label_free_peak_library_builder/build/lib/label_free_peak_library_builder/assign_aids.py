# -*- coding: utf-8 -*-
"""
Created on Mon Apr 08 10:49:02 2019

@author: pkiefer
"""
from collections import defaultdict
import itertools as it
import emzed
from basic_processing._adduct_grouping import AdductAssigner
from wtbox2 import utils, table_operations

from time import time
from contextlib import contextmanager


@contextmanager
def timer(msg):
    print
    print "start:", msg
    started = time()
    yield
    needed = time() - started
    print "finished: %s, needed %.1f [s]" % (msg, needed) 
    
    

def assign_consensus_adducts(lib, t, adductsdict):
    mode=_get_mode(lib)
    with timer('building parameter dictionaries'):
        fid2adducts, fid2z, aid2fids, node2mz0=_get_dicts(t, adductsdict)
    with timer('building graphs'):
        aid2graphs=_build_graph_dict(fid2adducts, aid2fids, node2mz0)    
    with timer('finding consistent adduct groups'):
        aid2fid2adducts=_get_aid2fid2adducts(aid2graphs, mode, adductsdict)
    with timer('updating consensus_adducts'):
        _update_consensus_cols(t, aid2fid2adducts)
        _update_aid(t, lib)
        add_lib_consensus_cols(lib, t)
    print 'Finished'


def _get_mode(t):
    try:
        mode=t.polarity.uniqueValue()
    except:
        mode=_read_polarity(t)
        t.updateColumn('polarity', mode, type_=str, insertAfter='peakmap')
    return 'positive_mode' if mode=='+' else 'negative_mode'


def _read_polarity(t):
    polarities=set([pm.polarity for pm in set(t.peakmap.values)])
    if len(t) == 0:
        # handling empty tables
        return []
    assert len(polarities)==1
    return polarities.pop()    


def _get_dicts(t, adductsdict):
    aid2fids=defaultdict(set)
    fid2z={}
    fid2adducts=defaultdict(set)
    node2mz0=dict(zip(t.fid, t.mz0mean))
    for aid, fid, z, adducts in set(zip(t.aid, t.fid, t.z_fid, t.possible_adducts)):
        fid2z[fid]=z
        adducts=set([ad for ad in adducts if adductsdict[ad][1]==z or z==0])
        if len(adducts):
            aid2fids[aid].add(fid)
            fid2adducts[fid].update(adducts)
            if z==0:
                zs=set([adductsdict[ad][1] for ad in adducts])
                if len(zs)==1:
                    fid2z[fid]=zs.pop()
    return fid2adducts, fid2z, aid2fids, node2mz0


def _build_graph_dict(fid2adducts, aid2fids, node2mz0):
    aid2graphs={}
    for aid, fids in aid2fids.items():
        fid2pairs={}
        vertices=defaultdict(list)
        graph=defaultdict(list)
        for fid in fids:
            adducts=fid2adducts[fid]
            pairs=it.product([fid], adducts)
            fid2pairs[fid]=list(pairs)
        node2m0={fid: node2mz0[fid] for fid in fids}
        combs=it.permutations(node2m0.keys(),2)     
        for comb in combs:
            graph[comb[0]].append(comb[1])
            for p1, p2 in it.product(fid2pairs[comb[0]], fid2pairs[comb[1]]):
                vertices[p1[0], p2[0]].append((p1[1], p2[1]))
        if all([len(graph), len(vertices)]):
            aid2graphs[aid]=(graph, vertices, node2m0)
    return aid2graphs


def _get_aid2fid2adducts(aid2graphs, mode, adducts_dict):
    aid2fid2adducts=dict()
    utils.show_progress_bar(aid2graphs.items(), _show_build_fid2adducts, 
                            args=[aid2fid2adducts,adducts_dict, mode], in_place=True) 
    return aid2fid2adducts


def _show_build_fid2adducts(item, aid2fid2adducts,adducts_dict, mode):
    aid, ntuple= item
    graph, vertices, node2mz0=ntuple
    fid2adducts=_build_fid2adducts(graph, vertices, node2mz0, adducts_dict, mode)
    if len(fid2adducts):
            aid2fid2adducts[aid]= fid2adducts
    
    
def _build_fid2adducts(graph, vertices, node2mz0, adducts_dict, mode):
    nodes=set(node2mz0.keys())
    fid2adducts=defaultdict(set)
    # for injection peaks a multitude of adducts can be grouped. However, the number of
    # possible adducts must be <= no of features
    adducts=emzed.adducts.negative.adducts if mode=='-' else emzed.adducts.positive.adducts
    presumption=len(node2mz0)<=len(adducts)
    if not presumption:
        return fid2adducts
    solutions=AdductAssigner.find_consistent_assignments(graph, vertices, nodes, mode)
    for d in solutions:
        if _is_valid(d, node2mz0, adducts_dict):
            for fid, adduct in d.items():
                fid2adducts[fid].add(adduct)
    return fid2adducts


def _is_valid(node2adduct, node2mz0, adducts_dict, mtol=0.008):
    m0s=[]
    for node in node2adduct.keys():
        add=node2adduct[node]
        delta, z=adducts_dict[add]
        m0s.append(node2mz0[node]*z-delta)
    for comb in it.combinations(m0s,2):
        if abs(comb[0]-comb[1])>mtol:
            return False
    return True


def _update_aid(t, lib):
    fid2aid=dict(zip(t.fid, t.aid))
    def _update_aid(fid, d):
        return d[fid] if  d.has_key(fid) else fid
    lib.updateColumn('aid', lib.apply(_update_aid, (lib.fid, fid2aid)), type_=int)


def _update_consensus_cols(t, d):
    def _update_consensus(aid, fid, d):
        if d.has_key(aid):
            return d[aid][fid] if d[aid].has_key(fid) else None
        return None
    t.updateColumn('consensus_adducts', t.apply(_update_consensus, (t.aid, t.fid, d),
                                                keep_nones=True), type_=list,
                   insertAfter='possible_adducts')
    t.updateColumn('consensus_adducts', t.consensus_adducts.apply(tuple), type_=tuple) 
    def _update_aid(aid, fid, d):
        if d.has_key(aid):
            return min(d[aid].keys()) if d[aid].has_key(fid) else fid
        return fid
    t.updateColumn('aid', t.apply(_update_aid, (t.aid, t.fid, d)), type_=int)
    reset_z(t)
    _replace_missing_adducts(t)
    

def add_lib_consensus_cols(lib, t):
    d=dict(zip(t.fid, t.consensus_adducts))
    def _update_consens(aid, d):
        return d[aid] if d.has_key(aid) else None
    lib.updateColumn('consensus_adducts', lib.apply(_update_consens, (lib.fid, d), keep_nones=True), 
                     type_=tuple, insertAfter='possible_adducts')
    reset_z(lib)
    _replace_missing_adducts(lib)
    

def reset_z(t):
   def _replace_z(cons, z, z_by_grouping):
       if cons is not None:
           return z
       return 0 if z_by_grouping else z
   def _replace_z_by(cons, by):
       return True if cons else False
   expr=(t.consensus_adducts, t.z_by_adduct_grouping)
   t.replaceColumn('z_by_adduct_grouping', t.apply(_replace_z_by, expr, keep_nones=True), type_=bool)
   expr=(t.consensus_adducts, t.z, t.z_by_adduct_grouping) 
   t.updateColumn('z', t.apply(_replace_z, expr, keep_nones=True), type_=int)
   t.updateColumn('z_fid', t.z.max.group_by(t.fid), type_=int)
   
   
def _replace_missing_adducts(t):
    z2polarity2adducts={0: {'+':('M+H', 'M+2H', 'M+3H'), '-':('M-H', 'M-2H', 'M-3H')},
                        1: {'+':('M+H',), '-':('M-H',)}, 
                        2: {'+':('M+2H',), '-':('M-2H',)},
                        3: {'+':('M+3H',), '-':('M-3H',)}}
    def _update_missing(con, z, polarity, d):
        return d[z][polarity] if con is None else con
    t.updateColumn('consensus_adducts', t.apply(_update_missing, 
                        (t.consensus_adducts, t.z_fid, t.polarity, z2polarity2adducts), keep_nones=True), type_=tuple)            
    
#######################################################
