# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 13:16:00 2019

@author: pkiefer
"""

import numpy as np
from collections import defaultdict
from wtbox2 import table_operations as top

def normalize_peaks(tables, bioamount_col='biomass', sample_name_col='sample_name', 
                    tic_normalize=True):
    area_col='area'
    for t in tables:
        exp=t.getColumn
        tic_normalize_peaks(t, tic_normalize)
        area_col='norm_area_by_tic'
        t.updateColumn('norm_peak_area', exp(area_col)/exp(bioamount_col), type_=float, 
                       format_='%.2e', insertAfter='unit')


def tic_normalize_peaks(t, tic_normalize):
    if len(set(t.peakmap.values))==1 and tic_normalize:
        _tic_normalize_peaks(t)
    else:
       t.updateColumn('tic_method', None, type_=str, insertAfter='rmse')
       t.updateColumn('tic_area', None, type_=float, format_='%.1e', insertAfter='tic_method')
       t.updateColumn('norm_area_by_tic', None, type_=float, format_='%.2e', insertAfter='tic_area') 


def  _tic_normalize_peaks(t):
    pm=t.peakmap.uniqueValue()
    area, rmse,  method, smoothed = get_tic_integrate_values(pm)
    t.updateColumn('tic_method', method, type_=str, insertAfter='rmse')
    t.updateColumn('tic_area', area, type_=float, format_='%.1e', insertAfter='tic_method')
    t.updateColumn('norm_area_by_tic', t.area/t.tic_area, type_=float, format_='%.2e', 
                   insertAfter='tic_area')



def get_tic_integrate_values(pm, rtmin=None, rtmax=None):
    rts=pm.allRts()
    ints=[spec.intensityInRange(*spec.mzRange()).tolist() for spec in pm.spectra]
    return float(np.trapz(ints, rts)),  0.0, 'trapez', None



##############################################################################################

def update_averaged_group_areas(t, group_ids=['crid', 'sample_name'], value_col='norm_peak_area', 
                                norm_col_name='norm_crid_area', weighted=True):
    exp=t.getColumn
    gid_tuple=[exp(gid) for gid in group_ids]                                
    t.updateColumn('_gid', zip(*gid_tuple), type_=tuple)
    gid2average=_calc_gid2average(t, value_col, weighted)
    
    top.update_column_by_dict(t, norm_col_name, '_gid', gid2average, type_=float, in_place=True,
                              insertAfter=value_col)
    t.dropColumns('_gid')                              



def _calc_gid2average(t, value_col, weighted):
    # since each adduct is listed in a separate row, the same individual peak might be present in 
    # more than one row and henve be overepresented. We therefore monitor collected values via
    # counted
    counted=set([])
    d=defaultdict(list)
    gid2average=dict()
    exp=t.getColumn
    for gid, value, ipid in zip(t._gid, exp(value_col), t.ipid): 
        if not counted.intersection(set([ipid])):
            d[gid].append(value)
    for key, values in d.items():
        weights=np.array(values)/np.sum(values) if weighted else None
        gid2average[key]=np.average(values, weights=weights)
    return gid2average