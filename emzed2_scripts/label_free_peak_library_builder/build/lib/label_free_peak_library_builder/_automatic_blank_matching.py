# -*- coding: utf-8 -*-
"""
Created on Thu Dec 05 15:22:42 2019

@author: pkiefer
"""

import os
import emzed
import re
import numpy as np
from glob import glob
from collections import defaultdict

def automatic_blank_matching(samples):
    sample_paths=_get_sample_pathes(samples)
    blank_paths=_get_blank_pathes(samples)  
    s_inj_nos, b_inj_nos = get_injection_indices(sample_paths, blank_paths)
    return match_sample_and_blank(sample_paths, blank_paths, s_inj_nos, b_inj_nos)

#-------------------------------------------------------

def get_injection_indices(sample_pathes, blank_pathes):
    no=len(sample_pathes)
    sample_item, sample_nos=potential_fields(sample_pathes, no)
    blank_item, blank_nos=potential_fields(blank_pathes, no)
    return select_fields(sample_item, sample_nos, blank_item, blank_nos)
    

def potential_fields(pathes, no, pattern='[0-9]{1,8}'):
    filenames=[os.path.basename(p) for p in pathes]
    i2fields=get_index2fields(filenames, seps='[_\-\s]', pattern=pattern)
    indices=_select_indices(i2fields, no)
    fields_groups = zip(*[i2fields[i] for i in indices])
    if len(fields_groups):
        choice_items= _build_choice_item(filenames[0], fields_groups[0])
        return choice_items, fields_groups
    return  [''], [[]]

def select_fields(sample_item, sample_nos, blank_item, blank_nos):
    title='select the correct injection order identifyer for samples and blanks'
    db=emzed.gui.DialogBuilder(title)
    si, bi=db.addChoice('select sample injection label', sample_item, 
                        help='<< i >> labels number of injection in sample file name')\
            .addChoice('select blank injection label', blank_item,
                       help='<< i >> labels number of injection in blank file name')\
            .show()
    try:
        return zip(*sample_nos)[si], zip(*blank_nos)[bi]
    except:
        return [], []

#-----------------------------------

def match_sample_and_blank(sample_pathes, blank_pathes, s_injects, b_injects):
    if not all([x is not None for x in [ s_injects, b_injects]]):
        return False, None
    # extract and convert variables
    s_injects=_string2int(s_injects)
    b_injects=_string2int(b_injects)
    sample2blank={}
    for i, name in zip(s_injects, sample_pathes):
        j=best_match(i, b_injects)
        if j>=0:
            sample2blank[name]=blank_pathes[j]            
        else:
            assert False, 'blank is missing for sample %s' %name
            
    if set(sample_pathes)-set(sample2blank.keys())==set([]):
        print '\n'.join([_get_line(pair) for pair in sample2blank.items()])
        correct=emzed.gui.askYesNo('is the sample -> blank assignment shown in the console correct?')
        return correct, sample2blank
    else:
        return False, None


#------------------------        
# helper funs
def _get_sample_pathes(samples):
    return [s.meta['full_source'] for s in samples]

    
def _get_blank_pathes(samples):
    # 1. find all peakmaps in dir_
    dir_=_get_sample_dir(samples)
    pathes=[]
    exts=['*.mzML', '*.mzXML']
    for ext in exts:
        path=os.path.join(dir_, ext)
        pathes.extend(glob(path))
    #2. get all samples pathes
    spathes=set([s.meta['full_source'] for s in samples])
    pathes=set(pathes)-set(spathes)
    #3. the path name must contain a blank_label 
    pathes=_extract_blank_pathes(pathes)
    return pathes

def _get_sample_dir(samples):
    pathes=[s.meta['full_source'] for s in samples]
    dirs=set([os.path.dirname(p) for  p in pathes])
    if len(dirs)==1:
        return dirs.pop()


def _extract_blank_pathes(pathes):
    patterns=['blank', 'Blank', 'BLANK']
    selected=[]
    for path in pathes:
        for pattern in patterns:
            if re.search(pattern, path):
                selected.append(path)
    return selected

#-------------------------------------------------

def get_index2fields(filenames, seps='[_\-\s]', pattern='[0-9]{1,8}'):
    index2fields=defaultdict(list)
    for name in filenames:
        fields=re.split(seps, name)
        
        for i,field in enumerate(fields):
            if re.match(pattern, field):
                index2fields[i].append(field)
    return index2fields


def _select_indices(index2fields, number):
    # there must be at least as many files as given by length: reason for blank
    # selection there might be more blanks than selected samples
    indices = [index for index, fields in index2fields.items() if len(fields)>=number]
    return [i for i in indices if _is_potential_injection_number(index2fields[i])]


def _is_potential_injection_number(values):
    try:
        numbers=[int(v) for v  in values]
        return _is_monotonic(numbers)
    except:
        return False
                    
def _is_monotonic(values):
    values=sorted(values)
    return all(np.diff(values))

#---

def _build_choice_item(filename, fields):
    items=[]
    for field in fields:
        match=re.search(field, filename)        
        i=match.start()
        j=match.end() # since pattern ends with not a number
        item=' << '.join([filename[:i], filename[i:j]])
        item=' >> '.join([item, filename[j:]])
        items.append(item)
    return items

def _string2int(values):
    try:
        return [int(v) for v in values]
    except:
        return []
    

def best_match(s_index, b_indices):
    diff=np.array(b_indices)-s_index
    diff=np.array([_replace(v) for v in diff])
    try:
        hit= np.nanmax(diff)
        indices=np.where(diff==hit)
        i=int(indices[0])
        return i
    except:
        pass


def _replace(v):
    return v if v<0 else np.nan

def _get_line(pair):
    return ' -> '.join([os.path.basename(p) for p in pair])    