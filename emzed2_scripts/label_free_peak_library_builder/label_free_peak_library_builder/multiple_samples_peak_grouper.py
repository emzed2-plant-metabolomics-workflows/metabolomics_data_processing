# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 10:12:19 2019

@author: pkiefer
"""

from wtbox2 import _shift_rt_windows as srw
from wtbox2 import collect_and_compare as cc
from wtbox2 import utils, table_operations
from collections import defaultdict, Counter
from itertools import combinations
from group_consensus_feature import group_consensus_features
from group_consensus_adduct import group_consensus_adduct
#from normalize_peaks import update_averaged_group_areas
from contextlib import contextmanager
from copy import copy
import emzed
import numpy as np
import time
import sys


@contextmanager
def timer(msg):
    print
    print "start:", msg
    started = time.time()
    yield
    needed = time.time() - started
    print "finished: %s, needed %.1f [s]" % (msg, needed) 



def samples_peaks_grouper(tables, mztol, max_rt_diff, min_hits=2, collection=None):
    update_missing_columns(tables)
    with timer('determine rttol'):
        rttol=determine_rttol(tables, mztol, max_rt_diff)
        if isinstance(rttol, float):
            print 'rttol was set to %.2f s' %rttol
    # 1. find peaks overlapping for given rttol in all samples
    with timer('find overlapping peaks'):
        source2multiple=source2multiple_peaks(tables, mztol, rttol)
        update_overlapping_id(tables, source2multiple)
    # 2. merge multiple peaks 
    oid2params=build_oid2merged_peaks(tables, source2multiple)
    # collect rtmins of all peaks
    oid2rtmins=build_oid2rtmins(tables)
#    return oid2rtmins, oid2params
    if not collection:
        ref, tabs=define_ref_table(tables)
        collection=ref.copy()
    else:
        tabs=tables
    with timer('collecting peaks from tables'):
        lastcent=-1
        i=0
        for t in tabs:
            selected = select_peaks(ref, t, oid2params, oid2rtmins, rttol, mztol)
            selected=selected.extractColumns(*collection.getColNames())
            collection=emzed.utils.stackTables([collection, selected])
            ref=build_ref_from_collection(collection)
            lastcent=show_progress(i, len(tabs), lastcent)
            i+=1
#        collection=select_peaks_from_collection(collection, min_hits)
        update_mean_values(collection, mztol)
        # underestimation of rttol can lead to split of unique peaks !
        # 
        collection=merge_convergent_pids(collection, rttol, mztol)
    with timer('assign consensus fid'):
        group_consensus_features(collection, mztol)
        # we remove all feature which do not fullfill the min_hits criterium
        # by default remove all unique feautures
        collection=select_peaks_from_collection(collection, min_hits)
#        return collection
    with timer('assign consensus aid'):
        group_consensus_adduct(collection)
    return edit_collection(collection)


def show_progress(i, length, lastcent):
    cent = ((i + 1) * 20) / length
    if cent != lastcent:
        print cent * 5,
        try:
            sys.stdout.flush()
        except IOError:
            # might happen on win cmd console
            pass
        lastcent = cent
    return lastcent


def update_missing_columns(tables):
    m=0
    for t in tables:
        colname=t.getColNames()[0]
        t.updateColumn('ipid', range(m, len(t)+m), type_=int, format_='%d', insertAfter=colname)
        t.updateColumn('mzmean', t.mz, type_=float, insertAfter='mz')
        t.updateColumn('rtmean', t.rt, type_=float, insertAfter='rt')
        m+=len(t)


def define_ref_table(tables):
    """ Peaks with maximum number of peaks
    """
    x=[len(t) for t in tables]
    i=x.index(max(x))
    tabs=tables[:i]
    tabs.extend(tables[i+1:])
    ref=tables[i].copy()
    ref.addEnumeration('pid', insertBefore='ipid')
    return ref , tabs
    
###--------------------------------------------------------------------------
def source2multiple_peaks(tables, mztol, rttol):
    d=defaultdict(dict)
    utils.show_progress_bar(tables, _update_overlapping, args=[d, rttol, mztol], in_place=True)
    return d

def _update_overlapping(t, d, rttol, mztol):
    source=t.source.uniqueValue() if len(t) else None
    id2overlaps, uniques=find_overlapping_peaks(t, rttol, mztol)
    d[source]['overlapping']=id2overlaps
    d[source]['unique']=uniques


def find_overlapping_peaks(ref, delta_rt, mztol, id_col='ipid'):
    # determine mztol from average mz window width
    if ref.hasColumn('idt'):
        ref.dropColumns('idt')
    ref.addEnumeration('idt')
    t=ref.copy()
    comp=cc.compare_tables(ref, t, {'mz':mztol, 'rt': delta_rt}, False)
    comp.sortBy('rt')
    # now we start with increasing rt values and hence id1 is always peak with 
    # lower rt if id1 != od2    
    comp.updateColumn('overlap', comp.apply(_select,(comp.idt, comp.idt__0)), type_=bool)
    overlaps=defaultdict(set)
    id2oid={}
    not_unique=set([])
    failed=set([])
    id_0='__'.join([id_col, '0'])
    for id1, rt1, id2, rt2, overlap in zip(comp.getColumn(id_col), comp.rt, 
                                 comp.getColumn(id_0), comp.rt__0, comp.overlap):
        ids=[id1, id2]
        if overlap:
            if id2oid.has_key(id1) and id2oid.has_key(id2):
                # this can happen as rare event by e.g. remaining satelite peaks 
                if not id2oid[id1]==id2oid[id2]:
                    remove=set([id1, walk_back(id1, id2oid), id2, walk_back(id2, id2oid)])
                    failed.update(remove)
                continue
            if id2oid.has_key(id1):
                key=walk_back(id1, id2oid)
                overlaps[key].update(set(ids))
                id2oid[id2]=key
            elif id2oid.has_key(id2):
                key=walk_back(id2, id2oid)
                overlaps[key].update(set(ids))
                id2oid[id1]=key
            else:    
                overlaps[id1].update(set(ids))
                id2oid.update({id_: id1 for id_ in ids})
                not_unique.update(set(ids))
    _merge_overlaps(overlaps)  
    uniques=set(t.getColumn(id_col).values)-not_unique
    ref.dropColumns('idt')
    if len(failed):
        print 'In total %d out of %d peaks were not unique but showed ambiguous overlap.'
        threshold=float(len(failed))/len(ref)
        if threshold<0.01:
            print 'peaks will be removed to keep data consistent'
            t=utils.fast_isIn_filter(t, 'ipid', failed, not_in=True)
            print 'table will be reprocessed ...'
            find_overlapping_peaks(ref, delta_rt, mztol, id_col)
        else:
            emzed.gui.showWarning('Too many critical peaks: Please check peaks and'\
            'remove peaks manualy')
            check=utils.fast_isIn_filter(t, 'ipid', failed)
            emzed.gui.inspect(check)
            removed=failed-set(check.ipid.values)
            do=emzed.gui.askYesNo('Do you want to remove peaks %s?' %', '.join(list[removed]))
            if do:
                t=utils.fast_isIn_filter(t, 'ipid', removed, not_in=True)
                print 'table will be reprocessed ...'
                find_overlapping_peaks(ref, delta_rt, mztol, id_col)
    return overlaps, uniques


def walk_back(key, d):
    counts=0
    while True:
        value=d[key]
        if value==key:
            return value
        key=value
        counts+=1
        if counts>100:
            assert False, 'This massage should never occur !!'

        
def _select(id1, id2):
    return True if id1!=id2 else False


def _merge_overlaps(d):
    keys=sorted(d.keys())
    while len(keys):
        key=keys.pop(0)
        if d.has_key(key):
            collect_keys(d, key)


def collect_keys(d, key):
    """ 
    Graph like Function to collect elements of a path where all connected values are assigned to starting 
    value 'key';  d=defaultdict(set). d[key] contains a 
    set of values. For all values that are also keys of d, d[key] is collected and 
    checked whether they are keys until only non key values remain.
    """
    processed_keys=set([key])
    keys=copy(d[key])-processed_keys
    while len(keys):
        key_=keys.pop()
        processed_keys.add(key_)
        d[key].add(key_)
        if d.has_key(key_):
            values=d.pop(key_)
            keys.update(values)

##-----------------------------------------------------------------------------------------------

def update_overlapping_id(tables, oid2overlap):
    for t in tables:
        try:
           source=t.source.uniqueValue()
        except:
           _show_missing_warning(t) 
           t.updateColumn('oid', [], type_=int)
           continue
        _update_oid(t, oid2overlap[source]['overlapping'], 'ipid')
        
        
        

def _update_oid(t, d, id_col):
    id2oid={}
    for key, values in d.items():
        for v in values:
            id2oid[v]=key
    def _update(key, d):
        return d [key] if d.has_key(key) else key
    t.updateColumn('oid', t.apply(_update, (t.getColumn(id_col), id2oid)), type_=int)

#-----------------------------------------------------------------------------------

def build_oid2rtmins(tables):
    oid2rtmins=defaultdict(list)
    for t in tables:
        for oid, params in zip(t.oid, t.params):
            oid2rtmins[oid].append(min(params[0]))
    # since rtmins are required in ascending order ...
    [v.sort() for v in oid2rtmins.values()]
    return oid2rtmins
            
##----------------------------------------------------------------------------------------------

def build_oid2merged_peaks(tables, source2multiple, id_col='ipid'):
    oid2params={}
    for t in tables:
        try:
            source=t.source.uniqueValue()
        except:
           _show_missing_warning(t) 
           continue
        d=get_merged_params(t, source2multiple[source]['overlapping'], id_col)
        oid2params.update(d)
        
            
    return oid2params
    
def _show_missing_warning(t):
    print 'WARNING: column `source` is missing in table. Table with unique id '\
            '%s \n will be ignored!'  %t.uniqueId()
 
def get_merged_params(t, id2overlap, id_col):
    merged=srw.merge_overlapping_peaks(t, id2overlap, id_col)
    extract_chromatograms(merged)
    return dict(zip(merged.oid, merged.params))


def extract_chromatograms(t):
    expr=(t.peakmap, t.mzmin, t.mzmax, t.rtmin, t.rtmax)
    t.updateColumn('params', t.apply(_extract_chromatogram, expr), type_=t.getColType('params'))


def _extract_chromatogram(pm, mzmin, mzmax, rtmin, rtmax):
    return pm.chromatogram(mzmin, mzmax, rtmin, rtmax)    

###############################################################################################

def determine_rttol(tables, mztol, max_rt_diff):
    key2tol={'mz':mztol, 'rt': max_rt_diff}
    upts=_get_unique_peaks_tables(tables, key2tol)
    ref, tabs=define_ref_table(upts)
    d=defaultdict(set)
    rttols=[]
    for t in tabs:
        comp=cc.compare_tables(ref, t, key2tol, False)
        for id_, rt1, rt2 in zip(comp.ipid, comp.rt, comp.rt__0):
            d[id_].add(rt1)
            d[id_].add(rt2)
    for rts in d.values():
        if len(rts)>=0.8*len(tables):
            rttols.append(max(rts)-min(rts))
    # rttol must be > 0 for abs_tol in join therefore we chose a value close t0 0.0
    return np.percentile(rttols, 75) if len(rttols) else 1e-10
        
    
def _get_unique_peaks_tables(tables, key2tol):
    upts=[]
    for t in tables:
        comp=cc.compare_tables(t,t,key2tol, False)
        x=Counter(comp.ipid.values)
        ids=[id_ for id_, no in x.items() if no==1]
        upt=utils.fast_isIn_filter(t, 'ipid', ids)
        upts.append(upt)
    return upts 
###############################################################################################

def select_peaks(ref, t, oid2params, oid2rtmins, rttol, mztol, id_col='ipid'):
    _check_id_col(ref, id_col)
    _check_id_col(t, id_col)
    selected=[]
    key2tol={'mzmean': mztol, 'rtmean': rttol}
    comp = cc.compare_tables(t, ref, key2tol, True)
    new=extract_new_peaks(comp, ref, t, id_col)
    selected.append(new)
    common=select_common_peaks(comp, t, oid2params, oid2rtmins, id_col, rttol)
    selected.append(common)
    return emzed.utils.stackTables(selected)
    

def _check_id_col(t, id_col):
    exp=t.getColumn
    assert len(set(t.getColumn(id_col).values))==len(t),'ids of column'\
            ' %s in table %s are not unique!!' %(id_col, exp('source').uniqueValue())


def extract_new_peaks(comp, ref, t, id_col):
    exp=comp.getColumn(id_col)
    exp0=comp.getColumn('__'.join([id_col, '0']))
    new_pids=[p[0] for p in zip(exp, exp0) if p[1] is None]
    select=utils.fast_isIn_filter(t, id_col, new_pids)
    pid_min= ref.pid.max() +1 if len(ref) else 0
    pid_max=pid_min + len(select)
    select.updateColumn('pid', range(pid_min, pid_max), 
                         type_=int, format_='%d', insertBefore='ipid')
    select.updateColumn('rtmean', select.rt.mean.group_by(select.pid), 
                        type_=float, insertAfter='rt')
    select.updateColumn('mzmean', select.mz.mean.group_by(select.pid), type_=float, insertAfter='mz')
    return select
                         
#-------------------------------------------------------------------------------------------------

def select_common_peaks(comp, t, oid2params, oid2rtmins, id_col, rttol):
    colname='__'.join([id_col, '0'])
    comp=comp.filter(comp.getColumn(colname).isNotNone()==True)
    ipid2pid={}
    uniques, ambiguous = _classify_matches(comp, id_col)
    _update_id2pid(uniques, ipid2pid)    
    if len(ambiguous):
        ta=ambiguous
        ta=_add_params_pair(ta, oid2params)
        #  we add corresponging peaks rtmin values of each params in params_pair
        update_rtmins(ta, oid2rtmins)
        # since update rtmins requires ref_params rts of the original peak (oid2min_rts refers to
        # original params, we adapt ref params rt values after rtmin update
        _adapt_ref_params_rt(ta)
        determine_rt_shift(ta, rttol)
        selected=filter_ambiguous(ta)
        _update_id2pid(selected, ipid2pid)    
    return _get_matches(t, ipid2pid)
    
#---
                        
def _classify_matches(t, id_col):
    # unique matches result in single counts of id_col values
    expr=t.getColumn(id_col)
    expr0=t.getColumn('__'.join([id_col, '0']))
    id2counts=Counter(expr.values)
    id02counts=Counter(expr0.values)
    def _check(d, d0, key, key0):
        summe=d[key]+d0[key0]
        return True if summe==2 else False
    t.updateColumn('unique_match', t.apply(_check, (id2counts, id02counts, expr, expr0)), 
                   type_=bool)
    if len(set(t.unique_match.values))==1:
        unique=set(t.unique_match.values).pop()
        return (t, t.buildEmptyClone()) if unique else (t.buildEmptyClone(), t)
    tables=t.splitBy('unique_match')
    if not len(tables): # t is a table of len [0]
        return [t,t]
    def _sort(t):
        return 0 if t.unique_match.uniqueValue()==True else 1
    return sorted(tables, key= _sort)


def _update_id2pid(t, ipid2pid):
    d=dict(zip(t.ipid.values, t.pid__0.values))
    ipid2pid.update(d)


def _add_params_pair(t, oid2params):
    """ params and params__0 are shaped to the same rt width. To this end the larger peak is
        detected and the narrower is enlarged to the same length from peakmap. In case the peak 
        is part of a merged peak the corroponding merged params are selected from oid 2 params.
        This improves matching.
    """
    exp=(t.params, t.params__0, t.oid, t.oid__0, oid2params)
    t.updateColumn('params_pair', t.apply(_select_params, exp), type_=tuple)
    t.updateColumn('_i', t.apply(_find_largest_peak, (t.params_pair,)), type_=int)
    subs=[]
    for sub in t.splitBy('_i'):
        if sub._i.uniqueValue(): # i==1
            # ref_params -> params_pair[1]
            exp=(sub.params_pair, sub.peakmap, sub.mzmin, sub.mzmax, sub._i)
        else:
            exp=(sub.params_pair, sub.peakmap__0, sub.mzmin__0, sub.mzmax__0, sub._i)
        sub.replaceColumn('params_pair', sub.apply(_enlarge_params, exp), type_=tuple)
        subs.append(sub)
    t=emzed.utils.stackTables(subs)
    return t
        

def _select_params(params1, params2, oid1, oid2, oid2params):
    params1=oid2params[oid1] if oid2params.has_key(oid1) else params1
    params2=oid2params[oid2] if oid2params.has_key(oid2) else params2
    return params1, params2


def _find_largest_peak(params_pair):
    lens=[len(params_pair[0][0]), len(params_pair[1][0])]    
    return lens.index(max(lens))


def _enlarge_params(params_pair, pm, mzmin, mzmax, i):
    # i indicates index of ref_params
    params, ref_params=params_pair if i else params_pair[::-1]
    rts_r=ref_params[0]
    rts_p=params[0]
    drt=max(rts_r)-min(rts_r)+min(rts_p)-max(rts_p)
    if drt>=0:
        delta_l=abs(min(rts_p)-min(rts_r))
        delta_r=abs(max(rts_p)-max(rts_r))
    else:
        delta_r=abs(min(rts_p)-min(rts_r))
        delta_l=abs(max(rts_p)-max(rts_r))
    frac_l=delta_l/(delta_l+delta_r)
    frac_r=1-frac_l
    rtmin=min(rts_p)-abs(drt) *frac_l
    rtmax=max(rts_p) + abs(drt)*frac_r
    adapted=pm.chromatogram(mzmin, mzmax, rtmin, rtmax)
    return (adapted, ref_params) if i else (ref_params, adapted)


#---
def _adapt_ref_params_rt(t):
    exp=(t.params_pair, t.rt__0, t.rtmean__0)
    t.replaceColumn('params_pair', t.apply(_adapt_ref_rts, exp), type_=tuple)


def _adapt_ref_rts(params_pair, rt, rtmean):
    params, ref_params=params_pair
    rts, ints=ref_params
    delta=rtmean-rt
    rts=rts+delta
    return (params, (rts, ints))
    

#---
def update_rtmins(t, oid2rtmins):
    """ returns a list of start_points of each peak. oid2rtmin refers to original rtmins.
        However in case of the reference table we use the mean values.
    """
    t.updateColumn('rtmins', t.apply(_update_rtmins,(oid2rtmins, t.oid, t.rtmin)), type_=tuple, 
                   insertAfter='rtmean')
    t.updateColumn('rtmins', t.apply(_replace_min_rtmin, (t.rtmins, t.params_pair, 0)), type_=tuple)   
    t.updateColumn('rtmins0', 
        t.apply(_update_rtmins,(oid2rtmins, t.oid__0, t.rtmin__0)), type_=tuple)
    # column with postfix__0 refers to reference: we have to adapt rts to rtmean
    t.updateColumn('rtmins0', t.apply(_replace_min_rtmin, (t.rtmins0, t.params_pair, 1)), type_=tuple)              
    t.updateColumn('rtmins0', t.apply(_adapt_rtmins, (t.rtmins0, t.rt__0, t.rtmean__0)), type_=tuple)
    t.updateColumn('rtmins_pair', zip(t.rtmins.values, t.rtmins0.values), type_=tuple)
    t.dropColumns('rtmins', 'rtmins0')

def _update_rtmins(d, key, rtmin):
    return sorted(d[key]) if d.has_key(key) else [rtmin]


def _adapt_rtmins(rtmins, rt, rtmean):
    delta=rtmean-rt
    return [rt_+delta for rt_ in rtmins]


def _replace_min_rtmin(rtmins, params_pair, i):
    # if peak was enlarged to the left side the first rtmin value changed. we replace it ...
        rts=params_pair[i][0]
        rtmins[0]=rts[0]
        return rtmins

#---

def determine_rt_shift(t, rttol):
    exp=(t.params_pair, t.rtmins_pair, rttol)
    t.updateColumn('rt_shift', t.apply(_determine_local_rt_shift, exp), type_=float)
    

def _determine_local_rt_shift(params_pair, rtmins_pair, rttol):
    i=_get_ref_index(params_pair, rtmins_pair)
    params, ref_params=params_pair if i else params_pair[::-1]
    rtmins=rtmins_pair[i]
    delta_rt=srw._determine_rt_shift(params, ref_params,rttol, rtmins)
    # limit shift to maximal allowed rt_shift
    delta_rt=delta_rt if delta_rt <= rttol else rttol
    return -1*delta_rt if i else delta_rt
    

def _get_ref_index(params_pair, rtmins_pair):
    l1, l2=[len(p[0]) for p in params_pair]
    return 0 if l1>l2 else 1


#---

def filter_ambiguous(t):
    _group_matches(t)
    _label_monotonic_grouping(t)
    t=t.filter(t.monotonic==True)
    t.updateColumn('rt_diff', (t.rt+t.rt_shift-t.rt__0).apply(abs), type_=float)
    t.updateColumn('rt_diff_min1', t.rt_diff.min.group_by(t.ipid), type_=float)
    t.updateColumn('rt_diff_min2', t.rt_diff.min.group_by(t.ipid__0), type_=float)
    def _det_rtdiffmin(v1, v2):
        return min([v1, v2])
    t.updateColumn('rt_diff_min', t.apply(_det_rtdiffmin, (t.rt_diff_min1, t.rt_diff_min2)), type_=float)
    t.dropColumns('rt_diff_min1', 'rt_diff_min2')
    t=t.filter(t.rt_diff==t.rt_diff_min)
    return t


def _group_matches(t):
    t.addEnumeration('uid')
    ntuples=zip(t.uid, t.pid__0, t.oid, t.oid__0, t.rt, t.rt__0)
    pid2oids=defaultdict(set)
    oid2pids=defaultdict(set)
    pid2uid2pairs=defaultdict(dict)
    for id_, pid, oid0, oid1, rt0, rt1 in ntuples:
        pid2oids[pid].update(set([oid0, oid1]))
        oid2pids[oid0].add(pid)
        oid2pids[oid1].add(pid)
        pid2uid2pairs[pid][id_]=(rt0, rt1)
    d = build_groups(pid2oids, oid2pids)
    table_operations.update_column_by_dict(t, 'gid', 'pid__0', d, type_=int, in_place=True)


def build_groups(pid2oids, oid2pids):
    gid=dict()
    index=0
    for oids in pid2oids.values():
        pids=set([])
        for oid in oids:
            pids.update(oid2pids[oid])
        pids=list(pids)
        gid.update(dict(zip(pids, [index]*len(pids))))
        index+=1
    return gid 
    

def _label_monotonic_grouping(t):
    """ if more than one peak grouping is possible rts of of grouped peak must not overlapp.
        e.g. s1: rt11, rt12 and s2 rt21, rt22 with rtx1 < rtx2 x in [1, 2]. the grouing
        [rt12, r21], [rt11, rt22] is not allowed since it would inverse the order of the 
        peaks in the sample.
    """
    keeps=set([])
    for sub in t.splitBy('gid'):
        d=dict(zip(zip(sub.rt.values, sub.rt__0.values), sub.uid.values))
        keeps=_build_possible(d, keeps)
    def _update(v, values):
        return False if len(set([v])-values) else True
    t.updateColumn('monotonic', t.apply(_update, (t.uid, keeps)), type_=bool)
     
     
def _build_possible(rt_pairs2row, keep):
    rt_pairs=rt_pairs2row.keys()
    rt_pairs.sort()
    if keep is None:
        keep=set([])
    rts1, rts2= zip(*rt_pairs)
    r_len=min(len(set(rts1)), len(set(rts2)))
    if r_len<2:
        keep.update(set(rt_pairs2row.values()))
        return keep
    for comb in list(combinations(rt_pairs, r_len)):
        diffs=np.diff(zip(*comb))
        req1=diffs<0
        req2=diffs>0
        if req1.all() or req2.all():
            keep.update(rt_pairs2row[p] for p in comb)
        else:
            pass
    return keep
    

def _get_matches(t, ipid2pid):
    matches=utils.fast_isIn_filter(t, 'ipid', ipid2pid.keys())
    def _update(d, key):
        return d[key]
    matches.updateColumn('pid', matches.apply(_update, (ipid2pid, matches.ipid)), type_=int, 
                         format_='%d', insertBefore='ipid')
    m=matches
    m.updateColumn('rtmean', m.rt.mean.group_by(m.pid), type_=float, insertAfter='rt')
    m.updateColumn('mzmean', m.mz.mean.group_by(m.pid), type_=float, insertAfter='mz')
    return matches
    
#########################################################################################


def build_ref_from_collection(collection):
    # 1. update mean values
    c=collection
    c.updateColumn('rtmean', c.rt.mean.group_by(c.pid), type_=float, insertAfter='rt')
    c.updateColumn('mzmean', c.mz.mean.group_by(c.pid), type_=float, insertAfter='mz')
    # 2. for each pid select most intense peak
    c.updateColumn('oid_area', c.area.sum.group_by(c.oid), type_=float)
    c.updateColumn('max_area', c.oid_area.max.group_by(c.pid), type_=float)
    sel=c.filter(c.oid_area==c.max_area)
    c.dropColumns('max_area', 'oid_area')
    return sel

##########################################################################################
def select_peaks_from_collection(t, min_hits, id_col='fid'):
    check=t.extractColumns(id_col, 'sample_name')
    check=check.uniqueRows()
    id2counts=Counter(check.getColumn(id_col).values)
    table_operations.update_column_by_dict(t, 'counts', id_col, id2counts, type_=int, 
                                           in_place=True)
    t=t.filter(t.counts>=min_hits)
#    t.dropColumns('counts')                                      
    return t

##################################################################################################

def update_mean_values(collection, mztol):
    c=collection
    c.updateColumn('rtmean', c.rt.mean.group_by(c.pid), type_=float, insertAfter='rt')
    c.updateColumn('mzmean', c.mz.mean.group_by(c.pid), type_=float, insertAfter='mz')
    c.replaceColumn('mzmin', c.mz-mztol, type_=float)
    c.replaceColumn('mzmax', c.mz+mztol, type_=float)
    



def merge_convergent_pids(t, rttol, mztol):
    """ merge peaks together were a split of unique peak occured due to rttol estimation
    """
    key2tol={'mzmean': mztol, 'rtmean': rttol*0.75}
    pid2values=dict(zip(t.pid, zip(t.mzmean, t.rtmean)))
    pids=sorted(pid2values.keys())
    pt=emzed.utils.toTable('pid', pids, type_=int, format_='%d')
    def _get_values(d, key, i):
        return d[key][i]
    pt.updateColumn('mzmean', pt.apply(_get_values, (pid2values, pt.pid, 0)), type_=float)
    pt.updateColumn('rtmean', pt.apply(_get_values, (pid2values, pt.pid, 1)), type_=float)
    added=set()
    pid2pids=defaultdict(set)    
    comp=cc.compare_tables(pt, pt, key2tol, False)
    expr=zip(comp.pid,comp.pid__0)
    for pid1, pid2, in expr:
        if not pid1==pid2:
            req=len(set([pid1])-added)>0
            if req:
               pid2pids[pid1].add(pid2)
               added.add(pid2)
    pid2pid={}
    pid2names=_get_pid2names(t)
    for key, values in pid2pids.items():
        for pid in values:
            if not len(pid2names[key].intersection(pid2names[pid])):
                pid2pid[pid]=key
    def _update_pid(pid, d):
        return d[pid] if d.has_key(pid) else pid
    t.updateColumn('pid', t.apply(_update_pid, (t.pid, pid2pid)), type_=int)
    t.updateColumn('rtmean', t.rt.mean.group_by(t.pid), type_=float)
    t.updateColumn('mzmean', t.mz.mean.group_by(t.pid), type_=float)
    return _remove_multiple_pid_per_sample(t)


def _get_pid2names(t):
    d=defaultdict(set)
    for pid, name in zip(t.pid, t.sample_name):
        d[pid].add(name)
    return d
        
    
def _remove_multiple_pid_per_sample(t):
    t.updateColumn('max_norm', t.norm_peak_area.max.group_by(t.pid, t.sample_name), type_=float)
    t=t.filter(t.max_norm==t.norm_peak_area)
    t.dropColumns('max_norm')
    return t
    


def edit_collection(collection, group_col='aid'):
#    norm_groupcol_name='_'.join(['norm', group_col, 'area'])
#    update_averaged_group_areas(collection, group_ids=[group_col, 'sample_name'], norm_col_name=norm_groupcol_name)
    """ the columns are sorted in a way that first all consensus columns are given in descending
        group_size, followed by individual sample columns
    """
    
    colnames=['aid', 'fid', 'pid', 'ipid', 'sample_group_id', 'sample_name', 'lc_method', 
              'polarity','mzmean','mz0mean', 'rtmean', 'z_fid', 'consensus_adducts', 'possible_m0', 
               'adduct_group', 'feature_id', 'id', 'mz', 'mzmin', 
              'mzmax','rt', 'rtmin', 'rtmax', 'z',  'fwhm', 'quality', 'peakmap', 'area', 
              'method', 'rmse', 'tic_area', 'tic_method', 'norm_peak_area', 'biomass', 
              'unit', 'baseline', 'params', 'source', 
              'possible_adducts', 'z_by_adduct_grouping', 'adduct_assigned_by']
    return collection.extractColumns(*colnames)

 
 
################################################
import os
from glob import glob
def test_grouper(dir_=r'Z:\pkiefer\temp\bemaier\20181203_exB7_precise\strains_reprocessed'):
    tables=[]
    for strain in [str(i) for i in range(1,4)]:
        folder='_'.join(['Con', strain])
        path=os.path.join(dir_, folder)    
        file_name='*C18*.table'
        pathes=glob(os.path.join(path, file_name))[:-1]
        tables.extend([emzed.io.loadTable(p) for p in pathes ])
    return tables

    
    
def _get_table(path):
    t=emzed.io.loadTable(path)
    return _edit_table(t)

def _edit_table(t):
    t.replaceColumn('mzmin', t.mz-0.003, type_=float)
    t.replaceColumn('mzmax', t.mz+0.003, type_=float)
#    t.updateColumn('minmz', t.mz.min.group_by(t.feature_id), type_=float)
#    t=t.filter(t.mz==t.minmz)
#    t.dropColumns('minmz')
    return emzed.utils.integrate(t, 'trapez', n_cpus=1)

def checker_check(t):
    ids=[]
    for id_, p_pair, rtmin_pair in zip(t.id, t.params_pair, t.rtmins_pair):
        check=p_pair[0][0][0]-rtmin_pair[0][0]+ p_pair[1][0][0] -rtmin_pair[1][0]
        if abs(check)>1.0:
            ids.append(id_)
    return t.filter(t.id.isIn(ids))

def _fun(v):
    try:
        int(v)
        return ''.join(['leaf', v])
    except:
        return v


def test_convergence(t):
    d=defaultdict(list)
    for pid, name in zip(t.pid, t.sample_name):
        d[pid].append(name)
    for pid, names in d.items():
        assert len(names)==len(set(names)), 'problem with pid %d' %pid
    
#def test(pairs):
#    for p in pairs:
#        print 'start'
#        id1, id2=p
#        if id1==id2:
#            print 'hallo'
#            continue
#        if id1==id1*5:
#            print id1
#        elif id2==id2*3:
#            print id2
#        else:
#            print 'nix'
#            
#        