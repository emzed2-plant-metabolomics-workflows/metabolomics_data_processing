# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 12:19:50 2019

@author: pkiefer
"""
import emzed
from collections import defaultdict
import wtbox2
import itertools
import numpy as np


def add_cross_reference(lib, mtol=0.005, max_rt_shift=6.0, id_col='fid', 
                        label_col='adduct_assigned_by'):
#    _check_gid_consistency(lib)
    t=_get_mz0_table(lib)
    t.sortBy('polarity', False)
    charge_pairs=_get_charge_pairs(t)
    row2crid=dict()
    # Several functions of label_free_untargeted.cross_ref_peaks are still missing
    for pair in charge_pairs:
        # only one charge state per LC Method
        if len(pair)==1:
            t_=pair[0]
            t_.updateColumn('crid', t_.aid, type_=int, insertBefore='aid')
            row2crid.update(dict(zip(t_.fid, t_.crid)))
            update_cross_ref_id(lib, row2crid, id_col)
        elif len(pair)==2:
            t_pos, t_neg=pair
            try:
                rttol=_estimate_rttol(t_pos, t_neg, max_rt_shift)
            except:
                # this is for testing only if data set to small for estimation !! 
                rttol=3.0
            print 'estimated rttol: %.1fs'%rttol
            print 'building graphs ...'
            row2cross_ref, cross_ref, rid2name=cross_ref_tables(t_pos, t_neg, id_col, mtol, rttol)
            row2crid.update(row_id2cross_id(cross_ref, rid2name, id_col))
            print 'done'
            print 'updating cross ref id `crid` ...'
            print 'Done.'
            update_cross_ref_id(lib, row2crid, id_col)
#            import pdb; pdb.set_trace()
            update_assigned_adducts(lib, rid2name, cross_ref, id_col, label_col)    
            print 'FINISHED'
        else:
            assert False, 'This should never happen'
    if not len(charge_pairs):
        lib.updateColumn('crid', lib.aid, type_=int, insertBefore='aid')
        
#
#def _check_gid_consistency(lib, id_cols=['fid', 'aid', 'pid', 'ipid']):
#    [_no_intersection(lib, id_cols, id_col) for id_col in id_cols]
#    
#
#def _no_intersection(t, id_cols, value_col):
#    d=defaultdict(set)
#    exp=t.getColumn
#    values=[exp(col) for col in id_cols]
#    for key, value in zip(zip(values), exp(value_col)):
#        d[key].add(value)
#    if len(d.keys())>1:
#        pairs=itertools.combinations(d.values(), 2)
#        for p0, p1 in pairs:
#            assert len(p0.intersection(p1))==0
#    

def _get_charge_pairs(lib):
    pairs=[]
    for sub in lib.splitBy('lc_method'):
        pair=sub.splitBy('polarity')
        # to get tables ordered by polarity '+' , '-'
        pair.sort(key =lambda v: v.polarity.uniqueValue())
        pairs.append(pair)
    return pairs


def _get_mz0_table(t):
    colnames=['aid', 'fid', 'lc_method', 'polarity', 'mzmean', 'mz0mean', 'rtmean', 'z_fid', 
              'consensus_adducts','possible_m0', ]
    t=t.extractColumns(*colnames)              
    return t.uniqueRows()
    


def _estimate_rttol(t_pos, t_neg, mtol, max_rt_shift=12.0):
    print 'estimating rttol ...'
    compare=wtbox2.collect_and_compare.compare_tables
    key2tol={'_m0': mtol, 'rtmean': max_rt_shift}
    print 'print filtering momoisotopic peaks ...'
    t1=wtbox2.utils.fast_isIn_filter(t_pos, 'consensus_adducts', [('M+H',)])
    t2=wtbox2.utils.fast_isIn_filter(t_neg, 'consensus_adducts', [('M-H',)])
    print 'Done.'
    # add m0
    def _m0(mz, polarity):
        p=emzed.mass.p
        return mz-p if polarity=='+' else mz+p
    for t in [t1, t2]:
        t.updateColumn('_m0', t.apply(_m0, (t.mzmean, t.polarity)), type_=float)
    print 'find common peaks ...'
    x=compare(t1, t2, key2tol, False)
    x.updateColumn('_delta', x.rtmean-x.rtmean__0, type_=float)
    print 'done'
    'determining rttol'
    deltas=list(set(x._delta.values))
    print 'finished'
    return np.percentile(deltas, 75) if len(deltas) else max_rt_shift/2.0
    
####################################################################
# block from label_free_untargeted.cross_ref_peaks



def cross_ref_tables(t_pos, t_neg, id_col='fid', mtol=0.003, rttol=5.0, mzcol='mz0mean', rtcol='rtmean'):
    """
    """
    adduct2lookup_pair_pos_neg=adduct2lookup_mass_shift_pos_neg(mtol)
    match_neg=build_matching_table(t_neg, id_col, mzcol, rtcol)
    match_pos=build_matching_table(t_pos, id_col, mzcol, rtcol, len(match_neg))
    rid2name=defaultdict(dict)
    update_rid2name2value(match_neg, rid2name)
    update_rid2name2value(match_pos, rid2name)
    cross_ref=match_groups(match_pos, match_neg, rttol, adduct2lookup_pair_pos_neg)
    row2cross_ref=row_id2cross_id(cross_ref, rid2name, id_col)
    return row2cross_ref, cross_ref, rid2name


def adduct2lookup_mass_shift_pos_neg(mtol=0.003):
    pos=emzed.adducts.positive.toTable()
    pairs=zip(pos.adduct_name, pos.mass_shift, pos.z)
    pairs.append(('M+H-NH3', emzed.mass.p-emzed.mass.of('NH3'), 1) )
    d={}
    for name, dm, z in pairs:
        neg=emzed.adducts.negative.toTable()
        # we only mapp pairs of the same charge state in both mode
        # to handle charge state == 0
        neg=neg.filter(neg.z==z)
        neg.addEnumeration('rid')
        neg.addColumn('delta_m', dm-neg.mass_shift, type_=float)
        d[name]=(neg.buildLookup('delta_m', mtol, None), neg)
    return d

#-------------------------------------------------------------
def build_matching_table(t, id_col, mzcol, rtcol, rid_start=0):
    colnames=[id_col, mzcol, 'z_fid', rtcol, 'aid', 'consensus_adducts', 'possible_m0']
    t=t.extractColumns(*colnames)
    t.uniqueRows()
    tt=t.buildEmptyClone()
    print 'building matching table rows'
    rows=[]
    iterable=zip(*[t.getColumn(n) for n in colnames])
    progress=wtbox2.utils.show_progress_bar
    progress(iterable, _build_lines, args=[rows], in_place=True)
#    for values in zip(*[t.getColumn(n) for n in colnames]):
#        _build_lines(values, rows)
    tt.rows=rows
    tt.updateColumn('ion_mass', tt.getColumn(mzcol)*tt.z_fid, type_=float)
    tt.addEnumeration('rid')
    tt.replaceColumn('rid', tt.rid+rid_start, type_=int)
    return tt
        


def _build_lines(values, rows):
    id_, mz, z, rt, adduct_group, names, m0s=values
    if z==0:
        if names == ('M+H', 'M+NH4') or names == ('M+H', 'M+H-NH3'):
            z=1
        elif not len(set(names)-set(('M+H', 'M+2H', 'M+3H'))) or not len(set(names)-set(('M-H', 'M-2H', 'M-3H'))):
            pass
        else:
            assert False    
    pairs=enumerate(names)
    name2z={'M+H': 1, 'M+2H':2, 'M+3H':3, 'M-H': 1, 'M-2H':2, 'M-3H':3} 
    for pair, m0 in zip(pairs, m0s):
        z_, name=pair
        if not z:
            line=[id_, mz, name2z[name], rt, adduct_group, name, m0]
        else:
            line=[id_, mz, z, rt, adduct_group, name, m0]
        rows.append(line)
#---

def update_rid2name2value(t, rid2name):
    assert type(rid2name)==type(defaultdict())
    colnames=[n for n in t.getColNames() if not n=='rid']
    values=zip(*[t.getColumn(n).values for n in colnames])
    keys=t.rid.values
    for key, value in zip(keys, values):
        rid2name[key].update(dict(zip(colnames, value)))        

#-----

def match_groups(t_ref, t_match, rttol, lu_mass_shift_pairs):
    progress=wtbox2.utils.show_progress_bar
    print 'matching groups ...'
    adduct2lookup=lu_mass_shift_pairs
    lookup=rt_lookup(t_match, rttol)
    cross_ref=defaultdict(set)
    iterable=zip(t_ref.rid, t_ref.ion_mass, t_ref.rtmean, t_ref.consensus_adducts)
    args=[lookup, t_match, adduct2lookup, cross_ref]
    progress(iterable, _match_group, args=args, in_place=True)    
    return cross_ref


def _match_group(ntuple, lookup, t_match, adduct2lookup, cross_ref):
    id_, mass, rt, adduct=ntuple
    subset=get_subtable(rt, t_match, lookup)
    subset.updateColumn('delta_m', mass-subset.ion_mass, type_=float)
    args=(subset.delta_m, subset.consensus_adducts, adduct2lookup[adduct])
    subset.updateColumn('match', subset.apply(find_matches, args), type_=bool)
    cand=subset.filter(subset.match==True)
    if len(cand):
        cross_ref[id_].update(cand.rid.values)


def rt_lookup(t, rttol):
    return t.buildLookup('rtmean', rttol, None)


def get_subtable(value, t, lookup, id_col='rid'):
    sub=lookup.find(value)
    return wtbox2.utils.fast_isIn_filter(t, id_col, sub)


def find_matches(delta_m, adduct, lookup_pair):
    lookup, adduct_table=lookup_pair
    d=_find_matches(adduct_table, lookup, delta_m)
    return d.has_key(adduct)


def _find_matches(t, lookup, delta_m):
    rids=lookup.find(delta_m)
    d={}
    for rid in rids:
        key=t.rows[rid][1] # the second column contains possible adducts
        d[key]=key
    return d

#---

def row_id2cross_id(cross_ref, rid2name2value, id_col='row_id'):
    """ cross ref id is based on adduct id: aid
    """
    r2n2v=rid2name2value
    row2cross_id=dict()
    for rid in cross_ref.keys():
        adduct_group=r2n2v[rid]['aid']
        rids=[rid]
        rids.extend(list(cross_ref[rid]))
        for _rid in rids:
            row_id=r2n2v[_rid][id_col]
            row2cross_id[row_id]=adduct_group
    return row2cross_id

#------------------------------------------------------------

def update_cross_ref_id(t, row_id2cross_id, id_col):
    def _update(key, d, aid):
        return d[key] if d.has_key(key) else aid
    expr=(t.getColumn(id_col), row_id2cross_id, t.aid)
    t.updateColumn('crid', t.apply(_update, expr), type_=int, insertBefore='aid')

#-------------------------------------------------------------

def update_assigned_adducts(t, rid2name, cross_ref, id_col, label_col='adduct_assigned_by'):    
    # sub functions
    def _update_label(d, key, adduct, label):
        if d.has_key(key):
            return label if d[key].get('consensus_adducts')==adduct else 'by_cross_ref'
        return label 
    #
    def _update(d, row_id, colname, value):
        value=d[row_id][colname] if d.has_key(row_id) else value
        return value if isinstance(value, int) else tuple(value)
        
    assert t.hasColumns(id_col, label_col)
    #
    def _sort(adducts, m0s, index):
        pairs=sorted(zip(adducts, m0s), key=lambda v: v[1])
        return tuple(zip(*pairs)[index])
    d=_update_assigned_adducts(rid2name, cross_ref, id_col)
    expr=(d, t.getColumn(id_col), t.consensus_adducts, t.getColumn(label_col))
    t.updateColumn(label_col, t.apply(_update_label, expr, keep_nones=True), type_=str)
    expr=(d, t.getColumn(id_col), 'consensus_adducts', t.consensus_adducts)
    t.updateColumn('consensus_adducts', t.apply(_update, expr), type_=tuple)
    expr=(d, t.getColumn(id_col), 'possible_m0', t.possible_m0)
    t.updateColumn('possible_m0', t.apply(_update, expr), type_=tuple)
    expr=(t.consensus_adducts, t.possible_m0, 1)
    t.updateColumn('possible_m0', t.apply(_sort, expr), type_=tuple)
    expr=(t.consensus_adducts, t.possible_m0, 0)
    t.updateColumn('consensus_adducts', t.apply(_sort, expr), type_=tuple)
    expr=(d, t.getColumn(id_col), 'z_fid', t.z_fid)
    t.updateColumn('z_fid', t.apply(_update, expr), type_=int)


def _update_assigned_adducts(rid2name, cross_ref, id_col):
    # master contains matched rids in slave; the idea any matched rid in master selects a 
    # cross reference adduct. In case, more than 1 adduct was assigned only the selected will
    # be updated
    
    d=defaultdict(dict)
    colnames=['fid', 'z_fid', 'consensus_adducts', 'possible_m0']
    rids=set([])
    # collecting rids of grouped peaks
    for rid_, rids_ in cross_ref.items():
        rids.add(rid_)
        rids.update(rids_)
#    import pdb; pdb.set_trace()
    while rids:
        rid_=rids.pop()
        row_id, z, name, m0=[rid2name[rid_][n] for n in colnames]
        if d.has_key(row_id):
            d[row_id]['consensus_adducts'].append(name)
            d[row_id]['z_fid']=z if d[row_id]['z_fid']==z else 0
            d[row_id]['possible_m0'].append(m0)
        else:
            d[row_id]['consensus_adducts']=[name]
            d[row_id]['z_fid']=z
            d[row_id]['possible_m0']=[m0]
    return d
#--------------------------------------------------------------------

def _replace_none_values(t):
    # temporary bug fixing
    # consensus_adducts=None
    z2polarity2adducts={0: {'+':('M+H', 'M+2H', 'M+3H'), '-':('M-H', 'M-2H', 'M-3H')},
                        1: {'+':('M+H',), '-':('M-H',)}, 
                        2: {'+':('M+2H',), '-':('M-2H',)},
                        3: {'+':('M+3H',), '-':('M-3H',)}}
    exp=(t.consensus_adducts, t.z_fid, t.polarity, z2polarity2adducts)
    def _update_add(adducts, z, polarity, d):
        if adducts==None:
            return d[z][polarity] 
        return adducts
    t.updateColumn('consensus_adducts', t.apply(_update_add, exp, keep_nones=True), type_=tuple)
    