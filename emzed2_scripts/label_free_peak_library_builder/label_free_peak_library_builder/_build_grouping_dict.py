# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 17:03:05 2019

@author: pkiefer
"""
from collections import defaultdict
from wtbox2 import table_operations as top





def update_grouping_id(lib, ugid='ufid', lgid='pid', group_id_name='fid', insertBefore=None,
                       insertAfter=None):        
    # ugid upper level grouping id e.g. feature id; 
    # lgid lower level grouping id e.g. peak id
    # tha ugid is sample specific unique identifyer
    ugid2lgids=defaultdict(set)
    lgid2ugids=defaultdict(set)
    for ugid, lgid in zip(lib.getColumn(ugid), lib.getColumn(lgid)):
        ugid2lgids[ugid].add(lgid)
        lgid2ugids[lgid].add(ugid)
    pid2fid=get_groups(ugid2lgids, lgid2ugids)
    top.update_column_by_dict(lib, group_id_name, lgid, pid2fid, type_=int, in_place=True, 
                              insertBefore=insertBefore, insertAfter=insertAfter)




def get_groups(ugid2lgids, lgid2ugids):
    pid2fid={}
    pset=set([])
    ufid=ugid2lgids.keys()[0]
    _update_pset(ugid2lgids, [ufid], pset)
    fid=0
    while len(lgid2ugids):
        ufids=_readout_dict(lgid2ugids, pset)
        if len(ufids):
            _update_pset(ugid2lgids, ufids, pset)
        else:
            pid2fid.update({pid:fid for pid in pset})
            ufid=ugid2lgids.keys()[0]
            pset=set([])
            _update_pset(ugid2lgids, [ufid], pset)
            fid+=1
    pid2fid.update({pid:fid for pid in pset})
    assert len(lgid2ugids)==0, False
    return pid2fid                              
    
def _update_pset(ufid2pid, ufids, fset):
    for ufid in ufids:
        if ufid2pid.has_key(ufid):
            fset.update(ufid2pid.pop(ufid))
        

def _readout_dict(d, keys):
    values=set([])
    [values.update(d.pop(key)) for key in keys if d.has_key(key)]
    return values