from wf_manager.run_workflow import run_workflow, inspect
from main_lib_builder import main_peak_library_builder
from label_free_configuration import get_lfp_config, get_default_config

def run():
    
    print 
    print 'name of executed workflow: %s' %main_peak_library_builder.func_name
    print
    split_cols=[]
    inspect1=(inspect, split_cols)
    run_workflow(main_peak_library_builder, get_default_config, my_config_gui=get_lfp_config, 
                 save_excel=True, inspect_or_reprocess=(inspect1,))

if __name__=='__main__':
    run()
