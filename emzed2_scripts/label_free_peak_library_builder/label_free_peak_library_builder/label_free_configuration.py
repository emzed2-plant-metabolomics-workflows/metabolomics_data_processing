# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 13:43:38 2019

@author: pkiefer
"""
from emzed import gui
from basic_processing import configs
from inspect import getargspec



def get_lfp_config(config=None):
    config = config if config else {}
    key2value2index, key2index2value=get_converting_dicts()
    # define choice lists:    
    key2choice=_get_key2choice()
    key2default=get_default_config() if not config else config
    # keys in order of the dialog builder values
    keys=['lc_method', 'ms', 'determine_noise_level', 'remove_shoulders', 'normalize', 
          'subtract_blank', 'assign_blanks_automaticaly', 'rt_align', 'destination', 'label_by', 
          'sample_pattern', 'sample_grouping', 'group_pattern', 'biomass_values', 
          'biomass_concentration', 'unit', 'mztol', 'max_rt_diff', 'min_hits', 'min_spectra']
    dia=gui.DialogBuilder('parameter setup')
    key2help=_key2help()
    params=dia.addChoice('select LC method', key2choice[keys[0]], help=key2help[keys[0]],
                  default=_get_default(keys[0], key2default, config, key2value2index))\
    .addChoice('select MS instrumet', key2choice[keys[1]], help=key2help[keys[1]],
               default=_get_default(keys[1], key2default, config, key2value2index))\
    .addBool('determine peakmap noise level', 
             default=_get_default(keys[2], key2default, config), help=key2help[keys[2]])\
    .addBool('remove_sholders', default=_get_default(keys[3], key2default, config), 
             help=key2help[keys[3]])\
    .addBool('normalize samples', default=_get_default(keys[4], key2default, config), 
             help=key2help[keys[4]])\
    .addChoice('subtract blank', key2choice[keys[5]], help=key2help[keys[5]],
               default=_get_default(keys[5], key2default, config, key2value2index))\
    .addBool('assign blanks automticaly', default=_get_default(keys[6], key2default, config), 
             help=key2help[keys[6]])\
    .addBool('rt alignmemt', default=_get_default(keys[7], key2default, config), 
             help=key2help[keys[7]])\
    .addDirectory('rt align plot destination', _get_default(keys[8], key2default, config),
                  help=key2help[keys[8]])\
    .addChoice('label sample names by', key2choice[keys[9]], help=key2help[keys[8]],
               default=_get_default(keys[9], key2default, config, key2value2index))\
    .addString('sample pattern', default=_get_default(keys[10], key2default, config),
               help=key2help[keys[10]])\
    .addChoice('group samples by', key2choice[keys[11]], help=key2help[keys[11]],
               default=_get_default(keys[11], key2default, config, key2value2index))\
    .addString('sample group pattern', default=_get_default(keys[12], key2default, config),
               help=key2help[keys[12]])\
    .addChoice('sample biomass values',  key2choice[keys[13]], help=key2help[keys[13]],
             default=_get_default(keys[13], key2default, config, key2value2index))\
    .addFloat('unique biomass concentration', default=_get_default(keys[14], key2default, config),
              help=key2help[keys[14]])\
    .addChoice('concentration unit', key2choice[keys[15]], help=key2help[keys[15]],
               default=_get_default(keys[15], key2default, config, key2value2index))\
    .addFloat('mz tolerance', default=_get_default(keys[16], key2default, config),
              help=key2help[keys[16]])\
    .addFloat('max RT difference', default=_get_default(keys[17], key2default, config),
              help=key2help[keys[17]],)\
    .addInt('minimal num of feature occurcence per group', 
            default=_get_default(keys[18], key2default, config), help=key2help[keys[18]])\
    .addInt('minimal number of spectra of dominating peak', _get_default(keys[19], key2default, config), 
            help=key2help[keys[19]])\
    .show()
    key2value=dict(zip(keys, params))
    # get ff metabo config
    _update_config(config, key2value, key2index2value)
    _update_ff_metabo_config(config)
    _update_adduct_grouping(config)
    return config


def get_converting_dicts():
    key2choice=_get_key2choice()
    key2value2index=_build_key2value2index(key2choice)
    key2index2value=_build_key2value2index(key2choice, True)
    return key2value2index, key2index2value
    
    
def _get_key2choice():
    d={}
    d['subtract_blank']=['individual', 'unique', 'no subtraction']
    d['ms']=['LTQ_Orbitrap', 'Q_exactive', 'other']
    d['lc_method']=['HILIC_BEH_NH3_metabolome', 'UPLC_C18_metabolome', 'nanoLC_TBA', 'other']
    d['label_by']=['pattern', 'from_sample_group', 'source']
    d['sample_grouping']=['pattern', 'individualy', 'no_grouping']
    d['unit']=['ug//uL', 'cells//uL']
    d['biomass_values']=['unique value', 'load existing file', 'build file', 'update_existing']
    return d

def _key2help():
    d={}
    d['lc_method']='select one of the preselected lc-method. If the applied lc method is not '\
                    'listed select the option OTHER'
    d['ms']='Parameter settings were optimized for Orbitrap instruments. If the instrument is not'\
             ' listed choose other'
    d['determine_noise_level']='if the option is chosen the peakmap noise baseline and S/N are '\
        'determined by the rule of Freedman und Diaconis'
    d['remove_shoulders']='If chosen Orbitrap artefact satelite peaks neighbouring high intensity mass '\
        'spectra peaks will be removed from spectra'
    d['normalize']='If chosen, the peak areas will be normlzed to TIC area. In case additional '\
        'normalization to biomass is selected, \nTIC areas will be normalized to biomass_amount'
    d['subtract_blank']='blank subtraction on peakmap level. Select this option only if the '\
    'peak retention time is stable! \nPeaks are subtracted on spectral level. If unique '\
    'subtraction is selected, \nyou need stable RTs over the complete batchs is required! '\
    '\n In general this option is only suited for single batch measuremented with low carry over'
    d['assign_blanks_automaticaly']='Preceding blanks are automaticaly assigned to the samples.'\
    ' Choose True if file names have a systematic structure containing an injection number identifyer'
    d['rt_align']='Select true if a retention time difference of more than 2 * typival FWHM is '\
            'observed. \nIt is reconmmended to apply it in case of the analysis of several batches'
    d['destination']='Required for RT alignment. Select the folder where to save the RT alignment '\
            'plots. \nFor technical reasons you also have to select a folder when rt alignment '\
            'option is not selected. \nIn that case any folder can be selected'
    d['label_by']='You can label the sample names by a pattern extracted from source file name, '\
     'based in sample grouping id, or you can simply keep the filename of the source. The label'\
     'IMPORTANT: in case you build a data matrix using lfu_data_analysis tool sample names '\
     'are used as column names in the data matrix and columns are sorted by  the sample name.'\
     'Therefore choose option `from pattern` or `from_sample_group` to keep samples of the '\
     'same group as grouped coplumn.'
    d['sample_pattern']= 'Extraxts the sample name from the source file name '\
        'by a combinatory expression comprising different text blocks. All elements in parenthesis'\
        ' will be combined with preceeding '\
        'text blocks. Ascending numbers or letters can be presented in short form e.g. \n'\
        '[1-9] or [b-e] ([b-e] corresponds to [b, c, d, e]), other iteration blocks'\
        'like words are writen as list e.g. [leaf, root, bact]. \nExample: [control, test]_r[1-3]'\
         'yields the sample names: control_r1, control_r2, control_r3, test_r1, test_r2, test_r3'
    d['sample_grouping']='sample group ids (labels) can be assigned based i.e. to samples '\
                'from the same species, environment etc. There are 3 different options: '\
                '\n- The grouping label is part of the filename \n- The grouping is assigned '\
                'manualy by the user \n- There is no grouping required'
    d['group_pattern']='To group samples corresponding group names can be generated by combinations.'\
     'The group must be part of the sample file names. Names will generated by combinations '\
        'off different text blocks. All elements in parenthesis will be combined with preceeding '\
        'text blocks. Ascending numbers or letters can be presented in short form e.g. \n'\
        '[1-9] or [b-e] ([b-e] corresponds to [b, c, d, e]), other iteration blocks'\
        'like words are writen as list e.g. [leaf, root, bact]. \nExample: [control, test]_[a-c]'\
        '_r[1-3] yields the groups: control_a, control_b, control_c, test_a, test_b, test_c'\
    '   NOTE: an empty string \"\" will result a single group'
            
    d['biomass_values']='There are 4 options: - unique value: all samples have the same biomass amount'\
            '\n- load existing file: a csv file exists containing for each sample the individual '\
            ' biomass value. There are two options for the sample column `source` or '\
            '`sample_name` and `biomass_concentration`. \n- build file: enter all values manualy'\
            '\n- update_existing: obsolete'
    d['biomass_concentration']='the biomass concentration value of all samples. Only valid if '\
        'option `unique` was chose for sample biomass values was selected.'
    d['unit']='select biomass concentration unit'
    d['mztol']='select the general absolute m/z accuracy over all samples'
    d['max_rt_diff'] ='upper retention time difference limit. It determines the upper assignment'\
       '\n limit to determine the observed retention time tolerance over all samples'
    d['min_hits'] = 'the minimal number of detections of a feature within the same sample group.' \
        '\n remark: it is sufficient that the condition is fullfilled in at least 1 group'
    d['min_spectra']='the minimal number of required ms spectra for a largest peak of each'\
            ' adduct_group to be accepted.'\
    ' peak area quantification a minimal number of 10 spectra per peak is recommended.'
    return d

def _build_key2value2index(key2choice, inverse=False):
    d={}
    for key, values in key2choice.items():
        i2value=_value2index(values)
        d[key]=_inverse_dict(i2value) if inverse else i2value
    return d


def _value2index(values):
    return dict(zip(values, range(len(values))))


def _inverse_dict(d):
    values, keys=zip(*d.items())
    return dict(zip(keys, values))


def get_default_config():
    # determines all default values of config
    d={}
    d['lc_method'] = 'HILIC_BEH_NH3_metabolome'
    d['ms'] = 'Q_exactive'
    d['determine_noise_level'] = True
    d['subtract_blank'] = 'individual'
    d['assign_blanks_automaticaly']=True
    d['remove_shoulders'] = True
    d['normalize']= True
    d['rt_align']=True
    d['destination'] = None
    d['label_by'] = 'pattern'
    d['sample_grouping']='pattern'
    d['sample_pattern'] = '[control, test]_r[1-3]'
    d['group_pattern'] = '[control, test]_[a-c]'
    d['biomass_values'] = 'unique value'
    d['biomass_concentration']= 1.0
    d['unit'] = 'ug//uL'
    d['mztol'] = 0.003
    d['max_rt_diff'] = 6.0
    d['min_hits'] =  1
    d['min_spectra']=10
    d['source2biomass']={}
    return d


def _get_default(key, key2default, config, key2value2index=None):
    default=config[key] if config.has_key(key) else key2default[key]
    return key2value2index[key][default] if key2value2index else default


def _update_config(config, key2value, key2index2value):
    for key, value in key2value.items():
        _value = key2index2value[key][value] if key2index2value.has_key(key) else value
        config[key]=_value


def _update_ff_metabo_config(config):
    default_ff=configs.default_feature_detection
    args=getargspec(default_ff).args
    kwargs=_get_kwargs(args, config)
    con_ff=default_ff(**kwargs)
    config['feature_detection']=con_ff


def _get_kwargs(args, key2value):
    return {arg: key2value[arg] for arg in args if key2value.has_key(arg)}


def _update_adduct_grouping(config):
    con_ag=configs.enhanced_adduct_grouper_config()
    config['adducts']= con_ag

#-------------------------------------------------------------------------------------------------
import os
import emzed
def save_subset(dir_, pms):
    for pm in pms:
        name=pm.meta['source']
        pre, ext=os.path.splitext(name)
        name=''.join(['short_',pre, ext])
        path=os.path.join(dir_, name)
        emzed.io.storePeakMap(pm, path)
