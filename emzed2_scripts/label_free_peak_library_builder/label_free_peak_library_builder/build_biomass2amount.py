# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 16:09:45 2019

@author: pkiefer
"""



from _utilities import extract_name_by_pattern
import emzed
import wtbox2.table_operations as top


def source2biomass(samples, config):
    if not config['normalize']:
        return 
    if incomplete_source2biomass(samples, config) or config['biomass_values']=='update existing':
        update_sourc2biomass(samples, config)
    
    
def update_sourc2biomass(samples, config):
    if config['biomass_values']=='unique value':
        _source2unique(config, samples)
    elif config['biomass_values']== 'load existing file':
        load_id2biomass(samples, config)
    elif config['biomass_values']== 'build file':
        build_id2biomass(config, samples)
    elif config['biomass_values']=='update existing':
        build_id2biomass(config, samples, update=True)
    

def incomplete_source2biomass(samples, config):
    if not config.has_key('source2biomass'):
        return True
    keys=[_get_key(sample, config) for sample in samples]
    return not all([config['source2biomass'].has_key(key) for key in keys])


def _source2unique(config, samples):
    value=config['biomass_concentration']
    d={_get_key(sample, config): value for sample in  samples}
    config['source2biomass']=d
    

def _get_key(sample, config):
    source=sample.meta['source']
    if config['label_by']=='pattern':
        source=extract_name_by_pattern(source, config['sample_pattern'])
    return source


def build_id2biomass(config, samples, update=False):
    sources=[s.meta['source'] for s in samples]
    t=emzed.utils.toTable('source', sources, type_=str)
    if config['label_by']=='pattern':
        t.addColumn('sample_name', t.apply(extract_name_by_pattern, (t.source, config['sample_pattern'])), 
                    type_=str)
    t.addColumn('biomass_conc', None, type_=float, format_='%.2f')
    t.addColumn('unit', config['unit'], type_=str)
    if update:
        key_col='sample_name' if t.hasColumn('sample_name') else 'source'
        top.update_column_by_dict(t, 'biomass_conc', key_col, config['source2biomass'], 
                                  type_=float, in_place=True)
    t=_manual_add_biomass_values(t)
    _update_source2biomass(t, config)
    
    
def _manual_add_biomass_values(t, biomass='biomass_conc'):
    emzed.gui.showInformation('please add the biomass concentration values to the csv'\
                            ' file and save it when you are done. Values must not be 0.0!!')
    while True:
        emzed.gui.inspect(t)
        if all(t.getColumn(biomass).values):
            break
        else:
            emzed.gui.showWarning('All biomass concentration values must be >0.0!. Please correct'\
            'value accordingly')
    return t
  
        
def _update_source2biomass(t, config):                                    
    key=t.sample_name if t.hasColumn('sample_name') else t.source
    config['source2biomass'] = dict(zip(key, t.biomass_conc))

#----------------------------------------------------------------------------

def load_id2biomass(samples, config):
    startAt=config['project_dir'] if config.has_key('project_dir') else None
    path=emzed.gui.askForSingleFile(caption='select biomass amount file', extensions=['csv'],
                                    startAt=startAt)
    t=emzed.io.loadCSV(path)
    _check_biomass_table(t, samples, config)
    _update_source2biomass(t, config)


def _check_biomass_table(t, samples, config):
    sources=[s.meta['source'] for s in samples]
    required=['biomass_conc']
    sample_names=None
    if config['label_by'] == 'pattern':
        pattern=config['sample_pattern']
        sample_names=[_extract_pattern(s, pattern) for s in sources]
        required.append('sample_name')
    else:
        required.append('source')
    assert t.hasColumns(*required), 'required columns are missing'
    _complete(t, sources, sample_names)


def _complete(t, sources, sample_names):
    colname='sample_name' if sample_names else 'source'
    values=set(t.getColumn(colname).values)
    missing=values-set(sample_names) if sample_names else values - set(sources)
    if len(missing):
        samples= ', '.join(list(missing))
    assert len(missing)==0, 'The select biomass table does not contain samples: %s' %samples
    


