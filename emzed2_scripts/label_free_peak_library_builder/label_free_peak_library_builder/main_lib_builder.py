# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 13:30:17 2019

@author: pkiefer
"""

from build_sample_tables import build_sample_tables
from multiple_samples_peak_grouper import samples_peaks_grouper
from _match_samples_and_blanks import get_sample_specific_blanks, get_unique_blank
from _utilities import get_kwargs_from_config
from build_biomass2amount import source2biomass
from sample_grouping import group_samples
from determine_isotope_pattern  import determine_pattern

def main_peak_library_builder(samples, config):
    blanks=get_blanks(samples, config)
    source2biomass(samples, config)
    group_samples(samples, config)
    tables=build_feature_tables(samples, blanks, config)
    lib=build_peaks_library(tables, config)
    determine_pattern(lib)
    return lib


def get_blanks(samples, config):
    if config['subtract_blank']== 'individual':
        blanks=get_sample_specific_blanks(samples, config)
    elif config['subtract_blank']== 'unique':
        blanks=get_unique_blank(samples, config)
    else:
        blanks=None
    return blanks
        

def build_feature_tables(samples, blanks, config):
    return build_sample_tables(samples, blanks, config)


def build_peaks_library(tables, config):
    kwargs=get_kwargs_from_config(samples_peaks_grouper, config)
    return samples_peaks_grouper(tables, **kwargs)
    
 
def load_existing_id2biomass(config):
    pass

def save_id2biomass(config):
    pass
    

