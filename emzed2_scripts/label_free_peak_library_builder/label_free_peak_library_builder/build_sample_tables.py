# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:53:59 2019

@author: pkiefer
"""
#import re
import basic_processing as baspro
from multiprocessing import cpu_count
import normalize_peaks as norm
from remove_lc_satelite_peaks import remove_satelite_lc_peaks
from wtbox2 import table_operations as top
from wtbox2 import peakmap_operations as pmop
from wtbox2.utils import show_progress_bar
import _utilities as ut
                
def build_sample_tables(samples, blanks, config):
    kwargs={'bioamount_col' : 'biomass', 'sample_name_col': 'sample_name', 
            'tic_normalize' : config['normalize']}        
    tables=detect_features(samples, blanks, config['feature_detection']) 
    _update_general_columns(tables, config)
    tables=assign_adducts(tables, config['adducts'])
    # filter for peak quality criterim on adduct level
    tables=select_adduct_group_by_spectra(tables, config['min_spectra'])
    norm.normalize_peaks(tables, **kwargs)
    mztol=config['feature_detection']['mztol']
    [remove_satelite_lc_peaks(t, mztol) for t in tables]
    return edit_sample_tables(tables)


def detect_features(samples, blanks, config):
    enhanced_feature_detection=baspro.enhanced_feature_detection.detect_features
    ncpus=_get_ncpus(samples)
    return enhanced_feature_detection(samples, config, blanks, ncpus)

def _get_ncpus(iterable, max_n=10): # max_n is set for the current environment
    max_cpu=cpu_count()-1
    max_n=max_cpu if max_cpu<max_n else max_n
    return len(iterable) if len(iterable)<max_n else max_n


def assign_adducts(tables, config):
    config['fid_col']='feature_id'
    ncpus=_get_ncpus(tables)
    adduct_grouper=baspro.enhanced_adduct_grouper.parallel_enhanced_adduct_grouper
    return adduct_grouper(tables, config, ncpus)


def edit_sample_tables(tables):
    colnames=['id', 'adduct_group', 'feature_id', 'lc_method', 'polarity', 'sample_name', 
              'sample_group_id', 'source', 'mz', 'mzmin', 'mzmax', 'rt', 'rtmin', 'rtmax', 
              'intensity', 'quality', 'fwhm', 'z', 'z_by_adduct_grouping',  'possible_adducts', 
              'no_of_adducts', 'adduct_assigned_by', 'method', 'area', 'baseline', 'rmse', 'params', 
              'tic_area', 'tic_method', 'norm_area_by_tic', 'peakmap',  'biomass',  
              'norm_peak_area', 'unit']
    return [t.extractColumns(*colnames) for t in tables]


def _update_general_columns(tables, config):
    for t in tables:
        _update_columns(t, config)


def _update_columns(t, config):
    polarity=t.peakmap.uniqueValue().polarity
    t.updateColumn('lc_method', config['lc_method'], type_=str, insertAfter='feature_id')
    t.updateColumn('polarity', polarity, type_=str, insertAfter='z')
    if config['label_by']=='pattern':
        pattern=config['sample_pattern']
        t.updateColumn('sample_name', t.apply(ut.extract_name_by_pattern, (t.source, pattern)), 
                       type_=str,  insertBefore='source')
    else:
        t.updateColumn('sample_name', t.source, type_=str, insertBefore='source')
    sample2group=config['source2group']
    sample2biomass=config['source2biomass']
    top.update_column_by_dict(t, 'sample_group_id', 'source', sample2group, type_=str, 
                              insertAfter='sample_name', in_place=True)
    top.update_column_by_dict(t, 'biomass', 'sample_name', sample2biomass, type_=float, 
                              insertAfter='source', in_place=True)
    t.updateColumn('unit', config['unit'], type_=str)
    
    
#def _extract_pattern(source, pattern, separators=['_']):
#    x=re.search(pattern, source)
#    sid=source[x.start():x.end()]
#    # since some sample ids in source name do not contain '_'
#    for sep in separators:
#        fields=sid.split(sep)
#        sid= ''.join(fields)
#    return sid


def select_adduct_group_by_spectra(tables, min_spectra):
    return show_progress_bar(tables, _filter_adduct_group_by_spectra, [min_spectra])

def _filter_adduct_group_by_spectra(t, min_spectra):
    count=pmop.count_chromatogram_spectra
    expr=(t.peakmap, t.rtmin, t.rtmax, t.mzmin, t.mzmax)
    t.updateColumn('_counts', t.apply(count, expr), type_=int)
    t.updateColumn('_max_per_group', t._counts.max.group_by(t.adduct_group), type_=int)
    t=t.filter(t._max_per_group>=min_spectra)
    t.dropColumns('_counts', '_max_per_group')
    return t
#############################################################################################
