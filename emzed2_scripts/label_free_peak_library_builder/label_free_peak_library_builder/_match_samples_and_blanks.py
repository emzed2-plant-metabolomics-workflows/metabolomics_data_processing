# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 12:01:15 2019

@author: pkiefer
"""
import emzed
import os
from _automatic_blank_matching import automatic_blank_matching
import wtbox2
# main funtions 
#==============================================================================================

def get_unique_blank(samples, config):
    dir_=_get_sample_dir(samples)
    exts=['mzML', 'mzXML']
    path=emzed.gui.askForSingleFile(startAt=dir_, caption='select_blank', extensions=exts)
    blank=emzed.io.loadPeakMap(path)
    blanks=[blank]*len(samples)
    return blanks


def get_sample_specific_blanks(samples, config):
    correct=False
    if config['assign_blanks_automaticaly']:
        correct, sample2blank=automatic_blank_matching(samples)
    if not correct:
        manual=emzed.gui.askYesNo('automatic blank assignment was not possible. '\
        'Do you want to assign blanks manualy?')
        if manual:
            sample2blank=select_blanks_manualy(samples)
        else:
            assert False, 'automatic sample assignmetn was not possible! Please check your '\
            'data set and // or addapt the configuration accordingly.'
    return _load_blanks(samples, sample2blank) 
        

#---------------------------------------------------------------------

def _load_blanks(samples, sample2blank):
    print
    print 'loading blanks ...'
    print
    return wtbox2.utils.show_progress_bar(samples, _load_blank, [sample2blank])
    


def _load_blank(sample, sample2blank):
    key = sample.meta['full_source']
    # the key must exists. Therefore for each key a blank must exists
    return emzed.io.loadPeakMap(sample2blank[key])
    


def select_blanks_manualy(samples):
    sample_dir=_get_sample_dir(samples)
    sample_pathes=[s.meta['full_source'] for s in samples]
    sample_names=_get_sample_names(samples)
    blank_pathes=_build_dialog_expression(sample_names, sample_dir)    
#    return blank_pathes
    return dict(zip(sample_pathes, blank_pathes))
    
    

def _build_dialog_expression(sample_names, sample_path):
    Builder = emzed.gui.DialogBuilder('Assign blanks to samples')
    elements=['Builder']
    params_text=', basedir=basedir)'
    file_open='addFileOpen('
    for name in sample_names:
        text=''.join(['\" ',name,'\"'])
        expr=''.join([file_open,  text, params_text])
        elements.append(expr)
#        return expr
    elements.append('show()')
    expression='.'.join(elements)
    globa={'Builder': Builder}
    loc={'basedir': sample_path}
    return eval(expression, globa, loc)

def _get_trick():
    return 'formats="[mzML, mzXML]"'
        

def _get_sample_dir(samples):
    pathes=[s.meta['full_source'] for s in samples]
    dirs=set([os.path.dirname(p) for  p in pathes])
    if len(dirs)==1:
        return dirs.pop()
    

def _get_sample_names(samples):
    paths=[s.meta['full_source'] for s in samples]
    return [os.path.basename(p) for p in paths]