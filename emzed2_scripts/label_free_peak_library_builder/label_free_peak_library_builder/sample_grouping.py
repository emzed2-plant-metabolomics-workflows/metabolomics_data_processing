# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 10:50:38 2019

@author: pkiefer
"""
from _utilities import build_source2name
from emzed.utils import toTable
from emzed import gui

def group_samples(samples, config):
    grouping=config['sample_grouping']
    config['source2group']={}
    if grouping == 'pattern':
        _groups_by_pattern(samples, config)
        


def _groups_by_pattern(samples, config, key='source2group'):
    pattern= config['group_pattern']
    sources=[s.meta['source'] for s in samples]
    source2group=build_source2name(sources, pattern)
    config['source2group']=source2group


def _individual_groups(samples, config):
    t=_build_table(samples)
    _update_group_ids(t)
    sample2group=dict(zip(t.source, t.sample_group))
    config['source2group']=sample2group

def _build_table(samples):
    sources=[s.meta['source'] for s in samples]
    t=toTable('source', sources, type_=str)
    t.addColumn('sample_group', '', type_=str)
    return t


def _update_group_ids(t):
    gui.showInformation('Please add the sample group ids manually. Close the table when done')
    gui.inspect(t)
    

def _no_grouping(samples, config):
    sample2group= {s.meta['source']: '' for s  in samples}
    config['source2group']=sample2group