This is a collection of applications and extension used for metabolomics data processing in the study 'general non-self response as part of plant immunity'.  All programs are written with emzed2 ( http://emzed.ethz.ch/2 ), a source toolbox for rapid and interactive development of LC-MS data analysis workflows in Python.  All application and extension packages are provided as wheels. To install wheels with emzed IPython console use the 
command 

___install_wheel()

To avoid dependency conflicts you have to install required extensions in following order
1)	wtbox2-0.0.45-py2-none-any.whl
2)	basic_processing-0.0.47-py2-none-any.whl
3)	wf_manager-0.0.25-py2-none-any.whl

Subsequently,  APPLICATIONS can be installed in any order
-	create_sirius_files-0.0.3-py2-none-any.whl
-	label_free_peak_library_builder-0.0.13-py2-none-any.whl
-	lfu_data_analysis-0.0.14-py2-none-any.whl

NOTES: 
-	- Each time you installed a package you have to open a new IPython console to load it. 
-	- Currently, emzed2 is only supported on Windows 64bit. To install emzed2 see the installation guide (http://emzed.ethz.ch/2/installation.html).
